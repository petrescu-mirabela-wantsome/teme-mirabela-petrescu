@echo off
REM Windows script which completes the setup of this project (cloning extra repos, like for curs)

REM NOTE: Choose here the prefix which works for you (git@ if you have ssh key configured, https otherwise)
set PREFIX=git@gitlab.com:
REM set PREFIX=https://gitlab.com/

echo Cloning course repo...
git clone %PREFIX%wantsome/java/seria6/curs.git

mkdir out

echo Downloading books...
powershell -Command (new-object System.Net.WebClient).DownloadFile('https://profs.info.uaic.ro/~acf/java/Cristian_Frasinaru-Curs_practic_de_Java.pdf','.\curs\doc\books\Cristian_Frasinaru-Curs_practic_de_Java.pdf')

echo ...done!
