# Wantsome - Home assignments - Template project

## About

This is a **template** project for home assignments - just a basic working IntelliJ Idea project, 
with some examples of solved assignment (code+tests)

It includes the library needed to compile/run JUnit tests directly
(you may add other libs as needed, like guava, joda-time etc) 

## Working on assignments

- **Package organization**: 
  - under `teme` package you should create a new package for each week (like `w01` .. `w14` )
  - under each week package you can place directly the java classes for the exercises of that week, 
    or you may further create sub-packages (to group multiple classes needed by a single exercise, like `teme.w1.ex3`)
  
- **Hints**: 
  - you may commit & push your work in progress **at any time** - as soon as you wrote a bigger chunk of code which looks ok,
    you may commit it, even if it's not yet perfect :) this ensures that you have a good backup (so you don't loose any work), 
    and also that you can 'go back in time' to a previous version of a class (to revert some mistake..)
  - remember to also **_push_ the changes to remote repo** (gitlab), not only **_commit_** them 
    (as this will just save them locally on your machine)
  - you can also **work from multiple machines** (or just have multiple local copies of this repo)
    just remember in this case to update from remote repo (`git pull` / **Ctrl+T** from Idea) when you switch to some other copy 

## Forking the repo

As this is a template, you _cannot modify it directly_, but instead you should:
- make your own copy first -> from GitLab UI, use the **Fork** button (top-right)
  - select a location for your copy (your personal GitLab namespace is ok)
  - optional: you may also rename it if you want (to something like: 'wantsome-teme_alina_b') -> 
    from GitLab left menu, **Settings\General** \ **Advanced** section \ 
    **Rename repository** , change Project name and/or Path, 

- clone your copy locally to your machine (`git clone`); then you may start 
  adding changes (`git commit`, `git push`) and getting updates (`git pull`) for it

- you may also run the setup script (`setup.bat` for windows, `setup.sh` for linux), 
  to have it also include the course materials in this same project (under `/curs` folder) 

- you may give access to your repo to other members (at least the trainers :) ) -> from GitLab UI, 
  use the **Settings/Members** menu, select access level **Reporter** or **Developer**

## Working with git

To be able to work with this locally you need first to **install git** - see: https://git-scm.com/

After this you can run git operations (clone, pull, commit, push..), using various tools:
- from command line (**shor**t tutorial: http://rogerdudler.github.io/git-guide/)
- using IntelliJ Idea directly (video guide: https://www.youtube.com/watch?v=uUzRMOCBorg)
- using a separate UI client (like _TortoiseGit_ or others, see:  https://git-scm.com/download/gui/windows )
