#!/bin/sh

# Linux script which completes the setup of this project (cloning extra repos, like for curs)

# NOTE: Choose here the prefix which works for you (git@ if you have ssh key configured, https otherwise)
PREFIX=git@gitlab.com:
#PREFIX=https://gitlab.com/

echo 'Cloning course repo...'
git clone ${PREFIX}wantsome/java/seria6/curs.git

mkdir out

echo 'Downloading books...'
wget -P curs/doc/books/ https://profs.info.uaic.ro/~acf/java/Cristian_Frasinaru-Curs_practic_de_Java.pdf

echo '...done!'
