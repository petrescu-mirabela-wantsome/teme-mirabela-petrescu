package teme.w08.ex4_w5_collections_warmup;

import java.util.*;
import java.util.stream.Collectors;

// Link : https://www.baeldung.com/java-groupingby-collector

public class WarmUp {

    /* g) For each word, count how many times it appears, and then print all unique words
            (in their initial order in text) together with the number of times each appears.
    */
    private static Map<String, Integer> printUniqueWordsCount(String text) {
    /*
    Not stream implementation
     */
        Map<String, Integer> orderedMapByWord = new LinkedHashMap<>();
        for (String word : text.toLowerCase().split("\\s+")) {
            Integer count = orderedMapByWord.get(word);
            if (count == null) {
                orderedMapByWord.put(word, 1);
            } else {
                orderedMapByWord.put(word, count + 1);
            }
        }
        System.out.println(" ------ For each word, count how many times it appears, and then print all unique words ------ ");
        System.out.println(orderedMapByWord);
        return orderedMapByWord;
    }

    /*
    Stream implementation
    Solution1: with 2 separate operations: a simple grouping...
     */
    private static void printUniqueWordsCountStream01(List<String> sList) {

        Map<String, List<String>> grouped = sList
                .stream()
                .collect(Collectors.groupingBy(s -> s));
        System.out.println(grouped);

        //...then another map() operation, on map entries, to replace the groups of letters with only the group size
        // Link : https://www.baeldung.com/java-groupingby-collector
        String counts = grouped
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + ":" + entry.getValue().size())
                .collect(Collectors.joining(", "));

        System.out.println(" ------ For each word, count how many times it appears, and then print all unique words ------ ");
        System.out.println(counts);
    }

    /*
    Stream implementation
    Solution2: all can be done in a SINGLE stream pipeline, but with a more complex collect,
    using .groupingBy() with extra params!
    see more info:  https://dzone.com/articles/the-ultimate-guide-to-the-java-stream-api-grouping
     */
    private static void printUniqueWordsCountStream02(List<String> sList) {
        Map<String, Long> groupSizes = sList
                .stream()
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        System.out.println(" ------ For each word, count how many times it appears, and then print all unique words ------ ");
        System.out.println(groupSizes);
    }


    /* h) The same, but now sort the word-count pairs alphabetically by word.
     */
    private static void printUniqueWordsCountSortByWord(Map<String, Integer> orderedMapByWord) {
        Map<String, Integer> sortedMapByWord = new TreeMap<>(orderedMapByWord);
        System.out.println(" ------ sort the word-count pairs alphabetically by word ------ ");
        System.out.println(sortedMapByWord);
    }

    /*
    Stream implementation
    */
    private static void printUniqueWordsCountSortByWordStream(List<String> sList) {
        Map<String, List<String>> grouped = sList
                .stream()
                .sorted()
                .collect(Collectors.groupingBy(s -> s));
        System.out.println(grouped);

        //...then another map() operation, on map entries, to replace the groups of letters with only the group size
        // Link : https://www.baeldung.com/java-groupingby-collector
        String counts = grouped
                .entrySet()
                .stream()
                .map(entry -> entry.getKey() + ":" + entry.getValue().size())
                .collect(Collectors.joining(", "));

        System.out.println(" ------ sort the word-count pairs alphabetically by word ------ ");
        System.out.println(counts);
    }

    public static void main(String[] args) {

        String text = "Once upon a time in a land far far away there lived a great king whose name was a great mystery";
        List<String> sList = new ArrayList<>(Arrays.asList(text.toLowerCase().split("\\s+")));

        /* g) For each word, count how many times it appears, and then print all unique words
            (in their initial order in text) together with the number of times each appears.
        */
        Map<String, Integer> orderedMapByWord = printUniqueWordsCount(text);
        System.out.println();

        /*
        Stream implementation
         */
        printUniqueWordsCountStream01(sList);
        System.out.println();
        printUniqueWordsCountStream02(sList);



        /* h) The same, but now sort the word-count pairs alphabetically by word.
         */
        System.out.println();
        printUniqueWordsCountSortByWord(orderedMapByWord);

        /*
        Stream implementation
         */
        printUniqueWordsCountSortByWordStream(sList);


    }


}
