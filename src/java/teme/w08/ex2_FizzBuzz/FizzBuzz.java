package teme.w08.ex2_FizzBuzz;

/*
2. FizzBuzz with Streams
Write 2 solutions to the the FizzBuzz problem:
- First a regular one (imperative style, with loops, ifs, etc)
- Then one only with streams and lambdas (hint: see
IntStream.rangeClosed to simulate a stream of numbers)
The FizzBuzz problem: Given a range of numbers, for each number,
display: fizz, if the number divides by 3, buzz if it divides by 5,
fizzbuzz if it divides by both or the number itself if none of the above.
 */

import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FizzBuzz {

    private static void printOptimised(int[] arr) {
        for (int item : arr) {
            if ((item % 15) == 0)
                System.out.print("fizzbuzz ");
            else if ((item % 5) == 0)
                System.out.print("buzz ");
            else if ((item % 3) == 0)
                System.out.print("fizz ");
            else
                System.out.print(item + " ");
        }
    }

    private static void printStream(int[] arr) {
        IntStream s2 = IntStream.of(arr);
        s2.mapToObj(s -> s % 15 == 0 ? "fizzbuzz" : s % 5 == 0 ? "buzz" : s % 3 == 0 ? "fizz" : s).forEach(System.out::print);
    }

    private static void printStreamFunction(Integer[] arr) {
        Function<Integer, String> formatter = s -> s % 15 == 0 ? "fizzbuzz" : s % 5 == 0 ? "buzz" : s % 3 == 0 ? "fizz" : String.valueOf(s);
        Stream.of(arr)
                .map(formatter)   // other solution: .map(i -> formatter.apply(i))
                .forEach(System.out::print);
    }

    public static void main(String[] args) {

        printOptimised(new int[]{1, 2, 3, 7, 8, 5, 30, 4, 10});
        System.out.println("\n");

        printStream(new int[]{1, 2, 3, 7, 8, 5, 30, 4, 10});

        System.out.println();
        printStreamFunction(new Integer[]{1, 2, 3, 7, 8, 5, 30, 4, 10});
    }

}
