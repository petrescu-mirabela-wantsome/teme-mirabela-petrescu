package teme.w08.ex5_w5_BuildingRegistry;

/* Create a Building class representing a building, with the following properties:
● name
● category (possible values: residential, office, hospital, religious)
● price
● neighborhood
Tip: you should use an enum for the category values/field.
*/

enum Category {
    RESIDENTIAL,
    OFFICE,
    HOSPITAL,
    RELIGIOUS
}

class Building {
    private final String name;
    private final Category category;
    private final double price;
    private final String neighborhood;

    public Building(String name, Category category, int price, String neighborhood) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.neighborhood = neighborhood;
    }

    public double getPrice() {
        return price;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public Category getCategory() {
        return category;
    }
}
