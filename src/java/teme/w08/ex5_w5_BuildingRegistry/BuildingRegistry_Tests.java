package teme.w08.ex5_w5_BuildingRegistry;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for BuildingRegistry class (should compile and pass all tests after you complete that one first)
 */
public class BuildingRegistry_Tests {
    private static final double PRECISION = 0.01;

    private final List<Building> noBuildings = new ArrayList<>();
    private final List<Building> buildings = Arrays.asList(
            new Building("a", Category.OFFICE, 10, "tudor"),
            new Building("b", Category.OFFICE, 40, "centru"),
            new Building("c", Category.OFFICE, 20, "pacurari"),
            new Building("d", Category.RESIDENTIAL, 15, "pacurari"),
            new Building("e", Category.HOSPITAL, 35, "pacurari"),
            new Building("f", Category.HOSPITAL, 30, "copou"));


    @Test
    public void testActualCategories_noBuildings() {
        assertEquals(0, BuildingRegistry.categoriesCount(noBuildings));
    }

    @Test
    public void testActualCategories_noBuildingsStream() {
        assertEquals(0, BuildingRegistry.categoriesCountStream(noBuildings));
    }

    @Test
    public void testActualCategories() {
        assertEquals(3, BuildingRegistry.categoriesCount(buildings));
    }

    @Test
    public void testActualCategoriesStream() {
        assertEquals(3, BuildingRegistry.categoriesCountStream(buildings));
    }

    @Test
    public void testActualNeighborhoods_noBuildings() {
        assertTrue(BuildingRegistry.neighborhoodsList(noBuildings).isEmpty());
    }

    @Test
    public void testActualNeighborhoods_noBuildingsStream() {
        assertTrue(BuildingRegistry.neighborhoodsListStream(noBuildings).isEmpty());
    }

    @Test
    public void testActualNeighborhoods() {
        assertEquals(
                Arrays.asList("centru", "copou", "pacurari", "tudor"),
                BuildingRegistry.neighborhoodsList(buildings));
    }

    @Test
    public void testActualNeighborhoodsStream() {
        assertEquals(
                Arrays.asList("centru", "copou", "pacurari", "tudor"),
                BuildingRegistry.neighborhoodsListStream(buildings));
    }

    @Test
    public void testAveragePriceForOneCategory() {
        assertEquals(0, BuildingRegistry.averagePriceForOneCategory(noBuildings, Category.OFFICE), PRECISION);
        assertEquals(23.33, BuildingRegistry.averagePriceForOneCategory(buildings, Category.OFFICE), PRECISION);
        assertEquals(0, BuildingRegistry.averagePriceForOneCategory(buildings, Category.RELIGIOUS), PRECISION);
    }

    @Test
    public void testAveragePriceForOneCategoryStream() {
        assertEquals(0, BuildingRegistry.averagePriceForOneCategoryStream(noBuildings, Category.OFFICE), PRECISION);
        assertEquals(23.33, BuildingRegistry.averagePriceForOneCategoryStream(buildings, Category.OFFICE), PRECISION);
        assertEquals(0, BuildingRegistry.averagePriceForOneCategoryStream(buildings, Category.RELIGIOUS), PRECISION);
        assertEquals(0, BuildingRegistry.averagePriceForOneCategoryStream.apply(buildings, Category.RELIGIOUS), PRECISION);
    }

    @Test
    public void testAveragePricePerCategory_noBuildings() {
        Map<Category, Double> res = BuildingRegistry.averagePricePerCategory(noBuildings);
        assertEquals(4, res.size());
        assertEquals(0, res.get(Category.OFFICE), PRECISION);
        assertEquals(0, res.get(Category.HOSPITAL), PRECISION);
        assertEquals(0, res.get(Category.RELIGIOUS), PRECISION);
        assertEquals(0, res.get(Category.RESIDENTIAL), PRECISION);
    }

    @Test
    public void testAveragePricePerCategory() {
        Map<Category, Double> res = BuildingRegistry.averagePricePerCategory(buildings);
        assertEquals(4, res.size());
        assertEquals(23.33, res.get(Category.OFFICE), PRECISION);
        assertEquals(32.5, res.get(Category.HOSPITAL), PRECISION);
        assertEquals(0, res.get(Category.RELIGIOUS), PRECISION);
        assertEquals(15, res.get(Category.RESIDENTIAL), PRECISION);
    }

    @Test
    public void testAveragePricePerCategoryStream_noBuildings() {
        Map<Category, Double> res = BuildingRegistry.averagePricePerCategoryStream(noBuildings);
        assertEquals(4, res.size());
        assertEquals(0, res.get(Category.OFFICE), PRECISION);
        assertEquals(0, res.get(Category.HOSPITAL), PRECISION);
        assertEquals(0, res.get(Category.RELIGIOUS), PRECISION);
        assertEquals(0, res.get(Category.RESIDENTIAL), PRECISION);
    }

    @Test
    public void testAveragePricePerCategoryStream() {
        Map<Category, Double> res = BuildingRegistry.averagePricePerCategoryStream(buildings);
        assertEquals(4, res.size());
        assertEquals(23.33, res.get(Category.OFFICE), PRECISION);
        assertEquals(32.5, res.get(Category.HOSPITAL), PRECISION);
        assertEquals(0, res.get(Category.RELIGIOUS), PRECISION);
        assertEquals(15, res.get(Category.RESIDENTIAL), PRECISION);
    }

    @Test
    public void testAveragePricePerNeighborhood_noBuildings() {
        assertTrue(BuildingRegistry.averagePricePerNeighborhood(noBuildings).isEmpty());
    }

    @Test
    public void testAveragePricePerNeighborhoodStream_noBuildings() {
        assertTrue(BuildingRegistry.averagePricePerNeighborhoodStream(noBuildings).isEmpty());
    }

    @Test
    public void testAveragePricePerNeighborhoodStream2_noBuildings() {
        assertTrue(BuildingRegistry.averagePricePerNeighborhoodStream2(noBuildings).isEmpty());
    }

    @Test
    public void testAveragePricePerNeighborhood() {
        Map<String, Double> res = BuildingRegistry.averagePricePerNeighborhood(buildings);
        assertEquals(4, res.size());
        assertEquals(40, res.get("centru"), PRECISION);
        assertEquals(30, res.get("copou"), PRECISION);
        assertEquals(10, res.get("tudor"), PRECISION);
        assertEquals(23.33, res.get("pacurari"), PRECISION);
    }

    @Test
    public void testAveragePricePerNeighborhoodStream() {
        Map<String, Double> res = BuildingRegistry.averagePricePerNeighborhoodStream(buildings);
        assertEquals(4, res.size());
        assertEquals(40, res.get("centru"), PRECISION);
        assertEquals(30, res.get("copou"), PRECISION);
        assertEquals(10, res.get("tudor"), PRECISION);
        assertEquals(23.33, res.get("pacurari"), PRECISION);
    }

    @Test
    public void testAveragePricePerNeighborhoodStream2() {
        Map<String, Double> res = BuildingRegistry.averagePricePerNeighborhoodStream2(buildings);
        assertEquals(4, res.size());
        assertEquals(40, res.get("centru"), PRECISION);
        assertEquals(30, res.get("copou"), PRECISION);
        assertEquals(10, res.get("tudor"), PRECISION);
        assertEquals(23.33, res.get("pacurari"), PRECISION);
    }
}
