package teme.w08.ex5_w5_BuildingRegistry;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
1. Buildings Registry

a) Create a Building class representing a building, with the following properties:
- name
- category (possible values: residential, office, hospital, religious)
- price
- neighborhood

Tip: you should use an enum for the values of category field.


b) Create a BuildingRegistry class, containing these static methods (each receiving a List<Building> as input parameter, and you need to decide on the return type):

- categoriesCount() - returns the number of categories (of the actual buildings, not all possible categories)
- neighborhoodsList() - return the list of names of unique neighborhoods, sorted alphabetically (hint: may use a Set; what implementation can you use to also help you with sorting?..)

- averagePriceForOneCategory() - return the average price for building from only one category (given as a 2nd input parameter to the method)
  - question: what should be the return type for this method?..

- averagePricePerCategory() - return the average price for each building category (for ALL defined categories, even the ones without building)
  - question: what kind of return type should this method have? (as it should return an average price for each used category, so kind of returning a list of pairs of 2 values - category and price)

- averagePricePerNeighborhood() - return the average price for each neighborhood.
*/

public class BuildingRegistry {

    /* stream implementation
     */
    public static final BiFunction<List<Building>, Category, Double> averagePriceForOneCategoryStream =
            (buildings, category) ->
                    buildings
                            .stream()
                            .filter(s -> s.getCategory().equals(category))
                            .mapToDouble(Building::getPrice)
                            .average()
                            .orElse(0);
    /* stream implementation
     */
    private static final BiFunction<List<Building>, String, Double> averagePriceForOneNeighborhoodStream =
            (buildings, neighborhood) ->
                    buildings
                            .stream()
                            .filter(s -> s.getNeighborhood().equals(neighborhood))
                            .mapToDouble(Building::getPrice)
                            .average()
                            .orElse(0);

    /* return the number of categories (of the actual buildings, not all possible categories)
     */
    public static int categoriesCount(List<Building> buildings) {

        Set<Category> setCategory = new HashSet<>();
        for (Building itemBuilding : buildings) {
            setCategory.add(itemBuilding.getCategory());
        }
        return setCategory.size();
    }

    /* stream implementation
     */
    public static long categoriesCountStream(List<Building> buildings) {
        return getCategoriesStream(buildings).size();
    }

    /* stream implementation
     */
    private static List<Category> getCategoriesStream(List<Building> buildings) {
        return buildings
                .stream()
                .map(Building::getCategory)
                .distinct()
                .collect(Collectors.toList());
    }

    /* return the list of names of unique neighborhoods, sorted alphabetically
    (hint: may use a Set; what implementation can you use to also help you with sorting?..)
     */
    public static List<String> neighborhoodsList(List<Building> buildings) {

        Set<String> setNeighborhoods = new TreeSet<>();
        for (Building itemBuilding : buildings) {
            setNeighborhoods.add(itemBuilding.getNeighborhood());
        }
        return new ArrayList<>(setNeighborhoods);
    }

    /* stream implementation
     */
    public static List<String> neighborhoodsListStream(List<Building> buildings) {
        return buildings
                .stream()
                .map(Building::getNeighborhood)
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    /* return the average price for building from only one category (given as a 2nd input parameter to the method)
     */
    public static double averagePriceForOneCategory(List<Building> buildings, Category category) {
        double averagePrice = 0;
        if ((buildings.size() > 0)) {
            int count = 0;
            for (Building building : buildings) {
                if (building.getCategory().equals(category)) {
                    averagePrice += building.getPrice();
                    count++;
                }
            }
            averagePrice = count > 0 ? averagePrice / count : 0;
        }
        return averagePrice;
    }

    /* stream implementation
     */
    public static double averagePriceForOneCategoryStream(List<Building> buildings, Category category) {
        return buildings
                .stream()
                .filter(s -> s.getCategory().equals(category))
                .mapToDouble(Building::getPrice)
                .average()
                .orElse(0);
    }

    /* return the average price for each building category (for ALL defined categories, even the ones without building)
  As it should return an average price for each used category, so kind of returning a list of pairs of 2 values - category and price.
  */
    public static Map<Category, Double> averagePricePerCategory(List<Building> buildings) {
        Map<Category, Double> mapPricePerCategory = new HashMap<>();
        for (Category categoryItem : Category.values()) {
            mapPricePerCategory.put(categoryItem, averagePriceForOneCategory(buildings, categoryItem));
        }
        return mapPricePerCategory;
    }

    /* stream implementation
     */
    public static Map<Category, Double> averagePricePerCategoryStream(List<Building> buildings) {
        Map<Category, Double> averagePricePerNeighborhood = new HashMap<>();

        // Java enum iteration: https://www.baeldung.com/java-enum-iteration
        Stream.of(Category.values())
                .forEach(category -> averagePricePerNeighborhood.put(category, averagePriceForOneCategoryStream.apply(buildings, category)));
        return averagePricePerNeighborhood;
    }

    /* return the average price for one neighborhood given as 2nd input parameter to the method
     */
    private static double averagePriceForOneNeighborhood(List<Building> buildings, String neighborhood) {
        double averagePrice = 0;
        int count = 0;
        for (Building building : buildings) {
            if (building.getNeighborhood().equals(neighborhood)) {
                averagePrice += building.getPrice();
                count++;
            }
        }
        averagePrice /= count;
        return averagePrice;
    }

    /* return the average price for each neighborhood .
     */
    public static Map<String, Double> averagePricePerNeighborhood(List<Building> buildings) {
        Map<String, Double> mapPricePerNeighborhood = new HashMap<>();
        for (String neighborhoodsItem : neighborhoodsList(buildings)) {
            mapPricePerNeighborhood.put(neighborhoodsItem, averagePriceForOneNeighborhood(buildings, neighborhoodsItem));
        }
        return mapPricePerNeighborhood;
    }

    /* stream implementation
     */
    public static Map<String, Double> averagePricePerNeighborhoodStream(List<Building> buildings) {
        // create a list with all the neighborhood, unique entities
        List<String> neighborhoods = neighborhoodsListStream(buildings);
        Map<String, Double> averagePricePerNeighborhood = new HashMap<>();
        neighborhoods
                .forEach(neighborhood -> averagePricePerNeighborhood.put(neighborhood, averagePriceForOneNeighborhoodStream.apply(buildings, neighborhood)));

        return averagePricePerNeighborhood;
    }

    /* stream implementation - grouping solution
     */
    // Link: http://www.java2s.com/Tutorials/Java/Stream_How_to/Stream_Group/Sum_for_each_group.htm
    public static Map<String, Double> averagePricePerNeighborhoodStream2(List<Building> buildings) {
        return buildings
                .stream()
                .collect(Collectors.groupingBy(Building::getNeighborhood, Collectors.averagingDouble(Building::getPrice)));
    }

    /**
     * Some manual tests
     */
    public static void main(String[] args) {

        final List<Building> buildings = Arrays.asList(
                new Building("a", Category.OFFICE, 10, "tudor"),
                new Building("b", Category.OFFICE, 40, "centru"),
                new Building("c", Category.OFFICE, 20, "pacurari"),
                new Building("d", Category.RESIDENTIAL, 15, "pacurari"),
                new Building("e", Category.HOSPITAL, 35, "pacurari"),
                new Building("f", Category.HOSPITAL, 30, "copou"));

        System.out.println("Actual categories: " + BuildingRegistry.categoriesCount(buildings));
        System.out.println("Actual categories - stream implementation: " + BuildingRegistry.categoriesCountStream(buildings));

        System.out.println("Actual neighborhoods: " + BuildingRegistry.neighborhoodsList(buildings));
        System.out.println("Actual neighborhoods - stream implementation: " + BuildingRegistry.neighborhoodsListStream(buildings));

        System.out.println("Average price per category: " + BuildingRegistry.averagePriceForOneCategory(buildings, Category.OFFICE));
        System.out.println("Average price per category - stream implementation: " + BuildingRegistry.averagePriceForOneCategoryStream(buildings, Category.OFFICE));
        System.out.println("Average price per category - BiFunction stream implementation: " + BuildingRegistry.averagePriceForOneCategoryStream.apply(buildings, Category.OFFICE));

        System.out.println("Average price per category: " + BuildingRegistry.averagePricePerCategory(buildings));
        System.out.println("Average price per category - stream implementation: ");
        BuildingRegistry.averagePricePerCategoryStream(buildings);

        System.out.println("Average price per neighborhood: " + BuildingRegistry.averagePricePerNeighborhood(buildings));
        System.out.println("Average price per neighborhood - stream implementation: ");
        BuildingRegistry.averagePricePerNeighborhoodStream(buildings);
        System.out.println("Average price per neighborhood - stream implementation - grouping solution: ");
        BuildingRegistry.averagePricePerNeighborhoodStream2(buildings);
    }
}
