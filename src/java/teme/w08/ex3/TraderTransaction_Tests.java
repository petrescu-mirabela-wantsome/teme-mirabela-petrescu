package teme.w08.ex3;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class TraderTransaction_Tests {

    //helper method for easy building a new Array with some given elements (of a custom type)
    @SafeVarargs
    private static <E> List<E> newList(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    //helper method for easy building a new Array with some given elements (of a custom type)
    @SafeVarargs
    private static <E> Set<E> newSet(E... elements) {
        return new HashSet<>(Arrays.asList(elements));
    }

    @Test
    public void testEmptyTraderTransaction() {

        TraderTransaction traderTransaction = new TraderTransaction();

        assertTrue(traderTransaction.listTransaction.isEmpty());
        assertTrue(traderTransaction.findTransactionFrom2011().isEmpty());
        assertTrue(traderTransaction.findTransactionFrom2011Lambda().isEmpty());
        assertTrue(traderTransaction.findTransactionFrom2011Predicate().isEmpty());
        assertTrue(traderTransaction.uniqueCities().isEmpty());
        assertTrue(traderTransaction.uniqueCitiesSet().isEmpty());
        assertTrue(traderTransaction.tradersFromCambridge().isEmpty());
        assertTrue(traderTransaction.allTradersName().isEmpty());
        assertFalse(traderTransaction.anyTraderFromMilan());
        traderTransaction.tradersMilanToCambridge();
        assertTrue(traderTransaction.listTransaction.isEmpty());
        assertEquals(Optional.empty(), traderTransaction.highestTrasactionValue());
        assertEquals(Optional.empty(), traderTransaction.lowestTrasactionValue());

    }

    @Test
    public void testNonEmptyTraderTransaction() {
        TraderTransaction traderTransaction = new TraderTransaction();

        Transaction t1 = new Transaction(2000, 123, new Trader("Popescu", "Iasi"));
        Transaction t2 = new Transaction(2000, 154, new Trader("Cazacu", "Bacau"));
        Transaction t3 = new Transaction(2011, 500, new Trader("Pintilie", "Milan"));
        Transaction t4 = new Transaction(2000, 100, new Trader("Rotaru", "Cambridge"));
        Transaction t5 = new Transaction(2010, 124, new Trader("Pricopi", "Cluj"));
        Transaction t6 = new Transaction(2011, 90, new Trader("Vasiliu", "Cambridge"));
        Transaction t7 = new Transaction(1999, 170, new Trader("Paun", "Constanta"));
        Transaction t8 = new Transaction(2001, 186, new Trader("Radacina", "Cambridge"));
        Transaction t9 = new Transaction(2011, 30, new Trader("Iliescu", "Bucuresti"));
        Transaction t10 = new Transaction(2011, 16, new Trader("Constantinescu", "Cluj"));

        traderTransaction.listTransaction = newList(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10);

        assertFalse(traderTransaction.listTransaction.isEmpty());
        assertEquals(newList(t10, t9, t6, t3), traderTransaction.findTransactionFrom2011());
        assertEquals(newList(t10, t9, t6, t3), traderTransaction.findTransactionFrom2011Lambda());
        assertEquals(newList(t10, t9, t6, t3), traderTransaction.findTransactionFrom2011Predicate());
        assertEquals(newList("Bacau", "Bucuresti", "Cambridge", "Cluj", "Constanta", "Iasi", "Milan"), traderTransaction.uniqueCities());
        assertEquals(newSet("Milan", "Bucuresti", "Bacau", "Constanta", "Iasi", "Cambridge", "Cluj"), traderTransaction.uniqueCitiesSet());
        assertEquals(newList(t6, t4, t8), traderTransaction.tradersFromCambridge());
        assertEquals(newList("Cazacu", "Constantinescu", "Iliescu", "Paun", "Pintilie", "Popescu", "Pricopi", "Radacina", "Rotaru", "Vasiliu"), traderTransaction.allTradersName());
        assertTrue(traderTransaction.anyTraderFromMilan());
        traderTransaction.tradersMilanToCambridge();
        assertEquals(newList(t1, t2, new Transaction(2011, 500, new Trader("Pintilie", "Cambridge")), t4, t5, t6, t7, t8, t9, t10), traderTransaction.listTransaction);
        assertEquals(Optional.of(500), traderTransaction.highestTrasactionValue());
        assertEquals(Optional.of(16), traderTransaction.lowestTrasactionValue());
    }
}