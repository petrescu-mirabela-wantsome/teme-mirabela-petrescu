package teme.w08.ex1;

/*
1. Set operations with Streams
Given 2 Set<T> instances (holding elements of a generic type T), find
the intersection, union, and difference between them, but only using
stream operations.
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class Ex1<T> {

    //helper method for easy building a new Array with some given elements (of a custom type)
    private static <E> Set<E> newSet(E... elements) {
        return new HashSet<>(Arrays.asList(elements));
    }

    public static void main(String[] args) {

        Ex1 ex1 = new Ex1();
        System.out.println(ex1.union(newSet(1, 2, 3, 4), newSet(3, 4, 7, 8)));

        System.out.println(ex1.intersection(newSet(1, 2, 3, 4), newSet(3, 4, 7, 8)));

        System.out.println(ex1.intersectionFilter(newSet(1, 2, 3, 4), newSet(3, 4, 7, 8)));

        System.out.println(ex1.difference(newSet(1, 2, 3, 4), newSet(3, 4, 7, 8)));

        System.out.println(ex1.differenceFilter(newSet(1, 2, 3, 4), newSet(3, 4, 7, 8)));

    }

    private Set<T> union(Set<T> s1, Set<T> s2) {
        // go through each value of s2, add each value to s1
        s2.stream()
                .forEach(s1::add);
        return s1;
    }

    private Set<T> intersection(Set<T> s1, Set<T> s2) {
        // go through each value of s2, add in s3 the values which are also in s1
        Set<T> s3 = new HashSet<>();
        s2.stream()
                .forEach(i ->
                {
                    if (s1.contains(i))
                        s3.add(i);
                });

        return s3;
    }

    private Set<T> intersectionFilter(Set<T> s1, Set<T> s2) {
        // go through each value of s1, filter only the values which are also in s2
        return s1.stream()
                .filter(s2::contains)
                .collect(Collectors.toSet());
    }

    private Set<T> difference(Set<T> s1, Set<T> s2) {
        // go through each value of s1, add in s3 the values which are not in s2
        Set<T> s3 = new HashSet<>();
        s1.stream()
                .forEach(i ->
                {
                    if (!s2.contains(i))
                        s3.add(i);
                });

        return s3;
    }

    private Set<T> differenceFilter(Set<T> s1, Set<T> s2) {
        // go through each value of s1, filter only the values which are not in s2
        return s1.stream()
                .filter(i -> !s2.contains(i))
                .collect(Collectors.toSet());
    }
}
