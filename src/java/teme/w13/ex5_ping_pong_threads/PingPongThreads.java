package teme.w13.ex5_ping_pong_threads;

/*
Problem 5: Ping Pong threads game
We will simulate a game of ping pong between 2 players, represented each by a thread.
Read first a number N from keyboard (between 1 and 10). This will determine the length
of the game. (how many ping-pong messages will be exchanged between players)
Then start 2 threads, T1 (‘Pinger’) and T2 (‘Ponger’):
- T2 starts in a waiting state
- T1 starts the game by sending " Ping 1 " to T2, which sends back " Pong 1 " …
- T1 waits for the reply, and after receiving “ Pong x” , will send to T2 the message
“ Ping x+1 ” for a new round, etc..
- the match 'ends' when message " Pong N " was sent -> in this case both threads
should end, and then also the main one.
Other rules:
- threads must not get out of sync: each thread must wait to receive the right message
before responding to it
- threads should sleep for a random amount of time (1000-2000ms) before sending a
message back
- threads should write messages to console when receiving and when sending any
messages (including thread role + id for clarity)
Questions:
- In what format should you send the message? Think about how you'll need to consider
both the type of the message (Ping/Pong) and the number of it. (hint: your message
doesn't need to be a single/primitive value..)
- How should you send it? Multiple ways are possible, what is the easiest/better fit here?
(shared variable, atomic object, a blocking queue?..)
- Do you need more than 1 channel / shared resource to send the messages? (how do
you send to different destinations?)
Optional: instead of the game starting immediately when T1 starts, change your code
so both threads start in waiting state, and game starts only when the main thread sends
a special message, “Start”, to thread T1 (which will then send “Ping1” etc..)
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class PingPongThreads {

    public static void main(String[] args) {

        PingPongThreads pingPongThreads = new PingPongThreads();
        /*
        gameLength - the length of the game. (how many ping-pong messages will be exchanged between players)
         */
        int gameLength = Integer.valueOf(pingPongThreads.readValueFromUser("Introduce the game length, between 1 and 10"));

        List<Integer> msgObj = new ArrayList<>();

        String startGame = "";

        /*
        Optional: instead of the game starting immediately when T1 starts, change your code
        so both threads start in waiting state, and game starts only when the main thread sends
        a special message, “Start”, to thread T1 (which will then send “Ping1” etc..)
         */
        synchronized (startGame) {
            while (!startGame.equals("Start")) {
                System.out.println("Introduce start game => Start");
                startGame = new Scanner(System.in).next();

                if (startGame.equals("Start")) {
                    Thread t1 = new Thread(new PingerMsg(msgObj, gameLength, startGame));
                    Thread t2 = new Thread(new PongerMsg(msgObj, gameLength));
                    t1.start();
                    t2.start();
                }
            }
        }
    }

    public String readValueFromUser(String text) {
        System.out.println(text);
        return new Scanner(System.in).next();
    }
}

/*
 Pinger -> This class produces ping message and puts it in a shared list
  */
class PingerMsg implements Runnable {

    private List<Integer> msgObj;
    private int numberOfGames;
    private String startGame;

    PingerMsg(List<Integer> msgObj, int numberOfGames, String startGame) {
        this.msgObj = msgObj;
        this.numberOfGames = numberOfGames;
        this.startGame = startGame;
    }

    @Override
    public void run() {
        synchronized (startGame) {
            while (!startGame.equals("Start")) {
                try {
                    msgObj.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            for (int i = 1; i <= numberOfGames; i++) {
                synchronized (msgObj) {
                    // loop checking wait condition to avoid spurious wakeup
                    while (msgObj.size() >= 1) {
                        try {
                            msgObj.wait();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    msgObj.add(i);
                /*
                threads should sleep for a random amount of time (1000-2000ms) before sending a message back
                */
                    int randomSleepTime = new Random().nextInt(1001) + 1000;
                    System.out.println("Message " + i + " => Pinger " + Thread.currentThread().getName() + " will sleep " + randomSleepTime + " ms");
                    try {
                        Thread.sleep(randomSleepTime);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Message " + i + " => Pinger " + Thread.currentThread().getName() + " Ping-" + msgObj.get(0));
                    msgObj.notify();
                }
            }
        }
    }
}

/*
Ponger -> This class consumes pong message from a shared list
*/
class PongerMsg implements Runnable {

    private List<Integer> msgObj;
    private int numberOfGames;

    public PongerMsg(List<Integer> msgObj, int numberOfGames) {
        this.msgObj = msgObj;
        this.numberOfGames = numberOfGames;
    }

    @Override
    public void run() {
        for (int i = 1; i <= numberOfGames; i++) {
            synchronized (msgObj) {
                // loop checking wait condition to avoid spurious wakeup
                while (msgObj.size() < 1) {
                    try {
                        msgObj.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                /*
                threads should sleep for a random amount of time (1000-2000ms) before sending a message back
                */
                int randomSleepTime = new Random().nextInt(1001) + 1000;
                System.out.println("  Message " + i + " => Ponger " + Thread.currentThread().getName() + " will sleep " + randomSleepTime + " ms");
                try {
                    Thread.sleep(randomSleepTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("  Message " + i + " => Ponger " + Thread.currentThread().getName() + " Pong-" + msgObj.get(0));
                msgObj.remove(0);
                msgObj.notify();
            }
        }
    }
}
