package teme.w13.ex3_sum_array_in_parallel;

/*
Problem 3: Sum array in parallel
Write a program that sums the values from an array of ints using 4 threads.
- Each thread gets to sum a quarter of the array and then stores its partial result somehow
(in some kind of shared location?).
- The main thread needs to start and wait for the above threads to finish, after which it
collects the 4 partial answers, sums them and displays the final result.
For this exercise, try to create the threads by implementing the Runnable interface.
 */

import java.util.ArrayList;
import java.util.List;

public class SumArrayInParallel {

    static List<Integer> initArray = new ArrayList<>();
    static List<Integer> newArray = new ArrayList<>();

    static void arrayFill() {
        for (int i = 0; i < 100; i++) {
            initArray.add(i);
        }
    }

    public static void main(String[] args) {

        SumArrayThread t1 = new SumArrayThread(initArray, 0, 20);
        SumArrayThread t2 = new SumArrayThread(initArray, 21, 40);
        SumArrayThread t3 = new SumArrayThread(initArray, 41, 60);
        SumArrayThread t4 = new SumArrayThread(initArray, 61, 99);

        arrayFill();

        System.out.println("Expected sum: " + initArray.stream().mapToInt(v -> v).sum());

        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Resulted sum: " + newArray.stream().mapToInt(v -> v).sum());

    }
}
