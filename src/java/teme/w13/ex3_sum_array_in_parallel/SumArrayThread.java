package teme.w13.ex3_sum_array_in_parallel;

import java.util.List;

import static teme.w13.ex3_sum_array_in_parallel.SumArrayInParallel.newArray;

public class SumArrayThread extends Thread {
    private int startIndex;
    private int endIndex;
    private List<Integer> list;

    SumArrayThread(List<Integer> list, int startIndex, int endIndex) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.list = list;
    }

    @Override
    public void run() {
        System.out.println("Thread " + Thread.currentThread().getName() + " has the start index: " + startIndex + "; end index: " + endIndex);
        for (int i = startIndex; i <= endIndex; i++) {
            synchronized (newArray) {
                SumArrayInParallel.newArray.add(list.get(i));
            }
        }
    }
}
