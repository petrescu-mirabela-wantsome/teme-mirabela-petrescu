package teme.w13.ex4_interactive_multi_threaded_greeter;

/*
Problem 4: Interactive multi-threaded greeter
Create 10 new threads. They should have each a number (1..10) and run in the background
waiting.
- The main thread should start reading from user (from console) some integer numbers, in a
loop.
- if the current read number matches the number of one of the threads, then only that thread
should print "Hello from thread [number] (thread id: [id])" (and only once)
- if current number is -1 then all threads should write a goodbye message and then end (each
one writing "Goodbye from thread [number]").
 */

// https://www.programcreek.com/2011/12/monitors-java-synchronization-mechanism/
// https://www.programcreek.com/2009/02/notify-and-wait-example/

import java.util.Scanner;

public class InteractiveMultiThreadedGreeter {

    //static AtomicInteger numberFromUser = new AtomicInteger(0);
//    static CopyOnWriteArrayList<ThreadGreeter> copyOnWriteArrayList = new CopyOnWriteArrayList();

    public static void main(String[] args) {

//        List<ThreadGreeter> threadList = new ArrayList<>();

//        ThreadReadNumber threadReadNumber = new ThreadReadNumber();
//
//        threadReadNumber.start();
//        try {
//            threadReadNumber.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        int numberFromUser = threadReadNumber.getNumberFromUser();
        Integer numberFromUser = 0;

//        for (int i = 1; i <= 10; i++) {
//            ThreadGreeter t = new ThreadGreeter(i, numberFromUser);
//            copyOnWriteArrayList.add(t);
//            System.out.println("Thread " + t.getName() + " state: " + t.getState());
//        }

        synchronized (numberFromUser) {
            while (numberFromUser != -1) {
                for (int i = 1; i <= 10; i++) {
                    //for (ThreadGreeter threadGreeterItem : copyOnWriteArrayList) {
                    ThreadGreeter t = new ThreadGreeter(i, numberFromUser);
                    t.start();
                    //System.out.println("Thread " + t.getName() + " state: " + t.getState());
                    //threadGreeterItem.start();
                    //System.out.println("Thread " + threadGreeterItem.getName() + " state: " + threadGreeterItem.getState());

                }
                numberFromUser = readFromUser();
            }
            System.out.println("[main thread finished]");
        }
    }

//    public static AtomicInteger readFromUser() {
//        System.out.println("Enter a number from 1 to 10, -1 to exit");
//        String valueFromUser = new Scanner(System.in).next();
//        return new AtomicInteger(Integer.valueOf(valueFromUser));
//    }

    public static int readFromUser() {
        System.out.println("Enter a number from 1 to 10, -1 to exit");
        String valueFromUser = new Scanner(System.in).next();
        return Integer.valueOf(valueFromUser);
    }

}

class ThreadGreeter extends Thread {

    Integer numberFromUser;
    private int counter;

    ThreadGreeter(int counter, int numberFromUser) {
        this.counter = counter;
        this.numberFromUser = numberFromUser;
    }

    @Override
    public void run() {
        synchronized (numberFromUser) {
            //System.out.println("started thread [" + counter + "] (thread id: [" + Thread.currentThread().getId() + "])");
            //System.out.println("Thread " + this.getName() + " state: " + this.getState());
            while (!numberFromUser.equals(counter)) {
                // Waiting ...
                try {
                    //numberFromUser.wait();
                    //Thread.currentThread().sleep(500);
                    //System.out.println("Thread " + this.getName() + " state (waiting to be called): " + this.getState());

                    numberFromUser.wait();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //System.out.println("Thread " + this.getName() + " state: " + this.getState());
            //System.out.println("Hello from thread [" + counter + "] (thread id: [" + Thread.currentThread().getId() + "])");

//            numberFromUser = 0;
//            if (numberFromUser.equals(-1)) {
//                numberFromUser.notifyAll();
//                System.out.println("Goodbye from thread [" + counter + "]");
//            }
            System.out.println("Hello from thread [" + counter + "] (thread id: [" + Thread.currentThread().getId() + "])");
            numberFromUser.notify();
        }
    }
}

//class ThreadReadNumber extends Thread {
//
//    int numberFromUser;
//
//    public int getNumberFromUser() {
//        return numberFromUser;
//    }
//
//    @Override
//    public void run() {
//        synchronized (this) {
//            readFromUser();
//            notify();
//        }
//    }
//
//    public static int readFromUser() {
//        System.out.println("Enter a number from 1 to 10, -1 to exit");
//        String valueFromUser = new Scanner(System.in).next();
//        return Integer.valueOf(valueFromUser);
//    }
//}