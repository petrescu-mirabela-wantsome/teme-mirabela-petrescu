package teme.w13.ex2_shared_resource;

/*
Problem 2: Shared resource
Start 2 new threads, and find a way to share a variable between them.
The 2 thread will have different roles:
- one should sleep a little, then change the value of the shared variable, then end;
- the 2nd one should wait for the change (in a loop with sleep), detect when the variable
has changed and print a message about the change (old and new value), and then end;
Optional : once you have a solution, think about a 2nd different way in which you can
share the variable between the 2 threads..
 */


public class SharedResourceThreadMain {

    public static void main(String[] args) {

        new SharedResourceThread().start();
        new SharedResourceThreadChild().start();
    }
}
