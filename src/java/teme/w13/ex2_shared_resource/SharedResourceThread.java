package teme.w13.ex2_shared_resource;


public class SharedResourceThread extends Thread {
    static int commonVar = 1;

    @Override
    public void run() {
        System.out.println("Thread 1 started common var is: " + commonVar);
        System.out.println("Thread 1 sleeping 1 sec ...");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread 1 sleeping ended 1 sec ...");
        System.out.println("Thread 1 incrementing common var ...");
        commonVar += 1;
        System.out.println("Thread 1 done, common var is " + commonVar);
        System.out.println(".. Thread 1 ended");
    }
}
