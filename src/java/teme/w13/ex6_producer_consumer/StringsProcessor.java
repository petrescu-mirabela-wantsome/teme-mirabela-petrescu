package teme.w13.ex6_producer_consumer;

/*
Write a StringsProcessor interface with the following implementations:
    ● StringsPrinter , prints a string to the console
    ● VowelsCounter , prints to the console the number of vowels in a word
    ● StringsLength , prints to the console the length of a string
    ● VowelsRemover , prints to the console the string that results from removing all vowels
 */

public interface StringsProcessor {

    /*
    prints a string to the console
     */
    default void StringsPrinter(String word) {
        System.out.println("Print the generated word: " + word);
    }

    /*
    prints to the console the number of vowels in a word
     */
    default int VowelsCounter(String word) {
        char[] wordArray = word.toCharArray();
        int count = 0;
        for (char c : wordArray) {
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u') {
                count++;
            }
        }
        System.out.println("Number of vowels: " + count);
        return count;
    }

    /*
    prints to the console the length of a string
     */
    default int StringsLength(String word) {
        System.out.println("Word length: " + word.length());
        return word.length();
    }

    /*
    prints to the console the string that results from removing all vowels
     */
    default StringBuilder VowelsRemover(String word) {
        char[] wordArray = word.toCharArray();
        StringBuilder newWordWithoutVowels = new StringBuilder();
//        char[] newWordArray = new char[wordArray.length - VowelsCounter(word)];
//        int count = 0;
        for (int i = 0; i < wordArray.length; i++) {
            if (wordArray[i] != 'a' && wordArray[i] != 'e' && wordArray[i] != 'i' && wordArray[i] != 'o' && wordArray[i] != 'u') {
                newWordWithoutVowels.append(wordArray[i]);
                //newWordArray[count] = wordArray[i];
                //count++;
            }
        }
        System.out.println("Word without vowels: " + newWordWithoutVowels);
        return newWordWithoutVowels;
    }

}
