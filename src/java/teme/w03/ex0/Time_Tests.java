package teme.w03.ex0;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for Time class (should compile and pass all tests after you complete that one first)
 */
public class Time_Tests {


    @Test
    public void testBuildTime() {
        Time t1 = new Time(0, 0, 0);
        assertEquals(0, t1.getHours());
        assertEquals(0, t1.getMinutes());
        assertEquals(0, t1.getSeconds());

        Time t2 = new Time(1, 2, 3);
        assertEquals(1, t2.getHours());
        assertEquals(2, t2.getMinutes());
        assertEquals(3, t2.getSeconds());

        Time t3 = new Time();
        assertEquals(23, t3.getHours());
        assertEquals(59, t3.getMinutes());
        assertEquals(59, t3.getSeconds());
    }

    @Test
    public void testSecondsSinceMidnight() {
        Time t1 = new Time(0, 0, 0);
        assertEquals(0, t1.secondsSinceMidnight());

        Time t2 = new Time(1, 2, 3);
        assertEquals(3723, t2.secondsSinceMidnight());
    }

    @Test
    public void testSecondsSince() {
        Time t1 = new Time(1, 2, 3);
        Time t2 = new Time(2, 30, 45);
        assertEquals(5322, t2.secondsSince(t1));
        assertEquals(-5322, t1.secondsSince(t2));
    }

    @Test
    public void testToString() {
        assertEquals("1:2:3", new Time(1, 2, 3).toString());
    }

}