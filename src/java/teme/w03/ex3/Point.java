package teme.w03.ex3;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/*
--------------------------
Ex3. Points and triangles
--------------------------

Create a class Point for modeling a 2-dimensional point:

a) Add some fields to store the values for 2 coordinates (x,y of type double).
   Question1: should you also make them ‘private’? What about ‘final’? (yes/no? why?)
   Question2: should you add getters/setter from them from the start? (are they needed for this case,
              considering all the requirements below?)

b) Add one constructor for easily building new points with given x,y values;
   Question: before you add this constructor, can you build instances like this: “new Point()” (with no params) ?
             what values will the x,y fields have in this case?  will that exact syntax work (no params)
             after you add your constructor? why yes/no?..

c) Add a few methods:
   - double distanceTo(Point other) -> should return the computed distance between this and another point
     hint1: using Pitagora theorem, the distance between the 2 points in a plane is:  dist = squareRoot(deltaX^2 +deltaY^2)
            where deltaX/Y are the difference between the x/y coordinates of the 2 points, and x^2 = x to power of 2
     hint2: for computations, you may use use these static methods from java.lang.Math class: sqrt(), pow().
            for a shorter code, try importing them directly, using a static import (instead of regular import of Math class)

   - void move(double deltaX, double deltaY) -> should move the current point in plane, by updating its current coordinates
     (add to each x,y the given deltaX/Y)

   - public String toString() - should return a string description of the point (including the values of x/y);
     make sure you declare this one as ‘public’ and exactly with this signature (has a special meaning for Java)

d) Add some static methods, which we can then use to check if a list of any three Point instances can be the corners
   of a regular or a right-angled triangle:
   - static boolean canFormTriangle(Point p1, Point p2, Point p3)
   - static boolean canFormRightAngledTriangle(Point p1, Point p2, Point p3)

   hint: to implement these, you could first compute the 3 distances between each of the pairs of 2 points
         (this being the sides of our possible triangle), then remember what you did for exercises from week 1.
*/

public class Point {

    private double x, y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * check if a list of any three Point instances can be the corners of a regular triangle
     */
    static boolean canFormTriangle(Point p1, Point p2, Point p3) {
        return (p1.distanceTo(p2) + p1.distanceTo(p3) > p2.distanceTo(p3)) &&
                (p1.distanceTo(p2) + p2.distanceTo(p3) > p1.distanceTo(p3)) &&
                (p1.distanceTo(p3) + p2.distanceTo(p3) > p1.distanceTo(p2));
    }

    /**
     * check if a list of any three Point instances can be the corners of a right triangle
     * /HINT: you may also reuse/call here the method 'canFormTriangle' (if you fixed that one) to first check the triangle is valid..
     */
    static boolean canFormRightAngledTriangle(Point p1, Point p2, Point p3) {
        double side1 = p1.distanceTo(p2);
        double side2 = p1.distanceTo(p3);
        double side3 = p2.distanceTo(p3);
        boolean isRightAngled = checkPitagora(side1, side2, side3) || checkPitagora(side1, side3, side2) || checkPitagora(side2, side3, side1);
        return canFormTriangle(p1, p2, p3) && isRightAngled;
    }

    private static boolean checkPitagora(double cateta1, double cateta2, double ipotenuza) {
        return ((cateta1 * cateta1 + cateta2 * cateta2) == ipotenuza * ipotenuza);
    }

    //--- this is here just for manual testing ---//
    public static void main(String[] args) {
        //Point p = ...
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * return the computed distance between this and another point
     * hint1: using Pitagora theorem, the distance between the 2 points in a plane is:  dist = squareRoot(deltaX^2 +deltaY^2)
     * where deltaX/Y are the difference between the x/y coordinates of the 2 points, and x^2 = x to power of 2
     * hint2: for computations, you may use use these static methods from java.lang.Math class: sqrt(), pow().
     * for a shorter code, try importing them directly, using a static import (instead of regular import of Math class)
     */
    double distanceTo(Point other) {
        return Math.sqrt(Math.pow(other.x - x, 2) + Math.pow(other.y - y, 2));
    }

    /**
     * should move the current point in plane, by updating its current coordinates (add to each x,y the given deltaX/Y)
     */
    void move(double deltaX, double deltaY) {
        x += deltaX;
        y += deltaY;
    }

    /**
     * return a string description of the point (including the values of x/y);
     * make sure you declare this one as ‘public’ and exactly with this signature (has a special meaning for Java)
     * >>>> overrides the toString method of Object class
     */
    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
