package teme.w03.ex6;

import java.util.Arrays;

public class MyArrayList {
    private int[] values;   // integers in a field of type array

    /* It should start with an empty collection (meaning an empty array - of size 0)
     */
    public MyArrayList() {
        values = new int[]{};
    }

    public MyArrayList(int[] values) {
        this.values = values;
    }

    @Override
    public String toString() {
        return "MyArrayList{" +
                "values=" + Arrays.toString(values) +
                '}';
    }

    /*
     * Add new values to its end
     */
    public void add(int newValue) {
        values = Arrays.copyOf(values, values.length + 1); // increase the memory allocated, to store the new value newValue
        values[values.length - 1] = newValue;
    }

    /*
     * Delete last value from the end:
     */
    public void remove() {
        if (values.length > 0)
            values = Arrays.copyOf(values, values.length - 1);  // resize the array length --> initial length - 1
    }

    /*
     * Get elements count:
     */
    public int size() {
        return values.length;
    }

    /*
     * Get element at specific index
     * in case index is out of valid range [0 .. values.length-1], the function shall return -1
     */
    int get(int index) {
        if (index >= 0 && index < values.length) {
            return values[index];
        }
        return -1;
    }

    /*
     * a method to insert a new element at a specific position (by index, shifting all elements after it to the next position)
     */
    public void addIndex(int newValue, int index) {
        if (index >= 0 && index < values.length) {
            values = Arrays.copyOf(values, values.length + 1); // the values length is now previous length + 1, to consider the new introduced value
            int j = values.length - 1;  // start to add from the end till the index where the value shall be added
            for (int i = 1; i < values.length; i++) {
                if ((i - 1) == index) {     // when the index position is found
                    // Insertion Sort algorithm from w01.ex12
                    // shift all the elements to the right from the last index of the initial array until the index + 1 position
                    while (j >= i) {
                        values[j] = values[j - 1];
                        j--;
                    }
                    values[index] = newValue;
                }
            }
        }
    }


    /*
     * A method to remove an element from a specified position (by index)
     * Hints: for operations which modify the number of elements (add, remove), you will need to
     * “change the size” of the array field holding the values -> basically you will need to create a new
     * array of proper size, and then copy some of the values there…
     */

    void removeIndex(int index) {
        if (index >= 0 && index < values.length) {
            for (int i = 1; i < values.length; i++) {
                if ((i - 1) == index) {
                    // Insertion Sort algorithm ideea from w01.ex12 but for removing an element
                    // shift all the elements to the left from the index position till the last index of the initial array
                    int j = i;
                    while (j < values.length) {
                        values[j - 1] = values[j];
                        j++;
                    }
                    values = Arrays.copyOf(values, values.length - 1);  // the values length is now previous length - 1, not to consider the removed element
                }
            }
        }
    }
}
