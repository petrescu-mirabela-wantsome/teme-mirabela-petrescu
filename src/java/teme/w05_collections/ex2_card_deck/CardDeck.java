package teme.w05_collections.ex2_card_deck;

import java.util.*;

/*
2. Card CardDeck

a) Create a Card class to remember the individual cards. It should have 2 properties:
- number (2 - 14)
- suit (one of these types: diamonds (♦), clubs (♣), hearts (♥) and spades (♠) ).

b) Create a CardDeck class used for dealing hands.
Should contain a list of all 52 cards and should remember which ones are dealt and which ones are available.

Should contain the following methods:
- List<Card> dealHand(int cards): this method returns the specified number of cards by randomly picking from the list of available cards.
  Once picked, a card is marked as “used” (removed from “available” cards list)
  If there are not enough available cards then return as many as possible, or an empty list when there are no longer any available cards.

- void shuffle(): this should mark all cards in the deck as available (essentially emptying the “used” collection) and re-shuffle all the cards.
- int getAvailableCardCount()
- int getUsedCardCount()

Hints:
- for storing the full list of cards, as well as the lists of available/used cards, you could use some instances of List
- for shuffling a collection, the Collections.shuffle() method can be used.
 */

class CardDeck {

    /* The map contains the Card and its status (true - available, false - dealt)
     */
    private final Map<Card, Boolean> mapCards = new LinkedHashMap<>();
    private List<Card> pickedCards = new ArrayList<>();
    private List<Card> availableCards = new ArrayList<>();

    public CardDeck() {
        generateCardDeck(); // generate a card from all the 52 possible card combinations
    }

    /**
     * Just for some manual tests
     */
    public static void main(String[] args) {
        CardDeck deck = new CardDeck();
        System.out.println(deck.dealHand(5)); // <- print 5 cards 3 times
        System.out.println(deck.dealHand(5));
        System.out.println(deck.dealHand(5));
        System.out.println(deck.dealHand(50)); // <- only 39 cards should be printed here
        System.out.println(deck.dealHand(50)); // <- and empty list should be printed
        deck.shuffle();
        System.out.println(deck.dealHand(5)); // <- another 5 cards
    }

    /*
        Generate a card from all the 52 possible card combinations
     */
    private void generateCardDeck() {
        for (int i = 2; i <= 14; i++) {
            for (Suit itemSuitCard : Suit.values()) {
                mapCards.put(new Card(i, itemSuitCard), true);
            }
        }
    }

    /* this should mark all cards in the deck as available (essentially emptying the “used” collection) and re-shuffle all the cards.
     */
    void shuffle() {
        List<Card> listCards = new ArrayList<>(mapCards.keySet());
        Collections.shuffle(listCards);     // shuffle the cards

//        Set<Card> listCards = mapCards.keySet();
//        Collections.shuffle(new ArrayList<>(listCards));

        mapCards.clear();   // removes all map entries
        for (Card itemListCards : listCards) {
            mapCards.put(itemListCards, true);  // add the new shuffle card values and mark all cards in the deck as available
        }
    }

    /* this method returns the specified number of cards by randomly picking from the list of available cards.
  Once picked, a card is marked as “used” (removed from “available” cards list)
  If there are not enough available cards then return as many as possible, or an empty list when there are no longer any available cards.
  */
    List<Card> dealHand(int handSize) {
        availableCards = getAvailableCards();
        pickedCards = new ArrayList<>();

        /* If there are not enough available cards then return as many as possible, or an
          empty list when there are no longer any available cards.
         */
        if (availableCards.size() > 0) {
            if (availableCards.size() < handSize) {
                handSize = availableCards.size();
            }
            // generate a random number in a specified range (from the available cards)
            // this random generation is performed for a number of cards time (parameter function)
            for (int i = 0; i < handSize; i++) {
                int index = new Random().nextInt(availableCards.size());    // generate a random number equal to the number of available cards
                Card randomGeneratedCard = availableCards.get(index);       // get from the available cards the Card vakue of the index which was generated randomly
                mapCards.put(randomGeneratedCard, false);       // once the card was used, this will be set as false - not available in the dealing hand
                pickedCards.add(randomGeneratedCard);           // add the used card into the list of picked cards
                availableCards.remove(randomGeneratedCard);     // remove the used card from the list of the available cards
            }
        }
        return pickedCards;
    }

    /*
    Return a list will all the available cards
 */
    private List<Card> getAvailableCards() {
        availableCards = new ArrayList<>();

        for (Card itemCard : mapCards.keySet()) {
            if (mapCards.get(itemCard)) {   // if a Card is available - not used (the value of the specified key is true)
                availableCards.add(itemCard);
            }
        }
        return availableCards;
    }

    int getAvailableCardCount() {
        return getAvailableCards().size();
    }

    int getUsedCardCount() {
        pickedCards = new ArrayList<>();

        for (Card itemCard : mapCards.keySet()) {
            if (!mapCards.get(itemCard)) {   // if a Card is NOT available - picked card (the value of the specified key is false)
                pickedCards.add(itemCard);
            }
        }
        return pickedCards.size();
    }
}
