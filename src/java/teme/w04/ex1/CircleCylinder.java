package teme.w04.ex1;

/*
Ex1. Circle & Cylinder (composition vs inheritance)

a) Define a Circle class (fields: centerX/Y,radius; methods: area(), length(), toString(), getters)
   - hint: circle area = PI*R^2, length = 2*PI*R ; may use Math.PI, Math.pow.. if needed
   - question: can you make the fields final? (can you think of any benefits it might bring?) What about the methods and/or the class itself?

b) Based on it, define a CylinderH class, first using inheritance (no composition)
   - with 2 constructors:
        Cylinder(double centerX, double centerY, double radius, double height)
        Cylinder(Circle circle, double height)
     Can you avoid repeating code between constructors? (hint: try to use this())

   - members: similar to Circle (inherited), but also with some extra fields (height) and extra methods:  volume(), getBase() (returns a Circle representing the base), getHeight()
     - hint: volume = base area * height
     - question: for volume(), can you use the existing area() method?
     - question: do all inherited methods make sense as they are for Cylinder? (like: area(), length(), toString())

   - now override methods to better fit Cylinder logic: area(), toString()
     - hint: cylinder surface area = 2*base area + h*base perimeter
     - question: can we use area() from Circle for computing area() for Cylinder, instead of repeating that code? how?..
     - question: after overriding area(), is the volume() still working correctly? if not: why, and how can we fix it?
     - question: can we reuse Circle.toString() when overriding toString() for cylinder?

   - question: what can we do with length(), as its really not relevant/needed anymore for Cylinder? (hide it somehow? return some special value?..)

c) Now solve it again, but using composition (instead of inheritance) and creating a different class named CylinderC (which should NOT extend Circle, but should instead contain a Circle field…)
   - question: does it look better/simpler, or worse that 1st? any pluses/minuses?

d) Define a Cylinder generic interface, and define in it the common properties of CylinderH and CylinderC
   - modify the 2 classes so that both of them implement this interface
   - question: how many lines did you need to change in the 2 classes?
   - question: does client code which wants to work with one of our 2 cylinder implementations need to still mention
               the name of one of the 2 classes (CylinderH/CylinderC), or can it use only Cylinder name everywhere?
               (if class names are still needs, where exactly?)

e) Is it possible to further reduce duplication between the 2 classes? Is there any code which you could move from
   the 2 cylinder implementation classes to the interface itself (reducing duplication) AND which would remain correct
   in the future, even if we add other type of cylinder classes implementing this interface? (hint: see ‘default’ methods)
   - is there any difference between the 2 classes, regarding how easy is to move some of their code to the interface?..
   - is any client code affected by such a move, what do you think?
   - do all the tests still pass? (check after you did the moving)

f) Optional: draw an UML class diagram for all these classes/interfaces
 */

interface Cylinder {
    Circle getBase();  //non-default method, requires implementation by classes..

    double getHeight(); //non-default method, requires implementation by classes..

    double area(); // --> for the composition case is needed, not for inheritance where area() is already overridden from Circle class
//     /* area() can not be default because it is implemented in Circle class and overridden in CylinderC and CylinderH class
//     if the following code is uncommented, then the result will be the Circle area instead of Cylinder area.*/
//    default double area(){
//        return 2 * getBase().area() + getBase().length() * getHeight();
//    }

    default double volume() {
        return getBase().area() * getHeight();
    }
}

    /*
    b) Based on it, define a CylinderH class, first using inheritance (no composition)
   - with 2 constructors:
        Cylinder(double centerX, double centerY, double radius, double height)
        Cylinder(Circle circle, double height)
     Can you avoid repeating code between constructors? (hint: try to use this())
     */

public class CircleCylinder {

    public static void main(String[] args) {

        Circle base = new Circle(0.1, 0.2, 2.5);
        System.out.println("base: " + base);
        System.out.println("base area: " + base.area() + ", length: " + base.length());

        //with inheritance:
        CylinderH ch = new CylinderH(0.1, 0.2, 2.5, 3.5);
        System.out.println("\nch: " + ch);
        System.out.println("ch: base: " + ch.getBase());
        System.out.println("ch: base area: " + ch.getBase().area());
        System.out.println("ch: total area: " + ch.area() + ", volume: " + ch.volume());
        System.out.println("ch length?: " + ch.length());

        //with composition:
        CylinderC cc = new CylinderC(0.1, 0.2, 2.5, 3.5);
        System.out.println("\ncc: " + cc);
        System.out.println("cc: base: " + cc.getBase());
        System.out.println("cc: base area: " + cc.getBase().area());
        System.out.println("cc: total area: " + cc.area() + ", volume: " + cc.volume());
        //System.out.println("cc length?: " + cc.length()); //=> will result in compile error
    }
}

//////// CASE - WITHOUT INTERFACE //////////
// class CylinderH extends Circle {
final class CylinderH extends Circle implements Cylinder {

    private final double height;  // visible only in CylinderH class, store the height of an instance of the class CylinderH

    public CylinderH(double centerX, double centerY, double radius, double height) {
        super(centerX, centerY, radius);   // constructor call of Circle superclass
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    /* - members: similar to Circle (inherited), but also with some extra fields (height) and extra methods:  volume(), getBase() (returns a Circle representing the base), getHeight()
     - hint: volume = base area * height
     - question: for volume(), can you use the existing area() method?
     - question: do all inherited methods make sense as they are for Cylinder? (like: area(), length(), toString())
     */

//     /* Not valid if volume() is default method in Cylinder interface */
//    public double volume() {
//        return getBase().area() * height;   // circle area * cylinder height
//    }

    // returns a Circle representing the base (Circle area)
    // baseArea = PI * radius * radius
    @Override
    public Circle getBase() {
        return new Circle(getCenterX(), getCenterY(), getRadius());
    }

    /* - now override methods to better fit Cylinder logic: area(), toString()
     - hint: cylinder surface area = 2*base area + h*base perimeter
     - question: can we use area() from Circle for computing area() for Cylinder, instead of repeating that code? how?..
     - question: after overriding area(), is the volume() still working correctly? if not: why, and how can we fix it?
     - question: can we reuse Circle.toString() when overriding toString() for cylinder?
     */

    // cylinder area = 2 * base cylinder area + circle length * cylinder height
    //implement interface methods:
    @Override
    public double area() {
        return 2 * super.area() + super.length() * height;   // call superclass area() method
        //return 2 * getBase().area() + getBase().length() * height;   // call superclass area() method
    }

    @Override
    public String toString() {
        return "CylinderH{" +
                super.toString() +
                "height=" + height +
                '}';
    }
}

/* d) Define a Cylinder generic interface, and define in it the common properties of CylinderH and CylinderC
   - modify the 2 classes so that both of them implement this interface
   - question: how many lines did you need to change in the 2 classes?
   - question: does client code which wants to work with one of our 2 cylinder implementations need to still mention
               the name of one of the 2 classes (CylinderH/CylinderC), or can it use only Cylinder name everywhere?
               (if class names are still needs, where exactly?)
 */

/*
c) Now solve it again, but using composition (instead of inheritance) and creating a different class named CylinderC (which should NOT extend Circle, but should instead contain a Circle field…)
- question: does it look better/simpler, or worse that 1st? any pluses/minuses?
*/
final class CylinderC implements Cylinder {   // no extend
    private final double height;
    private final Circle base;

    public CylinderC(double centerX, double centerY, double radius, double height) {
        this(new Circle(centerX, centerY, radius), height);
    }

    private CylinderC(Circle base, double height) {
        this.base = base;
        this.height = height;
    }

    @Override
    public Circle getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

//     /* Not valid if volume() is default method in Cylinder interface */
//    public double volume() {
//        return base.area() * height;   // circle area * cylinder height
//    }

    //implement interface methods:
    @Override
    public double area() {
        return 2 * base.area() + base.length() * height;   // call superclass area() method
    }

    @Override
    public String toString() {
        return "CylinderC{" +
                "height=" + height +
                ", base=" + base +
                '}';
    }
}

/* e) Is it possible to further reduce duplication between the 2 classes? Is there any code which you could move from
   the 2 cylinder implementation classes to the interface itself (reducing duplication) AND which would remain correct
   in the future, even if we add other type of cylinder classes implementing this interface? (hint: see ‘default’ methods)
   - is there any difference between the 2 classes, regarding how easy is to move some of their code to the interface?..
   - is any client code affected by such a move, what do you think?
   - do all the tests still pass? (check after you did the moving)

f) Optional: draw an UML class diagram for all these classes/interfaces
 */