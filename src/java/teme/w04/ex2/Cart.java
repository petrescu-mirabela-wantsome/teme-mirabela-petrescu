package teme.w04.ex2;

import teme.w04.ex2.customer.Customer;
import teme.w04.ex2.discount.Discount;
import teme.w04.ex2.product.Product;

import java.util.Arrays;
import java.util.Objects;

class Cart {
    private final Customer customer;
    private Product[] products;
    private Discount[] discounts;

    public Cart(Customer customer) {
        this.customer = customer;
        products = new Product[0];
        discounts = new Discount[0];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Cart)) return false;
        Cart cart = (Cart) o;
        return Objects.equals(customer, cart.customer) &&
                Arrays.equals(products, cart.products) &&
                Arrays.equals(discounts, cart.discounts);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(customer);
        result = 31 * result + Arrays.hashCode(products);
        result = 31 * result + Arrays.hashCode(discounts);
        return result;
    }

    public void addProduct(Product p) {
        products = Arrays.copyOf(products, products.length + 1); // increase the memory allocated, to store the new value newValue
        products[products.length - 1] = p;

    }


    public void removeProduct(Product p) {
        int i, j;
        for (i = 0; i < products.length; i++) {
            if (products[i].equals(p)) {
                j = i;
                while (j < products.length - 1) {
                    products[j] = products[j + 1];
                    j++;
                }
                products = Arrays.copyOf(products, j);
            }
        }
    }


    public void addDiscount(Discount d) {
        discounts = Arrays.copyOf(discounts, discounts.length + 1); // increase the memory allocated, to store the new value newValue
        discounts[discounts.length - 1] = d;
    }

    public void removeDiscount(Discount d) {
        int i, j;
        for (i = 0; i < discounts.length; i++) {
            if (discounts[i].equals(d)) {
                j = i;
                while (j < discounts.length - 1) {
                    discounts[j] = discounts[j + 1];
                    j++;
                }
                discounts = Arrays.copyOf(discounts, j);
            }
        }
    }

    /* Compute total price of products (without any discounts applied)
     */
    private double computeProductsPrice() {
        double totalPrice = 0;
        for (Product item : products) {
            totalPrice += item.getPrice();
        }
        return totalPrice;
    }

    /* Compute final cart price (total price of products + all discounts applied)
     */
    public double computeTotalPrice() {
        double totalPriceDiscount = computeProductsPrice();
        if (discounts.length != 0) {
            for (Discount itemDiscount : discounts) {
                totalPriceDiscount -= itemDiscount.priceWithDiscount(totalPriceDiscount);
            }
        }
        return totalPriceDiscount;
    }

    /* Should return a String (multi-line, use “\n” to separate lines) representing the invoice for current cart;
        It should contain this info:
        ○ Customer info
        ○ List of products
        ○ Total products price (without discounts)
        ○ List of discounts
        ○ Total amount (total products price + discounts)
     */

    String generateInvoice() {
        return ">>>>> Cart info: >>>>>\n" +
                customer.toString() + "\n" +
                "---- List of products ----" + Arrays.toString(products) + ", \n" +
                "---- Total products price (without discounts) = " + computeProductsPrice() + "\n" +
                "---- List of discounts ----" + Arrays.toString(discounts) + ", \n" +
                "Total amount (total products price + discounts) = " + computeTotalPrice() +
                '}' + "\n";
    }
}