package teme.w04.ex2.discount;

import java.util.Objects;

/*
    Use inheritance to somehow define/support these two different discount types.
 */
public class FixedDiscount implements Discount {
    private final double amount;  // a fixed amount (e.g 35 RON)

    public FixedDiscount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "FixedDiscount{" +
                "amount = " + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FixedDiscount)) return false;
        FixedDiscount that = (FixedDiscount) o;
        return Double.compare(that.amount, amount) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(amount);
    }

    /* method shall return the price (total price) after the discount is applied
    method parameter totalPrice is the total price before the discount is applied
    */
    @Override
    public double priceWithDiscount(double totalPriceBeforeDiscount) {
        double totalPriceWithDiscount = 0;
        if (amount != 0) {
            //if(new FixedDiscount(amount).equals(amount)){
            totalPriceWithDiscount = amount;
        }
        return totalPriceWithDiscount;
    }
}


