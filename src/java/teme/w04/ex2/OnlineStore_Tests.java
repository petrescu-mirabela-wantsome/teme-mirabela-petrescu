package teme.w04.ex2;


import org.junit.Test;
import teme.w04.ex2.customer.Address;
import teme.w04.ex2.customer.Customer;
import teme.w04.ex2.discount.Discount;
import teme.w04.ex2.discount.FixedDiscount;
import teme.w04.ex2.discount.PercentageDiscount;
import teme.w04.ex2.product.Product;

import static org.junit.Assert.assertEquals;


/**
 * Unit tests for classes of Online Store app (should compile and pass all tests after you complete those first)
 */

public class OnlineStore_Tests {

    private static final double PRECISION = 0.001; //precision used in assertEquals() for doubles

    @Test
    public void testCart() {
        Customer cust = new Customer("Ionel", "Popescu", "1234", new Address("Popauti", 1, "Iasi"));

        Cart cart = new Cart(cust);

        //1) Add products to cart (no discount yet):
        cart.addProduct(new Product(1, "p1", "phone", 1900, "blue"));
        cart.addProduct(new Product(2, "p2", "watch", 490, "black"));
        Product p3 = new Product(3, "p3", "hat", 990, "white");
        cart.addProduct(p3);

        //!should print: ... TOTAL: 3380.0
        System.out.println("Before discounts: \n " + cart.generateInvoice());
        assertEquals(3380.0, cart.computeTotalPrice(), PRECISION);

        //!should print: ... TOTAL: 3380.0
        System.out.println("Before discounts: \n " + cart.generateInvoice());
        assertEquals(3380.0, cart.computeTotalPrice(), PRECISION);

        //2) Also add some discounts:
        cart.addDiscount(new FixedDiscount(710));
        Discount d2 = new PercentageDiscount(10);
        cart.addDiscount(d2);

        //!should print: ... TOTAL: 2403.0
        System.out.println(" \n After discounts: \n " + cart.generateInvoice());
        assertEquals(2403.0, cart.computeTotalPrice(), PRECISION);


        //3) Replace a product and a discount:
        cart.removeProduct(p3);
        cart.addProduct(new Product(4, "p4", "gloves", 290, "brown"));
        cart.removeDiscount(d2);
        cart.addDiscount(new PercentageDiscount(10));

        //!should print: ... TOTAL: 1773.0
        System.out.println(" \n After replacing a product and a discount: \n " + cart.generateInvoice());
        assertEquals(1773.0, cart.computeTotalPrice(), PRECISION);

        Discount d3 = new FixedDiscount(5);
        cart.addDiscount(d3);

        Discount d4 = new PercentageDiscount(5);
        cart.removeDiscount(d4);

        System.out.println(" \n After adding a fixed discount and remove a percentage discount: \n " + cart.generateInvoice());
        assertEquals(1768.0, cart.computeTotalPrice(), PRECISION);
    }
}
