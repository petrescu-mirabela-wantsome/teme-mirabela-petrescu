package teme.w04.ex5;


/*
     Based on the solved problem from previous week for MyLinkedList, implement these classes
     based/derived from that class (somehow - inherit from it if possible, or make that one more
     general, or just copy part of the code, etc..):
     Doubly linked list :
         ● each element has two links - one to the next and one to the previous node
     Stack - a linked list with the following restrictions:
         ● adding new nodes only at the head of the list
         ● remove nodes only from the head of the list
         ● can access only the element from the head of the list
     Queue - a linked list with the following restrictions:
         ● adding new nodes only at the tail (end) of the list
         ● remove nodes only from the tail of the list
         ● can access only the element from the tail of the list
     Implement these specializations of a linked list in such a way that you take advantage of OOP
     features like inheritance and polymorphism.
 */

public class DoublyLinkedList implements Head, Tail {

    private Node head;           // reference to the first node
    private Node tail;           // reference to the last node

    public DoublyLinkedList() {
        head = null;
        tail = null;
    }

    @Override
    public String toString() {
        Node node = head;
        StringBuilder str = new StringBuilder();
        while (node != null) {
            str.append(" ").append(node.getDataNode());
            node = node.getNextLinkNode();
        }
        return str.toString().trim();
    }

    /* iterate over the elements */
    @Override
    public void iterateList() {
        Node currentNode = head;
        // there is no Node (empty list)
        // iterate until the last node --> currentNode != null is false
        while (currentNode != null) {
            currentNode = currentNode.getNextLinkNode();    /* iterate over the elements */
        }
    }

    /*
    ---------------------------------------------------------------------------------------------------
    ----------------------------------------------- ADD CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
    */

    /* add a new element in the last position of the doubly linked list */
    @Override
    public void addNodeLastPosition(int data) {
        Node newNode = new Node(tail, data, null);
        // there is no Node (empty list)
        if (head == null) {
            head = tail = newNode;
        } else {
            /*
             A Linked List is typically represented by the head and tail of it
              */
            tail.setNextLinkNode(newNode);  // last node in the list will have the link to the new introduced Node
            tail = newNode;     // the new tail of the list is now the newNode,(where its next Link is null from the instantiation)
        }
    }

    /* add a new element after a specified Node of the list */
    public void addNodeAfterNodeData(Node afterNodeData, int data) {
        Node newNode = new Node(data);
        Node currentNode = head;
        // there is no Node (empty list)
        // in case the data is not found at all in the list, currentNode != null is false and the last condition is not evaluated any more
        while (currentNode != null && currentNode.getDataNode() != afterNodeData.getDataNode()) {
            currentNode = currentNode.getNextLinkNode();    /* iterate over the elements */
        }
        if (currentNode != null) {
            if (currentNode.getPrevLinkNode() == null)     // currentNode is the head of the list (the data is found in the head of the list)
            {
                // new node will be inserted after current node (same as afterNodeData Node)
                newNode.setNextLinkNode(currentNode.getNextLinkNode()); // newNode will have as next link the value of tje currentNode next link
                currentNode.setNextLinkNode(newNode);   // currentNode next link will be now the newNode set above
                newNode.setPrevLinkNode(currentNode);   // newNode will have the prev link the currentNode set above
                head = currentNode;     // now the head will be the currentNode, with the newNode inserted
            } else if (currentNode.getNextLinkNode() == null)  // currentNode is the tail of the list (the data is found in the tail of the list)
            {
                currentNode.setNextLinkNode(newNode);   // currentNode next link will ne the newNode
                newNode.setPrevLinkNode(currentNode);   // newNode prev link will be the currentNode set above
                tail = currentNode;     // now the tail of the list is the new currentNode set above
            } else {
                Node temp1 = currentNode.getNextLinkNode();
                Node temp2 = temp1.getPrevLinkNode();
                newNode.setNextLinkNode(temp1);
                temp2.setPrevLinkNode(newNode);
                newNode.setPrevLinkNode(currentNode);
                currentNode.setNextLinkNode(newNode);
            }
        }
    }

    /* add a new element in the first position of the list */
    @Override
    public void addNodeFirstPosition(int data) {
        Node newNode = new Node(null, data, head);
        // there is no Node (empty list)
        if (head == null) {
            head = tail = newNode;  // there is only one node which is the head and tail in the same time
        } else {
            /*
             A Linked List is typically represented by the head and tail of it
              */
            head.setPrevLinkNode(newNode);  // current head link shall be the newNode
            head = newNode;     // now the new head is the newNode (where its prev Link is null from the instantiation)
        }
    }


    /*
    ---------------------------------------------------------------------------------------------------
    -------------------------------------------- REMOVE CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
     */

    /* remove an existing element */
    @Override
    public void removeNodeFirstPosition() {
        if (head != null) { // the Linked List shall not be empty
            if (head == tail)   // the list has only one element
            {
                head = tail = null;     // empty list  will result
            } else {
                head = head.getNextLinkNode();  // head will have the content of the head next link
                head.setPrevLinkNode(null);     // pre link of the new head will be now null, since will become the new head
            }
        }
    }

    /* remove an existing element using its data information */
    public void removeNodeData(int data) {
        Node currentNode = head;
        // there is no Node (empty list)
        // in case the data is not found at all in the list, currentNode != null is false and the last condition is not evaluated any more
        while (currentNode != null && currentNode.getDataNode() != data) {
            currentNode = currentNode.getNextLinkNode();    /* iterate over the elements */
        }
        if (currentNode != null) {
            if (currentNode.getPrevLinkNode() == null)     // currentNode is the head of the list (the data is found in the head of the list)
            {
                head = currentNode.getNextLinkNode();
                if (head != null) {
                    head.setPrevLinkNode(null);     // new head link will be null
                }
            } else if (currentNode.getNextLinkNode() == null)  // currentNode is the tail of the list (the data is found in the tail of the list)
            {
                tail = currentNode.getPrevLinkNode();
                if (head != null)  // the list is not empty
                {
                    tail.setNextLinkNode(null);     // new tail link will be null
                }
            } else {
                Node temp1 = currentNode.getPrevLinkNode();
                Node temp2 = currentNode.getNextLinkNode();
                temp1.setNextLinkNode(temp2);
                temp2.setPrevLinkNode(temp1);
            }
        }
    }

    /* remove the last element */
    @Override
    public void removeNodeLastPosition() {
        if (head != null) {     // the Linked List shall not be empty
            if (head == tail) {
                head = tail = null;     // empty list  will result
            } else {
                tail = tail.getPrevLinkNode();    // tail will have the content of the tail prev link
                tail.setNextLinkNode(null);     // next link of the new tail will be now null, since will become the new tail
            }
        }
    }

    /* update the data stored in an element */
    public void updateNodeData(int nodeData, int data) {
        Node currentNode = head;
        // there is no Node (empty list)
        // in case the data to be replaced is not found at all in the list, currentNode != null is false and the last condition is not evaluated any more
        while (currentNode != null && currentNode.getDataNode() != nodeData) {
            currentNode = currentNode.getNextLinkNode();    /* iterate over the elements */
        }
        if (currentNode != null) {
            currentNode.setDataNode(data);
        }
    }
}

