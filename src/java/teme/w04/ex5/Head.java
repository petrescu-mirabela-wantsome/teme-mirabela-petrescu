package teme.w04.ex5;

interface Head {
    String toString();

    void iterateList();

    void addNodeFirstPosition(int data);

    void removeNodeFirstPosition();
}
