package teme.w04.ex5;

/*
     Based on the solved problem from previous week for MyLinkedList, implement these classes
     based/derived from that class (somehow - inherit from it if possible, or make that one more
     general, or just copy part of the code, etc..):
     Doubly linked list :
         ● each element has two links - one to the next and one to the previous node
     Stack - a linked list with the following restrictions:
         ● adding new nodes only at the head of the list
         ● remove nodes only from the head of the list
         ● can access only the element from the head of the list
     Queue - a linked list with the following restrictions:
         ● adding new nodes only at the tail (end) of the list
         ● remove nodes only from the tail of the list
         ● can access only the element from the tail of the list
     Implement these specializations of a linked list in such a way that you take advantage of OOP
     features like inheritance and polymorphism.
 */

public class StackLinkedList implements Head {

    private Node head;           // reference to the first node

    public StackLinkedList() {
        head = null;
    }

    @Override
    public String toString() {
        Node node = head;
        StringBuilder str = new StringBuilder();
        while (node != null) {
            str.append(" ").append(node.getDataNode());
            node = node.getNextLinkNode();
        }
        return str.toString().trim();
    }

    /* iterate over the elements */
    @Override
    public void iterateList() {
        Node currentNode = head;
        // there is no Node (empty list)
        // iterate until the last node --> currentNode != null is false
        while (currentNode != null) {
            currentNode = currentNode.getNextLinkNode();    /* iterate over the elements */
        }
    }

    /*
    ---------------------------------------------------------------------------------------------------
    ----------------------------------------------- ADD CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
    */

    /* adding new nodes only at the head of the list */
    @Override
    public void addNodeFirstPosition(int data) {
        Node newNode = new Node(null, data, head);
        // there is no Node (empty list)
        if (head == null) {
            head = newNode;  // there is only one node which is the head and tail in the same time
        } else {
            /*
             A Stack Linked List is typically represented by the head of it
              */
            head.setPrevLinkNode(newNode);  // current head link shall be the newNode
            head = newNode;     // now the new head is the newNode (where its prev Link is null from the instantiation)
        }
    }


    /*
    ---------------------------------------------------------------------------------------------------
    -------------------------------------------- REMOVE CASE ------------------------------------------
    ---------------------------------------------------------------------------------------------------
     */

    /* remove nodes only from the head of the list */
    @Override
    public void removeNodeFirstPosition() {
        if (head != null) { // the Linked List shall not be empty
            if (head.getNextLinkNode() != null) {
                head = head.getNextLinkNode();  // head will have the content of the head next link
                head.setPrevLinkNode(null);     // pre link of the new head will be now null, since will become the new head
            } else {
                head = null;
            }
        }
    }

    /* can access only the element from the head of the list */
    public void updateNodeData(int data) {
        Node currentNode = head;
        // there is no Node (empty list)
        if (currentNode != null) {
            currentNode.setDataNode(data);
        }
    }
}

