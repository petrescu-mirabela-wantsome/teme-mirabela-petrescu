package teme.w04.ex3;

/*
Ex3. Complex numbers

Create a Complex class that represents a complex number (more info: https://en.wikipedia.org/wiki/Complex_number)
It should have the following methods:
    - Complex add(Complex other): adds this and other number and returns a new Complex instance; hint: (a+bi) + (c+di) = (a+c) + (b+d)i
    - Complex negate(): returns a new Complex number representing the negative value of this; hint: if z = (a+bi) then -z = (-a-bi)
    - Complex multiply(Complex other): returns a new complex number that is equal to this * other number; hint: (a+bi)(c+di) = (ac−bd) + (ad+bc)i
    - boolean equals(Object other): should return true only if ‘other’ is also an instance of Complex class and the 2 numbers are equal (both their parts..)
    - String toString(): should return strings such as “3 + 4i”

    - static Complex complex(double realPart, double imaginaryPart): should create a new Complex instance with given values;
      this will be basically just a shortcut for users of this class to create instances in a more concise manner
      (calling ‘complex(..)’ vs ‘new Complex(...)’)
 */

class Complex {
    private final double real;
    private final double imag;

    private Complex(double real, double imag) {
        this.real = real;
        this.imag = imag;
    }

    /* Should create a new Complex instance with given values;
      this will be basically just a shortcut for users of this class to create instances in a more concise manner
      (calling ‘complex(..)’ vs ‘new Complex(...)’)
     */
    static Complex complex(double realPart, double imaginaryPart) {
        return new Complex(realPart, imaginaryPart);
    }

    /**
     * Added just for running some manual tests
     */
    public static void main(String[] args) {

        Complex c1 = complex(1, 2);
        Complex c2 = complex(3, 4);

        //manual testing
        System.out.println("c1: " + c1 + ", c2: " + c2);
        System.out.println("!c1 = " + c1.negate());
        System.out.println("c1+c2 = " + c1.add(c2));
        System.out.println("c1*c2 = " + c1.multiply(c2));
    }

    /* Should return strings such as “3 + 4i”
     */
    @Override
    public String toString() {
        return real + "+" + imag + "i";
    }

    /* Returns a new complex number that is equal to this * number; hint: (a+bi)(c+di) = (ac−bd) + (ad+bc)i
     */

    /* Complex add(Complex other): adds this and other number and returns a new Complex instance; hint: (a+bi) + (c+di) = (a+c) + (b+d)i
     */
    Complex add(Complex number) {
        return new Complex(real + number.real, imag + number.imag);
    }

    /* returns a new Complex number representing the negative value of this; hint: if z = (a+bi) then -z = (-a-bi)
     */
    Complex negate() {
        return new Complex(-real, -imag);
    }

    Complex multiply(Complex number) {
        return new Complex(real * number.real - imag * number.imag, real * number.imag + imag * number.real);
    }

    /* Should return true only if ‘other’ is also an instance of Complex class and the 2 numbers are equal (both their parts..)
     */
    public boolean equals(Object other) {
        return other instanceof Complex && real == ((Complex) other).real && imag == ((Complex) other).imag;
    }
}
