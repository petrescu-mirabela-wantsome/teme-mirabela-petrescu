package teme.w04.ex4;

/*
Starting from the solved problems 6,7 from previous week (implementing MyArrayList and
MyLinkedList)
    - define a new interface named MyList which contains the common behavior between
    your 2 list implementations
    - Question: what methods should you add to it?
    - Change your 2 list classes so that they implement the new interface
    - How many lines of code did you need to change? (more than 1?)
    - Do all the tests for each list class still pass? (you did write your own unit tests for
    them, didn’t you? :)...)
    - Does any client code which wants to use one of your 2 list implementations need
    to care/know which one is working one? (beside the very first step when it calls
    the constructor of one or the other)
    - Based on this, can you (re)write your tests in such a way that you test
    both implementations by the same test code? (hint: in the test method you
    will need some different code to create 2 different instances, one of
    MyArrayList and one of MyLinkedList, but after this they can be tested by
    exactly same code - meaning some assert…() methods grouped in a
    single private method in test class, etc.. )
 */

interface MyList {

    String toString();

    /* Add new values to its end */
    void addValueLastPosition(int newValue);

    /* Delete last value from the end */
    void removeValueLastPosition();

    /* a method to insert a new element at a specific position (after the dataBefore, for Linked list it is the node which has dataBefore value) */
    void addValueAfter(int dataBefore, int index);

    void removeDataValue(int data);

    void updateNodeData(int currentData, int newData);
}
