package teme.w04.ex4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MyListTests {

    /* Test the MyList interface */
    @Test
    public void testMyListInterface01() {
        runTestTail01(new MyArrayList());
        runTestTail01(new MyLinkedList());
    }

    private void runTestTail01(MyList myList) {
        myList.addValueLastPosition(1);
        assertEquals("1", myList.toString());

        myList.addValueLastPosition(2);
        assertEquals("1 2", myList.toString());

        myList.removeValueLastPosition();
        assertEquals("1", myList.toString());

        myList.removeValueLastPosition();
        assertEquals("", myList.toString());

        myList.removeValueLastPosition();
        assertEquals("", myList.toString());

        // !!!!!!!!
        // Next lines commented due to: Error: java.lang.OutOfMemoryError: Java heap space

//        myList.addValueLastPosition(2);
//        assertEquals("2", myList.toString());
//
//        myList.addValueLastPosition(3);
//        assertEquals("2 3", myList.toString());
//
//        myList.addValueAfter(2, 10);
//        assertEquals("2 10 3", myList.toString());
//
//        myList.addValueAfter(2, 21);
//        assertEquals("2 21 10 3", myList.toString());
//
//        myList.removeDataValue(10);
//        assertEquals("2 21 3", myList.toString());
//
//        myList.addValueLastPosition(3);
//        assertEquals("2 21 3 3", myList.toString());
//
//        /* the same value is in the array/list twice --> only first value found will be deleted */
//        myList.removeDataValue(3);
//        assertEquals("2 21 3", myList.toString());
//
//        myList.updateNodeData(3, 100);
//        assertEquals("2 21 100", myList.toString());
//
//        myList.updateNodeData(2, 32);
//        assertEquals("32 21 100", myList.toString());
//
//        myList.updateNodeData(21, 0);
//        assertEquals("32 0 100", myList.toString());
    }

    /* Test the MyList interface */
    @Test
    public void testMyListInterface02() {
        runTestTail02(new MyArrayList());
        runTestTail02(new MyLinkedList());
    }

    private void runTestTail02(MyList myList) {
        myList.addValueLastPosition(2);
        assertEquals("2", myList.toString());

        myList.addValueAfter(2, 10);
        assertEquals("2 10", myList.toString());

        // !!!!!!!!
        // Next lines commented due to: Error: java.lang.OutOfMemoryError: Java heap space
//        myList.addValueAfter(2, 21);
//        assertEquals("2 21 10", myList.toString());
    }

    @Test
    public void testMyListInterface03() {
        runTestTail03(new MyArrayList());
        runTestTail03(new MyLinkedList());
    }

    private void runTestTail03(MyList myList) {
        myList.addValueLastPosition(10);
        assertEquals("10", myList.toString());

        myList.addValueLastPosition(3);
        assertEquals("10 3", myList.toString());

        myList.addValueLastPosition(3);
        assertEquals("10 3 3", myList.toString());

        /* the same value is in the array/list twice --> only first value found will be deleted */
        myList.removeDataValue(3);
        assertEquals("10 3", myList.toString());

    }

    @Test
    public void testMyListInterface04() {
        runTestTail04(new MyArrayList());
        runTestTail04(new MyLinkedList());
    }

    private void runTestTail04(MyList myList) {
        myList.addValueLastPosition(10);
        assertEquals("10", myList.toString());

        myList.addValueLastPosition(3);
        assertEquals("10 3", myList.toString());

        myList.addValueLastPosition(7);
        assertEquals("10 3 7", myList.toString());

        myList.updateNodeData(3, 100);
        assertEquals("10 100 7", myList.toString());

        myList.updateNodeData(7, 32);
        assertEquals("10 100 32", myList.toString());

        myList.updateNodeData(10, 0);
        assertEquals("0 100 32", myList.toString());
    }

    /* ---------------------------------------------------------------------------*/
    /* Test the MyArrayList specific functions, which are not in MyList interface */
    /* ---------------------------------------------------------------------------*/
    @Test
    public void testSize() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        assertEquals(6, ar1.size());

        MyArrayList ar2 = new MyArrayList(new int[]{});
        assertEquals(0, ar2.size());

        MyArrayList ar3 = new MyArrayList(new int[]{0});
        assertEquals(1, ar3.size());
    }

    @Test
    public void testGet() {
        MyArrayList ar1 = new MyArrayList(new int[]{1, 2, 3, 4, 5, 6});
        assertEquals(-1, ar1.get(-1));
        assertEquals(1, ar1.get(0));
        assertEquals(2, ar1.get(1));
        assertEquals(6, ar1.get(5));
        assertEquals(-1, ar1.get(6));

        MyArrayList ar2 = new MyArrayList(new int[]{});
        assertEquals(-1, ar2.get(0));
    }

    /* ---------------------------------------------------------------------------*/
    /* Test the MyLinkedList specific functions, which are not in MyList interface */
    /* ---------------------------------------------------------------------------*/
    @Test
    public void testAddNodeFirstPosition() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listFirstPos = new MyLinkedList();

        listFirstPos.addNodeFirstPosition(1);
        assertEquals("1", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(2);
        assertEquals("2 1", listFirstPos.toString());

        listFirstPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listFirstPos.toString());
    }

    @Test
    public void testRemoveNodeFirstPosition() {
        /* add a new element into the list - first position in the list */
        MyLinkedList listIndexPos = new MyLinkedList();

        listIndexPos.addNodeFirstPosition(1);
        listIndexPos.addNodeFirstPosition(2);
        listIndexPos.addNodeFirstPosition(3);
        assertEquals("3 2 1", listIndexPos.toString());

        listIndexPos.removeNodeFirstPosition();
        assertEquals("2 1", listIndexPos.toString());

        listIndexPos.removeNodeFirstPosition();
        assertEquals("1", listIndexPos.toString());

        listIndexPos.removeNodeFirstPosition();
        assertEquals("", listIndexPos.toString());

        /* remove an empty linked list */
        listIndexPos.removeNodeFirstPosition();
        assertEquals("", listIndexPos.toString());

    }
}
