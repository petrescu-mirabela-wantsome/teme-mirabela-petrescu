package teme.w04.ex7;

import java.util.Date;

public class DiscountSystem {

    public static void main(String[] args) {
        Customer customer1 = new Customer("Popa Florica");
        customer1.setMember(false);
        customer1.setMemberType("NoMembership");
        System.out.println(customer1.toString());
        System.out.println("Customer is member: " + customer1.isMember());

        Visit visit1 = new Visit(customer1, new Date());
        visit1.setProductExpense(500);
        visit1.setServiceExpense(1500);
        visit1.setProductExpense(320);
        visit1.setServiceExpense(15.7);
        System.out.println(visit1.toString());
        System.out.println("Total expense (considering discount rate): " + visit1.getTotalExpense() + " made by: " + visit1.getCustomerName());

        System.out.println();
        System.out.println("---------------------------------------");
        System.out.println();

        Customer customer2 = new Customer("Petrea George");
        customer2.setMember(true);
        customer2.setMemberType("NoMembership");
        System.out.println(customer2.toString());
        System.out.println("Customer is member: " + customer2.isMember());

        Visit visit2 = new Visit(customer2, new Date());
        visit2.setProductExpense(500);
        visit2.setServiceExpense(1500);
        visit2.setProductExpense(320);
        visit2.setServiceExpense(15.7);
        System.out.println(visit2.toString());
        System.out.println("Total expense (considering discount rate): " + visit2.getTotalExpense() + " made by: " + visit2.getCustomerName());
    }

}
