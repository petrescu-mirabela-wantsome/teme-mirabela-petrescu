package teme.w04.ex7;

import java.util.Date;

class Visit extends DiscountRate {

    private final Customer customer;
    private final Date data;
    private double serviceExpense;
    private double productExpense;

    public Visit(Customer customer, Date data) {
        this.customer = customer;
        this.data = data;
    }

    public String getCustomerName() {
        return customer.getName();
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense += serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense += productExpense;
    }

    public double getTotalExpense() {
        if (customer.isMember()) {
            return (serviceExpense - serviceExpense * getServiceDiscountRate(customer.getMemberType())) +
                    (productExpense - productExpense * getProductDiscountRate(customer.getMemberType()));
        } else {
            return 0;
        }
    }

    @Override
    public String toString() {
        return "Visit{" +
                "customer{" + customer.toString() +
                ", data=" + data +
                ", serviceExpense=" + serviceExpense +
                ", productExpense=" + productExpense +
                '}';
    }
}
