package teme.w14.ex1_logger_singleton;

/*
1. Logger (Singleton)
Create a Logger class:
- should have 3 methods (not static): info(String) , warn(String), error(String) -
each receives a string value and prints that value to the standard output, on one
line, and using a specific prefix ("[INFO ]"/"[WARN ]"/"[ERROR]")
- use the Singleton pattern to make sure that:
- one one Logger instance is ever created
- the instance is easily accessible to all the rest of code, in a thread-safe
manner
- write some code to test this logger class
 */

public class LoggerEagerInitialization {

    private static final LoggerEagerInitialization instance = new LoggerEagerInitialization();

    private LoggerEagerInitialization() {
    }

    private static LoggerEagerInitialization getInstance() {
        return instance;
    }

    public static void main(String[] args) {

        LoggerEagerInitialization logger1 = LoggerEagerInitialization.getInstance();
        LoggerEagerInitialization logger2 = LoggerEagerInitialization.getInstance();
        LoggerEagerInitialization logger3 = LoggerEagerInitialization.getInstance();

        System.out.println("logger1==logger2 ? " + (logger1 == logger2));
        System.out.println("logger2==logger3 ? " + (logger2 == logger3));

        logger1.info("Info text");
        logger2.warn("Warn text");
        logger3.error("Error text");
    }

    public void info(String txt) {
        System.out.println("[INFO]" + txt);
    }

    public void warn(String txt) {
        System.out.println("[WARN]" + txt);
    }

    public void error(String txt) {
        System.out.println("[ERROR]" + txt);
    }
}
