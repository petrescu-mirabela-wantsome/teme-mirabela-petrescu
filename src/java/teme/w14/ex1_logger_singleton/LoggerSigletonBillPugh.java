package teme.w14.ex1_logger_singleton;

public class LoggerSigletonBillPugh {

    private LoggerSigletonBillPugh() {
    }

    public static LoggerSigletonBillPugh getInstance() {
        return SingletonHelper.INSTANCE;
    }

    public static void main(String[] args) {


        LoggerSigletonBillPugh logger1 = LoggerSigletonBillPugh.getInstance();
        LoggerSigletonBillPugh logger2 = LoggerSigletonBillPugh.getInstance();
        LoggerSigletonBillPugh logger3 = LoggerSigletonBillPugh.getInstance();

        System.out.println("logger1==logger2 ? " + (logger1 == logger2));
        System.out.println("logger2==logger3 ? " + (logger2 == logger3));

        logger1.info("Info text");
        logger2.warn("Warn text");
        logger3.error("Error text");

        Thread t1 = new ThreadLoggerInfo(logger1);
        Thread t2 = new ThreadLoggerWarn(logger2);
        Thread t3 = new ThreadLoggerError(logger3);

        System.out.println("logger1==logger2 ? " + (logger1 == logger2));
        System.out.println("logger2==logger3 ? " + (logger2 == logger3));

        t1.start();
        t2.start();
        t3.start();
    }

    public void info(String txt) {
        System.out.println("[INFO]" + txt);
    }

    public void warn(String txt) {
        System.out.println("[WARN]" + txt);
    }

    public void error(String txt) {
        System.out.println("[ERROR]" + txt);
    }

    private static class SingletonHelper {
        private static final LoggerSigletonBillPugh INSTANCE = new LoggerSigletonBillPugh();
    }
}

class ThreadLoggerInfo extends Thread {

    LoggerSigletonBillPugh loggerSigletonBillPugh;

    ThreadLoggerInfo(LoggerSigletonBillPugh loggerSigletonBillPugh) {
        this.loggerSigletonBillPugh = loggerSigletonBillPugh;
    }

    @Override
    public void run() {
        loggerSigletonBillPugh.info("info text");
    }
}

class ThreadLoggerWarn extends Thread {

    LoggerSigletonBillPugh loggerSigletonBillPugh;

    ThreadLoggerWarn(LoggerSigletonBillPugh loggerSigletonBillPugh) {
        this.loggerSigletonBillPugh = loggerSigletonBillPugh;
    }

    @Override
    public void run() {
        loggerSigletonBillPugh.warn("warn text");
    }
}

class ThreadLoggerError extends Thread {

    LoggerSigletonBillPugh loggerSigletonBillPugh;

    ThreadLoggerError(LoggerSigletonBillPugh loggerSigletonBillPugh) {
        this.loggerSigletonBillPugh = loggerSigletonBillPugh;
    }

    @Override
    public void run() {
        loggerSigletonBillPugh.error("error text");
    }
}