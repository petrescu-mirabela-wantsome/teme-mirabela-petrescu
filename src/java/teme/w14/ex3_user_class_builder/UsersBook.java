package teme.w14.ex3_user_class_builder;

/*
3. User class (Builder)
Create a User class, for representing the users of some system.
It needs to have these fields:
- username: String, mandatory
- email: String, mandatory
- firstName, lastName: String, is mandatory to have one of them set (user can
have only one or both of them set, but cannot have both missing)
- age: int, optional
- gender: an enum, optional (but if specified, must be >=14)
- address: String, optional (if specified, must be at least 8 char long)
Provide an easy/clear way for some other client code to safely build valid User object,
for all combinations of possible parameters (making sure they always give the required
one, allowing to also give any of the optional ones, and doing all needed final
validations for received values)
- Hint: use the Builder pattern :)
Write now a UsersBook class, to store and show info on a list of User objects (think of
it as the next Facebook ;) )
It should have at least 2 methods:
- void add(User... users) - for adding new User instances to current list
- void showUsers() - it will print the list of current users, one per line (hint: add
toString() method to User and use that here..)
 */

import java.util.ArrayList;
import java.util.List;

public class UsersBook {

    private static List<User> userList = new ArrayList<>();

    public static void main(String[] args) {

        User user1 = new User.UserBuilder("bella", "mira@yahoo.com")
                .setFirstName("Petrescu")
                .setLastName("Mirabela")
                .setAddress("Arcu")
                .build();

        UsersBook.userList.add(user1);
        UsersBook.show();

        User user2 = new User.UserBuilder("ili", "ilinca@yahoo.com")
                .setFirstName("Popescu")
                .setLastName("Ilinca")
                .setAge(25)
                .build();

        UsersBook.userList.add(user2);
        UsersBook.show();

        User user3 = new User.UserBuilder("vasi", "vasile@yahoo.com")
                .setFirstName("Popa")
                .setLastName("Vasile")
                .setAge(50)
                .setGender(Gender.MALE)
                .build();

        UsersBook.userList.add(user3);
        UsersBook.show();

        User user4 = new User.UserBuilder("adina", "adina@yahoo.com")
                .setAge(37)
                .setGender(Gender.FEMALE)
                .build();

        UsersBook.userList.add(user4);
        UsersBook.show();
    }

    /*
    adding new User instances to current list
     */
    public static List<User> add(User user) {
        userList.add(user);
        return userList;
    }

    /*
    it will print the list of current users, one per line (hint: add toString() method to User and use that here..)
     */
    public static void show() {
        System.out.println(userList);
    }
}
