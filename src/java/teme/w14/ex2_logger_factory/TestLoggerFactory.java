package teme.w14.ex2_logger_factory;

/*
2. Logger (Factory)
Create a Logger interface, with 3 methods (info/warn/error, similar to class from 1, each
should print the given line with a proper prefix..)
Create 2 different implementations for it:
- ConsoleLogger: should write log lines to standard output
- FileLogger: should write log lines to a single text file (file path can be hard-coded
in a constant, and it should create the file if missing, or else just append to it)
Create also a LoggerFactory class, which will be responsible for creating a logger
instance, of one of the available types:
- should have a getLogger(LoggerType) method, which will return a logger
implementation depending on requested type (LoggerType is an enum, currently
with 2 values, for the 2 logger types)
Then write some code to test your classes:
- it should write some lines (of different log levels: info/error/warn) using either the
console or the file logger
Note: your factory may build and return a new instance of Logger each time, and that is
ok, but it may also choose to just build one instance of logger of each type, and return
one of those (would be more optimal, but pay attention to make your code thread-safe)
Question1: should LoggerFactory.getLogger() be static or not? (do you need to build
any instances of LoggerFactory?.. and if you don't, how you can prevent client code to
attempt that?..)
Question2: if you need to add a new type of logger in the future (like a database logger),
what parts of your current code would need to be changed?
 */

public class TestLoggerFactory {

    public static void main(String[] args) {

        Logger logger1 = LoggerFactory.getLogger(LoggerType.CONSOLEFILE);
        logger1.printInfo();
        logger1.printWarn();
        logger1.printError();

        Logger logger2 = LoggerFactory.getLogger(LoggerType.FILELOGGER);
        logger2.printInfo();
        logger2.printWarn();
        logger2.printError();
    }
}
