package teme.w14.ex2_logger_factory;

/*
Create a Logger interface, with 3 methods (info/warn/error, similar to class from 1, each
should print the given line with a proper prefix..)
Create 2 different implementations for it:
- ConsoleLogger: should write log lines to standard output
- FileLogger: should write log lines to a single text file (file path can be hard-coded
in a constant, and it should create the file if missing, or else just append to it)
 */

public interface Logger {

    void printInfo();

    void printWarn();

    void printError();
}
