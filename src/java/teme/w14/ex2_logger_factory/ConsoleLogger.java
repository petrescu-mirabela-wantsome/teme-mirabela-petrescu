package teme.w14.ex2_logger_factory;

/*
ConsoleLogger: should write log lines to standard output
 */


public class ConsoleLogger implements Logger {
    @Override
    public void printInfo() {
        System.out.println("[INFO]");
    }

    @Override
    public void printWarn() {
        System.out.println("[WARN]");
    }

    @Override
    public void printError() {
        System.out.println("[Error]");
    }
}
