package teme.exercitii_extra.ex2_paranteze;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Paranteze_Tests {

    @Test
    public void testParanteze() {
        Paranteze paranteze = new Paranteze();
        assertTrue(paranteze.parenthesis(""));
        assertTrue(paranteze.parenthesis("(2*(3+5)-a)"));
        assertFalse(paranteze.parenthesis("((2*a+b)"));
        assertFalse(paranteze.parenthesis(")(ceva"));
        assertTrue(paranteze.parenthesis("altceva bla"));
    }
}