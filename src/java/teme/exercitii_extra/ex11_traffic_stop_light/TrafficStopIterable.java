package teme.exercitii_extra.ex11_traffic_stop_light;

/*
11) Traffic stop light
a. Creati o clasa TrafficStopIterable care implementeaz Iterable si care la
chemarea metodei next() va avansa starea semaforului la urmatoarea
culoare si o va returna (ca un String/enum..). Culoarea initiala a
semaforului va fi verde. Scrieti apoi o bucla de test care sa tipareasca
culoarea semaforului pentru 6 pasi.
b. Optional : incercati sa faceti codul existent mai generic:
i. creati o clasa CyclicIterable, care implementeaza Iterable<E>
(unde E e un tip generic)
ii. Ar trebui sa primeasca in constructor un array de elemente de tip E
iii. Ar trebui sa furnizeze un iterator care stie sa parcurga intr-o bucla
toate elementele array-ului initial, si cand ajunge la sfarsit sa il reia
de la inceput (la inifinit)
Rezolvati folosind noua clasa:
- Refaceti exemplu cu traffic stop (scrieti o bucla care tipareste 6
schimbari de culoare a unui semafor)
- Aveti o lista de nume a N copii asezati intr-un cerc; ei incep sa isi
spuna numele, si cand ajung la ultimul continua iar de la primul.
Afisati primele M nume spuse (prefixate si cu un counter)
Exemplu: pentru N=4, M=7
1 : Ana
2 : Bogdan
3 : Codrin
4 : Dana
5 : Ana
6 : Bogdan
7 : Codrin
 */

import java.util.Iterator;

enum CuloareSemafor {
    VERDE,
    PORTOCALIU,
    ROSU
}

public class TrafficStopIterable implements Iterable<CuloareSemafor> {

    private final int maxValue;

    public TrafficStopIterable(Integer maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public Iterator<CuloareSemafor> iterator() {
        return new TrafficStopIterator(maxValue);
    }
}

class TrafficStopIterator implements Iterator<CuloareSemafor> {

    private final int maxValue;
    private int count = 0;

    public TrafficStopIterator(int maxValue) {
        this.maxValue = maxValue;
    }

    public static void main(String[] args) {

        TrafficStopIterable iterable = new TrafficStopIterable(6);

        Iterator<CuloareSemafor> it = iterable.iterator();
        while (it.hasNext()) {
            CuloareSemafor value = it.next();
        }
    }

    @Override
    public boolean hasNext() {
        return count < maxValue;
    }

    @Override
    public CuloareSemafor next() {
        CuloareSemafor colorSelected = CuloareSemafor.VERDE;
        while (hasNext()) {
            for (CuloareSemafor oneColor : CuloareSemafor.values()) {
                colorSelected = oneColor;
                System.out.println(colorSelected);
                count++;
            }
        }
        return colorSelected;
    }

}