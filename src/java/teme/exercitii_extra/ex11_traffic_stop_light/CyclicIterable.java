package teme.exercitii_extra.ex11_traffic_stop_light;

/*
b. Optional : incercati sa faceti codul existent mai generic:
i. creati o clasa CyclicIterable, care implementeaza Iterable<E>
(unde E e un tip generic)
ii. Ar trebui sa primeasca in constructor un array de elemente de tip E
iii. Ar trebui sa furnizeze un iterator care stie sa parcurga intr-o bucla
toate elementele array-ului initial, si cand ajunge la sfarsit sa il reia
de la inceput (la inifinit)
Rezolvati folosind noua clasa:
- Refaceti exemplu cu traffic stop (scrieti o bucla care tipareste 6
schimbari de culoare a unui semafor)
- Aveti o lista de nume a N copii asezati intr-un cerc; ei incep sa isi
spuna numele, si cand ajung la ultimul continua iar de la primul.
Afisati primele M nume spuse (prefixate si cu un counter)
Exemplu: pentru N=4, M=7
1 : Ana
2 : Bogdan
3 : Codrin
4 : Dana
5 : Ana
6 : Bogdan
7 : Codrin
*/

import java.util.Iterator;

public class CyclicIterable<E> implements Iterable<Integer> {

    private final E arr[];
    private final int maxValue;

    public CyclicIterable(E[] arr, int maxValue) {
        this.arr = arr;
        this.maxValue = maxValue;
    }

    @Override
    public Iterator iterator() {
        return new CyclicIterator(arr, maxValue);
    }
}

class CyclicIterator<E> implements Iterator<E> {

    private final E arr[];
    private final int maxValue;
    private int count = 0;

    public CyclicIterator(E[] arr, int maxValue) {
        this.arr = arr;
        this.maxValue = maxValue;
    }

    public static void main(String[] args) {

        CyclicIterable iterableTrafficStopLight = new CyclicIterable(new String[]{"VERDE", "PORTOCALIU", "ROSU"}, 6);

        Iterator<CuloareSemafor> iterTrafficStopLight = iterableTrafficStopLight.iterator();
        while (iterTrafficStopLight.hasNext()) {
            iterTrafficStopLight.next();
        }

        System.out.println();

        CyclicIterable iterablePersonInCircle = new CyclicIterable(new String[]{"Ana", "Bogdan", "Codrin", "Dana"}, 7);

        Iterator<CuloareSemafor> iterPersonInCircle = iterablePersonInCircle.iterator();
        while (iterPersonInCircle.hasNext()) {
            iterPersonInCircle.next();
        }
    }

    @Override
    public boolean hasNext() {
        return count < maxValue;
    }

    @Override
    public E next() {
        E itemSelected = arr[0];
        while (hasNext()) {
            for (int i = 0; i < arr.length && hasNext(); i++) {
                itemSelected = arr[i];
                count++;
                System.out.println(count + " : " + itemSelected);
            }
        }
        return itemSelected;
    }

}

