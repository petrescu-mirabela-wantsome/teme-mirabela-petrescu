package teme.exercitii_extra.ex7_dobanda_cumulativa;

import java.util.HashMap;
import java.util.Map;

/*
7) Dobanda cumulativa
Maria are o suma initiala de N ron. Daca o depune la banca intr-un depozit cu
dobanda de D % , in cati ani suma totala acumulata va depasi o valoare target
minima de T ron?
Scrieti o metoda care primeste (N,D,T) ca parametrii si calculeaza/afiseaza anul
si suma totala dupa fiecare an, pana in anul cand se depaseste valoare T.
Testati metoda (chemand-o din metoda main() a clasei) cu cateva combinatii de
valori.

Exemplu:
pentru (100, 3, 130) ar trebui sa afiseze ceva gen:
Initial sum: 100.0
year 1: sum = 103.0
year 2: sum = 106.09
year 3: sum = 109.2727
year 4: sum = 112.550881
year 5: sum = 115.92740743
year 6: sum = 119.4052296529
year 7: sum = 122.987386542487
year 8: sum = 126.67700813876162
year 9: sum = 130.47731838292447
Target of 130.0 was reached!
 */
public class DobandaCumulativa {

    private int year = 1;

    public static void main(String[] args) {

        DobandaCumulativa dobandaCumulativaI = new DobandaCumulativa();
        System.out.println("Initial sum = 100, interest rate = 3, target sum = 130");
        dobandaCumulativaI.calcDobandaCumulativa(100, 3, 130);

        System.out.println();
        System.out.println("Initial sum = 100, interest rate = 9, target sum = 150");
        dobandaCumulativaI.calcDobandaCumulativa(100, 9, 150);

        System.out.println();
        System.out.println("Initial sum = 60, interest rate = 5, target sum = 215");
        dobandaCumulativaI.calcDobandaCumulativa(60, 5, 215);
    }

    // return the deposit sum for a specified year
    private double calcSumOneYear(double initialSum, double interestRate) {
        return interestRate / 100 * initialSum + initialSum;
    }

    private void calcDobandaCumulativa(double initialSum, double interestRate, double targetSum) {

        Map<Integer, Double> finalDeposit = new HashMap<>();
        double finalSum = calcSumOneYear(initialSum, interestRate);
        while (finalSum <= targetSum) {
            finalSum = calcSumOneYear(finalSum, interestRate);
            if (finalSum <= targetSum) {     // needed to remove the last finalSum calculation
                finalDeposit.put(year, finalSum);
                System.out.println("year " + year + ": sum = " + finalSum);
                year++;
            }
        }
        System.out.println("Target of " + targetSum + " was reached!");
    }
}
