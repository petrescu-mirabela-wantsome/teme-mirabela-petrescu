package teme.exercitii_extra.ex3_josephus;

/*
A number of N soldiers (named as 1..N) are standing in a circle, waiting to be
executed. Counting starts in one direction (ascending by their numbers) and
every M -th is killed. The last remaining one is spared. Which soldier remains the
last one? (print the numbers of killed soldiers, in their elimination order, and of
the remaining one)
(more details: https://en.wikipedia.org/wiki/Josephus_problem )
Example:
- N=1, M=1 => remaining: 1
- N=3, M=1 => eliminated: 1, 2; remaining: 3
- N=7, M=2 => eliminated: 2,4,6,1,5,3; remaining: 7
- N=10, M=3 => eliminated: 3,6,9,2,7,1,8,5,10; remaining: 4
Hints:
- Would be nice/easier to check it if you print at each step the current
remaining list of soldiers, and number of the soldier chosen to be killed
next..
- May be solved using either:
- a regular List (or array) - but you need to make sure you count
correctly when reaching the end of list
- and/or a custom
 */

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Josephus {

    private List<Integer> listKilled = new ArrayList<>();

    public static void main(String[] args) {

        Josephus josephus = new Josephus();

        System.out.println(josephus.generateList(1).toString() + ", M = " + 1);
        // shall remain 1
        System.out.println("The last soldier(spared) is: " + josephus.generateJosephusNumber(josephus.generateList(1), 1));

        System.out.println();
        System.out.println(josephus.generateList(3).toString() + ", M = " + 1);
        // shall remain 3
        System.out.println("The last soldier(spared) is: " + josephus.generateJosephusNumber(josephus.generateList(3), 1));
        System.out.println("List of killed soldiers: " + josephus.listKilled.toString());

        System.out.println();
        System.out.println(josephus.generateList(7).toString() + ", M = " + 2);
        // shall remain 7
        System.out.println("The last soldier(spared) is: " + josephus.generateJosephusNumber(josephus.generateList(7), 2));
        System.out.println("List of killed soldiers: " + josephus.listKilled.toString());

        System.out.println();
        System.out.println(josephus.generateList(10).toString() + ", M = " + 3);
        // shall remain 4
        System.out.println("The last soldier(spared) is: " + josephus.generateJosephusNumber(josephus.generateList(10), 3));
        System.out.println("List of killed soldiers: " + josephus.listKilled.toString());

        System.out.println();
        System.out.println(josephus.generateList(10).toString() + ", M = " + 4);
        // shall remain 5
        System.out.println("The last soldier(spared) is: " + josephus.generateJosephusNumber(josephus.generateList(10), 4));
        System.out.println("List of killed soldiers: " + josephus.listKilled.toString());
    }

    public List<Integer> getListKilled() {
        return listKilled;
    }

    // n - number of soldiers
    // m - the m'th will be killed
    public List<Integer> generateList(int n) {
        List<Integer> listN = new ArrayList<>();
        for (int j = 1; j <= n; j++) {
            listN.add(j);
        }
        return listN;
    }

    private List<Integer> generateListJosephus(List<Integer> listN, int m) {
        List<Integer> listNStart = new ArrayList<>();
        int count = 0;  // start the index from 0
        Iterator<Integer> itr = listN.iterator();
        while (itr.hasNext()) {
            Integer value = itr.next();
            // count is used to get the index of the M -th soldier killed from the list with remaining soldiers
            if (count < m - 1) {
                listNStart.add(value); // index start from 0, add the soldiers before the killed soldier
                itr.remove();
            }
            if (count == m - 1) {
                System.out.println("Killed soldiers: " + value);
                listKilled.add(value);
                itr.remove();
            }
            count++;
        }
        listN.addAll(listNStart);   // add at the end the first soldiers before the killed soldier
        System.out.println("Remaining list of soldiers" + listN.toString());
        return listN;
    }

    public int generateJosephusNumber(List<Integer> listN, int m) {
        listKilled = new ArrayList<>();     // empty the list with killed soldiers
        int value = 1;
        if (listN.size() == 1 && m == 1) {
            return 1;
        }

        List<Integer> listJ = generateListJosephus(listN, m);
        while (listJ.size() > 1) {
            if (listJ.size() >= m) {
                generateListJosephus(listJ, m);
            }
            if (listJ.size() < m && listJ.size() > 1) {
                // example: remaining list of soldiers = [10, 4] and shall pe killed the 3-th soldier from the list
                generateListJosephus(listJ, m - listJ.size());
            }
        }
        if (listJ.size() == 1) {
            value = listJ.get(0);
        }
        return value;
    }
}

