package teme.exercitii_extra.ex3_josephus;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class Josephus_Tests {

    //helper method for easy building a new Array with some given elements (of a custom type)
    @SafeVarargs
    private static <E> ArrayList<E> newList(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    @Test
    public void testJosephus() {

        Josephus josephus = new Josephus();

        List<Integer> list1;
        list1 = new ArrayList<>(josephus.generateList(1));

        assertEquals(1, josephus.generateJosephusNumber(list1, 1));
        assertEquals(newList(), josephus.getListKilled());

        list1 = new ArrayList<>(josephus.generateList(3));
        assertEquals(3, josephus.generateJosephusNumber(list1, 1));
        assertEquals(newList(1, 2), josephus.getListKilled());

        list1 = new ArrayList<>(josephus.generateList(7));
        assertEquals(7, josephus.generateJosephusNumber(list1, 2));
        assertEquals(newList(2, 4, 6, 1, 5, 3), josephus.getListKilled());

        list1 = new ArrayList<>(josephus.generateList(10));
        assertEquals(4, josephus.generateJosephusNumber(list1, 3));
        assertEquals(newList(3, 6, 9, 2, 7, 1, 8, 5, 10), josephus.getListKilled());

        list1 = new ArrayList<>(josephus.generateList(10));
        assertEquals(5, josephus.generateJosephusNumber(list1, 4));
        assertEquals(newList(4, 8, 2, 7, 3, 10, 9, 1, 6), josephus.getListKilled());
    }
}