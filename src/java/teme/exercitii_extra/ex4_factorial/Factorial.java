package teme.exercitii_extra.ex4_factorial;


import java.math.BigInteger;

public class Factorial {

    public static void main(String[] args) {

        Factorial factorialI = new Factorial();

        System.out.println(factorialI.factorial(1));
        System.out.println(factorialI.factorial(12));
        System.out.println(factorialI.factorial(16));
        System.out.println(factorialI.factorialBigInteger(50));
        System.out.println(factorialI.factorialBigInteger(100));

        System.out.println("\n >>>>>>> findFirstWithMoreDigitsThan test >>>>>>");
        factorialI.findFirstWithMoreDigitsThan(4);
        factorialI.findFirstWithMoreDigitsThanBigInteger(50);
        factorialI.findFirstWithMoreDigitsThanBigInteger(100);
        factorialI.findFirstWithMoreDigitsThanBigInteger(1000);
    }

    /*
    4) Factorial
    a. Write an implementation of factorial function, which is defined in math like this: f(1)=1, f(n) = n * f(n-1)
    The solution should preferably be recursive (but iterative is also ok). Make sure it runs ok for n=12 and n=16.

    Example:
    12! = 479001600
    16! = 20922789888000
    */
    public long factorial(int n) {
        long result = 1;
        if (n > 1) {
            result = n * factorial(n - 1);
        }
        return result;
    }

    /*
    c. Write another method ‘ void findFirstWithMoreDigitsThan ( int minDigits)’
    which receives a number of digits and finds the first (smallest) value of N
    for which factorial(N) value has at least minDigits digits, and then prints
    out the number N, the number of digits for factorial(N), and the full value of that factorial.
    Run it then for minDigits = 1000.

    Example:
    findFirstWithMoreDigitsThan ( 4 );
    findFirstWithMoreDigitsThan ( 50 );
    findFirstWithMoreDigitsThan ( 100 );
    should output something like:
    First number for which factorial has at least 4 digits is: 7, for which
    factorial(i) has: 4 digits, value: 5040
    First number for which factorial has at least 50 digits is: 41, for
    which factorial(i) has: 50 digits, value: 33452526613163807108170062053440751665152000000000
    First number for which factorial has at least 100 digits is: 70, for
    which factorial(i) has: 101 digits, value: 11978571669969891796072783721689098736458938142546425
    857555362864628009582789845319680000000000000000
     */

//    public void findFirstWithMoreDigitsThan(int minDigits) {
//        int n = 1;
//        while (factorial(n) <= Long.MAX_VALUE) {
//            int countDigits = 0;
//            String text = String.valueOf(factorial(n));
//            System.out.println("factorial of n = " + n + " is => " + text);
//            String[] stringText = text.replaceAll(" ", "").split("");
//            for (String item : stringText) {
//                countDigits++;
//                if (countDigits >= minDigits) {
//                    System.out.println("value of N for which factorial(" + n + ") value has at least " + minDigits + " digits is: " + countDigits);
//                    System.out.println("factorial(" + n + ") =  " + factorial(n));
//                    return;
//                }
//            }
//            n++;
//        }
//    }

    /*
    b. Change your implementation to support bigger numbers, by using values of type BigInteger (instead of int/long)
    (more info: https://www.geeksforgeeks.org/biginteger-class-in-java )
    - test it with some bigger numbers now, like n=50, 100.., and print both the value itself, and the total number of digits it contains.
     */
    public BigInteger factorialBigInteger(int n) {
        BigInteger result = new BigInteger("1");
        if (n == 1) {
            result = BigInteger.valueOf(1);     // result keeps the initial value
        }
        if (n > 1) {
            result = BigInteger.valueOf(n).multiply(factorialBigInteger(n - 1));
        }
        return result;
    }

    private void findFirstWithMoreDigitsThan(int minDigits) {
        int n = 1;
        while (factorial(n) <= Long.MAX_VALUE) {
            String text = String.valueOf(factorial(n));
            if (text.length() >= minDigits) {
                System.out.println("value of N for which factorial(" + n + ") value has at least " + minDigits + " digits is: " + text.length());
                System.out.println("factorial(" + n + ") =  " + factorial(n));
                return;
            }
            n++;
        }
    }

    private void findFirstWithMoreDigitsThanBigInteger(int minDigits) {
        int n = 1;
        while (factorialBigInteger(n).compareTo(BigInteger.ZERO) > 0) {
            String text = String.valueOf(factorialBigInteger(n));
            if (text.length() >= minDigits) {
                System.out.println("value of N for which factorial(" + n + ") value has at least " + minDigits + " digits is: " + text.length());
                System.out.println("factorial(" + n + ") =  " + factorialBigInteger(n));
                return;
            }
            n++;
        }
    }
}
