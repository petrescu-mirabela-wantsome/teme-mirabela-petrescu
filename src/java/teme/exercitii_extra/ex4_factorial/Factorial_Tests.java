package teme.exercitii_extra.ex4_factorial;

import org.junit.Test;

import java.math.BigInteger;

import static org.junit.Assert.assertEquals;

public class Factorial_Tests {

    @Test
    public void testFactorial() {

        Factorial factorialI = new Factorial();

        assertEquals(1, factorialI.factorial(1));
        assertEquals(479001600, factorialI.factorial(12));
        assertEquals(new BigInteger("20922789888000"), factorialI.factorialBigInteger(16));
        assertEquals(new BigInteger("30414093201713378043612608166064768844377641568960512000000000000"), factorialI.factorialBigInteger(50));
        assertEquals(new BigInteger("93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000"), factorialI.factorialBigInteger(100));

    }
}