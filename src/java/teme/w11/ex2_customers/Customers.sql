-- 2. Customers
-- Based on the CUSTOMERS and ORDERS tables created by the scripts below:
-- a. select the customer_id and last_name from the customers table and select the
-- order_date from the orders table where there is a matching customer_id value
-- in both the customers and orders tables. Order the results by customer_id in
-- descending order
-- b. select the first_name and last_name of customers who initiated an order in the
-- last month
-- c. select the distinct favorite_websites of customers who made orders in April
-- 2018
-- d. select the customer_id and last_name from the customers table where there is
-- a record in the orders table for that customer_id . Order the results in ascending
-- order by last_name and then descending order by customer_id

CREATE TABLE customers (
    customer_id INT NOT NULL,
    last_name CHAR(50) NOT NULL,
    first_name CHAR(50) NOT NULL,
    favorite_website CHAR(50),
    CONSTRAINT customers_pk PRIMARY KEY (customer_id)
);

CREATE TABLE orders (
    order_id INT NOT NULL,
    customer_id INT,
    order_date DATETIME,
    CONSTRAINT orders_pk PRIMARY KEY (order_id)
);

INSERT INTO customers (customer_id, last_name, first_name,
favorite_website) VALUES (4000, 'Jackson', 'Joe',
'techonthenet.com');
INSERT INTO customers (customer_id, last_name, first_name,
favorite_website) VALUES (5000, 'Smith', 'Jane', 'digminecraft.com');
INSERT INTO customers (customer_id, last_name, first_name,
favorite_website) VALUES (6000, 'Ferguson', 'Samantha',
'bigactivities.com');
INSERT INTO customers (customer_id, last_name, first_name,
favorite_website) VALUES (7000, 'Reynolds', 'Allen',
'checkyourmath.com');
INSERT INTO customers (customer_id, last_name, first_name,
favorite_website) VALUES (8000, 'Anderson', 'Paige', NULL);
INSERT INTO customers (customer_id, last_name, first_name,
favorite_website) VALUES (9000, 'Johnson', 'Derek',
'techonthenet.com');
INSERT INTO orders (order_id, customer_id, order_date) VALUES
(1,7000,'2018/04/18');
INSERT INTO orders (order_id, customer_id, order_date) VALUES
(2,5000,'2018/04/18');
INSERT INTO orders (order_id, customer_id, order_date) VALUES
(3,8000,'2018/04/19');
INSERT INTO orders (order_id, customer_id, order_date) VALUES
(4,4000,'2018/04/20');
INSERT INTO orders (order_id, customer_id, order_date) VALUES
(5,null,'2018/05/01');
INSERT INTO orders (order_id, customer_id, order_date) VALUES
(6,4000,'2018/05/02');

SELECT * FROM customers;
SELECT * FROM orders;

-- a. select the customer_id and last_name from the customers table and select the
-- order_date from the orders table where there is a matching customer_id value
-- in both the customers and orders tables. Order the results by customer_id in
-- descending order
alter table orders add constraint customer_fk foreign key (customer_id) references customers (customer_id);

SELECT 
    c.customer_id AS CustomerID,
    c.last_name AS CustomerLastName,
    o.order_date AS OrderDate
FROM customers c
JOIN orders o ON o.customer_id = c.customer_id
ORDER BY CustomerID DESC;


-- b. select the first_name and last_name of customers who initiated an order in the last month
-- all dinstinct order_date
SELECT DISTINCT
    o.order_date
FROM orders o;

-- last order_date
SELECT 
    MAX(DISTINCT order_date) AS MaxOrderDate
FROM
    orders;
    
-- extract the month from a predefined date    
-- Link: https://www.sqltutorial.org/sql-date-functions/extract-month-from-date-sql/
SELECT EXTRACT(MONTH FROM '2018-08-01');    

-- extract the month from the max date
SELECT 
    EXTRACT(MONTH FROM (SELECT 
                MAX(DISTINCT o.order_date)
            FROM
                orders o)) AS MaxMonthOrderDate;


-- b. Solution
SELECT 
    c.first_name AS FirsName,
    c.last_name AS LastName
FROM
    customers c
        JOIN
    orders o ON o.customer_id = c.customer_id
WHERE
    EXTRACT(MONTH FROM o.order_date) = 
    EXTRACT(MONTH FROM 
					(SELECT MAX(DISTINCT order_date) FROM orders)
			);	
  
-- c. select the distinct favorite_websites of customers who made orders in April 2018

SELECT DISTINCT
    c.favorite_website AS FavoriteWebsite
FROM
    customers c
        JOIN
    orders o ON o.customer_id = c.customer_id
WHERE
    c.favorite_website IS NOT NULL
        AND o.order_date LIKE '2018-04-%';
    
    
-- d. select the customer_id and last_name from the customers table where there is
-- a record in the orders table for that customer_id . Order the results in ascending
-- order by last_name and then descending order by customer_id
SELECT DISTINCT
    c.last_name, c.customer_id
FROM
    customers c
        JOIN
    orders o ON o.customer_id = c.customer_id
ORDER BY c.last_name , c.customer_id DESC;
