create database wantsome_teme;

CREATE TABLE students (
id integer AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(20));

CREATE TABLE courses (
id integer AUTO_INCREMENT PRIMARY KEY, 
description VARCHAR(20));

CREATE TABLE grades (
student_id INTEGER NOT NULL,
course_id INTEGER NOT NULL,
grade INTEGER NOT NULL,
FOREIGN KEY (student_id) REFERENCES students(id),
FOREIGN KEY (course_id) REFERENCES courses(id));

INSERT INTO students VALUES(1, 'ana');
INSERT INTO students VALUES(2, 'ion');
INSERT INTO students VALUES(3, 'maria');

INSERT INTO courses VALUES(1, 'Java intro');
INSERT INTO courses VALUES(2, 'Python intro');
INSERT INTO courses VALUES(3, 'Java advanced');

INSERT INTO grades VALUES (1, 3, 10);
INSERT INTO grades VALUES (1, 1, 8);
INSERT INTO grades VALUES (2, 1, 9);
INSERT INTO grades VALUES (3, 2, 7);
INSERT INTO grades VALUES (2, 2, 9);
INSERT INTO grades VALUES (2, 3, 6);

SELECT * FROM students;
SELECT * FROM courses;
SELECT * FROM grades;


-- a) Display all student grades, showing columns:
-- course description, student name, grade; to be sorted by: course (ASC) + grade (DESC)
SELECT s.name AS NumeStudent, c.description AS DescriereCurs, g.grade AS Nota
FROM grades g
JOIN students s ON g.student_id = s.id
JOIN courses c ON c.id = s.id
ORDER BY c.description ASC, g.grade DESC;


-- b) Display the list of course (the description of each) with the statistics: 
-- the minimum, maximum and average grade per course
SELECT c.description, avg(grade), min(grade), max(grade), COUNT(grade)
FROM grades g
JOIN courses c ON c.id = g.course_id 
GROUP BY course_id;

-- c) Display the list of students (the name for each) and the number of courses followed by each student
SELECT s.id, s.name, COUNT(g.course_id) AS CourseCount
FROM grades g
JOIN students s ON s.id = g.student_id
GROUP BY g.student_id
ORDER BY CourseCount DESC;


-- d) Display the total count of students following Java related courses (which contain 'Java' in their description)
SELECT c.id, c.description FROM courses c
WHERE c.description LIKE 'java%';

SELECT c.description, count(student_id)
FROM grades g
JOIN courses c ON g.course_id = c.id
WHERE c.id IN (
	SELECT id FROM courses WHERE description LIKE '%java%')
GROUP BY course_id
ORDER BY c.description;


-- e) Display the name and average grade for the student with the greatest average grade 
-- (based on grades from all his followed courses)
SELECT s.name, avg(grade) AS Medie
FROM grades g
JOIN students s ON s.id = g.student_id
GROUP BY student_id
ORDER BY Medie DESC
LIMIT 1;

-- f) Update the grades of all students following the Java related courses, 
-- by increasing current grade by +1, but without getting them past 10
UPDATE grades g 
SET g.grade = g.grade + 1
WHERE g.course_id IN (
	SELECT id FROM courses WHERE description LIKE '%java%')
    AND g.grade < 10;
    
    
-- TRADER TRANSACTIONS
CREATE TABLE trader (name VARCHAR(20) PRIMARY KEY,
city VARCHAR(20) NOT NULL);
INSERT INTO trader(name, city) VALUES ('andu', 'iasi');
INSERT INTO trader(name, city) VALUES ('ana', 'botosani');
INSERT INTO trader(name, city) VALUES ('maria', 'iasi');
INSERT INTO trader(name, city) VALUES ('dan', 'bacau');
INSERT INTO trader(name, city) VALUES ('vasile', 'bacau');

CREATE TABLE transaction (
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
    year INTEGER NOT NULL,
    value INTEGER NOT NULL,
    trader_name VARCHAR(20) NOT NULL,
    CONSTRAINT fk_trader FOREIGN KEY (trader_name) REFERENCES trader (name)
);

INSERT INTO transaction (year, value, trader_name) VALUES (2019, 1000, 'andu');
INSERT INTO transaction (year, value, trader_name) VALUES (2018, 700, 'andu');
INSERT INTO transaction (year, value, trader_name) VALUES (2018, 850, 'ana');
INSERT INTO transaction (year, value, trader_name) VALUES (2019, 900, 'ana');
INSERT INTO transaction (year, value, trader_name) VALUES (2017, 400, 'maria');
INSERT INTO transaction (year, value, trader_name) VALUES (2019, 1100, 'dan');

SELECT COUNT(*) FROM trader;
SELECT COUNT(*) FROM trader WHERE city = 'iasi';

--  Find all transactions from 2018 and sort them by value (small to high)
SELECT * FROM transaction WHERE year = '2018' ORDER BY value;

--  What are all the unique cities where traders work?
SELECT DISTINCT city FROM trader;

--  Find all traders from iasi and sort them by name (descending)
SELECT * FROM trader WHERE city = 'iasi' ORDER BY name DESC;

--  Return a string of all traders’ names sorted alphabetically and separated by comma
SELECT STRING_AGG (names, ', ') FROM trader ORDER BY name;

SELECT avg(value) Medie, min(value) Minim, max(value) maxim, sum(value) Total, count(value) Count FROM TRANSACTION;
SELECT avg(value) Medie, min(value) Minim, max(value) maxim, sum(value) Total, count(value) Count FROM TRANSACTION 
	GROUP BY year;
    
SELECT trader_name, avg(value) Medie, min(value) Minim, max(value) Maxim, sum(value) Total, count(value) Count FROM transaction
	GROUP BY trader_name;
    
-- afiseaza tranzactiile facute de fiecare trader
SELECT tx.year YEAR, tx.value VALUE, tr.name Tradername, tr.city TraderCity
	FROM transaction tx
    JOIN trader tr ON tx.trader_name = tr.name;
    
SELECT city FROM trader GROUP BY city HAVING count(name) >= 2;
SELECT * FROM trader;

SELECT tx.year YEAR, tx.value Value, tr.name TraderName, tr.city TraderCity
	FROM transaction tx
	JOIN trader tr ON tx.trader_name = tr.name
	WHERE city IN (
		SELECT city FROM trader GROUP BY city HAVING count(name) >= 2);
        
SELECT t1.* FROM (
	SELECT city, count(name) ct FROM trader GROUP BY city) t1
    WHERE t1.ct >= 2;
    
SELECT tx.year Year, tx.value Value, tr.name TraderName, tr.city TraderCity
	FROM transaction tx
	RIGHT JOIN trader tr  ON tx.trader_name = tr.name
	WHERE city IN (
		SELECT city FROM trader GROUP BY city HAVING count(name) >= 2
);