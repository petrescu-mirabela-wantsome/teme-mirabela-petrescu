-- 1. Suppliers
-- Based on the SUPPLIERS table created by the script below:
-- a. select the unique city values that reside in the state of California and order the
-- results in descending order by city
-- b. select all records and order them in ascending order after state and city

CREATE TABLE suppliers (
    supplier_id INT NOT NULL,
    supplier_name CHAR(50) NOT NULL,
    city CHAR(50),
    state CHAR(25),
    CONSTRAINT suppliers_pk PRIMARY KEY (supplier_id)
);

INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (100, 'Microsoft', 'Redmond', 'Washington');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (200, 'Google', 'Mountain View', 'California');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (300, 'Oracle', 'Redwood City', 'California');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (400, 'Kimberly-Clark', 'Irving', 'Texas');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (500, 'Tyson Foods', 'Springdale', 'Arkansas');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (600, 'SC Johnson', 'Racine', 'Wisconsin');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (700, 'Dole Food Company', 'Westlake Village', 'California');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (800, 'Flowers Foods', 'Thomasville', 'Georgia');
INSERT INTO suppliers (supplier_id, supplier_name, city, state)
VALUES (900, 'Electronic Arts', 'Redwood City', 'California');


-- a. select the unique city values that reside in the state of California and order the results in descending order by city
select * from suppliers;


SELECT DISTINCT s.city, s.state FROM suppliers s
WHERE
    s.state LIKE 'California'
ORDER BY s.city DESC;


-- b. select all records and order them in ascending order after state and city
SELECT * FROM suppliers s
ORDER BY s.state , s.city;

-- select all records and order them in ascending order after state and city and supplier_name
SELECT * FROM suppliers s
ORDER BY s.state , s.city , s.supplier_name;