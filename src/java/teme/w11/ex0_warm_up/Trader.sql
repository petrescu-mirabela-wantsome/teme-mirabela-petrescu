/*
-- 3. Trader Transactions
-- Create 2 classes:
--  Trader - with properties: name, city
--  Transaction - with properties: year, value, trader
--
-- Compute the following based on a list of such transactions:
--  Find all transactions from 2018 and sort them by value (small to high)
--  What are all the unique cities where traders work?
--  Find all traders from Cambridge and sort them by name (descending)
--  Return a string of all traders’ names sorted alphabetically and separated by comma
--  Determine if there are any traders from Milan
--  Update all transactions so that the traders from Milan are moved to Cambridge
--  What’s the highest value in all transactions?
--  What’s the transaction with the lowest value?

-- Extra:
--   Show all transaction but also with the City of the trader for each transaction (JOIN)
--   Show all transaction for which the traders are from a certain city (like 'iasi')
*/

-- Create Trader - with properties: name, city
CREATE TABLE trader (name VARCHAR(20) PRIMARY key, city VARCHAR(20) not null);
INSERT INTO trader (name, city) VALUES ('andu', 'iasi');
INSERT INTO trader (name, city) VALUES ('ana', 'botosani');
INSERT INTO trader (name, city) VALUES ('maria', 'iasi');
INSERT INTO trader (name, city) VALUES ('dan', 'bacau');
INSERT INTO trader (name, city) VALUES ('vasile', 'bacau');

select * from trader;

-- Show all trader columns, ordering the city desc
select * from trader order by city desc;

-- Show the number of entries from trader table
select count(*) from trader;

-- Show the number of traders where city is iasi
select count(*) from trader where city = 'iasi';

-- Show the number of traders of each city
select city as Oras, count(name) Total_traders from trader group by city;

select city as Oras, count(name) Total_traders from trader group by city order by Total_traders desc;

update trader set city='galati' where city='bacau';
update trader set city='galati' where name='andu';

select * from trader;
delete from trader where city = 'bla';
delete from trader where city = 'botosani';

-- Create Transaction - with properties: year, value, trader
CREATE TABLE transaction (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    transaction_year INTEGER NOT NULL,
    value INTEGER NOT NULL,
    trader_name VARCHAR(20) NOT NULL,
    CONSTRAINT fk_trader FOREIGN KEY (trader_name) REFERENCES trader (name)
);

INSERT into transaction (transaction_year, value, trader_name) values (2019, 1000, 'andu');
INSERT into transaction (transaction_year, value, trader_name) values (2018, 700, 'andu');
INSERT into transaction (transaction_year, value, trader_name) values (2018, 850, 'ana');
INSERT into transaction (transaction_year, value, trader_name) values (2019, 900, 'ana');
INSERT into transaction (transaction_year, value, trader_name) values (2017, 400, 'maria');
INSERT into transaction (transaction_year, value, trader_name) values (2019, 1100, 'dan');

select * from transaction;

-- Find all transactions from 2018 and sort them by value (small to high)
select * from transaction where transaction_year = 2018 order by value;

select * from transaction where transaction_year = 2019 order by value desc limit 2; 

-- What are all the unique cities where traders work?
select distinct city from trader order by city;
-- same result as above
select distinct city from trader;

-- Find all traders from iasi and sort them by name (ascending)
select * from trader where city = 'iasi';

-- Find all traders from galati and sort them by name (descending)
select * from trader where city = 'galati' order by name desc;

-- select trader_name from trader where city = 'botosani';
select * from trader where city = 'botosani';

--  What’s the highest value in all transactions?
--  What’s the transaction with the lowest value?
select avg(value) Medie, min(value) Min, sum(value) Total from transaction;
select avg(value) Medie, min(value) Min, sum(value) Total, count(value) Count from transaction;
select transaction_year, avg(value) Medie, min(value) Min, sum(value) Total, count(value) Count from transaction group by transaction_year;
select trader_name, avg(value) Medie, min(value) Min, sum(value) Total, count(value) Count from transaction group by trader_name;

-- Show all transaction but also with the City of the trader for each transaction (JOIN)
SELECT 
    tx.transaction_year Year, tx.value Value, tr.name TraderName, tr.city TraderCity
FROM
    transaction tx
        JOIN
    trader tr ON tx.trader_name = tr.name;
    
    
-- Show all transaction for which the traders are from a certain city (like 'iasi')
-- afisam toate tranzactiile, dar tranzactiile pentru care traderii sunt din iasi
SELECT 
    tx.transaction_year Year, tx.value Value, tr.name TraderName, tr.city TraderCity
FROM
    transaction tx
        JOIN
    trader tr ON tx.trader_name = tr.name where tr.city = 'iasi' or tr.city='galati';


-- Show all transaction for which the traders are from a certain city (like 'iasi', 'galati')    
SELECT 
    tx.transaction_year Year, tx.value Value, tr.name TraderName, tr.city TraderCity
FROM
    transaction tx
        JOIN
    trader tr ON tx.trader_name = tr.name where tr.city in ('iasi', 'galati');
    

SELECT 
    tx.transaction_year Year,
    tx.value Value,
    tr.name TraderName,
    tr.city TraderCity
FROM
    transaction tx
        JOIN
    trader tr ON tx.trader_name = tr.name
WHERE
    city IN (SELECT 
            city
        FROM
            trader
        GROUP BY city
        HAVING COUNT(name) >= 2);

-- Show all transaction but also with the City of the trader for each transaction (JOIN)
-- the city shall have al least 3 different trader names
SELECT 
    tx.transaction_year Year,
    tx.value Value,
    tr.name TraderName,
    tr.city TraderCity
FROM
    transaction tx
        RIGHT JOIN
    trader tr ON tx.trader_name = tr.name
WHERE
    city IN (SELECT 
            city
        FROM
            trader
        GROUP BY city
        HAVING COUNT(name) >= 2);
        
        
select city, count(name) ct from trader group by city having ct >= 3;

-- show all cities and the number of traders from it
select t1.* from (select city, count(name) ct from trader group by city) t1;

-- show all cities and number of traders where the numbe of traders shall be al least 3
select t1.* from (select city, count(name) ct from trader group by city) t1 where t1.ct >= 3;

-- drop table trader;
-- drop table transaction;