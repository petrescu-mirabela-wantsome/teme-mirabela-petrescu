-- Student Grades

-- 0. Student grades (warm-up)
-- Create the necessary tables to hold this data:
-- - a list of STUDENTS; each student has a name, and a unique id
-- - a list of COURSES; each one has a description and a unique id
-- - a list of STUDENT GRADES, separated by course:
-- - a student may attend multiple courses, and have a grade for each of them
-- - a course can be attended by multiple students (so there is a
-- many-to-many relationship between students and courses)

-- Then write some SQL statements to do this:
-- a. Display all student grades, showing columns: course description, student name,
-- grade; to be sorted by: course (ASC) + grade (DESC)
-- b. Display the list of course (the description of each) with the statistics: the
-- minimum, maximum and average grade per course
-- c. Display the list of students (the name for each) and the number of courses
-- followed by each student
-- d. Display the total count of students following Java related courses (which contain
-- 'Java' in their description)
-- e. Display the name and average grade for the student with the greatest average
-- grade (based on grades from all his followed courses)
-- f. Update the grades of all students following the Java related courses, by
-- increasing current grade by +1, but without getting them past 10

CREATE TABLE students (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(20)
);
insert into students (name) values('ana');
insert into students (name) values('ion');
insert into students (name) values('maria');
SELECT 
    *
FROM
    students;

CREATE TABLE courses (
    id INTEGER AUTO_INCREMENT PRIMARY KEY,
    description VARCHAR(20)
);
insert into courses (description) values('Java intro');
insert into courses (description) values('Phyton intro');
insert into courses (description) values('Java advanced');
SELECT 
    *
FROM
    courses;

-- n to n relationship
CREATE TABLE grades (
    student_id INTEGER NOT NULL,
    course_id INTEGER NOT NULL,
    grade INTEGER NOT NULL,
    CONSTRAINT FOREIGN KEY (student_id)
        REFERENCES students (id),
    CONSTRAINT FOREIGN KEY (course_id)
        REFERENCES courses (id)
);
    
SELECT 
    *
FROM
    grades;

insert into grades values (1, 3, 10);
insert into grades values (1, 1, 8);
insert into grades values (2, 1, 9);
insert into grades values (3, 2, 7);
insert into grades values (2, 2, 9);

SELECT 
    student_id, course_id, grade
FROM
    grades;

-- afisati toate notele, coloanele, course description, student name, grade

SELECT 
    *
FROM
    grades g
        JOIN
    students s ON g.student_id = s.id;

SELECT 
    s.name, g.course_id, g.grade
FROM
    grades g
        JOIN
    students s ON g.student_id = s.id;
    
SELECT 
    s.name NumeStudent,
    c.description DescriereCurs,
    g.grade Nota
FROM
    grades g
        JOIN
    students s ON g.student_id = s.id
        JOIN
    courses c ON c.id = g.course_id;

-- a) Display all student grades, showing columns: course description, student name, grade; to be sorted by: course (ASC) + grade (DESC)    
-- ordonare dupa cursuri
-- desc dupa nota
SELECT 
    s.name NumeStudent,
    c.description DescriereCurs,
    g.grade Nota
FROM
    grades g
        JOIN
    students s ON g.student_id = s.id
        JOIN
    courses c ON c.id = g.course_id
ORDER BY c.description , g.grade DESC;


-- b) Display the list of course (the description of each) with the statistics: the minimum, maximum and average grade per course    
-- lista cursurilor si nota maxima si minima la fiecare
SELECT 
    course_id, AVG(grade), MIN(grade), MAX(grade), COUNT(grade)
FROM
    grades
GROUP BY course_id;

  
SELECT 
    courses.description,
    AVG(grade),
    MIN(grade),
    MAX(grade),
    COUNT(grade)
FROM
    grades
        JOIN
    courses ON courses.id = grades.course_id
GROUP BY course_id;


-- c) Display the list of students (the name for each) and the number of courses followed by each student      
-- lista studentilor (nume) si nr cursurilor urmat de fiecare student
SELECT 
    *
FROM
    students;
SELECT 
    *
FROM
    grades;

SELECT 
    student_id, COUNT(course_id) AS nr_cursuri
FROM
    grades
GROUP BY student_id
ORDER BY nr_cursuri DESC;
 
 

SELECT 
    grades.student_id,
    students.name,
    COUNT(course_id) AS nr_cursuri
FROM
    grades
        JOIN
    students ON grades.student_id = students.id
GROUP BY student_id
ORDER BY nr_cursuri DESC;
    
-- nr tottal de studenti care participa la cursuri a caror descriere incepe cu java
SELECT 
    *
FROM
    courses;
    
SELECT 
    *
FROM
    courses
WHERE
    description LIKE 'Java%';	-- % urmeaza orice dupa Java
    
SELECT 
    id, UPPER(description)
FROM
    courses
WHERE
    description LIKE 'Java%';	-- % urmeaza orice dupa Java cu litera mica/mare
    
SELECT 
    *
FROM
    courses
WHERE
    description LIKE '%java%';	-- % urmeaza orice dupa Java cu litera mica/mare

SELECT 
    c.description, COUNT(student_id)
FROM
    grades g
        JOIN
    courses c ON g.course_id = c.id
WHERE
    c.id IN (1 , 2)
GROUP BY course_id;

 -- d) Display the total count of students following Java related courses (which contain 'Java' in their description)    
SELECT 
    c.description, COUNT(student_id)
FROM
    grades g
        JOIN
    courses c ON g.course_id = c.id
WHERE
    c.id IN (SELECT 
            id
        FROM
            courses
        WHERE
            description LIKE 'Java%')
GROUP BY course_id;
    
SELECT 
    c.description, COUNT(student_id)
FROM
    grades g
        JOIN
    courses c ON g.course_id = c.id
WHERE
    c.id IN (SELECT 
            id
        FROM
            courses
        WHERE
            description LIKE '%phy%')
GROUP BY course_id;
    
SELECT 
    *
FROM
    courses;

-- numele si media studentului
SELECT 
    name, AVG(grade) AS medie
FROM
    grades
        JOIN
    students ON students.id = grades.student_id
GROUP BY student_id;


-- f) Update the grades of all students following the Java related courses, by increasing current grade by +1, but without getting them past 10
-- de updatat numele studentilor de la java si crescut nota cu 1 punct
SELECT 
    *
FROM
    grades g
        JOIN
    courses c ON c.id = g.course_id
WHERE
    c.id IN (SELECT 
            id
        FROM
            courses
        WHERE
            description LIKE 'Java%');

SELECT 
    *
FROM
    grades g
WHERE
    course_id IN (SELECT 
            id
        FROM
            courses
        WHERE
            description LIKE 'Java%');
            
UPDATE grades 
SET 
    grade = grade - 1;

UPDATE grades 
SET 
    grade = grade + 1
WHERE
    course_id IN (SELECT 
            id
        FROM
            courses
        WHERE
            description LIKE 'Java%');

UPDATE grades 
SET 
    grade = grade + 1
WHERE
    course_id IN (SELECT 
            id
        FROM
            courses
        WHERE
            description LIKE 'Java%')
        AND grade < 10;