package teme.w05.ex1;

/* Create a Building class representing a building, with the following properties:
● name
● category (possible values: residential, office, hospital, religious)
● price
● neighborhood
Tip: you should use an enum for the category values/field.
*/

enum Category {
    RESIDENTIAL,
    OFFICE,
    HOSPITAL,
    RELIGIOUS
}

public class Building {
    private String name;
    private Category category;
    private double price;
    private String neighborhood;

    public Building(String name, Category category, int price, String neighborhood) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.neighborhood = neighborhood;
    }

    public double getPrice() {
        return price;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public Category getCategory() {
        return category;
    }
}
