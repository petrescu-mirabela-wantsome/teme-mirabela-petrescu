package teme.w05.ex1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class BuildingRegistryTest {
    private final static double DELTA = 0.00001; //precision used for comparing doubles

    @Test
    public void testNonEmptyBuildingRegistry() {
        Building buidling01 = new Building("Main", Category.RESIDENTIAL, 1700, "Copou");
        Building buidling02 = new Building("Principal", Category.RESIDENTIAL, 1200, "Copou");
        Building buidling03 = new Building("Second", Category.OFFICE, 1800, "Alexandru");
        Building buidling04 = new Building("None", Category.HOSPITAL, 500, "Gara");
        Building buidling05 = new Building("Other", Category.RESIDENTIAL, 1300, "Tudor");
        List<Building> listBuilding = new ArrayList<Building>(Arrays.asList(buidling01, buidling02, buidling03, buidling04, buidling05));

        assertEquals(1400, BuildingRegistry.averagePriceEachBuilding(listBuilding, Category.RESIDENTIAL), DELTA);
        assertEquals(1450, BuildingRegistry.averagePriceEachNeighborhood(listBuilding, "Copou"), DELTA);
        assertEquals(3, BuildingRegistry.numberCategories(listBuilding));
        assertEquals(new HashSet<>(Arrays.asList(buidling03.getNeighborhood(), buidling04.getNeighborhood(), buidling05.getNeighborhood(), buidling01.getNeighborhood())), BuildingRegistry.uniqueNeighborhoods(listBuilding));
    }
}
