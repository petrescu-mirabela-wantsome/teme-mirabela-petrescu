package teme.w05.ex0;

import java.util.*;

public class WarmUp {

    public static void main(String[] args) {


        String s = "Once upon a time in a land far far away there lived a great king whose name was a great mystery";


        /* a) Print the number of all words (including any duplicates). Hint: may use
            String.split(), regexp..
         */
        List<String> sList = new ArrayList<String>();
        // List<String> sListOther = Arrays.asList(s.toLowerCase().split("\\s+"));
        sList.addAll(Arrays.asList(s.toLowerCase().split("\\s+")));
        System.out.println("\nNumber of words: " + sList.size());


        /* b) Print all words in the initial order they appear in text.
         */
        System.out.println(" ------ Print the number of all words (including any duplicates) ------ ");
        System.out.println(sList);

        /* c) Print all words, but sorted alphabetically. Hint: see Collections.sort()
         */
        /* Collections.sort method is sorting the elements of ArrayList in ascending order. */
        System.out.println("\n ------ Print all words, but sorted alphabetically ------ ");
        Collections.sort(sList);
        System.out.println(sList.toString());

        /* d) Print the number of unique words.
         */
        Set<String> sHashSet = new HashSet<>(sList);
        // sHashSet.addAll(Arrays.asList(s.split("\\s+")));
        System.out.println("\nNumber of unique words with HashSet: " + sHashSet.size());


        /* e) Print all unique words, in the initial order they appear in text.
         */
        Set<String> sLinkedHashSet = new LinkedHashSet<>();
        sLinkedHashSet.addAll(Arrays.asList(s.toLowerCase().split("\\s+")));
        System.out.println("\nNumber of unique words with LinkedHashSet: " + sLinkedHashSet.size());
        System.out.println(" ------ Print all unique words, in the initial order they appear in text. ------ ");
        System.out.println(sLinkedHashSet.toString());


        /* f) Print all unique words, sorted alphabetically.
         */
        // Set<String> sTreeSet = new TreeSet<>();
        //sTreeSet.addAll(Arrays.asList(s.split("\\s+")));
        Set<String> sTreeSet = new TreeSet<>(Arrays.asList(s.toLowerCase().split("\\s+")));
        System.out.println(" ------ Print all unique words, sorted alphabetically ------ ");
        System.out.println(sTreeSet.toString());


        /* g) For each word, count how many times it appears, and then print all unique words
            (in their initial order in text) together with the number of times each appears.
        */
        Map<String, Integer> orderedMap = new LinkedHashMap<>();
        for (String word : Arrays.asList(s.toLowerCase().split("\\s+"))) {
            Integer count = orderedMap.get(word);
            if (count == null) {
                orderedMap.put(word, 1);
            } else {
                orderedMap.put(word, count + 1);
            }
        }
        System.out.println(" ------ For each word, count how many times it appears, and then print all unique words ------ ");
        System.out.println(orderedMap);

        /* h) The same, but now sort the word-count pairs alphabetically by word.
         */
        Map<String, Integer> sortedMap = new TreeMap<>(orderedMap);
        System.out.println(" ------ sort the word-count pairs alphabetically by word ------ ");
        System.out.println(sortedMap);
    }
}
