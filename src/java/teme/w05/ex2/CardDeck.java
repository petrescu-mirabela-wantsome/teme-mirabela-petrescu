package teme.w05.ex2;

import java.util.*;

/* Create a CardDeck class used for dealing hands.
- Should contain a list of all 52 cards and should remember which ones are dealt and
which ones are available.
- Should contain the following methods:
- List<Card> dealHand(int cards) : this method returns the specified number of
cards by randomly picking from the list of available cards.
Once picked, a card is marked as “used” (removed from the list of “available”
cards).
If there are not enough available cards then return as many as possible, or an
empty list when there are no longer any available cards.
- void shuffle() : this should mark all cards in the deck as available (essentially
emptying the “used” collection) and re-shuffle all the cards.
- int getAvailableCardCount()
- int getUsedCardCount()
Hints:
- for storing the full list of cards, as well as the lists of available/used cards, you could use
some instances of List
- for shuffling a collection, the Collections.shuffle() method can be used.
*/

public class CardDeck {

    // The map contains the Card and its status (true - available, false - dealt)
    Map<Card, Boolean> mapCards = new LinkedHashMap<>();
    List<Card> pickedCards = new ArrayList<>();

    public CardDeck() {
        generateCardDeck(); // generate a card from all the 52 possible card combinations
    }

    public static void main(String[] args) {
        CardDeck deck = new CardDeck();
        System.out.println(deck.mapCards);
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println(deck.dealHand(5)); // <- print 5 cards 3 times
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println(deck.dealHand(5));
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println(deck.dealHand(5));
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println(deck.dealHand(50)); // <- only 39 cards should be printed here
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println(deck.dealHand(50)); // <- and empty list should be printed
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println(deck.mapCards);
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println("\n >>>>>>>>>>>> After shuffle >>>>>>>>>>>>");
        deck.shuffle();
        System.out.println(deck.mapCards);
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());

        System.out.println(deck.dealHand(5)); // <- another 5 cards
        System.out.println("Picked cards number: " + deck.getUsedCardCount());
        System.out.println("Available cards number: " + deck.getAvailableCardCount());
        System.out.println(deck.mapCards);
    }

    /*  Returns the specified number of cards by randomly picking from the list of available cards.
        - Once picked, a card is marked as “used” (removed from the list of “available” cards).
        - If there are not enough available cards then return as many as possible, or an empty list when there are no longer any available cards.
     */
    public List<Card> dealHand(int cards) {
        List<Card> availableCards = getAvailableCards();
        pickedCards = new ArrayList<>();

        /* If there are not enough available cards then return as many as possible, or an
          empty list when there are no longer any available cards.
         */
        if (availableCards.size() > 0) {
            if (availableCards.size() < cards) {
                cards = availableCards.size();
            }
            // generate a random number in a specified range (from the available cards)
            // this random generation is performed for a number of cards time (parameter function)
            for (int i = 0; i < cards; i++) {
                int index = new Random().nextInt(availableCards.size());    // generate a random number equal to the number of available cards
                Card randomGeneratedCard = availableCards.get(index);       // get from the available cards the Card vakue of the index which was generated randomly
                mapCards.put(randomGeneratedCard, false);       // once the card was used, this will be set as false - not available in the dealing hand
                pickedCards.add(randomGeneratedCard);           // add the used card into the list of picked cards
                availableCards.remove(randomGeneratedCard);     // remove the used card from the list of the available cards
            }
        }
        return pickedCards;
    }

    /*
        Generate a card from all the 52 possible card combinations
     */
    public void generateCardDeck() {
        for (int i = 2; i <= 14; i++) {
            for (SuitCard itemSuitCard : SuitCard.values()) {
                mapCards.put(new Card(i, itemSuitCard), true);
            }
        }
    }

    /*
        Return a list will all the available cards
     */
    public List<Card> getAvailableCards() {
        List<Card> availableCards = new ArrayList<>();

        for (Card itemCard : mapCards.keySet()) {
            if (mapCards.get(itemCard)) {   // if a Card is available - not used (the value of the specified key is true)
                availableCards.add(itemCard);
            }
        }
        return availableCards;
    }

    /*
        This should mark all cards in the deck as available (essentially emptying the “used” collection) and re-shuffle all the cards.
     */
    public void shuffle() {
        List<Card> listCards = new ArrayList<>();
        for (Card itemCard : mapCards.keySet()) {
            listCards.add(itemCard);
        }
        Collections.shuffle(listCards);     // shuffle the cards

        mapCards.clear();   // removes all map entries
        for (Card itemListCards : listCards) {
            mapCards.put(itemListCards, true);  // add the new shuffle card values and mark all cards in the deck as available
        }
    }

    public int getAvailableCardCount() {
        return getAvailableCards().size();
    }

    public int getUsedCardCount() {
        return pickedCards.size();
    }
}
