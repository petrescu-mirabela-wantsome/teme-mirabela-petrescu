package teme.w05.ex2;

/* Create a Card class to remember the individual cards. It should have 2 properties:
- number (2 - 14)
- suit (one of these types: diamonds ( ♦ ), clubs (♣), hearts ( ♥ ) and spades (♠) ).
 */

enum SuitCard {
    DIAMONDS,
    CLUBS,
    HEARTS,
    SPADES
}

public class Card {
    private final int number;
    private SuitCard cards;

    public Card(int number, SuitCard cards) {
        this.number = number;
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Card{" +
                number + " " +
                cards +
                '}';
    }
}
