package teme.w05.ex2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CardDeckTest {

    @Test
    public void testCardDeck() {

        CardDeck deck = new CardDeck();
        assertEquals(0, deck.getUsedCardCount());
        assertEquals(52, deck.getAvailableCardCount());
//        assertEquals(generateCardDeckTest(), deck.mapCards);
        deck.dealHand(5);
        assertEquals(5, deck.getUsedCardCount());
        assertEquals(47, deck.getAvailableCardCount());

        deck.dealHand(5);
        assertEquals(5, deck.getUsedCardCount());
        assertEquals(42, deck.getAvailableCardCount());

        deck.dealHand(5);
        assertEquals(5, deck.getUsedCardCount());
        assertEquals(37, deck.getAvailableCardCount());

        deck.dealHand(50); // <- only 39 cards should be printed here
        assertEquals(37, deck.getUsedCardCount());
        assertEquals(0, deck.getAvailableCardCount());

        deck.dealHand(50); // <- and empty list should be printed
        assertEquals(0, deck.getUsedCardCount());
        assertEquals(0, deck.getAvailableCardCount());

        deck.shuffle();
        assertEquals(0, deck.getUsedCardCount());
        assertEquals(52, deck.getAvailableCardCount());

        deck.dealHand(5); // <- another 5 cards
        assertEquals(5, deck.getUsedCardCount());
        assertEquals(47, deck.getAvailableCardCount());
    }

//    private static Map<Card, Boolean> generateCardDeckTest() {
//
//        Map<Card, Boolean> map = new LinkedHashMap<>();
//        for (int i = 2; i <= 14; i++) {
//            for (SuitCard itemSuitCard : SuitCard.values()) {
//                map.put(new Card(i, itemSuitCard), true);
//            }
//        }
//        return map;
//    }
}
