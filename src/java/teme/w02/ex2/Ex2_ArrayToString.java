package teme.w02.ex2;

import java.util.Arrays;

/**
 * Write a function that returns a string representation of an array of doubles.
 * Format the numbers with two decimals. (hint: look up info on using String.format() for float numbers)
 * E.g  [1.1, 2.345, 3.0] -> “[1.10, 2.34, 3.00]”
 */
public class Ex2_ArrayToString {

    public static String arrayToString(double[] array) {

//     First Version without Arrays.toString() function call
        String stringFromArray = "";
        if (array.length != 0) {
            stringFromArray += "[";
            for (int i = 0; i < array.length; i++) {
                stringFromArray += String.format("%.2f", array[i]);
                if (i < array.length - 1) {
                    stringFromArray += ", ";
                }
            }
            stringFromArray += "]";
        } else {
            stringFromArray = "[]";
        }
        return stringFromArray;


/*        String[] newArray = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = String.format("%.2f", array[i]);
        }
        return Arrays.toString(newArray);*/
    }

    public static void main(String[] args) {
        System.out.println(arrayToString(new double[]{}));
        System.out.println(arrayToString(new double[]{1.1, 2.345, 3.0}));
    }
}
