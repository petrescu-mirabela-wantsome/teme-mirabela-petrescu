package teme.w02.ex4;

/**
 * Write a function that checks whether a given array of numbers
 * is sorted (in ascending order) or not.
 */
public class Ex4_CheckArrayIsSorted {

    /**
     * @param intArray - array of numbers to be sorted (given as varargs here, for easier testing)
     * @return true if array is sorted in ascending order, false otherwise
     */
    static boolean isSorted(int... intArray) {
        for (int i = 0; i < intArray.length - 1; i++) {
            for (int j = i + 1; j < intArray.length; j++) {
                if (intArray[i] > intArray[j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(isSorted());
        System.out.println(isSorted(1));
        System.out.println(isSorted(1, 1, 2));
        System.out.println(isSorted(1, 2, 3, 5));
        System.out.println(isSorted(1, 2, 4, 3, 5));
    }
}
