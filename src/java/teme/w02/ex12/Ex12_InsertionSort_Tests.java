package teme.w02.ex12;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static teme.w02.ex12.Ex12_InsertionSort.sort;
import static teme.w02.ex12.Ex12_InsertionSort.sortSwap;

public class Ex12_InsertionSort_Tests {

    private static String sorted(int[] arr) {
        sort(arr);
        return Arrays.toString(arr);
    }

    private static String sortedSwap(int[] arr) {
        sortSwap(arr);
        return Arrays.toString(arr);
    }

    @Test
    public void testSort() {
        assertEquals("[]", sorted(new int[]{}));
        assertEquals("[1]", sorted(new int[]{1}));
        assertEquals("[1, 2, 3]", sorted(new int[]{1, 2, 3}));
        assertEquals("[1, 2, 3]", sorted(new int[]{1, 3, 2}));
        assertEquals("[1, 2, 3, 4, 5]", sorted(new int[]{5, 2, 3, 1, 4}));
        assertEquals("[0, 1, 2, 3, 4, 5]", sorted(new int[]{5, 4, 3, 2, 1, 0}));
    }

    @Test
    public void testSortSwap() {
        assertEquals("[]", sortedSwap(new int[]{}));
        assertEquals("[1]", sortedSwap(new int[]{1}));
        assertEquals("[1, 2, 3]", sortedSwap(new int[]{1, 2, 3}));
        assertEquals("[1, 2, 3]", sortedSwap(new int[]{1, 3, 2}));
        assertEquals("[1, 2, 3, 4, 5]", sortedSwap(new int[]{5, 2, 3, 1, 4}));
        assertEquals("[0, 1, 2, 3, 4, 5]", sortedSwap(new int[]{5, 4, 3, 2, 1, 0}));
    }
}
