package teme.w02.ex0;

import java.util.Arrays;

/**
 * a) Write a method which, given a string and a non-negative int n, returns a larger string that is n copies of the original string.
 * Example: xTimes(“Abc”, 2) => “AbcAbc”
 * <p>
 * b) Given a number non-negative int n, return the sum of the squares of all numbers between 1 and n.
 * Example: sumSquares(4) = 1*1+2*2+3*3+4*4 = 30
 * <p>
 * c) Given a positive number n (type: int), compute and return factorial of n (defined as: n! = 1*..*n). It should work well at least up to n=16
 * Try first to write an iterative version for it, then an recursive one (considering that: n! = n*(n-1)! and 1!=1)
 * <p>
 * d) Given the (romanian) name of a day of the week, return a number showing its position in the week, or -1 if name is invalid.
 * Try to use the switch block. Example: 'luni'->1 .. 'Duminica'->7, 'invalid'-> -1
 * (hint: check out also these useful methods of String class: toLowerCase, toUpperCase)
 * <p>
 * e) Given a string, count how many vowels (a,e,i,o,u) it contains. Try to solve it first without using arrays, then with arrays.
 * Hint: check String class for useful methods you may need here (like: toLowerCase(), length(), charAt(), toCharArray()..)
 * <p>
 * f) Given an array of numbers (of type double), compute their sum and then the average value (hint: when computing average, you may reuse the sum() method)
 * <p>
 * g) Given an array of numbers (of type double), find and return the max value (or special value Double.NEGATIVE_INFINITY if cannot find a max value - when can this happen?)
 * <p>
 * h) Given an array of numbers, compute the sum of all numbers until first negative one is found (excluding it)
 * <p>
 * i) Given an array of numbers, create and return another array which has same numbers but in reversed order
 * <p>
 * j) Given an array of numbers, and some other separate number x, update the array so that each initial number is
 * replaced by its value multiplied by the fixed x value
 * (note: the method should return void, but affect the initial array directly - doing an ‘in-place’ update)
 */
public class Ex0 {

    /**
     * a) Write a method which, given a string and a non-negative int n, returns a larger string that is n copies of the original string.
     * Example: xTimes(“Abc”, 2) => “AbcAbc”
     */
    static String xTimes(String s, int n) {

        String strMultimple = "";
        for (int i = 0; i < n; i++) {
            strMultimple += s;
        }
        return strMultimple;
    }

    /**
     * b) Given a number non-negative int n, return the sum of the squares of all numbers between 1 and n.
     * * Example: sumSquares(4) = 1*1+2*2+3*3+4*4 = 30
     */
    static int sumSquares(int n) {

        int sumSquare = 0;
        for (int i = 1; i <= n; i++) {
            sumSquare += i * i;
        }

        return sumSquare;
    }

    /**
     * c) Given a positive number n (type: int), compute and return factorial of n (defined as: n! = 1*..*n). It should work well at least up to n=16
     * * Try first to write an iterative version for it, then an recursive one (considering that: n! = n*(n-1)! and 1!=1)
     */
    static long factorial(int n) {

        long resultFactorial = 1;
        for (int i = 1; i <= n; i++) {
            resultFactorial *= i;
        }
        return resultFactorial;
    }

    static long factorialRec(int n) {
        //handle stop condition first
        if (n <= 1) {
            return 1;
        }
        //if not in stop case, do a recursive call as needed
        return n * factorialRec(n - 1); //calling itself, but with just another parameter
    }

    /**
     * d) Given the (romanian) name of a day of the week, return a number showing its position in the week, or -1 if name is invalid.
     * Try to use the switch block. Example: 'luni'->1 .. 'Duminica'->7, 'invalid'-> -1
     * (hint: check out also these useful methods of String class: toLowerCase, toUpperCase)
     */
    static byte dayOfWeek(String s) {

        byte positionNumber;
        switch (s.toLowerCase()) {
            case "luni":
                positionNumber = 1;
                break;
            case "marti":
                positionNumber = 2;
                break;
            case "miercuri":
                positionNumber = 3;
                break;
            case "joi":
                positionNumber = 4;
                break;
            case "vineri":
                positionNumber = 5;
                break;
            case "sambata":
                positionNumber = 6;
                break;
            case "duminica":
                positionNumber = 7;
                break;
            default:
                positionNumber = -1;
        }
        return positionNumber;
    }

    /**
     * e) Given a string, count how many vowels (a,e,i,o,u) it contains. Try to solve it first without using arrays, then with arrays.
     * Hint: check String class for useful methods you may need here (like: toLowerCase(), length(), charAt(), toCharArray()..)
     */
    static int countVowels(String s) {

        int stringLength = s.length();
        int numberVowels = 0;
        char charVowel;

        for (int i = 0; i <= stringLength - 1; i++) {
            charVowel = s.toLowerCase().charAt(i);
            if (isVowel(charVowel)) {
                numberVowels++;
            }
        }
        return numberVowels;
    }

    static int countVowelsWithArray(String s) {

        char[] arrayVowelsCount;//
        int numberVowels = 0;
        arrayVowelsCount = s.toLowerCase().toCharArray();

        for (char item : arrayVowelsCount)
            if (isVowel(item)) {
                numberVowels++;
            }
        return numberVowels;
    }

    private static boolean isVowel(char item) {
        return item == 'a' || item == 'e' || item == 'i' || item == 'o' || item == 'u';
    }

    /**
     * f) Given an array of numbers (of type double), compute their sum and then the average value
     * Hint: when computing average, you may reuse the sum() method
     */
    static double sum(double[] arr) {

        double sumValue = 0;
        for (double num : arr) {
            sumValue += num;
        }
        return sumValue;
    }

    static double avg(double[] arr) {

        return sum(arr) / arr.length;
    }

    /**
     * g) Given an array of numbers (of type double), find and return the max value
     * (or special value Double.NEGATIVE_INFINITY if cannot find a max value - when can this happen?)
     */
    static double max(double[] arr) {

        /**
         * NEGATIVE_INFINITY = -1.0 / 0.0
         */
        // Solution 1
/*        double maxValue;
        if (arr.length == 0) {
            maxValue = Double.NEGATIVE_INFINITY;
        } else {
            maxValue = arr[0];
            for (int i = 1; i < arr.length; i++) {
                if (maxValue < arr[i]) {
                    maxValue = arr[i];
                }
            }
        }*/

        // Solution 2:
        double maxValue = Double.NEGATIVE_INFINITY;
        for (double v : arr) {
            if (v > maxValue) {
                maxValue = v;
            }
        }

        return maxValue;
    }

    /**
     * h) Given an array of numbers, compute the sum of all numbers until first negative one is found (excluding it)
     */
    static double sumPositives(double[] arr) {

        double sumPosValues = 0;
        for (double value : arr) {
            if (value < 0) {
                break;
            }
            sumPosValues += value;
        }

        return sumPosValues;
    }

    static double sumPositivesWhile(double[] arr) {

        int i = 0;
        double sum = 0;
        {
            while (i < arr.length && arr[i] >= 0) {
                sum += arr[i];
                i++;
            }
        }

        return sum;
    }

    /**
     * i) Given an array of numbers, create and return another array which has same numbers but in reversed order
     */
    static double[] reversed(double arr[]) {

        double[] reversedArray = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            reversedArray[arr.length - 1 - i] = arr[i];
        }
        return reversedArray;
    }

    /**
     * j) Given an array of numbers, and some other separate number x, update the array so that each initial number is
     * * replaced by its value multiplied by the fixed x value
     * * (note: the method should return void, but affect the initial array directly - doing an ‘in-place’ update)
     */
    static void multiply(double arr[], double x) {

        for (int i = 0; i < arr.length; i++) {
            arr[i] *= x;
        }
    }

    static boolean contains(String[] names, String nameToCheck) {
        for (String item : names) {
            if (item.equalsIgnoreCase(nameToCheck)) {
                return true;
            }
        }
        return false;
    }

    static boolean containsChar(String text, char charToFind) {
        char[] stringArray = text.toLowerCase().toCharArray();
        for (char item : stringArray) {
            if (item == charToFind) {
                return true;
            }
        }
        return false;
    }

    static boolean containsChar0(String text, char charToFind) {
        char[] stringArray = text.toCharArray();
        Arrays.sort(stringArray);
        int idx = Arrays.binarySearch(stringArray, charToFind);
        return idx >= 0;
    }

    public static void main(String[] args) {
        System.out.println("xTimes('abc',2)= '" + xTimes("abc", 2) + "'");
        System.out.println("sumSquares(4)= " + sumSquares(4));

        System.out.println("\ndayOfWeek('luni') = " + dayOfWeek("luni"));
        System.out.println("dayOfWeek('DUMINICA') = " + dayOfWeek("DUMINICA"));
        System.out.println("dayOfWeek('invalid') = " + dayOfWeek("invalid"));

        System.out.println("\nfactorial(1) = " + factorial(1));
        System.out.println("factorial(5) = " + factorial(5));
        System.out.println("factorial(13) = " + factorial(13));
        System.out.println("factorialRec(1) = " + factorialRec(1));
        System.out.println("factorialRec(5) = " + factorialRec(5));
        System.out.println("factorialRec(13) = " + factorialRec(13));

        System.out.println("\ncountVowels('Run, Forest, run!') = " + countVowels("Run, Forest, run!"));
        System.out.println("countVowels('I will..') = " + countVowels("I will.."));
        System.out.println("countVowelsWithArray('Run, Forest, run!') = " + countVowelsWithArray("Run, Forest, run!"));
        System.out.println("countVowelsWithArray('I will..') = " + countVowelsWithArray("I will.."));

        double[] values1 = {1.1, 2.2, 3.3, 4.4};
        System.out.println("\nsum(" + Arrays.toString(values1) + ") = " + sum(values1));
        System.out.println("avg(" + Arrays.toString(values1) + ") = " + avg(values1));
        System.out.println("max(" + Arrays.toString(values1) + ") = " + max(values1));

        double[] values2 = {}; //an empty array
        System.out.println("\nsum(" + Arrays.toString(values2) + ") = " + sum(values2));
        System.out.println("avg(" + Arrays.toString(values2) + ") = " + avg(values2));
        System.out.println("max(" + Arrays.toString(values2) + ") = " + max(values2));

        double[] values3 = {1, 2, 3, -4, 5, 6};
        System.out.println("\nsumPositives(" + Arrays.toString(values3) + ") = " + sumPositives(values3));

        System.out.println("\nreverse(" + Arrays.toString(values2) + ") = " + Arrays.toString(reversed(values2)));
        System.out.println("reverse(" + Arrays.toString(values3) + ") = " + Arrays.toString(reversed(values3)));

        String[] names = {"Adi", "Gabi", "Maria"};
        System.out.println("contains method call: " + contains(names, "Adi"));
        System.out.println("contains method call: " + contains(names, "adi"));
        System.out.println("contains method call: " + contains(names, "Gigel"));

        System.out.println("contains f: " + containsChar("abc de..asf", 'f'));
        System.out.println("contains  : " + containsChar("abc de..asf", ' '));
        System.out.println("contains .: " + containsChar("abc de..asf", '.'));
        System.out.println("contains ?: " + containsChar("abc de..asf", '?'));

        System.out.println("contains f: " + containsChar0("abc de..asf", 'f'));
        System.out.println("contains  : " + containsChar0("abc de..asf", ' '));
        System.out.println("contains .: " + containsChar0("abc de..asf", '.'));
        System.out.println("contains ?: " + containsChar0("abc de..asf", '?'));
    }
}
