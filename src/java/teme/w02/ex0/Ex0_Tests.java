package teme.w02.ex0;

import org.junit.Test;

import static org.junit.Assert.*;
import static teme.w02.ex0.Ex0.*;

public class Ex0_Tests {

    private final double DELTA = 0.0001; //precision using when comparing double values for tests

    @Test
    public void testXTimes() {
        assertEquals("", xTimes("", 0));
        assertEquals("", xTimes("abc", 0));
        assertEquals("abcabc", xTimes("abc", 2));
        assertEquals("AAA", xTimes("A", 3));
    }

    @Test
    public void testSumSquares() {
        assertEquals(0, sumSquares(0));
        assertEquals(1, sumSquares(1));
        assertEquals(5, sumSquares(2));
        assertEquals(385, sumSquares(10));
    }

    @Test
    public void testDayOfWeek() {
        assertEquals(1, dayOfWeek("luni"));
        assertEquals(2, dayOfWeek("Marti"));
        assertEquals(3, dayOfWeek("MIERCURI"));
        assertEquals(4, dayOfWeek("jOi"));
        assertEquals(5, dayOfWeek("Vineri"));
        assertEquals(6, dayOfWeek("SaMbAtA"));
        assertEquals(7, dayOfWeek("duminica"));
        assertEquals(-1, dayOfWeek("altceva"));
        assertEquals(-1, dayOfWeek(""));
    }

    @Test
    public void testFactorial() {
        //values which still fit in range of int type
        assertEquals(1, factorial(1));
        assertEquals(2, factorial(2));
        assertEquals(120, factorial(5));
        assertEquals(3628800, factorial(10));
        assertEquals(479001600, factorial(12));

        //bigger values which fit in range of long type
        assertEquals(6227020800L, factorial(13));
        assertEquals(87178291200L, factorial(14));
        assertEquals(20922789888000L, factorial(16)); //about the max value that still fits in the range for long!
    }

    @Test
    public void testFactorialRec() {
        //values which still fit in range of int type
        assertEquals(1, factorialRec(1));
        assertEquals(2, factorialRec(2));
        assertEquals(120, factorialRec(5));
        assertEquals(3628800, factorialRec(10));
        assertEquals(479001600, factorialRec(12));

        //bigger values which fit in range of long type
        assertEquals(6227020800L, factorialRec(13));
        assertEquals(87178291200L, factorialRec(14));
        assertEquals(20922789888000L, factorialRec(16)); //about the max value that still fits in the range for long!
    }

    @Test
    public void testCountVowels() {
        assertEquals(4, countVowels("Run, Forest, run!"));
        assertEquals(2, countVowels("I will.."));
        assertEquals(0, countVowels(""));
    }

    @Test
    public void testCountVowelsWithArray() {
        assertEquals(4, countVowelsWithArray("Run, Forest, run!"));
        assertEquals(2, countVowelsWithArray("I will.."));
        assertEquals(0, countVowelsWithArray(""));
    }

    @Test
    public void testSum() {
        assertEquals(0, sum(new double[]{}), DELTA);
        assertEquals(-4, sum(new double[]{-4}), DELTA);
        assertEquals(2, sum(new double[]{1, 2, 3, -4}), DELTA);
    }

    @Test
    public void testAvg() {
        assertEquals(2, avg(new double[]{2}), DELTA);
        assertEquals(2.75, avg(new double[]{1.1, 2.2, 3.3, 4.4}), DELTA);

        double avgEmpty = avg(new double[]{});
        assertTrue(Double.isNaN(avgEmpty) || avgEmpty == 0);
    }

    @Test
    public void testMax() {
        assertEquals(Double.NEGATIVE_INFINITY, max(new double[]{}), DELTA);
        assertEquals(5, max(new double[]{5}), DELTA);
        assertEquals(-4, max(new double[]{-4}), DELTA);
        assertEquals(3, max(new double[]{1, 2, 3, -4}), DELTA);
    }

    @Test
    public void testSumPositives() {
        assertEquals(0, sumPositives(new double[]{}), DELTA);
        assertEquals(0, sumPositives(new double[]{-1, 2, 3}), DELTA);
        assertEquals(6, sumPositives(new double[]{1, 2, 3, -4, 15}), DELTA);
    }

    @Test
    public void sumPositivesWhile() {
        assertEquals(0, sumPositives(new double[]{}), DELTA);
        assertEquals(0, sumPositives(new double[]{-1, 2, 3}), DELTA);
        assertEquals(6, sumPositives(new double[]{1, 2, 3, -4, 15}), DELTA);
    }

    @Test
    public void testReversed() {
        assertArrayEquals(new double[]{}, reversed((new double[]{})), DELTA);
        assertArrayEquals(new double[]{1}, reversed((new double[]{1})), DELTA);
        assertArrayEquals(new double[]{3, 2, 1}, reversed((new double[]{1, 2, 3})), DELTA);
    }

    @Test
    public void testMultiply() {
        double[] arr1 = {};
        multiply(arr1, 2.5);
        assertArrayEquals(new double[]{}, arr1, DELTA);

        double[] arr2 = {1, 2, 3, 4, 5};
        multiply(arr2, 2);
        assertArrayEquals(new double[]{2, 4, 6, 8, 10}, arr2, DELTA);
        multiply(arr2, 0);
        assertArrayEquals(new double[]{0, 0, 0, 0, 0}, arr2, DELTA);
    }
}
