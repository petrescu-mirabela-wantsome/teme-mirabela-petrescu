package teme.w02.ex1;

/**
 * Write a function that receives a positive integer value and returns its base 2 representation as a string.
 * E.g. decimalToBinary(3) -> "11", decimalToBinary(10) -> “1010”, decimalToBinary(127) = “1111111
 * <p>
 * More info: https://en.wikipedia.org/wiki/Binary_number#Conversion_to_and_from_other_numeral_systems
 */
public class Ex1_DecimalToBinary {

    static String decimalToBinary(int n) {

        int[] binaryArray = new int[1000];  // array to store binary number
        // counter for binary array
        int i = 0;
        String binaryString = n == 0 ? "0" : "";    // string to store binary number
        while (n > 0) {
            // storing remainder in binary array
            binaryArray[i++] = n % 2;
            n = n / 2;
        }
        // read from the last elemen until the first element
        for (int j = i - 1; j >= 0; j--) {
            binaryString += binaryArray[j];
        }
        return binaryString;
    }

    /**
     * Just for manual testing
     */
    public static void main(String[] args) {
        System.out.println(decimalToBinary(0));
        System.out.println(decimalToBinary(1));
        System.out.println(decimalToBinary(2));
        System.out.println(decimalToBinary(3));
        System.out.println(decimalToBinary(4));
        System.out.println(decimalToBinary(10));
    }
}
