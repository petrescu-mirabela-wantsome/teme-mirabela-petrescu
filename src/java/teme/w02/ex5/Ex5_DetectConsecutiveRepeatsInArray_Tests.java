package teme.w02.ex5;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static teme.w02.ex5.Ex5_DetectConsecutiveRepeatsInArray.onlyConsecutiveRepeating;

public class Ex5_DetectConsecutiveRepeatsInArray_Tests {

    @Test
    public void testSimpleCases() {
        assertEquals("", onlyConsecutiveRepeating());
        assertEquals("", onlyConsecutiveRepeating(1));
        assertEquals("1", onlyConsecutiveRepeating(1, 1).trim());
        assertEquals("1", onlyConsecutiveRepeating(1, 1, 2, 3).trim());
        assertEquals("2", onlyConsecutiveRepeating(1, 2, 2, 3).trim());
        assertEquals("3", onlyConsecutiveRepeating(1, 2, 3, 3).trim());
        assertEquals("", onlyConsecutiveRepeating(1, 2, 1).trim());
    }

    @Test
    public void testTrickyCases() {
        assertEquals("1", onlyConsecutiveRepeating(1, 1, 1).trim());
        assertEquals("1", onlyConsecutiveRepeating(1, 1, 1, 2).trim());
        assertEquals("1", onlyConsecutiveRepeating(0, 1, 1, 1).trim());
        assertEquals("1 1", onlyConsecutiveRepeating(1, 1, 2, 1, 1).trim());
        assertEquals("4 5 4", onlyConsecutiveRepeating(1, 4, 4, 4, 3, 5, 5, 4, 4).trim());
        assertEquals("4 2 9", onlyConsecutiveRepeating(1, 4, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9).trim());
    }
}
