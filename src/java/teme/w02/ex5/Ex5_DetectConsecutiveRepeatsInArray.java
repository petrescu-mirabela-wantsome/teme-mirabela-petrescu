package teme.w02.ex5;

/**
 * Write a function that takes an array of numbers and returns just the numbers
 * that are repeating on consecutive positions in the array,
 * as a single String value (containing the numbers separated by space).
 * E.g:
 * [1, 4, 3, 4] -> “”
 * [1, 4, 4, 4, 3, 5, 5, 4, 4] -> “4 5 4”
 * [1, 4, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9] -> “4 2 9”
 */
public class Ex5_DetectConsecutiveRepeatsInArray {

    static String onlyConsecutiveRepeating(int... numbersArray) {

        String onlyConsRepString = "";
        int lastEqualValueFound = Integer.MIN_VALUE;    // impossible value considered in numbersArray
        for (int i = 1; i < numbersArray.length; i++) {
            if (numbersArray[i] == numbersArray[i - 1] && numbersArray[i] != lastEqualValueFound) {
                lastEqualValueFound = numbersArray[i];  // keep the first equal value found
                onlyConsRepString += " " + numbersArray[i];
            } else {
                lastEqualValueFound = Integer.MIN_VALUE;    // impossible value considered in numbersArray
            }
        }
        return onlyConsRepString;
    }

    public static void main(String[] args) {
        System.out.println(onlyConsecutiveRepeating());
        System.out.println(onlyConsecutiveRepeating(1, 1));
        System.out.println(onlyConsecutiveRepeating(1, 1, 1));
        System.out.println(onlyConsecutiveRepeating(1, 4, 3, 4));
        System.out.println(onlyConsecutiveRepeating(1, 4, 4, 4, 3, 5, 5, 4, 4));
        System.out.println(onlyConsecutiveRepeating(1, 4, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9));
    }
}
