package teme.w02.ex3;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static teme.w02.ex3.Ex3_FilterArray.onlyOdd;

public class Ex3_FilterArray_Tests {
    @Test
    public void testFilterArray() {
        assertArrEquals(new int[]{}, onlyOdd(new int[]{}));

        assertArrEquals(new int[]{}, onlyOdd(new int[]{0}));
        assertArrEquals(new int[]{1}, onlyOdd(new int[]{1}));
        assertArrEquals(new int[]{}, onlyOdd(new int[]{2}));

        assertArrEquals(new int[]{1, 3}, onlyOdd(new int[]{1, 3}));
        assertArrEquals(new int[]{}, onlyOdd(new int[]{2, 4}));
        assertArrEquals(new int[]{1, 3}, onlyOdd(new int[]{1, 2, 3, 4}));
    }

    private void assertArrEquals(int[] expected, int[] actual) {
        assertEquals(Arrays.toString(expected), Arrays.toString(actual));
    }
}
