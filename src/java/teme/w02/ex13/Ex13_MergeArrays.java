package teme.w02.ex13;

/**
 * Write a function that receives two sorted arrays and returns a new sorted array with elements from both arrays.
 * (note: this is called the merge algorithm)
 */

/**
 * The idea is to use Merge function of Merge sort.
 * Create an array arr3[] of size n1 + n2.
 * Simultaneously traverse arr1[] and arr2[].
 * Pick smaller of current elements in arr1[] and arr2[], copy this smaller element to next position in arr3[] and move ahead in arr3[] and the array whose element is picked.
 * If there are are remaining elements in arr1[] or arr2[], copy them also in arr3[].
 */
public class Ex13_MergeArrays {

    static int[] merge(int[] arr1, int[] arr2) {


        //int[] arr3;// = new int[arr1.length + arr2.length];
        if (arr1.length == 0) { // in case arr1 is empty, copy only arr1 content
            return arr2;
        } else if (arr2.length == 0) {  // in case arr2 is empty, copy only arr1 content
            return arr1;
        } else {
            int arr3[] = new int[arr1.length + arr2.length];    //Create an array arr3[] of size n1 + n2
            int i = 0, j = 0, k = 0;
            // Traverse both array
            while (i < arr1.length && j < arr2.length) {
                if (arr1[i] < arr2[j])
                    // Pick smaller of current elements in arr1[]
                    // copy this smaller element to next position in arr3[] and move ahead in arr3[] and arr1[]
                    arr3[k++] = arr1[i++];
                else
                    // Pick smaller of current elements in arr2[]
                    // copy this smaller element to next position in arr3[] and move ahead in arr3[] and arr2[]
                    arr3[k++] = arr2[j++];
            }
            // Store remaining elements of first array
            while (i < arr1.length)
                arr3[k++] = arr1[i++];
            // Store remaining elements of second array
            while (j < arr2.length)
                arr3[k++] = arr2[j++];

            return arr3;
        }

    }

    //for manual tests
    public static void main(String[] args) {
        int[] arr31 = sortAndPrint(new int[]{5}, new int[]{1, 2, 3, 4});
        //System.out.println(Arrays.toString(arr31));
        int[] arr32 = sortAndPrint(new int[]{1, 2, 3}, new int[]{1, 2, 3});
        //System.out.println(Arrays.toString(arr32));
    }

    //helper function for main
    private static int[] sortAndPrint(int[] arr1, int[] arr2) {
        int[] arr3 = merge(arr1, arr2);
        return arr3;
    }
}


