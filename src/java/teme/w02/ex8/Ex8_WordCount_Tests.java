package teme.w02.ex8;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static teme.w02.ex8.Ex8_WordCount.countWords;

public class Ex8_WordCount_Tests {

    @Test
    public void testEmptyCase() {
        assertEquals(0, countWords(""));
    }

    @Test
    public void testSimpleCases() {
        assertEquals(1, countWords("abc"));
        assertEquals(2, countWords("aa bb"));
    }

    @Test
    public void testTrickyCases() {
        assertEquals(0, countWords("   "));
        assertEquals(1, countWords("  abc "));
        assertEquals(4, countWords("  aa   bb cc  aa "));
    }
}
