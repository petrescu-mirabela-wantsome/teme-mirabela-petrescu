package teme.w02.ex7;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static teme.w02.ex7.Ex7_DecimalToBase.decimalToBase;
import static teme.w02.ex7.Ex7_DecimalToBase.digitAsString;

public class Ex7_DecimalToBase_Tests {

    @Test
    public void testDigitAsString() {
        assertEquals("0", digitAsString(0));
        assertEquals("1", digitAsString(1));
        assertEquals("9", digitAsString(9));
        assertEquals("A", digitAsString(10));
        assertEquals("F", digitAsString(15));
        assertEquals("Z", digitAsString(35));
    }

    @Test
    public void testConversionToBase10() {
        assertEquals("0", decimalToBase(0, 10));
        assertEquals("1", decimalToBase(1, 10));
        assertEquals("9", decimalToBase(9, 10));
        assertEquals("10", decimalToBase(10, 10));
        assertEquals("100", decimalToBase(100, 10));
        assertEquals(String.valueOf(Integer.MAX_VALUE), decimalToBase(Integer.MAX_VALUE, 10));
    }

    @Test
    public void testConversionToBase2() {
        assertEquals("0", decimalToBase(0, 2));
        assertEquals("1", decimalToBase(1, 2));
        assertEquals("10", decimalToBase(2, 2));
        assertEquals("11", decimalToBase(3, 2));
        assertEquals("100", decimalToBase(4, 2));
        assertEquals("1000", decimalToBase(8, 2));
        assertEquals("1010", decimalToBase(10, 2));
        assertEquals("1111111", decimalToBase(127, 2));
        assertEquals("101100101", decimalToBase(357, 2));
        assertEquals("1111111111111111111111111111111", decimalToBase(Integer.MAX_VALUE, 2));
    }

    @Test
    public void testConversionToBase16() {
        assertEquals("0", decimalToBase(0, 16));
        assertEquals("1", decimalToBase(1, 16));
        assertEquals("A", decimalToBase(10, 16));
        assertEquals("F", decimalToBase(15, 16));
        assertEquals("10", decimalToBase(16, 16));
        assertEquals("7F", decimalToBase(127, 16));
        assertEquals("FF", decimalToBase(255, 16));
        assertEquals("7FFFFFFF", decimalToBase(Integer.MAX_VALUE, 16));
    }

    @Test
    public void testConversionToBase36() {
        assertEquals("0", decimalToBase(0, 36));
        assertEquals("1", decimalToBase(1, 36));
        assertEquals("A", decimalToBase(10, 36));
        assertEquals("F", decimalToBase(15, 36));
        assertEquals("G", decimalToBase(16, 36));
        assertEquals("Z", decimalToBase(35, 36));
        assertEquals("10", decimalToBase(36, 36));
        assertEquals("ZZ", decimalToBase(1295, 36));
    }
}
