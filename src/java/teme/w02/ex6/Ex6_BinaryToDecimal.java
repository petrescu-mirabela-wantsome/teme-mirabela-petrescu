package teme.w02.ex6;

/**
 * 6. Binary to decimal:
 * Write a function that receives a number’s string representation in base 2 and returns the
 * number itself as an int (in base 10).
 * E.g. binaryToDecimal(“11”) -> 3 , binaryToDecimal(“101”) -> 5
 * Hint: to iterate through the characters in a string, you may either use String.charAt()
 * method (+String.length), OR you may convert it to a char array and iterate over that (you
 * also have to convert each char to its corresponding digit, like ‘0’(=48) -> 0; you may
 * create a separate method for that ..) :
 * String numberInBinary = "110101";
 * char[] binaryDigits = numberInBinary.toCharArray();
 */

public class Ex6_BinaryToDecimal {

    static int binaryToDecimal(String binaryNumber) {

/**
 *  Solution 1: Convert the string to a char array and iterate over that
 */
       /* int decimalNumber = 0;
        char[] binaryNumberArray = binaryNumber.toCharArray();
        for (int i = 0; i <= binaryNumberArray.length - 1; i++) {
            if (binaryNumberArray[i] == '1') {
                decimalNumber += Math.pow(2, binaryNumberArray.length - 1 - i);
            }
            //decimalNumber += binaryNumberArray[i] * Math.pow(2, binaryNumberArray.length - 1 - i);
        }
        return decimalNumber;*/


/**
 * Solution2: Iterate through the characters in a string, you may either use String.charAt() method (+String.length)
 */
        int decimalNumber = 0;
        for (int i = 0; i < binaryNumber.length(); i++) {
            if (binaryNumber.charAt(i) == '1')
                decimalNumber += (int) Math.pow(2, (binaryNumber.length() - 1 - i));

        }
        return decimalNumber;
    }

    //for manual testing
    public static void main(String[] args) {
        System.out.println(binaryToDecimal("0"));
        System.out.println(binaryToDecimal("11"));
        System.out.println(binaryToDecimal("101"));
        System.out.println(binaryToDecimal("1010"));
    }
}
