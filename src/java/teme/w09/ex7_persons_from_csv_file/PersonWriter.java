package teme.w09.ex7_persons_from_csv_file;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/*
PersonWriter for writing a List<Person> to a CSV file
 */
class PersonWriter {

    public void writePersonToCsv(List<Person> listPersonWriter, String csvFilePath) throws IOException {
        StringBuilder stringToCsvCopy = new StringBuilder();
        for (Person itemPerson : listPersonWriter) {
            stringToCsvCopy.append(itemPerson.getName()).append(",").append(itemPerson.getCnp()).append(",").append(itemPerson.getHeight()).append(",").append(itemPerson.isSex()).append("\n");
        }
        writeToFileInfo(stringToCsvCopy.toString(), csvFilePath);
    }

    private void writeToFileInfo(String text, String csvFilePath) throws IOException {
        try (OutputStream out = new FileOutputStream(csvFilePath)) {
            char[] cArray = text.toCharArray();
            for (char textChar : cArray) {
                out.write(textChar);
            }
        }
    }
}
