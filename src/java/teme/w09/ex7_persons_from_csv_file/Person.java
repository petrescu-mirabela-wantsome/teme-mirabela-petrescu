package teme.w09.ex7_persons_from_csv_file;

import java.util.Objects;

enum SEX {
    F,
    M
}

/*
Field types:
name - a String
cnp - a long number
age, height - a positive integer value
sex - one of these 2 values: 'F'/'M'
 */
class Person {
    private final String name;
    private final long cnp;
    private final SEX sex;
    private int age;
    private int height;

    public Person(String name, long cnp, int age, int height, SEX sex) {
        this.name = name;
        this.cnp = cnp;
        if (age > 0) {
            this.age = age;
        }
        if (height > 0) {
            this.height = height;
        }
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public long getCnp() {
        return cnp;
    }

    public int getAge() {
        return age;
    }

    public int getHeight() {
        return height;
    }

    public SEX isSex() {
        return sex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return cnp == person.cnp &&
                age == person.age &&
                height == person.height &&
                sex == person.sex &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, cnp, age, height, sex);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", cnp=" + cnp +
                ", age=" + age +
                ", height=" + height +
                ", sex=" + sex +
                '}';
    }
}
