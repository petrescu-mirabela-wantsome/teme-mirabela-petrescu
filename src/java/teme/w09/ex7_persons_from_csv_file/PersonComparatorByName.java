package teme.w09.ex7_persons_from_csv_file;

/*
PersonComparator (for sorting by name)
 */

import java.util.Comparator;

class PersonComparatorByName implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
