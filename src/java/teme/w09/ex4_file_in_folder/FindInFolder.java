package teme.w09.ex4_file_in_folder;

/*
4. Find in folder
- Read from user: the path of a base directory and a pattern to search for (a String value),
and then search under that base dir (and all its subfolders ) for a file/dir which contains in
its name the given pattern (case insensitive)
- You should stop and return only the first file found (we don't care if there are multiple
matching files), and display its full path, or display another message if no file was found
Example:
- Given path: ".\doc":
- for pattern: "week8_TE" it should find and display the path of this file
(if started from this project, like: "C:\....\doc\s4_week8_tema.txt")
- for pattern: "week9_" it should display a message like: "No files found matching pattern:
'week9_'"
BONUS:
- if a file was found, show more info about it: name, size, if it's a dir or not (how can you do
that in more optimal way, what could you change to the signature of your recursive
function?)
- show all the matching files, not just the first one (show full info for each, not just name)
 */

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.Collectors;

public class FindInFolder {

    private static String pattern;
    private final List<File> filesFromPath = new ArrayList<>();
    private final List<FileProperty> listFileProperty = new ArrayList<>();

    public static void main(String[] args) {

        FindInFolder files = new FindInFolder();
        String path = FindInFolder.readPath();
        File dir = new File(path);
        if (dir.exists()) {     // the file or directory denoted by this abstract pathname exists
            System.out.println("\n >>>>>>>>>> display all files from the path " + path + " >>>>>>>>>>>>");
            files.recursiveSearch(dir);
            files.addFileProperty();

            System.out.println("\n >>>>>>>>>> display first files which contains the pattern " + pattern + " >>>>>>>>>>>>");
            pattern = FindInFolder.readPatternToSearch();
            files.searchFirstByPattern();

            System.out.println("\n >>>>>>>>>> display all files which contains the pattern " + pattern + " >>>>>>>>>>>>");
            files.searchAllByPattern();
        } else {
            System.out.println("!!!!!! Not valid directory (path) name");
        }
    }

    /*
    Read from user: the path of a base directory and a pattern to search for (a String value)
     */
    private static String readPath() {
        System.out.println("Enter the path of a base directory (example: d:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\curs\\doc\\ )");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    private static String readPatternToSearch() {
        System.out.println("Enter pattern to search (example: week8_TE): ");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }

    /*
    search under that base dir (and all its subfolder) - all the files/directories are listed
     */
    private void recursiveSearch(File dir) {
        // the file or directory denoted by this abstract pathname exists
        File[] filesOrDir = dir.listFiles();
        for (File itemFileOrDir : filesOrDir != null ? filesOrDir : new File[0]) {
            if (itemFileOrDir.isDirectory()) {
                filesFromPath.add(itemFileOrDir);
                System.out.println(itemFileOrDir);
                recursiveSearch(itemFileOrDir);
            }
            if (!itemFileOrDir.isDirectory()) {
                    /*
                    add into a list all the files from the path given as input
                     */
                filesFromPath.add(itemFileOrDir);
                System.out.println(itemFileOrDir);
            }
        }
    }

    /*
    add into the list the property of each file
    */
    private void addFileProperty() {
        for (File itemFileOrDir : filesFromPath) {
            if (itemFileOrDir.isDirectory()) {     // directory case
                File[] files = itemFileOrDir.listFiles();
                // calculate total length of each directory
                long dirLength = 0;
                for (File itemFile : files != null ? files : new File[0]) {
                    dirLength += itemFile.length();     // total length of each directory (length sum all of the files under the folder)
                }
                listFileProperty.add(new FileProperty(itemFileOrDir.isDirectory(), itemFileOrDir.getName(), dirLength));
            }
            if (!itemFileOrDir.isDirectory()) {   // file case
                listFileProperty.add(new FileProperty(itemFileOrDir.isDirectory(), itemFileOrDir.getName(), itemFileOrDir.length()));
            }

        }
    }

    /*
    You should stop and return only the first file found (we don't care if there are multiple
    matching files), and display its full path, or display another message if no file was found
     */
    private void searchFirstByPattern() {
        Optional<FileProperty> filePattern = listFileProperty.stream()
                .filter(file -> file.getFileName().toLowerCase().contains(pattern.toLowerCase()))
                // if a file was found, show more info about it: name, size, type
                .findFirst();
        if (filePattern.isPresent()) {
            System.out.println(filePattern.toString());
        } else {
            System.out.println("No files found matching pattern: " + pattern);
        }
    }

    /*
    search under that base dir (and all its subfolders ) for a file/dir which contains in
    its name the given pattern (case insensitive)
    show all the matching files, not just the first one (show full info for each, not just name)
     */
    private void searchAllByPattern() {
        List<FileProperty> fileByPattern = listFileProperty.stream()
                .filter(file -> file.getFileName().toLowerCase().contains(pattern.toLowerCase()))
                .collect(Collectors.toList());

        // if a file was found, show more info about it: name, size, type
        if (!fileByPattern.isEmpty()) {
            System.out.println(fileByPattern.toString());
        } else {
            System.out.println("No files found matching pattern: " + pattern);
        }
    }
}
