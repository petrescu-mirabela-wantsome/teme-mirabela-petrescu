package teme.w09.ex6_find_all_files;

import java.util.Comparator;

class FileComparatorBySize implements Comparator<FileProperty> {

    @Override
    public int compare(FileProperty o1, FileProperty o2) {
        return -Long.compare(o1.getFileSize(), o2.getFileSize());   // descending order by size
    }
}
