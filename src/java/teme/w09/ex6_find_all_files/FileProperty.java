package teme.w09.ex6_find_all_files;

class FileProperty {

    private final String filePath;
    private final boolean fileType;
    private final String fileName;
    private final long fileSize;

    public FileProperty(String filePath, boolean fileType, String fileName, long fileSize) {
        this.filePath = filePath;
        this.fileType = fileType;
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public boolean isFileType() {
        return fileType;
    }

    public long getFileSize() {
        return fileSize;
    }

    @Override
    public String toString() {
        return "FileProperty{" +
                "filePath='" + filePath + '\'' +
                ", fileType(Directory=true, File=false)=" + fileType +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                "}\n";
    }
}
