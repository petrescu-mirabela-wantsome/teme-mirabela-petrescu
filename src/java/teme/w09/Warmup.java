package teme.w09;

import java.io.*;
import java.util.Properties;

public class Warmup {

    public static void main(String[] args) throws IOException {
        readproperties();
        listChildren();
    }

    private static void readproperties() throws IOException {
        System.out.println("Reading properties from d:\\wantsome_files\\input");
        Properties prop = new Properties();
        try (InputStream in = new FileInputStream("d:\\wantsome_files\\input\\config.properties")) {
            prop.load(in);
        }
        System.out.println("Loaded properties: " + prop);

//        prop.getProperty("connection", "jdbc:oracle:thin@kdksjja");

        prop.setProperty("key5", "value5");
        try (OutputStream out = new FileOutputStream("d:\\wantsome_files\\output\\config.properties")) {
            prop.store(out, "I updated the config file");
        }
    }

    private static void listChildren() {
        File dir = new File("d:\\"); // create new file object
        String[] paths = dir.list(); // get child files/folders
        System.out.println("Contents of folder '" + dir.getPath() + "'");
        for (String path : paths != null ? paths : new String[0]) { // for each name in the path array
            System.out.println(path); // prints file path
        }
    }
}
