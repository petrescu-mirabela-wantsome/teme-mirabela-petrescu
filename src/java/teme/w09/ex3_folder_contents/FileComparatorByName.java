package teme.w09.ex3_folder_contents;

import java.util.Comparator;

class FileComparatorByName implements Comparator<FileProperty> {

    @Override
    public int compare(FileProperty o1, FileProperty o2) {
        return o1.getFileName().compareTo(o2.getFileName());
    }
}
