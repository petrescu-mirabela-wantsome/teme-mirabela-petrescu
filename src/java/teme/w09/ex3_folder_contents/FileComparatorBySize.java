package teme.w09.ex3_folder_contents;

import java.util.Comparator;

class FileComparatorBySize implements Comparator<FileProperty> {

    @Override
    public int compare(FileProperty o1, FileProperty o2) {
        return -Long.compare(o1.getFileSize(), o2.getFileSize());
    }
}
