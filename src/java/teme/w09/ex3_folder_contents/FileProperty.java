package teme.w09.ex3_folder_contents;

class FileProperty {

    private final boolean fileType;
    private final String fileName;
    private final long fileSize;

    public FileProperty(boolean fileType, String fileName, long fileSize) {
        this.fileType = fileType;
        this.fileName = fileName;
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public long getFileSize() {
        return fileSize;
    }

    public boolean isFileType() {
        return fileType;
    }


    @Override
    public String toString() {
        return "FileProperty{" +
                "fileType(Directory=true, File=false)=" + fileType +
                ", fileName='" + fileName + '\'' +
                ", fileSize=" + fileSize +
                '}';
    }
}
