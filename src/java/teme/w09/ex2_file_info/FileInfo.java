package teme.w09.ex2_file_info;

import java.io.File;
import java.util.Date;
import java.util.Scanner;

/*
2. File Info
Given the path of a file, print this info about it:
- if file exists
- its absolute path, type (dir or file), size and last update date
- if it's writable
 */
public class FileInfo {

    public static void main(String[] args) {

        String path = readPath();
        File file = new File(path);
        if (file.exists()) {
            dirOrFileInfo(file);
        } else {
            System.out.println("\n !!!!!!!! Not a valid path");
        }
    }

    private static void dirOrFileInfo(File file) {
        if (file.isFile()) {
            System.out.println("\n >>>>>>>>> FILE CASE >>>>>>>>>>>\n");
            System.out.println("File exist: " + file.exists());
            System.out.println("Absolute path: " + file.getAbsolutePath());
            System.out.println("Type file: " + file.isFile());
            System.out.println("Type directory: " + file.isDirectory());
            System.out.println("Size: " + file.length());               //file size (in bytes)
            System.out.println("Last update date: " + file.lastModified());     // date of last update (type is 'long', in ms!)
            System.out.println(new Date(file.lastModified()).toString());       //date in readable format..
            System.out.println("Is writable: " + file.canWrite());
            System.out.println("Is readable: " + file.canRead());
            System.out.println("Is executable: " + file.canExecute());
            String path2 = "d:" + File.separator + "wantsome_files2"; //relative, portable format!
            file.renameTo(new File(path2));
        }
        if (file.isDirectory()) {
            System.out.println("\n >>>>>>>>> DIRECTORY CASE >>>>>>>>>>>\n");
            String[] folders = file.list();
            for (String folderItem : folders != null ? folders : new String[0]) {
                System.out.println("Folder name: " + folderItem);
            }
            File[] files = file.listFiles();
            for (File fileItem : files != null ? files : new File[0]) {
                System.out.println("File name: " + fileItem);
            }
        }

    }

    /*
Read from user: the path of a base directory and a pattern to search for (a String value)
 */
    private static String readPath() {
        System.out.println("Enter the path of a base directory (example: d:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\curs\\doc\\ )");
        System.out.println("Enter the path of a base directory (example: d:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\curs\\doc\\w01_intro_curs.pdf )");
        Scanner in = new Scanner(System.in); //need this first, to read from input
        return in.next();
    }
}
