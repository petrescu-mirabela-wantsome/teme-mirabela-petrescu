package teme.w10.ex3_hangman;

/*
3) Hangman

Code the 'hangman' game:
- computer reads some words from a text file (dictionary, contains one word per line) and chooses a random one
- it then prints the first and last letter of the word, and some '_' for each letter between
- user is given a number of attempts (like 7) to guess the remaining letters: computer reads a letter, and if it belongs to the word,
  it displays it in the proper location (instead of _); if letter doesn't belong to the word, user looses an attempt
- if user guesses all letters, he wins; if he runs out of attempts, he looses

Extra ideas:
- make the game have 3 difficulty levels: easy, medium, hard;  depending on them, select only shorter or longer words for the dictionary files
- depending on word's length and/or difficulty level, show him a different number of letters at start (2-3-4..)
- also change your code to display first the letters from some random positions (instead of always the first and last letters)
 */

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Hangman {

    private static final int TRIES = 7;
    private static String hangmanFilePath;
    private static String hangmanLevel;
    private List<String> listAllHangmanWords = new ArrayList<>();
    private char[] charFoundFromWord;
    private char[] hangmanWord;

    public static void main(String[] args) {

        try {
            Hangman hangman = new Hangman();
            hangmanFilePath = hangman.readFromUser("Hangman file path: (example: D:\\Trainings\\Wantsome\\teme_mirabela_petrescu\\src\\java\\teme\\w10\\ex3_hangman\\words.txt)");
            hangman.readInputFile();
            hangmanLevel = hangman.readFromUser("Choose a level of difficulty: easy (e), medium (m), hard (h) ");
            if (hangmanLevel.equals("e") || hangmanLevel.equals("m") || hangmanLevel.equals("h")) {
                List<String> listWordsEachLevel = hangman.selectWordsEachLevel();
                int randomIndex = hangman.randomIndexGenerator(listWordsEachLevel);
                hangman.hangmanWord = (hangman.chooseRandomString(listWordsEachLevel, randomIndex)).toCharArray();
                hangman.charFoundFromWord = new char[hangman.hangmanWord.length];
                System.out.println(hangman.hangmanWord);
                hangman.printHangmanWord();
            } else {
                System.out.println("Wrong difficulty level. It shall be easy (e), medium (m), hard (h) ");
            }

        } catch (IOException e) {
            System.out.println(" !!!!! No hangman file introduced");
        }
    }

    private String readFromUser(String text) {
        System.out.println(text);
        Scanner in = new Scanner(System.in);
        return in.next();
    }

    private void readInputFile() throws IOException {
        //List<String> listAllHangmanWords = new ArrayList<>();
        File file = new File(hangmanFilePath);
        Scanner fileScanner = new Scanner(file);
        while (fileScanner.hasNext()) {
            listAllHangmanWords.add(fileScanner.nextLine());
        }
    }

    private int randomIndexGenerator(List<String> listHangmanWords) {
        Random randomIndex = new Random();
        return randomIndex.nextInt(listHangmanWords.size());
    }

    private String chooseRandomString(List<String> listHangmanWords, int randomIndex) {
        return listHangmanWords.get(randomIndex);
    }

    /*
    make the game have 3 difficulty levels: easy, medium, hard;  depending on them, select only shorter or longer words for the dictionary files
     */
    private List<String> selectWordsEachLevel() {
        switch (hangmanLevel) {
            case "e":
                return listAllHangmanWords
                        .stream()
                        .filter(s -> s.length() < 5)
                        .collect(Collectors.toList());
            case "m":
                return listAllHangmanWords
                        .stream()
                        .filter(s -> s.length() >= 5 && s.length() < 10)
                        .collect(Collectors.toList());
            case "h":
                return listAllHangmanWords
                        .stream()
                        .filter(s -> s.length() >= 10)
                        .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    /*
    - it then prints the first and last letter of the word, and some '_' for each letter between
    - user is given a number of attempts (like 7) to guess the remaining letters: computer reads a letter, and if it belongs to the word,
  it displays it in the proper location (instead of _); if letter doesn't belong to the word, user looses an attempt
     */
    private void printHangmanWord() {
        int[] indexesArray;
        int countTries = 0;
        int numberOfIndexes = 0;
        /*
        user is given a number of attempts (like 7) to guess the remaining letters: computer reads a letter, and if it belongs to the word,
        it displays it in the proper location (instead of _); if letter doesn't belong to the word, user looses an attempt
         */
        switch (hangmanLevel) {
            case "e":
                numberOfIndexes = 1;    // generate 1 indexes - easy level
                break;
            case "m":
                numberOfIndexes = 2;    // generate 2 indexes - medium level
                break;
            case "h":
                numberOfIndexes = 3;    // generate 3 indexes - hard level
                break;
        }
        indexesArray = generateRandomIndex(hangmanWord.length, numberOfIndexes);     // generate 1 indexes - easy level
        printHangmanWordBeforeTry(indexesArray);
        while (countTries < TRIES) {
            printHangmanWordEachTry(indexesArray);
            System.out.print(Arrays.toString(charFoundFromWord));
            countTries++;
            System.out.println();
            if (Arrays.equals(hangmanWord, charFoundFromWord)) {
                System.out.println("!!!! Game over, word found");
                break;
            }
        }
    }

    private void printHangmanWordEachTry(int[] indexesArray) {
        String charFromUser = readFromUser("Introduce a char from the Hangman word: ").toLowerCase();
        for (int i = 0; i < hangmanWord.length; i++) {
            final int j = i;    // stream requires the key to be a final variable
            // if the char is in the hangman word and it was not found until this moment
            if (charFromUser.equals(String.valueOf(hangmanWord[i])) &&
                    (!Arrays
                            .stream(indexesArray)
                            .anyMatch(index -> index == j))) {
                charFoundFromWord[i] = hangmanWord[i];
            }
        }
    }

    /*
    also change your code to display first the letters from some random positions (instead of always the first and last letters)
     */
    private void printHangmanWordBeforeTry(int[] indexesArray) {
        for (int i = 0; i < hangmanWord.length; i++) {
            final int j = i;    // stream requires the key to be a final variable
            if (Arrays
                    .stream(indexesArray)
                    .anyMatch(index -> index == j)) {
                charFoundFromWord[i] = hangmanWord[i];
            } else {
                charFoundFromWord[i] = '_';
            }
        }
        System.out.print(Arrays.toString(charFoundFromWord));
        System.out.println();
    }

    /*
    generate a random index between 0 and length - 1
    (Method Porpoise: also change your code to display first the letters from some random positions (instead of always the first and last letters))
     */
    private int[] generateRandomIndex(int length, int numberOfIndexes) {
        int[] indexesArray = new int[numberOfIndexes];
        for (int i = 0; i < numberOfIndexes; i++) {
            Random index = new Random();
            indexesArray[i] = index.nextInt(length);
            System.out.println("Random index: " + indexesArray[i]);
        }
        return indexesArray;
    }
}
