package teme.w10.ex5_vote_validation;

import java.util.Date;

/*
input file content is in CSV format (no header row), with these columns:
    name (string), cnp (string, 13 digits), date (a date+time value, format: 'yyyy-MM-dd HH:mm:ss')
 */
class CsvInputProperty {
    private String name;
    private String cnp;
    private Date date;

    public CsvInputProperty(String name, String cnp, Date date) {
        this.name = name;
        this.cnp = cnp;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getCnp() {
        return cnp;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "CsvInputProperty{" +
                "name='" + name + '\'' +
                ", cnp='" + cnp + '\'' +
                ", date=" + date +
                '}';
    }
}
