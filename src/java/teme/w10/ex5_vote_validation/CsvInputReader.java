package teme.w10.ex5_vote_validation;

/*
Read info from all csv files from input folder
*/


import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static teme.w10.ex5_vote_validation.VoteValidation.listCsvInvalidVoter;

class CsvInputReader {
    private List<CsvInputProperty> listCsvInputReader = new ArrayList<>();

    public List<CsvInputProperty> getListCsvInputReader() {
        return listCsvInputReader;
    }

    @Override
    public String toString() {
        return "CsvInputReader{" +
                "listCsvInputReader=" + listCsvInputReader +
                '}';
    }

    public void readCsvInput(File csvFile) throws FileNotFoundException, ArrayIndexOutOfBoundsException {
        int lineCount = 0;
        Scanner fileScanner = new Scanner(csvFile);
        // hints: for date operations, see: http://tutorials.jenkov.com/java-date-time/parsing-formatting-dates.html
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        while (fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            String[] csvLineArray = line.split(",");
            lineCount++;
            try {
                // for successfully parsed rows
                if (statusLineCsv(line, lineCount)) {
                    try {
                        Date date = format.parse(csvLineArray[2]);
                        listCsvInputReader.add(new CsvInputProperty(csvLineArray[0], csvLineArray[1], date));
                    } catch (ParseException e) {
                        System.out.println(" !!!! Date parser NOK");
                    }
                }
            } catch (NumberFormatException e1) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));
            } catch (ArrayIndexOutOfBoundsException e2) {
                System.out.println("Line " + lineCount + " has less fields than expected => " + Arrays.toString(csvLineArray));
            }
        }
    }

    /*
    voter age (computed from CNP value and current date) should be >= 18 years
     */
    private int voterAge(String cnp) {
        char[] cnpArray = cnp.toCharArray();
        String birthDateString = "19" + Integer.valueOf(cnpArray[1] + "") + Integer.valueOf(cnpArray[2] + "") + "-" + Integer.valueOf(cnpArray[3] + "") + Integer.valueOf(cnpArray[4] + "") + "-" + Integer.valueOf(cnpArray[5] + "") + Integer.valueOf(cnpArray[6] + "");
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date birthDate = format.parse(birthDateString);
            long ageInMillis = new Date().getTime() - birthDate.getTime();
            Date age = new Date(ageInMillis);
            age.getYear();
        } catch (ParseException e) {
            System.out.println(" !!!! Wrong parse date - isVoter");
        }
        return 0;
    }

    /*
      - for successfully parsed rows, perform some extra validation:
    - CNP format should be correct;
    - CNP values should be unique in whole set of rows
    - voter age (computed from CNP value and current date) should be >= 18 years
    - voter name should contain at least 2 parts (separated by spaces), and each part should have length of at least 3 chars
     */
    private boolean statusLineCsv(String lineText, int lineCount) {
        String[] csvLineArray = lineText.split(",");
        String[] nameArray = csvLineArray[0].split(" ");
        int countChecks = 0;
        String errorMessage;
        if (nameArray.length != 2) {
            errorMessage = "Line " + lineCount + " => Voter name without 2 parts (separated by spaces): " + nameArray[0];
            System.out.println(errorMessage);
            listCsvInvalidVoter.add(lineText);
            countChecks += 1;
        }
        if (nameArray[0].toCharArray().length >= 3 && nameArray[1].toCharArray().length < 3) {
            errorMessage = "Line " + lineCount + " => Voter name with 2 parts, each part has a length of at most 2 chars: " + nameArray[0];
            System.out.println(errorMessage);
            listCsvInvalidVoter.add(lineText);
            countChecks += 1;
        }
        if (csvLineArray[1].toCharArray().length != 13) {
            errorMessage = "Line " + lineCount + " => cnp " + csvLineArray[1] + " with digits less than 13 ";
            System.out.println(errorMessage);
            listCsvInvalidVoter.add(lineText);
            countChecks += 1;
            int voterAgeValue = voterAge(csvLineArray[1]);
            if (voterAgeValue >= 18) {
                errorMessage = "Line " + lineCount + " => Voter has " + voterAgeValue + " year, age less than 18";
                System.out.println(errorMessage);
                listCsvInvalidVoter.add(lineText);
                countChecks += 1;
            }
        }
        return countChecks <= 0;
    }
}