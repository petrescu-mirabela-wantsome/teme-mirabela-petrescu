package teme.w10.ex4_tic_tac_toe;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

/*
4) Three in a row (TicTacToe)

Implement the game 'three in a row':
- there is an empty board, size 3x3
- 2 players, each marking a cell of the board with his sign ('X' or 'O'), taking turns
- first one which has 3 of his marks in a line (in any direction) wins
- if table fills up first, nobody wins

a) Implement the basic game first:
  - should be able to play 1 game
  - displaying the board after each move - simple text mode UI, just display the rows each on a separate line,
    with cell contents separated by space, each cell shown as 'X', 'O' or '_' (for empty)
  - get the positions players want to fill by reading from console 2 int values (row and column of the cell)
  - should validate the attempted move is valid, and also detect when the game ended, and display the winner.

b) Change your code so players can specify the cell address by a string like 'A2' - where 'A'..'C' represents the row,
  and '1'..'3' the column; to help him visualize this, display 1..3 above the board columns, and A..C in front of the rows.

c) Make the player names configurable: read them at the start of game, and show them when reading the next move, and on final results.

d) Add support for playing multiple games in a row:
  - at the end of one game, ask the users if they want to continue with another game (Y/N)
  - if he chooses Y, start a new game (resetting the board, etc)
  - you need to keep the overall score (considering all played games), which you should update and display at the end of each game
  - the starting player should vary between games: game 1 stars with player 1, game 2 with player 2, game 3 with player 1 again etc..

e) Add support for board of arbitrary size:
  - at the start of the program, ask the user for the board size - must be between 3 and 26 (default may be 3)
  - you should then work with a board of that size - displaying it, reading moves
  - the rule for winning: for board size <=4, player wins with in a row; for board size >= 5, player wins with 5 in a row.

f) Optional: improve your display of the board by using the special ASCII characters for displaying margins and corners.
   See: https://theasciicode.com.ar/extended-ascii-code/box-drawings-single-vertical-line-character-ascii-code-179.html
   You can copy-paste them each from that page, and they should allow you do draw tables with nice margins, like this:

       1   2   3
     ┌───┬───┬───┐
   A │ X │ O │ X │
     ├───┼───┼───┤
   B │ X │ O │ X │
     ├───┼───┼───┤
   C │ O │   │ X │
     └───┴───┴───┘
 */
public class TicTacToe {

    private static String[] playersList;       // USER1 will start the game
    private static int matrixDimension;
    private static int numberPlayers;      // the number of players (at least 2 shall be)
    private static char[] signList = new char[]{'X', '0', '#', '%', '&'};

    private static char[][] initFillMatrix() {
        char[][] initMatrix = new char[matrixDimension][matrixDimension];
        for (int i = 0; i < initMatrix.length; i++) {
            for (int j = 0; j < initMatrix.length; j++) {
                initMatrix[i][j] = '_';
            }
        }
        return initMatrix;
    }

    private static char[][] fillMatrix(char[][] initMatrix, int iUser, int jUser, int indexUser) {

        initMatrix[iUser][jUser] = signList[indexUser];    // each player will have a predefine sign during all the games

        return initMatrix;
    }

    /*
    - displaying the board after each move - simple text mode UI, just display the rows each on a separate line,
        with cell contents separated by space, each cell shown as 'X', 'O' or '_' (for empty)
    - get the positions players want to fill by reading from console 2 int values (row and column of the cell)
     */
//    public static void printMatrix(char[][] initMatrix) {
//        System.out.print("   0 1 2");
//        System.out.println();
//        System.out.println("   ------");
//        for (int i = 0; i < initMatrix.length; i++) {
//            System.out.print(i + "| ");
//            for (int j = 0; j < initMatrix.length; j++) {
//                System.out.print(initMatrix[i][j] + " ");
//            }
//            System.out.println();
//        }
//    }

    /*
    b) Change your code so players can specify the cell address by a string like 'A2' - where 'A'..'C' represents the row,
  and '1'..'3' the column; to help him visualize this, display 1..3 above the board columns, and A..C in front of the rows.
     */
    // Link: http://ascii-table.com/ascii-extended-pc-list.php
    private static void printMatrixABC(char[][] initMatrix) {
        System.out.print("    ");
        for (int i = 1; i <= initMatrix.length; i++) {
            System.out.print(i + "   ");
        }
        System.out.println();
        System.out.print("  " + (char) 0x250C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "");
        for (int i = 1; i < initMatrix.length - 1; i++) {
            System.out.print((char) 0x252C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "");
        }
        System.out.print((char) 0x252C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2510 + "");
        System.out.println();
        for (int i = 0; i < initMatrix.length; i++) {
            System.out.print((char) ('A' + i) + " " + (char) 0x2502);
            for (int j = 0; j < initMatrix.length; j++) {
                System.out.print(" " + initMatrix[i][j] + " " + (char) 0x2502 + "");
            }
            System.out.println();
            if (i < initMatrix.length - 1) {
                //System.out.println("  " + (char) 0x251C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x253C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x253C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2524 + "");
                System.out.print("  " + (char) 0x251C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "");
                for (int k = 1; k < initMatrix.length - 1; k++) {
                    System.out.print((char) 0x253C + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "");
                }
                System.out.println((char) 0x2534 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2524 + "");
            }
        }
        System.out.print("  " + (char) 0x2514 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "");
        for (int i = 1; i < initMatrix.length - 1; i++) {
            System.out.print((char) 0x2534 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "");
        }
        System.out.print((char) 0x2534 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2500 + "" + (char) 0x2518 + "");
    }

    /*
    b) Change your code so players can specify the cell address by a string like 'A2' - where 'A'..'C' represents the row,
  and '1'..'3' the column; to help him visualize this, display 1..3 above the board columns, and A..C in front of the rows.
     */
    private static int[] getOptionFromUserABC() {
        int[] iAndjFromUser = new int[2];
        System.out.print("Introduce the cell address (ex. A2, - where 'A'..'C' represents the row, and '1'..'3' the column;): ");
        String cellAddressFromUser = new Scanner(System.in).next();

        char[] charArrayAddressFromUser = cellAddressFromUser.toCharArray();
        iAndjFromUser[0] = charArrayAddressFromUser[0] - 'A';
        iAndjFromUser[1] = charArrayAddressFromUser[1] - '1';
        if (iAndjFromUser[0] >= 0 && iAndjFromUser[0] < matrixDimension &&
                iAndjFromUser[1] >= 0 && iAndjFromUser[0] < matrixDimension &&
                charArrayAddressFromUser.length == 2) {
            return iAndjFromUser;
        } else {
            System.out.println("!!! Wrong cell address.");
            System.out.print("Introduce the cell address (ex. A2, - where 'A'..'C' represents the row, and '1'..'3' the column;): ");
            cellAddressFromUser = new Scanner(System.in).next();
            charArrayAddressFromUser = cellAddressFromUser.toCharArray();
            iAndjFromUser[0] = charArrayAddressFromUser[0] - 'A';
            iAndjFromUser[1] = charArrayAddressFromUser[1] - '1';
        }
        return iAndjFromUser;
    }

    private static String getUserInfo(String text) {
        System.out.print(text);
        return new Scanner(System.in).next();
    }

    private static boolean statusTicTacToeGameMoreThan2Players(char[][] finalMatrix) {
        int countPositions = 0;
        int sameValuesInLineToWin = finalMatrix.length;
        /*
        - the rule for winning: for board size <=4, player wins with in a row; for board size >= 5, player wins with 5 in a row.
         */
        if (finalMatrix.length >= 5) {
            sameValuesInLineToWin = 5;
        }

        /*
        check if all the cells are filled by users
         */
        for (int i = 0; i < finalMatrix.length; i++) {
            for (int j = 0; j < finalMatrix.length; j++) {
                if (finalMatrix[i][j] != '_') {
                    countPositions++;
                }
            }
            if (countPositions == finalMatrix.length * finalMatrix.length) {
                return true;      // all the cells are filled by users
            }
        }

        /*
        check if one of the users has a full line
         */
        StringBuilder playerOptionPerLine;
        for (int i = 0; i < finalMatrix.length; i++) {
            playerOptionPerLine = new StringBuilder();     // next line is verified => clear the map
            for (int j = 0; j < finalMatrix.length; j++) {
                if (finalMatrix[i][j] != '_') {
                    playerOptionPerLine.append(finalMatrix[i][j]);
                }
            }
            if (findAnyFullLineInMatrix(sameValuesInLineToWin, playerOptionPerLine.toString())) {
                return true;
            }
        }

        /*
        check if one of the users has a full column
         */
        StringBuilder playerOptionPerColumn;
        for (int j = 0; j < finalMatrix.length; j++) {
            playerOptionPerColumn = new StringBuilder();     // next line is verified => clear the map
            for (int i = 0; i < finalMatrix.length; i++) {
                if (finalMatrix[i][j] != '_') {
                    playerOptionPerColumn.append(finalMatrix[i][j]);
                }
            }
            if (findAnyFullLineInMatrix(sameValuesInLineToWin, playerOptionPerColumn.toString())) {
                return true;
            }
        }

        /*
        check if one of the users has a full first diagonal
         */
        StringBuilder playerOptionPerFirstDiagonal = new StringBuilder();
        for (int i = finalMatrix.length - 1, j = 0; i >= 0 && j < finalMatrix.length; i--, j++) {
            if (finalMatrix[i][j] != '_') {
                playerOptionPerFirstDiagonal.append(finalMatrix[i][j]);
            }
            if (findAnyFullLineInMatrix(sameValuesInLineToWin, playerOptionPerFirstDiagonal.toString())) {
                return true;
            }
        }

        /*
        check if one of the users has a full second diagonal
         */
        StringBuilder playerOptionPerSecondDiagonal = new StringBuilder();
        for (int i = 0; i < finalMatrix.length; i++) {
            if (finalMatrix[i][i] != '_') {
                playerOptionPerSecondDiagonal.append(finalMatrix[i][i]);
            }
            if (findAnyFullLineInMatrix(sameValuesInLineToWin, playerOptionPerSecondDiagonal.toString())) {
                return true;
            }
        }

        return false;
    }

    private static boolean findAnyFullLineInMatrix(final int sameValuesInLineToWin, String playerOptionPerLine) {
        Map<String, Long> mapPlayerOptPerLineCount;
        // key - player sign (X/0/# and so on, depending on the number of players)
        // value - number of the same signs on a line

        String[] stringPlayersOptionsPeLine = playerOptionPerLine.split("");

        mapPlayerOptPerLineCount =
                Arrays
                        .stream(stringPlayersOptionsPeLine)
                        .collect(Collectors.groupingBy(s -> s, Collectors.counting()));

        return mapPlayerOptPerLineCount
                .values()
                .stream()
                .anyMatch(s -> s == sameValuesInLineToWin);
    }

    /*
    Introduce the user name
    */
    private static String[] getPlayersList(int numberPlayers) {
        String[] userList = new String[numberPlayers];
        for (int i = 0; i < numberPlayers; i++) {
            userList[i] = getUserInfo("Introduce the user name: ");
        }
        return userList;
    }

    /*
    one round game is implemented here
     */
    private static void getOneTicTacToeGame(int gameNumberPerformed) {

        /*
        initial matrix -> all values are _
         */
        char[][] initMatrix = initFillMatrix();
        printMatrixABC(initMatrix);
        System.out.println();

        int[] iAndjFromUser;
        /*
        users will introduce some values until all the matrix is filled
         */
        int indexUser = gameNumberPerformed;
        while (!statusTicTacToeGameMoreThan2Players(initMatrix)) {        // as long as the cells are not filled by the users
            System.out.println("Current player name is: " + playersList[indexUser] + " => Player sign: " + signList[indexUser]);
            iAndjFromUser = getOptionFromUserABC();
            if (initMatrix[iAndjFromUser[0]][iAndjFromUser[1]] == '_' && iAndjFromUser[0] < initMatrix.length && iAndjFromUser[0] >= 0 &&
                    iAndjFromUser[1] < initMatrix.length && iAndjFromUser[1] >= 0) {
                fillMatrix(initMatrix, iAndjFromUser[0], iAndjFromUser[1], indexUser);
                printMatrixABC(initMatrix);
                System.out.println();
                indexUser = indexUser + 1 == numberPlayers ? 0 : indexUser + 1;
            } else {
                System.out.println("Wrong line was introduced");
            }
        }
        /*
        Display the winner
        */
        System.out.println("!!!! THE WINNER IS: " + playersList[indexUser - 1]);
    }

    /*
    - at the end of one game, ask the users if they want to continue with another game (Y/N)
    - if he chooses Y, start a new game (resetting the board, etc)
     */
    private static String getUserOptionNewGame() {
        System.out.print("Do you want to start current / other game (Y/N)? ");
        return new Scanner(System.in).next();
    }

    private static void validateMatrixDimension() {
        matrixDimension = Integer.valueOf(getUserInfo("Introduce the matrix dimension: "));
        if (matrixDimension < 3) {
            matrixDimension = 3;
            System.out.println("Matrix dimension lower than 3. Default matrix dimension is " + matrixDimension);
        } else if (matrixDimension >= 26) {
            matrixDimension = 26;
            System.out.println("Matrix dimension higher than 26. Matrix dimension is set to " + matrixDimension);
        }
    }

    public static void main(String[] args) {
        /*
        - at the end of one game, ask the users if they want to continue with another game (Y/N)
        - if he chooses Y, start a new game (resetting the board, etc)
         */

        int gameNumberPerformed = 0;    // the number of games already performed
        while (getUserOptionNewGame().toLowerCase().compareTo("n") != 0) {
            /*
            Introduce the matrix dimension
            - at the start of the program, ask the user for the board size - must be between 3 and 26 (default may be 3)
            - you should then work with a board of that size - displaying it, reading moves
             */
            validateMatrixDimension();

            /*
            Introduce the number of players
             */
            numberPlayers = Integer.valueOf(getUserInfo("Introduce the number of players: "));

            /*
            get the players name list
             */
            playersList = getPlayersList(numberPlayers);


            getOneTicTacToeGame(gameNumberPerformed);
            gameNumberPerformed++;
        }
    }
}