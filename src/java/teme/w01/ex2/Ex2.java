package teme.w01.ex2;

/**
 * 2. Time Converter:
 * Print the total number of milliseconds contained in H hours, M minutes and S seconds,
 * where H, M, S are inputs introduced by the user. (check first that each value is in valid
 * range, for example 0<= S < 60...)
 */
public class Ex2 {

    /**
     * Given a time duration expressed in a number of hours + some minutes + some seconds,
     * it converts it to a total number of milliseconds
     *
     * @param hours   number of hours (valid range: 0..23)
     * @param minutes number of minutes (valid range: 0..59)
     * @param seconds number of seconds (valid range: 0..59)
     * @return the total number of milliseconds, or -1 if any of the input parameters has invalid value
     */
    static int convertToMilliseconds(int hours, int minutes, int seconds) {

        //TODO: fill in your code here!

        int result = (seconds + minutes * 60 + hours * 60 * 60) * 1000;
        boolean isValid = timeValid(hours, minutes, seconds);

        return (isValid) ? result : -1;
    }

    static boolean timeValid(int hours, int minutes, int seconds) {
        return (seconds >= 0) && (seconds <= 59) &&
                (minutes >= 0) && (minutes <= 59) &&
                (hours >= 0) && (hours <= 23);
    }

    /**
     * Main method added just to easily run some manual tests (for automatic tests see separate class)
     */
    public static void main(String[] args) {
        //1h + 2min + 3sec => should result in: 3 723 000 ms
        System.out.println("convertToMilliseconds(1,2,3)=" + convertToMilliseconds(1, 2, 3));

        //1h + 61min + 0 sec => invalid range (for min), should return: -1
        System.out.println("convertToMilliseconds(1,62,0)=" + convertToMilliseconds(1, 62, 0));
    }
}
