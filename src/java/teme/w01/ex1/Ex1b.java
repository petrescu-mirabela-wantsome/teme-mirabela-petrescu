package teme.w01.ex1;

/**
 * Ex1 b)
 * Given three values representing the LENGTH of the edges of a possible triangle,
 * write 2 methods which should:
 * - compute if they can form a valid triangle (if the sum of ANY 2 edges is greater than the 3rd one)
 * - compute if they can form a RIGHT-ANGLED valid triangle (hint: use Pythagoras theorem)
 * Some tests:
 * 1,2,4 => not a valid triangle
 * 4,1,3 => not a valid triangle
 * 1,2,2 => valid, not right-angled
 * 3,4,5 => valid, also right-angled
 */
public class Ex1b {

    //========== THE 2 METHODS YOU NEED TO COMPLETE/FIX! ==========//
    //You should:
    // - complete missing code/fix these methods...
    // - ...then run tests from Ex1b_Tests class
    // - repeat until all tests pass successfully :)
    //=============================================================//

    /**
     * Method which computes if 3 given LENGTH values can be the sides of a valid triangle
     *
     * @return true if they can form a triangle, false otherwise
     */
    static boolean canFormValidTriangle(int side1, int side2, int side3) {

        return (side1 + side2 > side3) &&
                (side1 + side3 > side2) &&
                (side2 + side3 > side1);
    }

    /**
     * Method which computes if 3 given LENGTH values:
     * - can be the sides of a valid triangle
     * - AND that triangle is also right-angled
     *
     * @return true if they can form a right-angled triangle, false otherwise
     */
    static boolean canFormValidRightAngledTriangle(int side1, int side2, int side3) {

        //HINT: you may also reuse/call here the method 'canFormValidTriangle' (if you fixed that one) to first check the triangle is valid..
        boolean isRightAngled = checkPitagora(side1, side2, side3) || checkPitagora(side1, side3, side2) || checkPitagora(side2, side3, side1);

        return canFormValidTriangle(side1, side2, side3) && isRightAngled;
    }

    static boolean checkPitagora(int cateta1, int cateta2, int ipotenuza) {
        return ((cateta1 * cateta1 + cateta2 * cateta2) == ipotenuza * ipotenuza);
    }
    //========== METHOD FOR MANUAL TESTING ==========//

    /**
     * MAIN method, you can use it to easily run some manual tests (before running all automatic tests from separate test class)
     */
    public static void main(String[] args) {
        System.out.println("Can segments of length (1,2,4) form a valid triangle? : " + canFormValidTriangle(1, 2, 4));
        System.out.println("Can segments of length (1,2,4) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(1, 2, 4));
        System.out.println("Can segments of length (4,1,3) form a valid triangle? : " + canFormValidTriangle(4, 1, 3));
        System.out.println("Can segments of length (4,1,3) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(4, 1, 3));
        System.out.println("Can segments of length (1,2,2) form a valid triangle? : " + canFormValidTriangle(1, 2, 2));
        System.out.println("Can segments of length (1,2,2) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(1, 2, 2));
        System.out.println("Can segments of length (3,4,5) form a valid triangle? : " + canFormValidTriangle(3, 4, 5));
        System.out.println("Can segments of length (3,4,5) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(3, 4, 5));
    }
}
