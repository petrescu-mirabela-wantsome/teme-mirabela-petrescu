package teme.w01.ex5;

/**
 * Ex5: Ordered checker:
 * - Write a program that accepts three integers from the user and returns true if they are in strict
 * ascending order (first number is less than 2nd which is less than 3rd).
 * - Modify your program to check if they are sorted in descending order
 * - Modify again your program to check now if 9 given numbers are in descending order
 */
public class Ex5 {

    static boolean numbersAreSortedAscending3(int n1, int n2, int n3) {
        return (n1 < n2) && (n2 < n3) ? true : false;
        //return numberAreSortedAscending2(n1,n2) && numberAreSortedAscending2(n2,n3);
    }

    static boolean numbersAreSortedDescending3(int n1, int n2, int n3) {
        return (n1 > n2) && (n2 > n3) ? true : false;
        //return numberAreSortedDescendingg2(n1,n2) && numberAreSortedDescendingg2(n2,n3);
    }

    static boolean numbersAreSortedDescending9(int n1, int n2, int n3, int n4, int n5, int n6, int n7, int n8, int n9) {
        //Question: can you somehow reuse the logic of numbersAreSortedDescending3() here? (by calling it as needed..). If yes, does it make your solution simpler/nicer?..

        return numbersAreSortedDescending3(n1, n2, n3) && numbersAreSortedDescending3(n3, n4, n5) &&
                numbersAreSortedDescending3(n5, n6, n7) && numbersAreSortedDescending3(n7, n8, n9);
    }

}
