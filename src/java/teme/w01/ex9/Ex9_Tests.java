package teme.w01.ex9;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Ex9_Tests {

    private static final double DELTA = 0.0001; //precision to use when comparing double values in asserts

    @Test
    public void testAbs() {
        assertEquals(0.0, Ex9.abs(0.0), DELTA);
        assertEquals(2.7, Ex9.abs(-2.7), DELTA);
        assertEquals(3.2, Ex9.abs(3.2), DELTA);
    }
}
