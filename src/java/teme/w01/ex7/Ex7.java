package teme.w01.ex7;

/**
 * 7. Common digit:
 * Write a program that given 2 integers values between 13 and 89, it prints
 * true if there is (at least one) common digit in both numbers, or false otherwise.
 * For the case the numbers are outside the allowed range, it should also return false.
 * <p>
 * Example:
 * x=34, y=48 => should return: true
 * x=10, y=20 => should return: false (x is outside range)
 * x=42, y=81 => should return: false (no common digits)
 * x=42, y=24 => should return: true (has at least one common digit, actually has 2)
 */
public class Ex7 {

    /**
     * Helper method, which just checks if a number is in expected range (13..89)
     *
     * @param n the number to check
     * @return true if number is valid (is between 13 and 89), false otherwise
     */
    static boolean isValid(int n) {
        return (n >= 13) && (n <= 89);
    }

    /**
     * Checks if 2 numbers have at least 1 common digit
     *
     * @param x 1st number (valid range: 13..89)
     * @param y 2nd number (valid range: 13..89)
     * @return true if both numbers are in valid range and have at least 1 common digit, false otherwise
     */
    static boolean haveACommonDigit(int x, int y) {
        //Hint: for easier working, you may first extract the 2 digits of x,y to 4 separate int variables..

        boolean value =
                (x / 10 == y / 10) ||
                        (x / 10 == y % 10) ||
                        (x % 10 == y / 10) ||
                        (x % 10 == y % 10);

        return (isValid(x) && isValid(y) && value);
    }
}
