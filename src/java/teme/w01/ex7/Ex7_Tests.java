package teme.w01.ex7;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static teme.w01.ex7.Ex7.haveACommonDigit;
import static teme.w01.ex7.Ex7.isValid;

public class Ex7_Tests {

    @Test
    public void testIsValid() {
        assertFalse(isValid(-90));
        assertFalse(isValid(12));
        assertTrue(isValid(13));
        assertTrue(isValid(89));
        assertFalse(isValid(90));
        assertFalse(isValid(999));
    }

    @Test
    public void testCommonDigit() {
        assertFalse(haveACommonDigit(12, 22)); //x outside range
        assertFalse(haveACommonDigit(90, 29)); //x outside range
        assertFalse(haveACommonDigit(19, 90)); //y outside range
        assertFalse(haveACommonDigit(13, 89)); //no common digit

        assertTrue(haveACommonDigit(18, 28));
        assertTrue(haveACommonDigit(17, 72));
        assertTrue(haveACommonDigit(73, 47));
        assertTrue(haveACommonDigit(70, 71));
        assertTrue(haveACommonDigit(42, 24));
    }
}
