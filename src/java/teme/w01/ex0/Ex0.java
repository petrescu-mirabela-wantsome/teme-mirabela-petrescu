package teme.w01.ex0;

/**
 * A warm-up / simple example
 */
public class Ex0 {

    /**
     * Method which receives 2 numbers and computes/returns their sum
     */
    public static int computeSum(int a, int b) {
        int sum = a + b;
        return sum;
    }

    //--- Some simple methods for you to implement ---//

    /**
     * Method which returns the square value of a number (number to power 2)
     */
    public static int squareValue(int x) {
        return x * x;
    }

    /**
     * Receives 2 string values, joins them and returns as a single string
     */
    public static String joinStrings(String s1, String s2) {
        return s1 + s2;
    }

    /**
     * Receives a String, computes and returns it's length (number of characters)
     * Hint: check what methods are available on the String object - type 's.' and wait for autocomplete by Idea...
     */
    public static int lengthOf(String s) {
        return s.length();
    }

    /**
     * Receives a double value (with some fractional part), should return only the integer part of it (drop the decimals)
     * Hint: you need to use a forced casting to transform between these types (double -> int)
     */
    public static int truncate(double x) {
        return (int) x;
    }

    /**
     * Returns true if value x is in the given interval (minLimit <= x <= maxLimit), false otherwise
     */
    public static boolean isInInterval(int x, int minLimit, int maxLimit) {
        return (x >= minLimit) && (x <= maxLimit);
    }

    /**
     * Main method, we use here just for easy manual testing of our methods above.
     * NOTE: after manual test run ok, don't forget to also run the automatic tests, see Ex0_Tests class!
     */
    public static void main(String[] args) {
        System.out.println("sum(2,3) = " + computeSum(2, 3));
        System.out.println("square(3) = " + squareValue(3));
        System.out.println("joinStrings('ab', 'cd') = '" + joinStrings("ab", "cd") + "'");
        System.out.println("lengthOf('abc') = " + lengthOf("abc"));
        System.out.println("truncate(3.21) = " + truncate(3.21));
        System.out.println("isInInterval(3, 0, 5) = " + isInInterval(3, 0, 5));
        System.out.println("isInInterval(3, 5, 10) = " + isInInterval(3, 5, 10));
    }
}
