package teme.w01.ex4;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static teme.w01.ex4.Ex4.convertEvenOddToOneZero;
import static teme.w01.ex4.Ex4.convertEvenOddToString;

public class Ex4_Tests {

    @Test
    public void testConvertEvenOddToNumber_forInvalidValue() {
        assertEquals(-1, convertEvenOddToOneZero(0));
        assertEquals(-1, convertEvenOddToOneZero(1001));
    }

    @Test
    public void testConvertEvenOddToNumber_forValidValue() {
        assertEquals(1, convertEvenOddToOneZero(1));
        assertEquals(1, convertEvenOddToOneZero(17));
        assertEquals(0, convertEvenOddToOneZero(200));
        assertEquals(0, convertEvenOddToOneZero(1000));
    }

    @Test
    public void testConvertEvenOddToString_forInvalidValue() {
        assertEquals("invalid", convertEvenOddToString(0));
        assertEquals("invalid", convertEvenOddToString(1001));
    }

    @Test
    public void testConvertEvenOddToString_forValidValue() {
        assertEquals("odd", convertEvenOddToString(1));
        assertEquals("odd", convertEvenOddToString(17));
        assertEquals("even", convertEvenOddToString(200));
        assertEquals("even", convertEvenOddToString(1000));
    }
}
