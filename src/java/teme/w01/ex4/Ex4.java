package teme.w01.ex4;

/**
 * Ex4: Odd-Even Checker:
 * Write a program that is given a positive number, not bigger than 1000, and checks if the
 * number is even or not.
 * a. Prints 1 if the number is odd or 0 if the number is even.
 * b. Change your program so it prints “even”/”odd” instead of 1/0 (hint: may use the
 * ternary operator)
 */
public class Ex4 {

    /**
     * Checks a given integer number if is even or odd
     *
     * @param n number to check, valid interval 1..1000
     * @return 1 if number is even, 0 if it's odd, or -1 if outside valid interval
     */

    static int convertEvenOddToOneZero(int n) {
        // int isValid = inRange(n) ? 1 : -1;
        // return ((isValid == "valid") && (n%2 == 0)) ? 0 : 1;
        if (inRange(n) == true) {
            return ((n % 2 == 0)) ? 0 : 1;
        } else {
            return -1;
        }
    }

    /**
     * Checks a given integer number if is even or odd
     *
     * @param n number to check, valid interval 1..1000
     * @return "even" if number is even, "odd" if it's odd, or "invalid" if outside valid interval
     */
    static String convertEvenOddToString(int n) {

        //String isValid = inRange(n) ? "valid" : "invalid";
        //return ((isValid == "valid") && (n%2 == 0)) ? "even" : "odd";

        if (inRange(n) == true) {
            return ((n % 2 == 0)) ? "even" : "odd";
        } else {
            return "invalid";
        }
    }

    static boolean inRange(int n) {
        return ((n >= 1) && (n <= 1000));
    }

    public static void main(String[] args) {
        System.out.println("0 is " + convertEvenOddToOneZero(0) + " number");
        System.out.println("0 is " + convertEvenOddToString(0) + " number");
        System.out.println("1 is " + convertEvenOddToOneZero(1) + " number");
        System.out.println("1 is " + convertEvenOddToString(1) + " number");
        System.out.println("2 is " + convertEvenOddToOneZero(2) + " number");
        System.out.println("2 is " + convertEvenOddToString(2) + " number");
    }
}
