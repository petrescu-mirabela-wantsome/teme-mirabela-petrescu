package teme.w07.ex2;

import java.util.Random;

/*
2. A Reactor class:
a. The class constructor receives a int that specifies power output level at which the reactor goes critical.
b. Starts with a power output of 0.
c. Has a method which returns the current output as an int value.
d. Has two methods that increase and decrease the output (with a random value between 1 and 100).
i. The increase method will throw an exception if the reactor is going critical
(current power output becomes greater than critical level)
ii. The decrease method should not decrease the output to negative values
(but only down to 0)
 */
class Reactor {
    private final int powerLevelReactorCritical;
    private int currentPowerLevel = 0;  // power output level at which the reactor goes critical, starts with a power output of 0.

    public Reactor(int powerLevelReactorCritical) {
        this.powerLevelReactorCritical = powerLevelReactorCritical;
    }

    public int getPowerLevelReactorCritical() {
        return powerLevelReactorCritical;
    }

    /* returns the current output as an int value
     */
    public int currentPowerLevel() {
        return currentPowerLevel;
    }

    /* i. The increase method will throw an exception if the reactor is going critical
    (current power output becomes greater than critical level)
    */
    public void increasePowerLevel() throws ReactorCriticalException {
        /* In order to generate a random number between 1 and 100 we create an object of java.util.Random class and call its nextInt method with 100 as argument.
        This will generate a number between 0 and 99 and add 1 to the result which will make the range of the generated value as 1 to 100.
        */
        Random randomGenerator = new Random();
        int randomPowerToIncrease = randomGenerator.nextInt(100) + 1;

        // this if shall be the first one (if an exception appears, the next if is not evaluated)
        if ((currentPowerLevel + randomPowerToIncrease) > powerLevelReactorCritical) {
            System.out.println("The power level above critical level is: " + (currentPowerLevel + randomPowerToIncrease));
            throw new ReactorCriticalException();       // throw exception if the reactor power level is above the critical level
        }

        if ((currentPowerLevel + randomPowerToIncrease) <= powerLevelReactorCritical) {
            currentPowerLevel += randomPowerToIncrease;
            System.out.println("After increase the power level is: " + currentPowerLevel);
        }
    }

    /* ii. The decrease method should not decrease the output to negative values (but only down to 0)
     */
    public void decreasePowerLevel() {
        /* In order to generate a random number between 1 and 100 we create an object of java.util.Random class and call its nextInt method with 100 as argument.
        This will generate a number between 0 and 99 and add 1 to the result which will make the range of the generated value as 1 to 100.
        */
        Random randomGenerator = new Random();
        int randomPowerToDecrease = randomGenerator.nextInt(100) + 1;
        if ((currentPowerLevel - randomPowerToDecrease) >= 0) {
            currentPowerLevel -= randomPowerToDecrease;
        }
        System.out.println("After decrease the power level is (the level repeats if it decrease below 0): " + currentPowerLevel);
    }
}
