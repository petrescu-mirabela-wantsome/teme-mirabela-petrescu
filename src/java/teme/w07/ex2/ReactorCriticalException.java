package teme.w07.ex2;

/* i. The increase method will throw an exception if the reactor is going critical
(current power output becomes greater than critical level)
*/
class ReactorCriticalException extends Exception {
    public ReactorCriticalException() {
        super("Reactor is going critical (current power output becomes greater than critical level");
    }
}
