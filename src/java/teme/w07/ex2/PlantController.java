package teme.w07.ex2;

/* 3. A PlantController class:
a. Receives a PowerPlant and a Reactor instance in its constructor
b. Has a needAdjustment() method that returns true when the difference between
the reactor output and plant desired output differs by a value larger than 10
c. Has a method adjust() which adjust the output by repeatedly calling the
increase() method of the reactor as long as needed (until needAdjustment() returns false)
d. Has a shutdown() method that repeatedly calls the decrease method of the reactor until the output is 0
e. Has a run() method that runs constantly and checks whether the reactor needs
adjustment and calls the adjust method. If the reactor goes critical call the sound alarm method and shutdown the reactor.
*/
class PlantController {
    private final PowerPlant powerPlant;
    private final Reactor reactor;
    private int state = 0;  // Level 0

    // a. Receives a PowerPlant and a Reactor instance in its constructor
    public PlantController(PowerPlant powerPlant, Reactor reactor) {
        this.powerPlant = powerPlant;
        this.reactor = reactor;
    }

    public int getState() {
        return state;
    }

    // b. returns true when the difference between the reactor output and plant desired output differs by a value larger than 10
    public boolean needAdjustment() {
        return (powerPlant.desiredOutput() - reactor.currentPowerLevel()) >= 10;
    }

    // c. adjust the output by repeatedly calling the increase() method of the reactor as long as needed (until needAdjustment() returns false)
    public void adjust() throws ReactorCriticalException {
        while (needAdjustment()) {
            reactor.increasePowerLevel();
            state++;    // state will return the iteration number, Level(n) = Level(n-1) + 1
            System.out.println("PowerPlant state level (increase times): " + state);
        }
    }

    // d. repeatedly calls the decrease method of the reactor until the output is 0
    public void shutdown() {
        while (reactor.currentPowerLevel() > 0) {
            reactor.decreasePowerLevel();
            state--;    // state will return the iteration number, Level(n-1) = Level(n) - 1
            System.out.println("PowerPlant state level (increase - decrease times): " + state);
        }
    }

    // e. runs constantly and checks whether the reactor needs adjustment and calls the adjust method.
    // If the reactor goes critical call the sound alarm method and shutdown the reactor.
    public void run() {
        try {
            adjust();
        } catch (ReactorCriticalException e) {
            powerPlant.soundAlert();
            shutdown();
        }
    }
}
