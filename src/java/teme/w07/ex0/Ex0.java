package teme.w07.ex0;

import java.util.ArrayList;
import java.util.List;

public class Ex0 {

    static public int toPositiveInt(String value) throws NotANumberException, NegativeNumberException {
        try {
            int intValue = Integer.parseInt(value);
            if (intValue < 0) {
                throw new NegativeNumberException(value);
            }
            return intValue;
        } catch (NumberFormatException e) {
            throw new NotANumberException(value);
        }
    }

    static public List<Integer> toPositiveInt(List<String> list) {
        List<Integer> listInteger = new ArrayList<>();
        for (String listItem : list) {
            try {
                listInteger.add(toPositiveInt(listItem));
            } catch (NotANumberException | NegativeNumberException e) {
                System.err.println(e.getMessage());
            }
        }
        return listInteger;
    }

    public static void main(String[] args) {

        /* a. Write a (static) method ‘ int toPositiveInt(String value) ’
        - It takes as input parameter a String value and tries to convert it to a positive
        integer and return it
        - In case of errors, it should throw 2 different types of exceptions (you’ll need to
        define 2 separate exception classes for it)
        - One for the case the string value is not a number at all
        - A different one for the case the value is a number, but a negative one
        */
        try {
            System.out.println(toPositiveInt("ana"));
        } catch (NotANumberException | NegativeNumberException t) {
            //System.out.println("An exception was thrown, but was caught here: " + t);
            t.printStackTrace();
        }
        try {
            System.out.println(toPositiveInt("-1"));
        } catch (NotANumberException | NegativeNumberException t) {
            //System.out.println("An exception was thrown, but was caught here: " + t);
            t.printStackTrace();
        }
        try {
            System.out.println(toPositiveInt("10"));
        } catch (NotANumberException | NegativeNumberException t) {
            //System.out.println("An exception was thrown, but was caught here: " + t);
            t.printStackTrace();
        }

        /* c. Write another (static) method ‘ List<Integer> toPositiveInt(List<String>
        list) ’ (has same name as first one, so it overloads it)
        - It will receive a list of string values, which may be numbers
                - It tries to convert each string value to a positive number:
        - Use the first method toPositiveInt() to try to convert each string value
                - In case a value cannot be converted to a number (one of the 2 types of
                exceptions are thrown..), simply ignore it and don’t add it to the result list
                - But would be nice/useful to write a message about it (with
        System.err.println())
        - Note that this method should never throw exceptions itself, but it may just return
                a smaller list (even empty) */
        List<String> values = new ArrayList<>();
        values.add("lucian");
        values.add("andrei");
        values.add("-1");
        values.add("-2");
        values.add("1234");
        System.out.println(toPositiveInt(values));

    }
}

class NotANumberException extends Exception {
    NotANumberException(String value) {
        super("Value " + value + " not a number");
    }
}

class NegativeNumberException extends Exception {
    NegativeNumberException(String value) {
        super("Value " + value + " is not positive number");
    }
}