package teme.w07.ex3;

/*
b. Create a class named Compute , with a method compute() that:
- takes 2 input parameters:
- an array of int values
- an instance of type Function (so may pass here an instance of any class which implements that interface)
- returns: an array of same size as input array, but it contains as values the result
of applying the function passed as parameter to each of the elements of the input
(it basically transforms each value in input array by applying the given Function)
 */
class Compute {

    public int[] compute(int[] arr, Function function) {
        int[] arr2 = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            arr2[i] = function.evaluate(arr[i]);
        }
        return arr2;
    }
}
