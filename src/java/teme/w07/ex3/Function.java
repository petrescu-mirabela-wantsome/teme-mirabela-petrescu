package teme.w07.ex3;

/* a. Declare an interface named Function that has a single method named evaluate that
takes an int parameter and returns an int value.
*/
interface Function {

    int evaluate(int value);
}
