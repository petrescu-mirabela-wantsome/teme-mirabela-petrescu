package teme.w07.ex3;

/*
e. Write in the main() method of a new class (may name it HalfAndPrint) some code which
does the following:
- creates an array of some int values from 1 to 10
- print the array using an instance of the Print class from above (and an instance of
the Compute class)
- halves all the values of the array and prints the resulting values, using the
Compute, Half and Print classes.
 */

public class HalfAndPrint {
    public static void main(String[] args) {
        int[] arr1 = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Compute compute1 = new Compute();
        Print print = new Print();
        compute1.compute(arr1, print);

        System.out.println("\n>>>>>>> After half >>>>>>> \n");
        Compute compute2 = new Compute();
        Half half = new Half();
        int[] arr2 = compute2.compute(arr1, half);
        compute2.compute(arr2, print);

    }
}
