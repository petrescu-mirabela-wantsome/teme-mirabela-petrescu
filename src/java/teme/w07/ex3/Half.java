package teme.w07.ex3;

/*
c. Create a class named Half that implements the Function interface. Make the
implementation of the method evaluate() return the value obtained by dividing the int argument by 2.
*/
public class Half implements Function {
    @Override
    public int evaluate(int value) {
        return value / 2;
    }
}
