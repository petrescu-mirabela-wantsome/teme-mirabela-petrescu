package teme.w07.ex1;

public class ConstantSensor implements Sensor {
    private final int constantSensor;

    public ConstantSensor(int constantSensor) {
        this.constantSensor = constantSensor;
    }

    @Override
    public String toString() {
        return "ConstantSensor{" +
                "constantSensor=" + constantSensor +
                '}';
    }

    @Override
    public boolean isOn() {
        return true;
    }

    @Override
    public void on() {
        // do nothing
    }

    @Override
    public void off() {
        // do nothing
    }

    @Override
    public int measure() {
        return this.constantSensor;
    }
}
