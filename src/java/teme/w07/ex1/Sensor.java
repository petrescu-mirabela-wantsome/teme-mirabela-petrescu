package teme.w07.ex1;

interface Sensor {
    boolean isOn(); //returns true if the sensor is on

    void on();      //switches the sensor on

    void off();     //switches the sensor off

    //returns the sensor reading if the sensor is on,
    //or throws and exception if the sensor is off
    int measure() throws ThermometerOffException, EmptyAverageSensorException;
}
