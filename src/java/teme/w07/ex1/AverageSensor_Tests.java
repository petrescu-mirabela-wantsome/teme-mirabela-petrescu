package teme.w07.ex1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class AverageSensor_Tests {

    //helper method for easy building a new Array with some given elements (of a custom type)
    private static <E> ArrayList<E> newList(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    @Test
    public void testEmptyAverageSensor() {
        List<Sensor> sensors = new ArrayList<>();
        assertTrue(sensors.isEmpty());

    }

    @Test
    public void testAverageSensor() {
        AverageSensor sensors = new AverageSensor();
        assertTrue(sensors.sensorList.isEmpty());
        Sensor sensor1 = new ConstantSensor(30);
        /* test
        1. A constant sensor:
            a. Is on all the time
            b. The methods on and off do nothing
            c. The class has a constructor with an int parameter and the measure method
            returns the value received as parameter
        */
        assertTrue(sensor1.isOn());     // 1a. Is on all the time
        sensor1.on();
        sensor1.off();
        assertTrue(sensor1.isOn()); // 1b. The methods on and off do nothing
        try {
            assertEquals(30, sensor1.measure());
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }

        /* test
        2. A thermometer sensor
        a. After creation the thermometer is off
        b. When the measure method is called it returns a random int between -30 and 30
        c. If the thermometer is off the measure method will throw an exception
         */
        Sensor sensor2 = new ThermometerSensor();
        assertFalse(sensor2.isOn());     // 2a. After creation the thermometer is off
        // 2b. measure method will throw an exception
        try {
            sensor2.measure();
            fail("Thermometer sensor is off");
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }
        sensor2.on();     // switch the thermometer sensor on
        assertTrue(sensor2.isOn());
        // 2c. sensor on --> measure method will not throw an exception
        try {
            assertTrue(sensor1.measure() <= 30);
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }
        try {
            assertTrue(sensor1.measure() >= -30);
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }

        /* test
        3. An average sensor
        a. Contains multiple instances of sensors
        b. Has an additional method addSensor which adds a new sensor
        c. The average sensor is on when all of it’s sensors are on
        d. When the average sensor switched on all of its sensors are switched on if not
        already on
        e. When the average sensor is switched off some of its sensors are switched off (at
        least one)
        f. The measure method will return an average of the measured values of all its
        registered sensors
        g. If the measure method is called when the sensor is off or when there are no
        sensors registered to the average sensor throw an exception
         */

        // 3g. If the measure method is called when the sensor is off or when there are no sensors registered to the average sensor throw an exception
        try {
            assertTrue(sensors.sensorList.isEmpty());
            sensors.measure();
            fail("empty sensor");
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }
        sensors.addSensor(sensor1);   // 3b. Has an additional method addSensor which adds a new sensor
        sensors.addSensor(sensor2);
        assertEquals(2, sensors.sensorList.size());
        assertEquals(new AverageSensor(newList(sensor1, sensor2)), sensors);   // 3b. Has an additional method addSensor which adds a new sensor

        // c. The average sensor is on when all of it’s sensors are on
        assertTrue(sensor1.isOn());
        assertTrue(sensor2.isOn());
        assertTrue(sensors.isOn());

        // c. The average sensor is on when all of it’s sensors are on
        assertTrue(sensor1.isOn());
        sensor2.off();
        assertFalse(sensor2.isOn());
        assertFalse(sensors.isOn());

        // d. When the average sensor switched on all of its sensors are switched on if not already on
        assertFalse(sensor2.isOn());
        sensors.on();
        assertTrue(sensor2.isOn());     // sensor2 will switch from off to on

        // e. When the average sensor is switched off some of its sensors are switched off (at least one)
        assertTrue(sensor2.isOn());     // sensor2 is on before switching the average sensor off
        sensors.off();
        assertFalse(sensor2.isOn());     // sensor2 is off after switching the average sensor off

        // f. The measure method will return an average of the measured values of all its registered sensors
        sensors.on();   // all the sensors are on
        assertTrue(sensors.isOn());     // average sensor is on => all the sensors are on
        assertTrue(sensor2.isOn());     // thermometer sensor is on
        try {
            int allSensorMeasure = sensors.measure();   // measure method shall be called only once for thermometer sensor/average sensor
            // thermometer sensor measure value is taken from the average sensor measure
            assertEquals((sensor1.measure() + (allSensorMeasure * 2 - sensor1.measure())) / 2, allSensorMeasure);
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }
        sensor2.off();   // all the sensors are off
        assertFalse(sensors.isOn());     // average sensor is off
        assertTrue(sensor1.isOn());     // constant sensor is on all the time as requirement
        assertFalse(sensor2.isOn());     // thermometer sensor is off
        // average sensor off => an exception is thrown
        try {
            sensors.measure();
            fail("average sensor is off");
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }

        Sensor sensor3 = new ConstantSensor(-10);
        try {
            assertEquals(-10, sensor3.measure());
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }

        Sensor sensor4 = new ThermometerSensor();
        sensors.addSensor(sensor3);
        sensors.addSensor(sensor4);
        sensor2.on();
        assertFalse(sensors.isOn());    // if at least one sensor is off => the average sensor is off (sensor3 is off)
        assertTrue(sensor3.isOn());     // costant sensor is true all the time
        assertFalse(sensor4.isOn());    // After creation the thermometer sensor is off
        // average sensor off => an exception is thrown
        try {
            sensors.measure();
            fail("average sensor is off");
        } catch (ThermometerOffException | EmptyAverageSensorException e) {
            //ok, expected to get this exception when test works well (so just 'bury' it)
        }

        sensor4.on();
        assertTrue(sensor4.isOn());
        assertTrue(sensors.isOn());     // if all the sensors are on => the average sensor is on
        assertEquals(new AverageSensor(newList(sensor1, sensor2, sensor3, sensor4)), sensors);   // 3b. Has an additional method addSensor which adds a new sensor
    }
}