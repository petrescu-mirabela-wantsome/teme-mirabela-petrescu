package teme.w06.ex1;

import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for StockMarket class (should compile and pass all tests after you complete that one first)
 */
public class StockMarket_Tests {
    private final static double DELTA = 0.001; //precision used for comparing doubles

    //helper method for easy building a new Array with some given elements (of a custom type)
    private static <E> ArrayList<E> newSet(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    @Test
    public void testEmptyStockMarket() {
        StockMarket stockMarket = new StockMarket();

        Date d1 = new Date(23226483232L);
        Date d2 = new Date(83226483232L);
        //assertEquals(0, stockMarket);
        assertTrue(stockMarket.queryUpdates(d1, d2).isEmpty());
        assertTrue(stockMarket.queryUpdates(d1, d2, "AMZN").isEmpty());
        assertTrue(stockMarket.queryUpdates(d1, d2, "").isEmpty());
        assertEquals(0, stockMarket.getPrice(d1, "AMZN"), DELTA);
        assertEquals(0, stockMarket.getPrice(d1, ""), DELTA);
        assertTrue(stockMarket.getPrices(d1).isEmpty());
        assertTrue(stockMarket.getPrices(d2).isEmpty());
    }

    //helper method for easy building a new Set with some given elements (of a custom type)
//    private static <E> Set<E> newSet(E... elements) {
//        return new HashSet<>(Arrays.asList(elements));
//    }

    @Test
    public void testNonEmptyStockMarket() {
        StockMarket stockMarket = new StockMarket();

        StockUpdate s1 = new StockUpdate("AMZN", new Date(73226483232L), 1700);
        StockUpdate s2 = new StockUpdate("MSFT", new Date(23226483232L), 1900);
        StockUpdate s3 = new StockUpdate("NTRS", new Date(99232253232L), 1500);
        StockUpdate s4 = new StockUpdate("NTRS", new Date(69232253232L), 1300);
        StockUpdate s5 = new StockUpdate("VXWA", new Date(99232253232L), 1500);
        StockUpdate s6 = new StockUpdate("NTRS", new Date(45322253232L), 1000);

        stockMarket.add(s1);
        stockMarket.add(s2);
        stockMarket.add(s3);
        stockMarket.add(s4);
        stockMarket.add(s5);
        stockMarket.add(s6);

        Date d1 = new Date(53226483232L);
        Date d2 = new Date(83226483232L);

        assertEquals(newSet(s1, s4), stockMarket.queryUpdates(d1, d2));

        assertEquals(newSet(s1), stockMarket.queryUpdates(d1, d2, "AMZN"));

        assertTrue(stockMarket.queryUpdates(d1, d2, "").isEmpty());

        Date d3 = new Date(99232253232L);
        assertEquals(0, stockMarket.getPrice(d3, ""), DELTA);
        assertEquals(1500, stockMarket.getPrice(d3, "NTRS"), DELTA);


        Date d4 = new Date(59232253232L);
        assertEquals(0, stockMarket.getPrice(d4, ""), DELTA);
        assertEquals(1300, stockMarket.getPrice(d4, "NTRS"), DELTA);

        Date d5 = new Date(45322253232L);
        assertEquals(0, stockMarket.getPrice(d5, ""), DELTA);
        assertEquals(1000, stockMarket.getPrice(d5, "NTRS"), DELTA);

        Map<String, Double> stockMarketMap01 = new HashMap<>();
        stockMarketMap01.put(s3.getStockCode(), s3.getPrice());
        stockMarketMap01.put(s5.getStockCode(), s5.getPrice());
        assertEquals(stockMarketMap01, stockMarket.getPrices(d3));

        Map<String, Double> stockMarketMap02 = new HashMap<>();
        stockMarketMap02.put(s1.getStockCode(), s1.getPrice());
        stockMarketMap02.put(s4.getStockCode(), s4.getPrice());
        stockMarketMap02.put(s5.getStockCode(), s5.getPrice());
        assertEquals(stockMarketMap02, stockMarket.getPrices(d4));

        Map<String, Double> stockMarketMap03 = new HashMap<>();
        stockMarketMap03.put(s3.getStockCode(), s3.getPrice());
        stockMarketMap03.put(s5.getStockCode(), s5.getPrice());
        assertEquals(stockMarketMap03, stockMarket.getPrices(d2));

    }
}