package teme.w06.ex1;

import java.util.Comparator;

class StockComparatorByDate implements Comparator<StockUpdate> {

    @Override
    public int compare(StockUpdate o1, StockUpdate o2) {
        return (o1.getTimeUpdate()).compareTo(o2.getTimeUpdate());  // ascending order by date
    }
}
