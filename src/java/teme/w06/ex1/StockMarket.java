package teme.w06.ex1;

import java.util.*;

public class StockMarket {

    private final List<StockUpdate> listStockMarket;

    public StockMarket() {
        listStockMarket = new ArrayList<>();
    }

    public static void main(String[] args) {

        StockUpdate s1 = new StockUpdate("AMZN", new Date(73226483232L), 1700);
        StockUpdate s2 = new StockUpdate("MSFT", new Date(23226483232L), 1900);
        StockUpdate s3 = new StockUpdate("NTRS", new Date(99232253232L), 1500);
        StockUpdate s4 = new StockUpdate("NTRS", new Date(69232253232L), 1300);
        StockUpdate s5 = new StockUpdate("VXWA", new Date(99232253232L), 1500);

        StockMarket stockMarket = new StockMarket();
        stockMarket.add(s1);
        stockMarket.add(s2);
        stockMarket.add(s3);
        stockMarket.add(s4);
        stockMarket.add(s5);

        System.out.println(stockMarket.toString());

        Date d1 = new Date(53226483232L);
        Date d2 = new Date(83226483232L);
        System.out.print("All stock updates between " + d1 + " and " + d2 + " ==> ");
        System.out.println((stockMarket.queryUpdates(d1, d2)).toString());

        String stockCode = "AMZN";
        System.out.print("All stock updates between " + d1 + " and " + d2 + "for a stockCode " + stockCode + " ==> ");
        System.out.println((stockMarket.queryUpdates(d1, d2, stockCode)).toString());

        System.out.print("All stock updates between " + d1 + " and " + d2 + "for an empty stockCode " + "" + " ==> ");
        System.out.println((stockMarket.queryUpdates(d1, d2, "")).toString());

        Date d3 = new Date(99232253232L);
        String stockCode1 = "NTRS";
        System.out.print("The stock price before the date " + d3 + " for stock code " + stockCode1 + " is ==> ");
        System.out.println((stockMarket.getPrice(d3, stockCode1)).toString());

        Date d4 = new Date(59232253232L);
        System.out.print("The stock price before the date " + d4 + " for stock code " + stockCode1 + " is ==> ");
        System.out.println((stockMarket.getPrice(d4, stockCode1)).toString());

        System.out.print("The prices at a given date " + d3 + " for all stocks code " + "are ==> ");
        System.out.println((stockMarket.getPrices(d3).toString()));
    }

    @Override
    public String toString() {
        return "StockMarket{" +
                "listStockMarket=" + listStockMarket +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StockMarket)) return false;
        StockMarket that = (StockMarket) o;
        return Objects.equals(listStockMarket, that.listStockMarket);
    }

    @Override
    public int hashCode() {
        return Objects.hash(listStockMarket);
    }

    public void add(StockUpdate update) {
        listStockMarket.add(update);
    }

    /* get all stock updates between two dates
     */
    public List<StockUpdate> queryUpdates(Date from, Date to) {
        List<StockUpdate> stockUpdatesFromToDate = new ArrayList<>();

        if (from.before(to)) {   // from date lower than to date
            Iterator<StockUpdate> iter = listStockMarket.iterator();
            while (iter.hasNext()) {
                StockUpdate value = iter.next();
                if (value.getTimeUpdate().after(from) && value.getTimeUpdate().before(to)) {
                    stockUpdatesFromToDate.add(value);
                }
            }
        }
        return stockUpdatesFromToDate;
    }

    /* get all stock updates between two dates for a stock code
     */
    public List<StockUpdate> queryUpdates(Date from, Date to, String stockCode) {
        List<StockUpdate> stockUpdatesFromToDateCode = new ArrayList<>();
        List<StockUpdate> stockUpdatesFromToDate = queryUpdates(from, to);  // take all the stockUpdates from a specified period ( Date from .. Date to)
        Iterator<StockUpdate> iter = stockUpdatesFromToDate.iterator();
        while (iter.hasNext()) {
            StockUpdate value = iter.next();
            if (value.getStockCode().compareTo(stockCode) == 0) {   // if the stock code is the same
                stockUpdatesFromToDateCode.add(value);
            }
        }
        return stockUpdatesFromToDateCode;
    }

    /* gets the price that a stock had at a given date.
    Note that there may not be an entry for that exact date, so you need to
    search for the last stock update that happened before the specified date.
    Hint: the TreeSet class has a “ceiling” method that could be useful here:
    https://docs.oracle.com/javase/8/docs/api/java/util/TreeSet.html#floor-E-
    */
    public Double getPrice(Date date, String stockCode) {
        // create a collection with all the elements, sorted by date updates(ascending)
        TreeSet<StockUpdate> stockCodePrice = new TreeSet<>(new StockComparatorByDate());
        // iterate over all the stockMarket list
        Iterator<StockUpdate> iter = listStockMarket.iterator();
        while (iter.hasNext()) {
            StockUpdate itemStock = iter.next();
            if ((itemStock.getStockCode().compareTo(stockCode) == 0)) {  // same stock code
                stockCodePrice.add(itemStock);  // create a collection with all the stock elements which have the desired stockCode
            }
        }
        if (stockCodePrice.ceiling(new StockUpdate(date)) != null) {
            // ceiling - Returns the least element in this set greater than or equal to the given element, or null if there is no such element
            return Objects.requireNonNull(stockCodePrice.ceiling(new StockUpdate(date))).getPrice();
        }
        return 0.0;
    }

    /* gets the prices for all stocks at a given date. The note from previous point applies here as well
     */
    public Map<String, Double> getPrices(Date date) {
        Map<String, Double> allStockPriceBeforeDate = new HashMap<>();
        Set<StockUpdate> stockCode = new TreeSet<>(listStockMarket);    // add all the stock codes (unique elements, remove duplicates)
        Iterator<StockUpdate> iter = stockCode.iterator();      // iterate over each stock code name (no duplicates are listed)
        while (iter.hasNext()) {
            StockUpdate iterItem = iter.next();
            Double stockPrice = getPrice(date, iterItem.getStockCode());
            if (stockPrice > 0.0) { // if the stockCode was updated at the date introduced as parameter
                allStockPriceBeforeDate.put(iterItem.getStockCode(), stockPrice);
            }
        }
        return allStockPriceBeforeDate;
    }
}
