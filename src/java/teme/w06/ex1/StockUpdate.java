package teme.w06.ex1;

import java.util.Date;
import java.util.Objects;

class StockUpdate implements Comparable<StockUpdate> {
    private final Date timeUpdate;
    private String stockCode;
    private double price;

    public StockUpdate(Date timeUpdate) {
        this.timeUpdate = timeUpdate;
    }

    public StockUpdate(String stockCode, Date timeUpdate, double price) {
        this(timeUpdate);
        this.stockCode = stockCode;
        this.price = price;
    }

    public Date getTimeUpdate() {
        return timeUpdate;
    }

    public String getStockCode() {
        return stockCode;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof StockUpdate)) return false;
        StockUpdate that = (StockUpdate) o;
        return Double.compare(that.price, price) == 0 &&
                Objects.equals(stockCode, that.stockCode) &&
                Objects.equals(timeUpdate, that.timeUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockCode, timeUpdate, price);
    }

    @Override
    public String toString() {
        return "StockUpdate{" +
                "stockCode='" + stockCode + '\'' +
                ", timeUpdate=" + timeUpdate +
                ", price=" + price +
                '}';
    }

    @Override
    public int compareTo(StockUpdate s) {
        return stockCode.compareTo(s.stockCode);
    }   // ascending order by stockCode
}
