package teme.w06.ex2;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Unit tests for StockMarket class (should compile and pass all tests after you complete that one first)
 */
public class Library_Tests {
    private final static double DELTA = 0.001; //precision used for comparing doubles

    //helper method for easy building a new Array with some given elements (of a custom type)
    private static <E> ArrayList<E> newList(E... elements) {
        return new ArrayList<>(Arrays.asList(elements));
    }

    @Test
    public void testEmptyLibrary() {
        Library mediaLibrary = new Library();

        assertEquals(new Library(), mediaLibrary);

        mediaLibrary.setTopListLimit(0);

        assertTrue(mediaLibrary.top20.isEmpty());
        assertEquals(0, mediaLibrary.top20.size(), DELTA);
        assertTrue(mediaLibrary.archive.isEmpty());
        assertEquals(0, mediaLibrary.archive.size(), DELTA);
    }

    @Test
    public void testNonEmptyLibrary() {
        Library mediaLibrary = new Library();

        mediaLibrary.setTopListLimit(20);

        // 15 books
        MediaEntity b1 = new Book(MediaEntityType.BOOK, "Last night", 1, "Petrescu", "Junimea");
        mediaLibrary.addDynamic(b1);
        MediaEntity b2 = new Book(MediaEntityType.BOOK, "Last summer", 3, "Lazarescu", "Corint");
        mediaLibrary.addDynamic(b2);
        MediaEntity b3 = new Book(MediaEntityType.BOOK, "King", 23, "Dumas", "Luceafarul");
        mediaLibrary.addDynamic(b3);
        MediaEntity b4 = new Book(MediaEntityType.BOOK, "Queen", 20, "Dumas Jr", "Adevarul");
        mediaLibrary.addDynamic(b4);
        MediaEntity b5 = new Book(MediaEntityType.BOOK, "Rainy day", 14, "Petrovici", "Corint");
        mediaLibrary.addDynamic(b5);
        MediaEntity b6 = new Book(MediaEntityType.BOOK, "Game night", 34, "Dostoievski", "Adevarul");
        mediaLibrary.addDynamic(b6);
        MediaEntity b7 = new Book(MediaEntityType.BOOK, "Quess", 21, "Anonim", "Adevarul");
        mediaLibrary.addDynamic(b7);
        MediaEntity b8 = new Book(MediaEntityType.BOOK, "Who is the Guest?", 17, "Petrescu", "Tineretul");
        mediaLibrary.addDynamic(b8);
        MediaEntity b9 = new Book(MediaEntityType.BOOK, "Who are you ?", 50, "Dumas Jr", "Plus");
        mediaLibrary.addDynamic(b9);
        MediaEntity b10 = new Book(MediaEntityType.BOOK, "Notre Dame", 0, "Dumas Jr", "Dreptatea");
        mediaLibrary.addDynamic(b10);
        MediaEntity b11 = new Book(MediaEntityType.BOOK, "Castelul", 46, "Anonim", "Junimea");
        mediaLibrary.addDynamic(b11);
        MediaEntity b12 = new Book(MediaEntityType.BOOK, "Proza", 23, "Rebreanu", "Junimea");
        mediaLibrary.addDynamic(b12);
        MediaEntity b13 = new Book(MediaEntityType.BOOK, "Dealul", 17, "Popescu", "Adevarul");
        mediaLibrary.addDynamic(b13);
        MediaEntity b14 = new Book(MediaEntityType.BOOK, "Storm", 60, "Calinescu", "Plus");
        mediaLibrary.addDynamic(b14);
        MediaEntity b15 = new Book(MediaEntityType.BOOK, "Rande vu", 16, "Dumas", "Adevarul");
        mediaLibrary.addDynamic(b15);

        // 5 videos
        MediaEntity v1 = new Video(MediaEntityType.VIDEO, "Hello", 10, 60, true);
        mediaLibrary.addDynamic(v1);
        MediaEntity v2 = new Video(MediaEntityType.VIDEO, "Good bye", 4, 45, false);
        mediaLibrary.addDynamic(v2);
        MediaEntity v3 = new Video(MediaEntityType.VIDEO, "Blues", 23, 60, true);
        mediaLibrary.addDynamic(v3);
        MediaEntity v4 = new Video(MediaEntityType.VIDEO, "Strange", 50, 120, false);
        mediaLibrary.addDynamic(v4);
        MediaEntity v5 = new Video(MediaEntityType.VIDEO, "Known", 20, 30, true);
        mediaLibrary.addDynamic(v5);

        // 7 mp3
        MediaEntity mp3No1 = new Mp3(MediaEntityType.MP3, "The one", 26, "Anastasia", "Good bye");
        mediaLibrary.addDynamic(mp3No1);
        MediaEntity mp3No2 = new Mp3(MediaEntityType.MP3, "Blues", 16, "Bianca", "Hello");
        mediaLibrary.addDynamic(mp3No2);
        MediaEntity mp3No3 = new Mp3(MediaEntityType.MP3, "Come here", 20, "Roberta", "80s");
        mediaLibrary.addDynamic(mp3No3);
        MediaEntity mp3No4 = new Mp3(MediaEntityType.MP3, "Let's dance", 19, "Anonim", "Rock");
        mediaLibrary.addDynamic(mp3No4);
        MediaEntity mp3No5 = new Mp3(MediaEntityType.MP3, "Bye", 25, "Carol", "Hello");
        mediaLibrary.addDynamic(mp3No5);
        MediaEntity mp3No6 = new Mp3(MediaEntityType.MP3, "Wake up", 51, "Cristian", "The best");
        mediaLibrary.addDynamic(mp3No6);
        MediaEntity mp3No7 = new Mp3(MediaEntityType.MP3, "Who are you?", 90, "Team", "Good bye");
        mediaLibrary.addDynamic(mp3No7);

        List<MediaEntity> archiveListUploadTest01 = Library.union(mediaLibrary.top20, mediaLibrary.archive);
        archiveListUploadTest01.sort(new LibraryComparatorByNoOfDownloads());
        List<MediaEntity> archiveListUploadTest02 = newList(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, v1, v2, v3, v4, v5, mp3No1, mp3No2, mp3No3, mp3No4, mp3No5, mp3No6, mp3No7);
        archiveListUploadTest02.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(archiveListUploadTest01.toString(), archiveListUploadTest02.toString());

        assertEquals(27, archiveListUploadTest01.size(), DELTA);
        assertEquals(27, archiveListUploadTest02.size(), DELTA);

        assertEquals(20, mediaLibrary.top20.size(), DELTA);
        List<MediaEntity> top20ListTest01 = newList(b3, b4, b6, b7, b8, b9, b11, b12, b13, b14, v3, v4, v5, mp3No1, mp3No2, mp3No3, mp3No4, mp3No5, mp3No6, mp3No7);
        top20ListTest01.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(top20ListTest01.toString(), mediaLibrary.top20.toString());

        assertEquals(7, mediaLibrary.archive.size(), DELTA);
        List<MediaEntity> archiveListTest02 = newList(b1, b2, b5, b10, b15, v1, v2);
        archiveListTest02.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(archiveListTest02.toString(), mediaLibrary.archive.toString());


        ///////////////// SEARCH A MediaEntity PROPERTY ////////////////
        List<MediaEntity> archiveListPropTest01 = newList(v1, v2, v3, v4, v5);
        archiveListPropTest01.sort(new LibraryComparatorByNoOfDownloads());
        List<MediaEntity> archiveListPropTest02 = mediaLibrary.search(MediaEntityType.VIDEO);
        archiveListPropTest02.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(archiveListPropTest01.toString(), archiveListPropTest02.toString());

        assertEquals(newList(v1).toString(), mediaLibrary.search("Hello").toString());

        List<MediaEntity> archiveListPropTest03 = newList(b15, mp3No2);
        archiveListPropTest03.sort(new LibraryComparatorByNoOfDownloads());
        List<MediaEntity> archiveListPropTest04 = mediaLibrary.search(16);
        archiveListPropTest04.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(archiveListPropTest03.toString(), archiveListPropTest04.toString());

        assertTrue(mediaLibrary.search("").isEmpty());
        assertTrue(mediaLibrary.search(1000).isEmpty());

        assertFalse(mediaLibrary.top20.contains(b10));
        assertTrue(mediaLibrary.archive.contains(b10));

        ///////////////// AFTER UPDATE - archive list ///////////////////////

        mediaLibrary.updateNoOfDownloads(b10, 100);
        List<MediaEntity> archiveListUploadTest03 = Library.union(mediaLibrary.top20, mediaLibrary.archive);
        archiveListUploadTest03.sort(new LibraryComparatorByNoOfDownloads());
        List<MediaEntity> archiveListUploadTest04 = newList(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, v1, v2, v3, v4, v5, mp3No1, mp3No2, mp3No3, mp3No4, mp3No5, mp3No6, mp3No7);
        archiveListUploadTest04.sort(new LibraryComparatorByNoOfDownloads());

        assertEquals(27, archiveListUploadTest03.size(), DELTA);
        assertEquals(27, archiveListUploadTest04.size(), DELTA);

        assertTrue(mediaLibrary.top20.contains(b10));
        assertFalse(mediaLibrary.archive.contains(b10));

        assertEquals(20, mediaLibrary.top20.size(), DELTA);
        List<MediaEntity> top20ListTest03 = newList(b3, b4, b6, b7, b8, b9, b10, b11, b12, b13, b14, v3, v4, v5, mp3No1, mp3No3, mp3No4, mp3No5, mp3No6, mp3No7);
        top20ListTest03.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(top20ListTest03.toString(), mediaLibrary.top20.toString());

        assertEquals(7, mediaLibrary.archive.size(), DELTA);
        List<MediaEntity> archiveListTest04 = newList(b1, b2, b5, b15, v1, v2, mp3No2);
        archiveListTest04.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(archiveListTest04.toString(), mediaLibrary.archive.toString());

        ///////////////// AFTER UPDATE - top20 list - coverage porpoise ///////////////////////
        assertTrue(mediaLibrary.top20.contains(mp3No7));
        assertFalse(mediaLibrary.archive.contains(mp3No7));

        mediaLibrary.updateNoOfDownloads(mp3No7, 86);
        List<MediaEntity> archiveListUploadTest05 = Library.union(mediaLibrary.top20, mediaLibrary.archive);
        archiveListUploadTest05.sort(new LibraryComparatorByNoOfDownloads());
        List<MediaEntity> archiveListUploadTest06 = newList(b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, v1, v2, v3, v4, v5, mp3No1, mp3No2, mp3No3, mp3No4, mp3No5, mp3No6, mp3No7);
        archiveListUploadTest06.sort(new LibraryComparatorByNoOfDownloads());

        //assertEquals(27, archiveListUploadTest05.size(), DELTA);
        //assertEquals(27, archiveListUploadTest06.size(), DELTA);

        assertTrue(mediaLibrary.top20.contains(mp3No7));
        assertFalse(mediaLibrary.archive.contains(mp3No7));

        assertEquals(20, mediaLibrary.top20.size(), DELTA);
        List<MediaEntity> top20ListTest05 = newList(b3, b4, b6, b7, b8, b9, b10, b11, b12, b13, b14, v3, v4, v5, mp3No1, mp3No3, mp3No4, mp3No5, mp3No6, mp3No7);
        top20ListTest05.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(top20ListTest05.toString(), mediaLibrary.top20.toString());

        assertEquals(7, mediaLibrary.archive.size(), DELTA);
        List<MediaEntity> archiveListTest06 = newList(b1, b2, b5, b15, v1, v2, mp3No2);
        archiveListTest06.sort(new LibraryComparatorByNoOfDownloads());
        assertEquals(archiveListTest06.toString(), mediaLibrary.archive.toString());
    }
}