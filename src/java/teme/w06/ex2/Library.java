package teme.w06.ex2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Library {
    final List<MediaEntity> top20 = new ArrayList<>();  // keeps only the last 20 most accessed media entities according to noOfDownloads field
    final List<MediaEntity> archive = new ArrayList<>();  // keeps the rest of media entities (outside of top 20)
    private int top20ListLimit;

    public Library() {

    }

    // GENERIC
    static <T> List<T> union(List<T> set1, List<T> set2) {
        List<T> result = new ArrayList<>(set1);
        result.addAll(set2);
        return result;
    }

    public static void main(String[] args) {

        // create an instance of the library that contains: 15 books, 5 videos and 7 mp3
        Library mediaLibrary = new Library();

        // introduce the top list limit, here is 20
        mediaLibrary.setTopListLimit(20);

        // 15 books
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Last night", 1, "Petrescu", "Junimea"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Last summer", 3, "Lazarescu", "Corint"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "King", 23, "Dumas", "Luceafarul"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Queen", 20, "Dumas Jr", "Adevarul"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Rainy day", 14, "Petrovici", "Corint"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Game night", 34, "Dostoievski", "Adevarul"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Quess", 21, "Anonim", "Adevarul"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Who is the Guest?", 17, "Petrescu", "Tineretul"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Who are you ?", 50, "Dumas Jr", "Plus"));
        //mediaLibrary.add(new Book(MediaEntityType.BOOK, "Notre Dame", 0, "Dumas Jr", "Dreptatea"));
        MediaEntity m1 = new Book(MediaEntityType.BOOK, "Notre Dame", 0, "Dumas Jr", "Dreptatea");
        mediaLibrary.addDynamic(m1);
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Castelul", 46, "Anonim", "Junimea"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Proza", 23, "Rebreanu", "Junimea"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Dealul", 17, "Popescu", "Adevarul"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Storm", 60, "Calinescu", "Plus"));
        mediaLibrary.addDynamic(new Book(MediaEntityType.BOOK, "Rande vu", 16, "Dumas", "Adevarul"));

        // 5 videos
        mediaLibrary.addDynamic(new Video(MediaEntityType.VIDEO, "Hello", 10, 60, true));
        mediaLibrary.addDynamic(new Video(MediaEntityType.VIDEO, "Good bye", 4, 45, false));
        mediaLibrary.addDynamic(new Video(MediaEntityType.VIDEO, "Blues", 23, 60, true));
        mediaLibrary.addDynamic(new Video(MediaEntityType.VIDEO, "Strange", 50, 120, false));
        mediaLibrary.addDynamic(new Video(MediaEntityType.VIDEO, "Known", 20, 30, true));

        // 7 mp3
        mediaLibrary.addDynamic(new Mp3(MediaEntityType.MP3, "The one", 26, "Anastasia", "Good bye"));
        mediaLibrary.addDynamic(new Mp3(MediaEntityType.MP3, "Blues", 16, "Bianca", "Hello"));
        mediaLibrary.addDynamic(new Mp3(MediaEntityType.MP3, "Come here", 20, "Roberta", "80s"));
        mediaLibrary.addDynamic(new Mp3(MediaEntityType.MP3, "Let's dance", 19, "Anonim", "Rock"));
        mediaLibrary.addDynamic(new Mp3(MediaEntityType.MP3, "Bye", 25, "Carol", "Hello"));
        mediaLibrary.addDynamic(new Mp3(MediaEntityType.MP3, "Wake up", 51, "Cristian", "The best"));
        mediaLibrary.addDynamic(new Mp3(MediaEntityType.MP3, "Who are you?", 90, "Team", "Good bye"));

        System.out.println("All the media items => " + union(mediaLibrary.top20, mediaLibrary.archive));
        System.out.println("All media size => " + union(mediaLibrary.top20, mediaLibrary.archive).size());

        System.out.println("Search by Hello property => " + mediaLibrary.search("Hello"));

        // display all the media entities that are of type video
        System.out.println("Search by VIDEO type => " + mediaLibrary.search(MediaEntityType.VIDEO));

        System.out.println("Search by number of downloads = 21 => " + mediaLibrary.search(21));

        System.out.println("Top20 list items => " + mediaLibrary.top20);
        System.out.println("Top20 list size: " + mediaLibrary.top20.size());

        System.out.println("archive list items => " + mediaLibrary.archive);
        System.out.println("archive List size: " + mediaLibrary.archive.size());

        System.out.println("\n -------- Update noOfDownloads ----------- \n");
        System.out.println("The future updated item belongs to top20 list: " + mediaLibrary.top20.contains(m1));
        System.out.println("The future updated item belongs to archive list: " + mediaLibrary.archive.contains(m1));
        mediaLibrary.updateNoOfDownloads(m1, 100);

        System.out.println("All the media items after update => " + union(mediaLibrary.top20, mediaLibrary.archive));
        System.out.println("All media size => " + union(mediaLibrary.top20, mediaLibrary.archive).size());

        System.out.println("Top20 list items after update => " + mediaLibrary.top20);
        System.out.println("Top20 list size: " + mediaLibrary.top20.size());

        System.out.println("archive list items after update => " + mediaLibrary.archive);
        System.out.println("archive List size: " + mediaLibrary.archive.size());

        System.out.println("After update the item belongs to top20 list: " + mediaLibrary.top20.contains(m1));
        System.out.println("After update the item belongs to archive list: " + mediaLibrary.archive.contains(m1));

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Library)) return false;
        Library library = (Library) o;
        return top20ListLimit == library.top20ListLimit &&
                Objects.equals(top20, library.top20) &&
                Objects.equals(archive, library.archive);
    }

    public void setTopListLimit(int topListLimit) {
        this.top20ListLimit = topListLimit;
    }

    /* searching by MediaEntity properties (select any type of MediaEntity => title OR MediaEntityType OR noOfDownloads)
        from top20 and archive lists
     */
    public <T> List<MediaEntity> search(T property) {
        Iterator<MediaEntity> iter = union(top20, archive).iterator();      // search into a list
        MediaEntity itemValue;
        List<MediaEntity> itemSearchProperty = new ArrayList<>();
        while (iter.hasNext()) {
            itemValue = iter.next();
            switch (property.getClass().getSimpleName()) {
                case "MediaEntityType":
                    if (itemValue.getType().equals(property)) {
                        itemSearchProperty.add(itemValue);
                    }
                    break;
                case "String":
                    if (itemValue.getTitle().equals(property)) {
                        itemSearchProperty.add(itemValue);
                    }
                    break;
                case "Integer":
                    if (itemValue.getNoOfDownloads().equals(property)) {
                        itemSearchProperty.add(itemValue);
                    }
                    break;
            }
        }
        return itemSearchProperty;
    }

    public void addDynamic(MediaEntity mediaItem) {
        top20.add(mediaItem);       // add items in top20 list
        top20.sort(new LibraryComparatorByNoOfDownloads());    // sort top20 list in descending order

        if (top20.size() > top20ListLimit) {
            Iterator<MediaEntity> iter = top20.iterator();
            MediaEntity value = new MediaEntity();
            while (iter.hasNext()) {
                value = iter.next();    // iterate to get the last value of top20 list
            }
            iter.remove();          // remove the last element from top20 list
            archive.add(value);     // add the removed element into archive list
        }
        archive.sort(new LibraryComparatorByNoOfDownloads());
    }

    /* Update noOfDownloads field of any list (top20 or archive can be)
     */
    private MediaEntity updateNoOfDownloads(MediaEntity mediaItem, int number, List<MediaEntity> list) {
        Iterator<MediaEntity> itr = list.iterator();
        MediaEntity valueToBeReplace = new MediaEntity();
        while (itr.hasNext()) {
            MediaEntity value = itr.next();
            if (value.equals(mediaItem)) {
                value.setNoOfDownloads(number);
                valueToBeReplace = value;
                itr.remove();     // remove the item
            }
        }
        return valueToBeReplace;
    }

    /* update for the noOfDownloads field of a MediaEntity
     */
    public void updateNoOfDownloads(MediaEntity mediaItem, int number) {
        MediaEntity value;
        if (top20.contains(mediaItem)) {
            value = updateNoOfDownloads(mediaItem, number, top20);
            addDynamic(value);      // add the removed item and then sync the list
        } else if (archive.contains(mediaItem)) {
            value = updateNoOfDownloads(mediaItem, number, archive);
            addDynamic(value);      // add the removed item and then sync the list
        }
    }

}
