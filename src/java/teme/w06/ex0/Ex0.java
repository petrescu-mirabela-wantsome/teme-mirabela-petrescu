package teme.w06.ex0;

import java.util.*;

public class Ex0 {

    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>(Arrays.asList("aa", "bb", "cc"));
        Set<String> set2 = new HashSet<>(Arrays.asList("bb", "cc", "dd"));

        System.out.println("set1: " + set1);
        System.out.println("set2: " + set2);
        System.out.println("union <String>: " + union(set1, set2));
        System.out.println("intersection <String>: " + intersection(set1, set2));
        System.out.println("difference <String>: " + difference(set1, set2));

        Set<Integer> si1 = new HashSet<>(Arrays.asList(1, 2, 3));
        Set<Integer> si2 = new HashSet<>(Arrays.asList(2, 3, 4));
        System.out.println("si1: " + si1);
        System.out.println("si2: " + si2);
        System.out.println("union <Integer>: " + union(si1, si2));
        System.out.println("intersection <Integer>: " + intersection(si1, si2));
        System.out.println("difference <Integer>: " + difference(si1, si2));

    /*
    c) Comparing Persons :
    ii) Make it sortable by name by default, by implementing Comparable based on
    name. Test it by having adding some persons in a sorted type of Set, and
    then print them in the default iteration order. Question: do you also need to
    override equals & hashCode for this to work?..
     */

        List<Person> personList = Arrays.asList(
                new Person(1111, "Ion", 20, 180),
                new Person(3333, "Ana", 22, 170),
                new Person(2222, "Vasile", 23, 170),
                new Person(5555, "Andrei", 31, 170),
                new Person(4444, "Maria", 19, 160)
        );

        List<Person> sortedPerson = new ArrayList<>(personList);
        System.out.println(sortedPerson);
        Collections.sort(sortedPerson);
        System.out.println("Sorted by name: " + sortedPerson);
        Collections.sort(sortedPerson, new PersonComparatorByCnp());
        System.out.println("Sorted by cnp: " + sortedPerson);

        Set<Person> sortedSet = new TreeSet<>(personList);
        System.out.println(sortedSet);

        Set<Person> sortedSet2 = new TreeSet<>(new PersonComparatorByCnp());
        sortedSet2.addAll(personList);
        System.out.println("Sorted by cnp (TreeSet): " + sortedSet2);

        /*
        c) Comparing Persons :
        iv) Implement another comparator which orders persons first by height
        (ascending) then by age (descending). Test it on a list of persons, making
        sure it works correctly for all cases (persons of same height..). Try to test it by
        adding persons in a List, then using Collections.sort() to sort it (using this
        specific comparator), and then print the contents.
         */
        Collections.sort(sortedPerson, new PersonComparatorByHeight());
        System.out.println("Sorted by height: " + sortedPerson);

        /* e) Symmetrical array:
        i) Write a static method ‘boolean isSymmetrical(int[] array)’, which receives an
        array of int values and returns true if the array is symmetrical - meaning the
        order of elements from start to end is the same as from end to start.
        ii) Make the method more generic, to be able to work with an array of elements
        of any type (instead of just int). Test it on some arrays of String, Double,
        Person..
        Question: how much of the code did you need to change for this? (anything
        besides the method signature? why?)
         */
        System.out.println(isSymmetrical(new int[]{}));
        System.out.println(isSymmetrical(new int[]{1}));
        System.out.println(isSymmetrical(new int[]{1, 2, 2, 1}));
        System.out.println(isSymmetrical(new int[]{3, 2, 1, 2, 3}));
        System.out.println(isSymmetrical(new int[]{1, 2, 1, 2}));
        System.out.println(isSymmetrical(new int[]{1, 2, 3}));

        System.out.println(isSymmetricalGen(new Integer[]{}));
        System.out.println(isSymmetricalGen(new Integer[]{1}));
        System.out.println(isSymmetricalGen(new Integer[]{1, 2, 2, 1}));
        System.out.println(isSymmetricalGen(new Integer[]{3, 2, 1, 2, 3}));
        System.out.println(isSymmetricalGen(new Integer[]{1, 2, 1, 2}));
        System.out.println(isSymmetricalGen(new Integer[]{1, 2, 3}));
        System.out.println(isSymmetricalGen(new String[]{"aa", "bb", "cc"}));
        System.out.println(isSymmetricalGen(new String[]{"aa", "bb", "bb", "aa"}));

        /* f) Palindrome :
        Write a method ‘boolean isPalindrome(String)’ which takes a phrase as a String,
        eliminates the spaces between words (if any), lowercase it, then returns true if the
        resulting string is a palindrome - meaning it has the same value if read backwards or
        forwards.
        */
        System.out.println("Palindrome: " + isPalindrome("rotitor"));
        System.out.println("Palindrome: " + isPalindrome("calabalac"));
        System.out.println("Palindrome: " + isPalindrome(" Ene purta patru pen"));
        System.out.println("Palindrome: " + isPalindrome(" Ion a luat luni vinul tau la noi"));
        System.out.println("Palindrome: " + isPalindrome("Step on no pets"));
        System.out.println("Palindrome: " + isPalindrome(" Never odd or even"));
        System.out.println("Palindrome: " + isPalindrome(" Was it a car or a cat I sa"));

    }

    /* a) Set operations:
        Define these static methods; each of them receives 2 parameters of type Set<String>
        and returns a new set of the same type:
        i) union() - returns a set with all elements of both sets combined.
        ii) intersection() - returns a set with only the elements common to both sets.
        iii) difference() - returns a set with only the elements found in first given set, but
        not in 2nd one.
        Note: your code should NOT modify in
        */

//    static Set<String> union(Set<String> set1, Set<String> set2) {
//        Set<String> result = new HashSet<>(set1);
//        result.addAll(set2);
//        return result;
//    }

    /*b) Generic set operations:
    Make the previous methods more generic, so they will work on sets of any type of
    objects. Test them on sets of Integer, Double, Person, etc...
    i) question: how much did you need to change your method’s code? what about
    your test code?
    ii) question: why is this way (using generics) better than just working with
    Set<Object>?
    */
    // GENERIC
    private static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>(set1);
        result.addAll(set2);
        return result;
    }

    private static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>();
        for (T itemSet1 : set1) {
            if (set2.contains(itemSet1)) {
                result.add(itemSet1);
            }
        }
        return result;
    }

    private static <T> Set<T> difference(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>();
        for (T itemSet1 : set1) {
            if (!set2.contains(itemSet1)) {
                result.add(itemSet1);
            }
        }
        return result;
    }

    /* e) Symmetrical array:
    i) Write a static method ‘boolean isSymmetrical(int[] array)’, which receives an
    array of int values and returns true if the array is symmetrical - meaning the
    order of elements from start to end is the same as from end to start.
    ii) Make the method more generic, to be able to work with an array of elements
    of any type (instead of just int). Test it on some arrays of String, Double,
    Person..
    Question: how much of the code did you need to change for this? (anything
    besides the method signature? why?)
     */
    private static boolean isSymmetrical(int[] array) {
        for (int i = 0, j = array.length - 1; i < array.length / 2 && j >= array.length / 2; i++, j--) {
            if (array[i] != array[j]) {
                return false;
            }
        }
        return true;
    }

    private static <T> boolean isSymmetricalGen(T[] array) {
        for (int i = 0, j = array.length - 1; i < array.length / 2 && j >= array.length / 2; i++, j--) {
            if (!array[i].equals(array[j])) {
                return false;
            }
        }
        return true;
    }

    /*
    f) Palindrome :
        Write a method ‘boolean isPalindrome(String)’ which takes a phrase as a String,
        eliminates the spaces between words (if any), lowercase it, then returns true if the
        resulting string is a palindrome - meaning it has the same value if read backwards or
        forwards.
        Try to reuse the isSymmetrical() methods from previous exercise!
        (hint: you will need to convert your String to an array of letters; but note that
        .toCharArray() will not work, as it returns a char[] - an array of primitives, not usable
        with your generic isSymetrical method; try instead to call String.split(“”) to get a
        String[] array...)
        Your palindrome method should respond with true for strings like these:
        “aerisirea”, “rotitor”, “ calabalac”, “ Ene purta patru pene” , “ Ion a luat luni vinul tau la noi”,
        “Step on no pets”, “ Never odd or even”, “ Was it a car or a cat I saw”
        (more info: https://en.wikipedia.org/wiki/Palindrome , https://ro.wikipedia.org/wiki/Palindrom )
     */
    private static boolean isPalindrome(String str) {
        String[] strChar = str.replaceAll(" ", "").toLowerCase().split("");
        return isSymmetricalGen(strChar);
    }

}
