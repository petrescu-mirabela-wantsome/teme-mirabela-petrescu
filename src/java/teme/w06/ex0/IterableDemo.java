package teme.w06.ex0;


import java.util.Iterator;

public class IterableDemo {
    public static void main(String[] args) {

        SquaresIterable iterable = new SquaresIterable(1024);

        Iterator<Long> it = iterable.iterator();
        while (it.hasNext()) {
            Long value = it.next();
//            if (value > 100) {
//                break;
//            }
            System.out.println(value);
        }

        for (Long value : iterable) {
//            if (value > 100) {
//                break;
//            }
            System.out.println(value);
        }
    }
}
