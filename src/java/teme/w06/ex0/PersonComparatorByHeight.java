package teme.w06.ex0;

/*
c) Comparing Persons :
iv) Implement another comparator which orders persons first by height
(ascending) then by age (descending). Test it on a list of persons, making
sure it works correctly for all cases (persons of same height..). Try to test it by
adding persons in a List, then using Collections.sort() to sort it (using this
specific comparator), and then print the contents.
 */

import java.util.Comparator;

class PersonComparatorByHeight implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        int heightResult = Integer.compare(o1.getHeight(), o2.getHeight());
        if (heightResult == 0) {    // if the height is the same
            // order the age
            int ageResult = Integer.compare(o1.getAge(), o2.getAge());  // will result an ascending order
            return -ageResult;  // reverse the ascending order => descending order
        }
        return heightResult;
    }
}
