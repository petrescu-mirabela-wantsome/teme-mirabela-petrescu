package teme.w06.ex0;


class Person implements Comparable<Person> {

    /*
    c) Comparing Persons :
    i) Write a class Person with fields: cnp, name, age, height (also having a
    constructor with values for all fields, and toString)
     */
    private final int cnp;
    private final String name;
    private final int age;
    private final int height;

    public Person(int cnp, String name, int age, int height) {
        this.cnp = cnp;
        this.name = name;
        this.age = age;
        this.height = height;
    }

    public int getCnp() {
        return cnp;
    }

    public int getAge() {
        return age;
    }

    public int getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "Person{" +
                "cnp='" + cnp + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", height=" + height +
                '}';
    }

    /*
    c) Comparing Persons :
    ii) Make it sortable by name by default, by implementing Comparable based on
    name. Test it by having adding some persons in a sorted type of Set, and
    then print them in the default iteration order. Question: do you also need to
    override equals & hashCode for this to work?..
     */
    @Override
    public int compareTo(Person p) {
        return name.compareTo(p.name);
    }
}
