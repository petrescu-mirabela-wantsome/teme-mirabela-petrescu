package teme.w12.ex1_pet_shop;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PetTypesReader {

    private final List<PetTypesDTO> listPetTypesReader = new ArrayList<>();

    public List<PetTypesDTO> getListPetTypesReader() {
        return listPetTypesReader;
    }

    @Override
    public String toString() {
        return "PetTypesReader{" +
                "listPetTypesReader=" + listPetTypesReader +
                '}';
    }

    public void readPetTypesFromCsv(String csvFilePath) throws FileNotFoundException, ArrayIndexOutOfBoundsException {
        File csvFile = new File(csvFilePath);
        int lineCount = 0;
        Scanner fileScanner = new Scanner(csvFile);
        while (fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            String[] csvLineArray = line.split(",");
            lineCount++;
            try {
                listPetTypesReader.add(new PetTypesDTO(Integer.valueOf(csvLineArray[0]), csvLineArray[1]));
            } catch (NumberFormatException e1) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));
            } catch (ArrayIndexOutOfBoundsException e2) {
                System.out.println("Line " + lineCount + " has less fields than expected => " + Arrays.toString(csvLineArray));
            } catch (IllegalArgumentException e3) {
                System.out.println("Line " + lineCount + " has wrong type => " + Arrays.toString(csvLineArray));        // SEX enum case
            }
        }
    }
}
