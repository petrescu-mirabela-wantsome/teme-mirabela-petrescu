package teme.w12.ex1_pet_shop;


import java.sql.Date;

public class PetsDTO {
    private int id;
    private String name;
    private int person_id;
    private Date birthdate;
    private String gender;
    private int type;

    public PetsDTO() {
    }

    public PetsDTO(int id, String name, int person_id, Date birthdate, String gender, int type) {
        this.id = id;
        this.name = name;
        this.person_id = person_id;
        this.birthdate = birthdate;
        this.gender = gender;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "PetsDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", person_id=" + person_id +
                ", birthdate=" + birthdate +
                ", gender='" + gender + '\'' +
                ", type=" + type +
                '}';
    }
}
