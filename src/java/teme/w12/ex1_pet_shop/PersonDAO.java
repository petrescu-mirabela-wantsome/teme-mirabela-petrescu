package teme.w12.ex1_pet_shop;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PersonDAO extends AbstractDao<PersonDTO> {
    @Override
    protected List<PersonDTO> executeSelect(Connection c) {
        String sql = "SELECT * FROM person ORDER BY id";
        List<PersonDTO> results = new ArrayList<>();
        try (PreparedStatement ps = c.prepareStatement(sql);
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                PersonDTO item = new PersonDTO();
                item.setId(rs.getInt("id"));
                item.setFirst_name(rs.getString("first_name"));
                item.setLast_name(rs.getString("last_name"));
                item.setAge(rs.getInt("age"));
                item.setGender(rs.getString("gender"));
                results.add(item);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return results;
    }

    @Override
    protected PersonDTO executeSelect(Connection c, int id) {
        String sql = "SELECT * FROM person WHERE id = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                PersonDTO item = new PersonDTO();
                item.setId(rs.getInt("id"));
                item.setFirst_name(rs.getString("first_name"));
                item.setLast_name(rs.getString("last_name"));
                item.setAge(rs.getInt("age"));
                item.setGender(rs.getString("gender"));
                return item;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void executeInsert(Connection c, PersonDTO object) {
        String sql = "INSERT INTO person (first_name, last_name, age, gender) VALUES(?,?,?,?)";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getFirst_name());
            ps.setString(2, object.getLast_name());
            ps.setInt(3, object.getAge());
            ps.setString(4, object.getGender());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeUpdate(Connection c, PersonDTO object) {
        String sql = "UPDATE person SET first_name = ?, last_name = ?, age = ?, gender = ? WHERE id = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setString(1, object.getFirst_name());
            ps.setString(2, object.getLast_name());
            ps.setInt(3, object.getAge());
            ps.setString(4, object.getGender());
            ps.setInt(5, object.getId());
            ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void executeDelete(Connection c, PersonDTO object) {
        String sql = "DELETE FROM person WHERE id = ?";
        try (PreparedStatement ps = c.prepareStatement(sql)) {
            ps.setInt(1, object.getId());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
