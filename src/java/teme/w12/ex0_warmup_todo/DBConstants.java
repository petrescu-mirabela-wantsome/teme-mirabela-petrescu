package teme.w12.ex0_warmup_todo;

public class DBConstants {

    enum Priority {
        LOW,
        MEDIUM,
        HIGH
    }

    enum State {
        COMPLETED,
        STILL_ACTIVE
    }

}
