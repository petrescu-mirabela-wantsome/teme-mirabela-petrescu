package teme.w12.ex0_warmup_todo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TodoDAO extends AbstractDao<TodoDTO> {

    private static DBConstants.Priority getPriority(String priority) {
        switch (priority) {
            case "LOW":
                return DBConstants.Priority.LOW;
            case "MEDIUM":
                return DBConstants.Priority.MEDIUM;
            case "HIGH":
                return DBConstants.Priority.HIGH;
        }
        return null;
    }

    private static DBConstants.State getState(String state) {
        if (state.equals("COMPLETED")) {
            return DBConstants.State.COMPLETED;
        } else if (state.equals("STILL_ACTIVE")) {
            return DBConstants.State.STILL_ACTIVE;
        }
        return null;
    }

    @Override
    protected List<TodoDTO> executeSelect(Connection c) {
        String SQL = "SELECT * FROM todo ORDER BY deadline DESC";
        List<TodoDTO> sqlInfo = new ArrayList<>();
        try (PreparedStatement preparedStatement = c.prepareStatement(SQL);
             ResultSet resultSet = preparedStatement.executeQuery()) {
            while (resultSet.next()) {
                TodoDTO todoDTO = new TodoDTO();
                todoDTO.setId(resultSet.getLong("id"));
                todoDTO.setDescription(resultSet.getString("description"));
                todoDTO.setPriority(getPriority(resultSet.getString("priority")));
                todoDTO.setDeadline(resultSet.getDate("deadline"));
                todoDTO.setState(getState(resultSet.getString("state")));
                sqlInfo.add(todoDTO);
            }
        } catch (SQLException e) {
        }
        return sqlInfo;
    }

    @Override
    protected TodoDTO executeSelect(Connection c, long id) {
        String SQL = "SELECT * FROM todo WHERE id = ?";
        try (PreparedStatement preparedStatement = c.prepareStatement(SQL)) {
            preparedStatement.setLong(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                TodoDTO todoDTO = new TodoDTO();
                todoDTO.setId(resultSet.getLong("id"));
                todoDTO.setDescription(resultSet.getString("description"));
                todoDTO.setPriority(getPriority(resultSet.getString("priority")));
                todoDTO.setDeadline(resultSet.getDate("deadline"));
                todoDTO.setState(getState(resultSet.getString("state")));
                return todoDTO;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void executeInsert(Connection c, TodoDTO object) {
        String SQL = "INSERT INTO todo (description, priority, deadline, state) VALUES (?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = c.prepareStatement(SQL)) {
            preparedStatement.setString(1, object.getDescription());
            preparedStatement.setString(2, object.getPriority().name());
            preparedStatement.setDate(3, object.getDeadline());
            preparedStatement.setString(4, object.getState().name());
            preparedStatement.execute();
        } catch (SQLException e) {
        }
    }

    @Override
    protected void executeUpdate(Connection c, TodoDTO object) {
        String SQL = "UPDATE todo SET description = ?, priority = ?, deadline = ?, state = ? WHERE id = ?";
        try (PreparedStatement preparedStatement = c.prepareStatement(SQL)) {
            preparedStatement.setString(1, object.getDescription());
            preparedStatement.setString(2, object.getPriority().name());
            preparedStatement.setDate(3, object.getDeadline());
            preparedStatement.setString(4, object.getState().name());
            preparedStatement.setLong(5, object.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
        }
    }

    @Override
    protected void executeDelete(Connection c, TodoDTO object) {
        String SQL = "DELETE FROM todo WHERE id = ?";
        try (PreparedStatement preparedStatement = c.prepareStatement(SQL)) {
            preparedStatement.setLong(1, object.getId());
            preparedStatement.execute();
        } catch (SQLException e) {
        }
    }
}
