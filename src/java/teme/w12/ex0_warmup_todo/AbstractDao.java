package teme.w12.ex0_warmup_todo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

abstract class AbstractDao<T> {

    public List<T> get() {
        try (Connection connection = getConnection()) {
            return executeSelect(connection);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public T get(long id) {
        try (Connection connection = getConnection()) {
            return executeSelect(connection, id);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void insert(T object) {
        try (Connection connection = getConnection()) {
            executeInsert(connection, object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void update(T object) {
        try (Connection connection = getConnection()) {
            executeUpdate(connection, object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void delete(T object) {
        try (Connection connection = getConnection()) {
            executeDelete(connection, object);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected abstract List<T> executeSelect(Connection c);

    protected abstract T executeSelect(Connection c, long id);

    protected abstract void executeInsert(Connection c, T object);

    protected abstract void executeUpdate(Connection c, T object);

    protected abstract void executeDelete(Connection c, T object);

    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                "jdbc:mysql://159.69.118.199/wantsome_java6_mirabela",
                "wantsome_qa", "22ehf!E0");
    }
}
