package teme.w12.ex0_warmup_todo;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

public class TodoMain {

    public static void main(String[] args) {

        TodoDAO todoDao = new TodoDAO();
        List<TodoDTO> items = todoDao.get();
        System.out.println("Initial size: " + items.size());
        System.out.println("Actual objects: " + items);

        TodoDTO newItem = new TodoDTO();
        newItem.setDescription("drink");
        newItem.setPriority(DBConstants.Priority.HIGH);
        newItem.setDeadline(new Date(Calendar.getInstance().getTimeInMillis()));
        newItem.setState(DBConstants.State.STILL_ACTIVE);

        // I don't have an ID set!!!
        todoDao.insert(newItem);
        items = todoDao.get();

        System.out.println("Actual objects: " + items);
        newItem = items.stream().filter(i -> i.getDescription().equalsIgnoreCase("drink")).findFirst().get();
        System.out.println("Newest inserted item: " + newItem);
        newItem.setDescription("drink and sleep");
        todoDao.update(newItem);
        newItem = todoDao.get(newItem.getId());
        System.out.println("Updated item: " + newItem);
        System.out.println("Current list size: " + items.size());
        todoDao.delete(newItem);
        items = todoDao.get();
        System.out.println("Current list size after delete: " + items.size());

        System.out.println("Actual objects before state change: " + items);
        for (TodoDTO item : items) {
            if (item.getState().equals(DBConstants.State.STILL_ACTIVE)) {
                item.setState(DBConstants.State.COMPLETED);
            } else if (item.getState().equals(DBConstants.State.COMPLETED)) {
                item.setState(DBConstants.State.STILL_ACTIVE);
            }
            todoDao.update(item);
        }
        System.out.println("Actual objects after state change: " + items);
    }
}
