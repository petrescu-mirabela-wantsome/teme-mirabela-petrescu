package rezolvari_teme.w07_exceptions.ex2_reactor;

/*
2. Nuclear reactor

A nuclear power plant is comprised of the following parts:
- A reactor and
- A plant controller

Write the following classes:
1. A ​ PowerPlant​ class that:
  a. Receives as parameter in the constructor an int that signifies the desired output of the nuclear plant.
  b. Has a method that returns the desired output
  c. Has a method soundAlart() which prints an alarm message to console

2. A ​ Reactor​ class:
  a. The class constructor receives a int that specifies power output level at which the reactor goes critical.
  b. Starts with a power output of 0.
  c. Has a method which returns the current output as an int value.
  d. Has two methods that increase and decrease the output (with a random value between 1 and 100).
    i. The increase method will throw an exception if the reactor is going critical (current power output becomes greater than critical level)
   ii. The decrease method should not decrease the output to negative values (but only down to 0)

3. A ​ PlantController​ class:
  a. Receives a PowerPlant and a Reactor instance in its constructor
  b. Has a needAdjustment() method that returns true when the difference between the reactor output and plant desired output differs by a value larger than 10
  c. Has a method adjust() which adjust the output by repeatedly calling the increase() method of the reactor as long as needed (until needAdjustment() returns false)
  d. Has a shutdown() method that repeatedly calls the decrease method of the reactor until the output is 0
  e. Has a run() method that runs constantly and checks whether the reactor needs adjustment and calls the adjust method.
     If the reactor goes critical call the sound alarm method and shutdown the reactor.

Write a test class which creates a power plant with desired output is 120, using an reactor for which the critical threshold is 150.
The test should also create a plant controller, on which it should call the run() method.

Run the test a few times, and check that only one of these 2 valid scenarios happen:
  - The power plant reaches a stable output (with difference between it and the desired output being less than 10 units),
    and alarm is not sounded / the power remains running
  - OR: after a few increases, the reactor goes critical; then the alarm should be sounded and the reactor should be shutdown,
    and after a few decrease steps it should reach current output of 0.

Note: for easier testing, methods should also print some info about the current status of the power plant / reactor, after each step that changes their states.
*/

class PlantMain {
    public static void main(String[] args) {

        System.out.println("Creating and starting a new power plant and reactor:");

        PlantController pc = new PlantController(new PowerPlant(120), new Reactor(150));
        pc.run();
    }
}
