package rezolvari_teme.w07_exceptions.ex2_reactor;

class PlantController {

    private final PowerPlant plant;
    private final Reactor reactor;

    PlantController(PowerPlant plant, Reactor reactor) {
        this.plant = plant;
        this.reactor = reactor;
    }

    void run() {
        System.out.println("Powering up the reactor... (desired output: " + plant.getDesiredOutput() + ")");
        try {
            while (needAdjustment()) {
                reactor.inc();
            }
            System.out.println("Reactor reached stable power level of: " + reactor.getOutput());
        } catch (ReactorCriticalException e) {
            System.out.println("Exception: " + e.getMessage());
            plant.soundAlarm();
            shutdown();
        }
    }

    private boolean needAdjustment() {
        return (reactor.getOutput() < plant.getDesiredOutput() - 10);
    }

    private void shutdown() {
        System.out.println("Shutting down reactor...");
        while (reactor.getOutput() > 0) {
            reactor.dec();
        }
        System.out.println("...done (reactor output: " + reactor.getOutput() + ")");
    }
}
