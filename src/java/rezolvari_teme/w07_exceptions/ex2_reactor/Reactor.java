package rezolvari_teme.w07_exceptions.ex2_reactor;

class Reactor {

    private final int criticalLevel;
    private int output = 0;

    Reactor(int criticalLevel) {
        this.criticalLevel = criticalLevel;
    }

    int getOutput() {
        return output;
    }

    void inc() throws ReactorCriticalException {
        output += randomStep();
        System.out.println("Reactor output inc to: " + output);
        if (output > criticalLevel) {
            throw new ReactorCriticalException("Reactor went critical! output: " + output + " > critical level: " + criticalLevel);
        }
    }

    void dec() {
        output -= randomStep();
        if (output < 0) {
            output = 0;
        }
        System.out.println("Reactor output dec to: " + output);
    }

    private int randomStep() {
        return (int) (Math.random() * 100);
    }
}
