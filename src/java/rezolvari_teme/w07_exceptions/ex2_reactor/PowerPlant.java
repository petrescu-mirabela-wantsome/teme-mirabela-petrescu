package rezolvari_teme.w07_exceptions.ex2_reactor;

class PowerPlant {

    private final int desiredOutput;

    PowerPlant(int desiredOutput) {
        this.desiredOutput = desiredOutput;
    }

    int getDesiredOutput() {
        return desiredOutput;
    }

    void soundAlarm() {
        System.out.println("!ALARM: reactor is critical! Shutting it down...");
    }
}
