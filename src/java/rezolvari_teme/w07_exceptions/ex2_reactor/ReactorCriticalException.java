package rezolvari_teme.w07_exceptions.ex2_reactor;

class ReactorCriticalException extends Exception {
    ReactorCriticalException(String message) {
        super(message);
    }
}
