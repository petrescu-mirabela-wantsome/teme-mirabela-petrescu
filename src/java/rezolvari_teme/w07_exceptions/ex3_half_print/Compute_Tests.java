package rezolvari_teme.w07_exceptions.ex3_half_print;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Unit tests for Compute and some Function implementations
 */
public class Compute_Tests {

    @Test
    public void testHalfFunction() {
        Function hf = new HalfFunction();
        assertEquals(0, hf.evaluate(0));
        assertEquals(3, hf.evaluate(6));
        assertEquals(3, hf.evaluate(7));
    }

    @Test
    public void testComputeWithHalf() {
        Compute computer = new Compute();
        Function halfFunc = new HalfFunction();

        int[] values = {10, 20, 30, 40, 50};
        assertArrayEquals(new int[]{5, 10, 15, 20, 25}, computer.compute(values, halfFunc));
    }

    @Test
    public void testPrintFunction() {
        Function pf = new PrintFunction();
        assertEquals(5, pf.evaluate(5));

        //TODO: not enough, should check that the function also prints the input value to the console!!
        //BUT: we have NO EASY WAY TO CHECK THIS! (side effect..)
    }
}
