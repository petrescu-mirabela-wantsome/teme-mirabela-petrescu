package rezolvari_teme.w07_exceptions.ex3_half_print;

/*
3. Half and print

a. Declare an ​ interface​ named ​ Function​ that has a single method named ​ evaluate​ that takes an int parameter and returns an int value.

b. Create a ​ class​ named ​ Compute​ , with a method compute() that:
  - takes 2 input parameters:
    - an array of int values
    - an instance of type Function (so may pass here an instance of any class which implements that interface)
  - returns: an array of same size as input array, but it contains as values the result of applying the function passed as parameter
    to each of the elements of the input (it basically transforms each value in input array by applying the given Function)
  - note: the method should NOT affect the array of input values given as parameter (by updating any of them), but build and return a separate new array!

c. Create a ​ class​ named ​ HalfFunction​ that implements the Function interface. Make the implementation of the method evaluate()
   return the value obtained by dividing the int argument by 2.

d. Create a ​ class​ named ​ Print​Function that implements the Function interface, and its implementation of evaluate() method
   simply prints out the give int value, and then also returns it (as is, no changes).

e. Write in the main() method of a new class (may name it HalfAndPrint) some code which does the following:
  - creates an array of some int values from 1 to 10
  - prints the array using an instance of the PrintFunction class from above (and an instance of the Compute class)
  - halves all the values of the array and prints the resulting values, using the Compute, HalfFunction and PrintFunction classes.

f. Optional: write a JUnit test to test the functionality of Compute and HalfFunction classes
  - Question: can you write a similar test for the PrintFunction class? Is it easier or harder to do than for HalfFunction class? (why?)
*/

import java.util.Arrays;

public class HalfAndPrint {
    public static void main(String[] args) {


        Compute computer = new Compute();
        Function halfFunc = new HalfFunction();
        Function printFunc = new PrintFunction();

        int[] values = {10, 20, 30, 40, 50};
        System.out.println("Input values: " + Arrays.toString(values));

        System.out.println("\nApplying just printFunc to input values:");
        computer.compute(values, printFunc);

        System.out.println("\nApplying halfFunc then printFunc to input values:");
        computer.compute(
                computer.compute(values, halfFunc),
                printFunc);
    }
}
