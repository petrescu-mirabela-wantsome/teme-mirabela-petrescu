package rezolvari_teme.w07_exceptions.ex3_half_print;

public class Compute {
    int[] compute(int[] values, Function function) {
        int[] result = new int[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = function.evaluate(values[i]);
        }
        return result;
    }
}
