package rezolvari_teme.w07_exceptions.ex3_half_print;

public class HalfFunction implements Function {
    @Override
    public int evaluate(int value) {
        return value / 2;
    }
}
