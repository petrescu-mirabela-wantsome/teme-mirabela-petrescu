package rezolvari_teme.w07_exceptions.ex3_half_print;

public interface Function {
    int evaluate(int value);
}
