package rezolvari_teme.w07_exceptions.ex3_half_print;

public class PrintFunction implements Function {
    @Override
    public int evaluate(int value) {
        System.out.println(value);
        return value;
    }
}
