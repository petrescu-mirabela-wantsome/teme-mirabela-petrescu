package rezolvari_teme.w07_exceptions.ex1_sensor;

class ThermoSensor implements Sensor {

    private boolean on = false;

    @Override
    public boolean isOn() {
        return on;
    }

    @Override
    public void on() {
        on = true;
    }

    @Override
    public void off() {
        on = false;
    }

    @Override
    public int measure() {
        if (!on) {
            throw new RuntimeException("Thermo: failed to measure, sensor is off!");
        }
        int result = (int) (Math.random() * 60 - 30);
        System.out.println("Thermo measured: " + result);
        return result;
    }
}
