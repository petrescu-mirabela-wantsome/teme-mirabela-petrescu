package rezolvari_teme.w07_exceptions.ex1_sensor;

/*
1. Sensors and Measurements

You are given the Sensor interface.

Implement three types of sensors:

1. A ​ constant​ sensor:
  a. Is on all the time
  b. The methods on and off do nothing
  c. The class has a constructor with an int parameter and the measure method returns the value received as parameter

2. A ​ thermometer​ sensor
  a. After creation the thermometer is off
  b. When the measure method is called it returns a random int between -30 and 30
  c. If the thermometer is off the measure method will throw an exception

3. An ​ average​ sensor
  a. Contains multiple instances of sensors
  b. Has an additional method addSensor which adds a new sensor
  c. The average sensor is on when all of it’s sensors are on
  d. When the average sensor switched on all of its sensors are switched on if not already on
  e. When the average sensor is switched off some of its sensors are switched off (at least one)
  f. The measure method will return an average of the measured values of all its registered sensors
  g. If the measure method is called when the sensor is off or when there are no sensors registered to the average sensor throw an exception

Write a class that creates multiple instances of the constant and thermometer sensors, adds them
to an average sensor instance, and calls the methods of each to test the functionality specified above.
*/

class SensorMain {
    public static void main(String[] args) {
        Sensor c = new ConstantSensor(10);
        System.out.println("Constant: on: " + c.isOn() + ", measure: " + c.measure() + "\n");

        Sensor t = new ThermoSensor();
        t.on();
        System.out.println("Termo: on: " + t.isOn() + ", measure: " + t.measure() + "\n");

        Average a = new Average();
        a.addSensor(c);
        a.addSensor(t);
        System.out.println("Avg: on: " + a.isOn() + ", measure: " + a.measure() + "\n");

        //can add even itself, will create cycle!
        //a.addSensor(a);
        //System.out.println("Avg measure (with self!): " + a.measure()); //-> Exception in thread "main" java.lang.StackOverflowError
    }
}
