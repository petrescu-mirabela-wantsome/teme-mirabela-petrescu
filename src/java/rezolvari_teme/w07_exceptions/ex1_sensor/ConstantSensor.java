package rezolvari_teme.w07_exceptions.ex1_sensor;

class ConstantSensor implements Sensor {

    private final int value;

    ConstantSensor(int value) {
        this.value = value;
    }

    @Override
    public boolean isOn() {
        return true;
    }

    @Override
    public void on() {
    }

    @Override
    public void off() {
    }

    @Override
    public int measure() {
        return value;
    }
}
