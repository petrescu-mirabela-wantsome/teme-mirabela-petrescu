package rezolvari_teme.w07_exceptions.ex1_sensor;

import java.util.ArrayList;
import java.util.List;

class Average implements Sensor {

    private final List<Sensor> sensors = new ArrayList<>();

    void addSensor(Sensor s) {
        sensors.add(s);
    }

    @Override
    public boolean isOn() {
        for (Sensor s : sensors) {
            if (!s.isOn()) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void on() {
        for (Sensor s : sensors) {
            s.on();
        }
    }

    @Override
    public void off() {
        for (Sensor s : sensors) {
            s.off();
        }
    }

    @Override
    public int measure() {
        if (sensors.isEmpty() || !isOn()) {
            throw new RuntimeException("Average: failed to measure, no sensors or some are off!");
        }
        int sum = 0;
        for (Sensor s : sensors) {
            sum += s.measure();
        }
        return sum / sensors.size();
    }
}
