package rezolvari_teme.w07_exceptions.ex0_string_to_int;

public class NotANumberException extends Exception {
    public NotANumberException(String value) {
        super("Value '" + value + "' is not a number!");
    }
}
