package rezolvari_teme.w07_exceptions.ex0_string_to_int;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Unit tests for StringToInt
 */
public class StringToInt_Tests {

    @Test
    public void testNotANumber() {
        try {
            StringToInt.toPositiveInt("lucian");
            fail("Test failed: 'lucian' is not a number");
        } catch (NotANumberException e) {
            //ok, expected
        } catch (NegativeNumberException e) {
            fail("Test failed: got wrong exception for 'lucian': " + e);
        }
    }

    @Test
    public void testNegativeNumber() {
        try {
            StringToInt.toPositiveInt("-1");
            fail("Test failed: '-1' is not a positive number");
        } catch (NegativeNumberException e) {
            //ok, expected
        } catch (NotANumberException e) {
            fail("Test failed: got wrong exception for '-1': " + e);
        }
    }

    @Test
    public void testValidNumber() {
        try {
            StringToInt.toPositiveInt("1");
        } catch (NotANumberException | NegativeNumberException e) {
            fail("Test failed: '1' is a valid number, got exception: " + e);
        }
    }

    @Test
    public void testNumbersList() {

        List<String> stringValues = Arrays.asList("lucian", "-1", "1", "", "+2", "?", "3");

        List<Integer> convertedIntValues = StringToInt.toPositiveInt(stringValues);
        List<Integer> expectedValues = Arrays.asList(1, 2, 3);

        assertEquals(expectedValues, convertedIntValues);
    }
}
