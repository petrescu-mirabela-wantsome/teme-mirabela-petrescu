package rezolvari_teme.w07_exceptions.ex0_string_to_int;

public class NegativeNumberException extends Exception {
    public NegativeNumberException(String value) {
        super("Value '" + value + "' is a negative number!");
    }
}
