package rezolvari_teme.w07_exceptions.ex0_string_to_int;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
0. Warm-up

a. Write a (static) method `int toPositiveInt(String value)​`
  - It takes as input parameter a String value and tries to convert it to a positive integer and return it
  - In case of errors, it should throw 2 different types of exceptions (you’ll need to define 2 separate exception classes for it)
    - One for the case the string value is not a number at all
    - A different one for the case the value is a number, but a negative one

b. Write some code to test the toPositiveInt method:
  - First with some manual code (in a main() method), testing all 3 scenarios (not a number, a number but negative, a valid positive number)
  - Then write a JUnit test for it, covering also all 3 cases! (not only the positive one)
  - Question: how would you test the exception cases, to make sure the expected type of exception is really thrown? (and only when needed)

c. Write another (static) method ‘​ List<Integer> toPositiveInt(List<String> list)​’ (has same name as first one, so it overloads it)
  - It will receive a list of string values, which may be numbers
  - It tries to convert each string value to a positive number:
  - Use the first method toPositiveInt() to try to convert each string value
  - In case a value cannot be converted to a number (one of the 2 types of exceptions are thrown..), simply ignore it and don’t add it to the result list
  - But would be nice/useful to write a message about it (with System.err.println())
  - Note that this method should never throw exceptions itself, but it may just return a smaller list (even empty)
    with only the numbers for which conversion from string was successful

d. After you complete this version of toPositiveInt with List param, also write a few JUnit tests for it, trying to test
   all cases you can think of for the input (like: empty list, list of only valid numbers, list with valid and invalid values..)
  - Question: assuming you have the tests written well/completely for 1st method (at point b), how detailed need your test to be for this 2nd method?

e. Run again your tests for b) d), but this time with Coverage. What is the coverage for each of the 2 versions of the toPositiveInt() method?
   Do you notice any important logic which remained untested? And can you write some new tests to cover those spots / increase your coverage?
  - Question: what do you think is a good minimal coverage percent to aim for, in practice? (like: 10%, 25%, 50%, 80%, 90%, 99%, 100% ? )
  - Question: Is it realistic to aim/require 100% coverage for all code?...
*/

public class StringToInt {

    static int toPositiveInt(String value) throws NotANumberException, NegativeNumberException {
        try {
            int intValue = Integer.parseInt(value);
            if (intValue < 0) {
                throw new NegativeNumberException(value);
            }
            return intValue;
        } catch (NumberFormatException e) {
            throw new NotANumberException(value);
        }
    }

    static List<Integer> toPositiveInt(List<String> values) {
        List<Integer> result = new ArrayList<>();
        for (String value : values) {
            try {
                result.add(toPositiveInt(value));
            } catch (NotANumberException | NegativeNumberException e) {
                System.err.println("list value '" + value + "' skipped, reason: " + e.getMessage());
            }
        }
        return result;
    }

    /**
     * Some manual tests
     */
    public static void main(String[] args) {
        try {
            System.out.println(toPositiveInt("gigi"));
        } catch (NotANumberException | NegativeNumberException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(toPositiveInt("-1"));
        } catch (NotANumberException | NegativeNumberException e) {
            e.printStackTrace();
        }
        try {
            System.out.println(toPositiveInt("1"));
        } catch (NotANumberException | NegativeNumberException e) {
            e.printStackTrace();
        }

        List<String> values = Arrays.asList("lucian", "andrei", "-1", "-2", "1234", "2  ", "", "+3");
        System.out.println(toPositiveInt(values));
    }
}
