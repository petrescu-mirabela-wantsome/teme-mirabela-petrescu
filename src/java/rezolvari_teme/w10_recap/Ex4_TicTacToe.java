package rezolvari_teme.w10_recap;

import java.util.Arrays;
import java.util.Optional;
import java.util.Scanner;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.joining;

/*
4) TicTacToe game

Implement the game 'tic tac toe' (or 'three in a row'):
- there is an empty board, size 3x3
- 2 players, each marking a cell of the board with his sign ('X' or 'O'), taking turns
- first one which has 3 of his marks in a line (in any direction) wins
- if table fills up first, nobody wins

a) Implement the basic game first:
   - should be able to play 1 game
   - displaying the board after each move - simple text mode UI, just display the rows each on a separate line,
     with cell contents separated by space, each cell shown as 'X', 'O' or '_' (for empty)
   - get the positions players want to fill by reading from console 2 int values (row and column of the cell)
   - should validate the attempted move is valid, and also detect when the game ended, and display the winner.

b) Change your code so players can specify the cell address by a string like 'A2' - where 'A'..'C' represents the row,
   and '1'..'3' the column; to help him visualize this, display 1..3 above the board columns, and A..C in front of the rows.

c) Make the player names configurable: read them at the start of game, and show them when reading the next move, and on final results.

d) Add support for playing multiple games in a row:
   - at the end of one game, ask the users if they want to continue with another game (Y/N)
   - if he chooses Y, start a new game (resetting the board, etc)
   - you need to keep the overall score (considering all played games), which you should update and display at the end of each game
   - the starting player should vary between games: game 1 stars with player 1, game 2 with player 2, game 3 with player 1 again etc..

e) Add support for board of arbitrary size:
   - at the start of the program, ask the user for the board size - must be between 3 and 26 (default may be 3)
   - you should then work with a board of that size - displaying it, reading moves
   - the rule for winning: for board size <=4, player wins with in a row; for board size >= 5, player wins with 5 in a row.

f) Optional: improve your display of the board by using the special ASCII characters for displaying margins and corners.
   See: https://theasciicode.com.ar/extended-ascii-code/box-drawings-single-vertical-line-character-ascii-code-179.html
   You can copy-paste them each from that page, and they should allow you do draw tables with nice margins, like this:

       1   2   3
     ┌───┬───┬───┐
   A │ X │ O │ X │
     ├───┼───┼───┤
   B │ X │ O │ X │
     ├───┼───┼───┤
   C │ O │   │ X │
     └───┴───┴───┘

g) Even more optional: create a computer adversary! Write the code which can control one of the players,
   so a human player can play against the computer.
   Some simpler strategies you can start with:
     i. Random: computer should select at random one of the empty table squares. Then play yourself a few times against it,
        on a 3x3 table. How probable is the computer to ever win? (or at least make a draw)
    ii. Defensive: computer should try to block the opponent from completing any line: it should check if the user is about
        to complete any line (has 2 in a row somewhere) and they block the square on which user would win; if there is no
        immediate danger from opponent, then computer can select an empty square at random (but can still be useful to
        select one of the squares close to the positions of the other user)
   iii. Smart/offensive: try to apply the general strategy for any 2-player game: select one of empty positions in such
        a way that: first any urgent threat of the opponent is neutralized AND/OR you can create a threatening position
        yourself (will also need to be able to evaluate a risk/reward score for multiple possible positions and somehow
        choose one of the best options)
*/

public class Ex4_TicTacToe {

    private static final char EMPTY_MARK = ' ';
    private static final char[] PLAYER_MARKS = {'X', 'O'};

    public static void main(String[] args) {


        int boardSize = Integer.parseInt(readString("Table size: "));
        int winSize = boardSize >= 5 ? 5 : 3;
        System.out.println("Players win by having " + winSize + " marks in a row.\n");

        String player1 = readString("Player 1 name: ");
        String player2 = readString("Player 2 name: ");
        String[] playerNames = {player1, player2};

        int[] score = {0, 0};
        int startPlayer = 0;

        boolean oneMore = true;
        while (oneMore) {
            playOneGame(startPlayer, playerNames, score, boardSize, winSize);
            startPlayer = (startPlayer + 1) % 2;

            oneMore = readString("Continue with another game? (Y/N): ").equalsIgnoreCase("Y");
        }
    }

    private static String readString(String message) {
        System.out.print(message);
        Scanner sc = new Scanner(System.in);
        return sc.next();
    }

    private static void playOneGame(int crtPlayer, String[] playerNames, int[] score, int boardSize, int winSize) {

        char[][] board = newBoard(boardSize);
        display(board);

        while (true) {

            playMove(playerNames[crtPlayer], PLAYER_MARKS[crtPlayer], board);

            int winner = computeWinner(board, winSize);
            if (winner != -1) {
                updateScoreAndPrintResults(winner, score, playerNames);
                break; //game ended
            }

            crtPlayer = (crtPlayer + 1) % 2;
        }
    }

    private static void updateScoreAndPrintResults(int winner, int[] score, String[] playerNames) {
        if (winner == 0 || winner == 1) {
            score[winner] = score[winner] + 1;
            System.out.println("Player " + playerNames[winner] + "(" + PLAYER_MARKS[winner] + ") has WON!!");
        } else {
            System.out.println("Game ended with no winner (table full)");
        }
        System.out.println("Score: " + playerNames[0] + ":" + score[0] + "  " + playerNames[1] + ":" + score[1]);
    }

    private static char[][] newBoard(int size) {
        char[][] board = new char[size][size];
        for (char[] row : board) {
            Arrays.fill(row, EMPTY_MARK);
        }
        return board;
    }

    /**
     * Display board, also with header row/column (with coordinates labels)
     */
    private static void display1(char[][] board) {
        displayHeaderRow(board.length);

        char rowLabel = 'A';
        for (char[] row : board) {
            System.out.print(rowLabel + " ");
            for (char cell : row) {
                System.out.print(cell + " ");
            }
            System.out.println();
            rowLabel++;
        }
    }

    private static void display(char[][] board) {
        displayHeaderRow(board.length);

        char rowLabel = 'A';
        for (char[] row : board) {
            if (rowLabel == 'A') {
                horizontalLine(board.length, "┌", "┬", "┐");
            } else {
                horizontalLine(board.length, "├", "┼", "┤");
            }

            System.out.println(rowLabel + " " + new String(row).chars().mapToObj(i -> " " + ((char) i) + " ").collect(joining("│", "│", "│")));

            rowLabel++;
        }

        horizontalLine(board[0].length, "└", "┴", "┘");
    }

    private static void displayHeaderRow(int cellCount) {
        System.out.println("    " +
                IntStream.rangeClosed(1, cellCount)
                        .mapToObj(i -> i + "   ")
                        .collect(joining()));
    }

    private static void horizontalLine(int cellCount, String startCorner, String middleCorner, String endCorner) {
        System.out.println("  " +
                IntStream.range(0, cellCount)
                        .mapToObj(i -> "───")
                        .collect(joining(middleCorner, startCorner, endCorner)));
    }


    private static void playMove(String name, char mark, char[][] board) {
        while (true) {
            String move = readString("\n" + name + "'s move (" + mark + "): ");
            Optional<Position> position = Position.fromString(move, board.length);

            if (position.isPresent()) {
                Position pos = position.get();
                if (isEmpty(pos, board)) {
                    updateBoard(pos, mark, board);
                    break;
                }
            }

            System.err.println("Move '" + move + "' is invalid, please retry!");
        }
    }

    private static boolean isEmpty(Position position, char[][] board) {
        return board[position.row][position.column] == EMPTY_MARK;
    }

    private static void updateBoard(Position position, char mark, char[][] board) {
        board[position.row][position.column] = mark;
        display(board);
    }


    /**
     * Checks the board for end conditions, returns:
     * 0/1 - if game ended due to player 0/1 winning
     * 2   - if game ended but with no winner (board is full)
     * -1  - if game is not finished yet
     */
    private static int computeWinner(char[][] board, int winSize) {
        if (!hasEmptyCells(board)) {
            return 2;
        }
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                int winner = winnerStartingAt(i, j, board, winSize);
                if (winner != -1) {
                    return winner;
                }
            }
        }
        return -1;
    }

    private static boolean hasEmptyCells(char[][] board) {
        for (char[] row : board) {
            for (char cell : row) {
                if (cell == EMPTY_MARK) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks the possible lines which start at a given board position and go in one of the possible directions
     * (row, column, diagonals) and returns:
     * - the player (0/1) which has a full line on one of these directions
     * - or -1 if none of the players has such a line there
     */
    private static int winnerStartingAt(int startRow, int startCol, char[][] board, int winSize) {
        char start = cell(startRow, startCol, board);

        int playerIdx;
        if (start == PLAYER_MARKS[0]) {
            playerIdx = 0;
        } else if (start == PLAYER_MARKS[1]) {
            playerIdx = 1;
        } else {
            return -1;
        }

        boolean wonRow = IntStream.range(0, winSize)
                .map(i -> cell(startRow, startCol + i, board))
                .boxed()
                .allMatch(v -> v == start);
        boolean wonCol = IntStream.range(0, winSize)
                .map(i -> cell(startRow + i, startCol, board))
                .boxed()
                .allMatch(v -> v == start);
        boolean wonDiag1 = IntStream.range(0, winSize)
                .map(i -> cell(startRow + i, startCol + i, board))
                .boxed()
                .allMatch(v -> v == start);
        boolean wonDiag2 = IntStream.range(0, winSize)
                .map(i -> cell(startRow + i, startCol - i, board))
                .boxed()
                .allMatch(v -> v == start);

        return (wonRow || wonCol || wonDiag1 || wonDiag2) ? playerIdx : -1;
    }

    //returns value of a cell, or '?' if indices are outside the table
    private static char cell(int row, int col, char[][] board) {
        return (row < 0 || row >= board.length || col < 0 || col >= board.length) ? '?' : board[row][col];
    }
}

class Position {
    final int row;
    final int column;

    Position(int row, int column) {
        this.row = row;
        this.column = column;
    }

    /**
     * Tries to convert a string value (like 'A2') to a Position(row+column), checking the string format and
     * that the resulted indices are in valid range (considering the board size)
     */
    static Optional<Position> fromString(String move, int boardSize) {
        String s = move.trim().toUpperCase();
        if (s.length() < 2) {
            return Optional.empty();
        }

        int row = s.charAt(0) - 'A';

        String columnPart = s.substring(1);
        int column;
        try {
            column = Integer.parseInt(columnPart) - 1;
        } catch (NumberFormatException e) {
            return Optional.empty();
        }

        if (row < 0 || row >= boardSize || column < 0 || column >= boardSize) {
            return Optional.empty();
        }
        return Optional.of(new Position(row, column));
    }
}
