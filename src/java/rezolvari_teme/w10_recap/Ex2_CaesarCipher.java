package rezolvari_teme.w10_recap;

import java.util.Scanner;

/*
2) Caesar cipher

a) Write methods to encrypt and decrypt a text (String) using the Caesar cipher
   see: https://en.wikipedia.org/wiki/Caesar_cipher
   Method will receive as param the original text (String), and a shift value to use (int)

   Question: do we need here 2 separate functions, or is one enough?..

b) Write an app which reads from the user a name of an existing text file, and gives him the option
   to encrypt or decrypt that file, using the Caesar cipher (and with a shift value read from user)
   The new text should be written to a new file (with same name as old one, but with a suffix like '_encrypted'/'_decrypted')
*/
public class Ex2_CaesarCipher {

    private static final char ALLOWED_RANGE_START = 'a';
    private static final char ALLOWED_RANGE_END = 'z';

    private static String encrypt(String text, int shift) {
        String result = "";
        for (char c : text.toLowerCase().toCharArray()) {
            result += encrypt(c, shift);
        }
        return result;
    }

    private static char encrypt(char c, int shift) {
        if (c < ALLOWED_RANGE_START || c > ALLOWED_RANGE_END) {
            return c; //encryption not supported for chars outside allowed range, so just return them as is
        }

        int rangeLength = ALLOWED_RANGE_END - ALLOWED_RANGE_START + 1;

        char newC = (char) (c + shift % rangeLength); //using % for case when shift value is too big
        if (newC > ALLOWED_RANGE_END) {
            newC -= rangeLength;
        } else if (newC < ALLOWED_RANGE_START) {
            newC += rangeLength;
        }

        return newC;
    }

    public static void main(String[] args) {

        System.out.println("SOME TESTS: ");
        System.out.println("a+2 -> " + encrypt('a', 2));
        System.out.println("z+2 -> " + encrypt('z', 2));
        System.out.println("c-2 -> " + encrypt('c', -2));
        System.out.println("b-2 -> " + encrypt('b', -2));

        String text = "Caesar's secret instructions here!";
        String encoded7 = encrypt(text, 7);
        String decoded = encrypt(encoded7, -7);
        System.out.println("Original text: '" + text + "'");
        System.out.println(" - encoded (shift: +7): '" + encoded7 + "'");
        System.out.println(" - decoded back (shift: -7): '" + decoded + "'");

        System.out.print("\nEnter your text to encode(decode): ");
        Scanner sc = new Scanner(System.in);
        String input = sc.nextLine();
        System.out.print("Enter shift to use (int value, negative for decode): ");
        int shift = sc.nextInt();
        System.out.println("Encoded text: " + encrypt(input, shift));
    }
}
