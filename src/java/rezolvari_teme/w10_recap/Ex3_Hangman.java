package rezolvari_teme.w10_recap;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;

import static java.util.stream.Collectors.toList;

/*
3) Hangman game

Code the 'hangman' game:
- computer reads some words from a text file (dictionary, contains one word per line) and chooses a random one
- it then prints the first and last letter of the word, and some '_' for each letter between
- user is given a number of attempts (like 7) to guess the remaining letters: computer reads a letter, and if it belongs to the word,
  it displays it in the proper location (instead of _); if letter doesn't belong to the word, user looses an attempt
- if user guesses all letters, he wins; if he runs out of attempts, he looses

Hint: for a list of words, see the attached file (top500_cautari_cuvinte_romanesti.csv), or find/build one yourself starting from data found online

Optional: some extra ideas to implement:
- make the game have 3 difficulty levels: easy, medium, hard;  depending on them, select only shorter or longer words for the dictionary files
- depending on word's length and/or difficulty level, show him a different number of letters at start (2-3-4..)
- also change your code to display first the letters from some random positions (instead of always the first and last letters)
*/
public class Ex3_Hangman {

    public static void main(String[] args) throws IOException {

        Difficulty diff = chooseDifficulty();

        List<String> words = getWordsForDifficulty(diff);

        do {
            playOneGame(words, diff);
        } while (readOption("\nContinue with another game?", "Y/N") == 'Y');
    }

    private static Difficulty chooseDifficulty() {
        char option = readOption("Difficulty level - Easy/Medium/Hard ?", "E/M/H");
        Difficulty diff = Difficulty.startingWith(option);
        System.out.println("Difficulty level: " + diff);
        return diff;
    }

    private static char readOption(String message, String options) {
        System.out.print(message + " (" + options + "): ");
        Scanner sc = new Scanner(System.in);
        while (true) {
            String in = sc.next().toUpperCase();
            if (in.length() == 1 && options.contains(in.charAt(0) + "")) {
                return in.charAt(0);
            }
        }
    }

    private static List<String> getWordsForDifficulty(Difficulty diff) throws IOException {
        List<String> allWords = readAllWordsFromFile();
        return filterByDifficulty(allWords, diff);
    }

    private static List<String> readAllWordsFromFile() throws IOException {
        File dictionary = new File("curs/doc/w10_pt_tema__top500_cautari_cuvinte_romanesti.csv");
        List<String> allWords = Files.readAllLines(dictionary.toPath());
        System.out.println("Load dictionary, found " + allWords.size() + " words.");
        return allWords;
    }

    private static List<String> filterByDifficulty(List<String> words, Difficulty diff) {
        int min, max;
        if (diff == Difficulty.EASY) {
            min = 3;
            max = 5;
        } else if (diff == Difficulty.MEDIUM) {
            min = 6;
            max = 8;
        } else { //HARD
            min = 9;
            max = 30;
        }

        List<String> filtered = words.stream().filter(w -> min <= w.length() && w.length() <= max).collect(toList());

        System.out.println("Considering only words with length between " + min + " and " + max
                + " => using " + filtered.size() + " words (out of the total of " + words.size() + ")");

        return filtered;
    }

    private static void playOneGame(List<String> words, Difficulty diff) {

        //choose random word
        String word = words.get(new Random().nextInt(words.size()));

        //build its masked form
        List<Character> masked = maskAllLettersExcept2(word);

        //initialize some game progress variables
        long attemptsLeft = initAttemptsLeft(word, diff);
        Set<Character> alreadyTried = new TreeSet<>();

        //enter game loop
        while (!isGameOver(masked, attemptsLeft, word)) {

            System.out.println("\nWord to guess: " + masked + " (attempts left: " + attemptsLeft + ")");

            char letter = readLetter(alreadyTried);

            if (!unmaskIfGuessed(letter, masked, word)) {
                System.out.println("Wrong guess, word doesn't contain '" + letter + "'");
                alreadyTried.add(letter);
                attemptsLeft--;
            }
        }
    }

    private static List<Character> maskAllLettersExcept2(String word) {
        List<Character> masked = word.chars().mapToObj(c -> '_').collect(toList());

        //choose 2 different letters to unmask:
        Random random = new Random();
        char c1 = word.charAt(random.nextInt(word.length()));
        char c2 = c1;
        while (c2 == c1) {
            c2 = word.charAt(random.nextInt(word.length()));
        }

        //unmask them using the regular method for this (like the user had guessed them)
        unmaskIfGuessed(c1, masked, word);
        unmaskIfGuessed(c2, masked, word);

        return masked;
    }

    private static long initAttemptsLeft(String word, Difficulty diff) {
        //compute attempts left as: the number of unique letters remaining to be guessed (removing the 2 already displayed)
        long attemptsLeft = word.chars().distinct().count() - 2;
        if (diff == Difficulty.EASY) {
            attemptsLeft += 2;
        } else if (diff == Difficulty.MEDIUM) {
            attemptsLeft += 1;
        }
        return attemptsLeft;
    }

    private static boolean isGameOver(List<Character> masked, long attemptsLeft, String word) {
        if (!masked.contains('_')) {
            System.out.println("\nYOU WON! You guessed all letters! Bravo!");
            System.out.println("Word was: " + word);
            return true;
        } else if (attemptsLeft == 0) {
            System.out.println("\nYOU LOST - no attempts left.");
            System.out.println("Word was: " + word);
            return true;
        }
        return false;
    }

    private static char readLetter(Set<Character> alreadyTried) {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.print("Enter a letter (in a..z, different from: " + alreadyTried + "): ");
            String in = sc.next().toLowerCase();
            if (in.length() == 1) {
                char first = in.charAt(0);
                if (first >= 'a' && first <= 'z' && !alreadyTried.contains(first)) {
                    return first;
                }
            }
        }
    }

    private static boolean unmaskIfGuessed(char letter, List<Character> masked, String original) {
        boolean found = false;
        for (int i = 0; i < original.length(); i++) {
            if (original.charAt(i) == letter) {
                masked.set(i, letter);
                found = true;
            }
        }
        return found;
    }

    enum Difficulty {
        EASY, MEDIUM, HARD;

        static Difficulty startingWith(char c) {
            for (Difficulty diff : values()) {
                if (diff.name().startsWith((c + "").toUpperCase())) {
                    return diff;
                }
            }
            return null;
        }
    }
}
