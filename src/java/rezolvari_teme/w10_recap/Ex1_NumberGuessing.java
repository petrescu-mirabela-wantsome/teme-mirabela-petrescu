package rezolvari_teme.w10_recap;

import java.util.Random;
import java.util.Scanner;

/*
1) Number guessing game

a) Code this simple game:
   - computer chooses a random number between 1 and 100
   - it then asks the user to guess the number
   - if user guesses it, is game over; if he didn't guess it, the computer responds with 'Higher'/'Lower'
     depending on how the guess value is compared to the real number
   - it should allow the user max 10 tries (after that, game is lost)

   Question: play it yourself for a few games. What is your strategy, and what is the maximum number of guesses
   after which you can be sure you will find it? Can you play in such a way that it will always take you at most 7 guesses?
   Why is 7 the minimum limit here? (for this range of 100 numbers)

b) Reversed game:
   - now the user is asked to choose a number between 1..100 (and keep it secret)
   - then computer tries to guess it (max 10 attempts), and the user needs to respond if number is lower, equal or higher to his chosen number
   Can you make the computer always win? (what strategy does it need when picking a guess?)
*/

public class Ex1_NumberGuessing {
    private static final int MAX_ATTEMPTS = 7;

    public static void main(String[] args) {

        int x = new Random().nextInt(100) + 1;
        System.out.println("I've chosen a random number between 1 and 100.\nCan you guess it? (in " + MAX_ATTEMPTS + " attempts)");

        Scanner sc = new Scanner(System.in);
        int attempts = 0;

        while (true) {

            attempts++;
            System.out.print("\nGuess " + attempts + ": ");

            int guess = sc.nextInt();

            if (guess < x) {
                System.out.println("My number is HIGHER than that ...");
            } else if (guess > x) {
                System.out.println("My number is LOWER than that ...");
            } else {
                System.out.println("\nYou guessed it, bravo! (in " + attempts + " attempts)");
                break;
            }
        }

        if (attempts > MAX_ATTEMPTS) {
            System.out.println("It took you more than 7 guesses.. Play again and focus, you can do better that that! :)");
        }
    }
}
