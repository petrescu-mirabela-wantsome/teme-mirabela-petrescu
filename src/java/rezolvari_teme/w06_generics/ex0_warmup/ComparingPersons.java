package rezolvari_teme.w06_generics.ex0_warmup;

import java.util.*;

/*
Ex0 c) Comparing Persons:

- Write a class Person with fields: cnp, name, age, height (also having a constructor with values for all fields, and toString)

- Make it sortable by name by default, by implementing Comparable based on name. Test it by having adding some persons
  in a sorted type of Set, and then print them in the default iteration order.
  Question: do you also need to override equals & hashCode for this to work?..

- Implement a separate comparator which will order persons by cnp (ascending), and test it. Try to test it by adding
  same persons to a new sorted set, but which uses by default this specific comparator to keep the set sorted,
  and the just iterate over that set and print them.

- Implement another comparator which orders persons first by height (ascending) then by age (descending). Test it
  on a list of persons, making sure it works correctly for all cases (persons of same height..).
  Try to test it by adding persons in a List, then using Collections.sort() to sort it (using this specific comparator),
  and then print the contents.
  Question: what happens when you have 2 Persons with same height and age, but different details in rest -
            how are they added to set and ordered?... (and does this depend on overriding equals/hashCode?)
*/

public class ComparingPersons {

    public static void main(String[] args) {

        List<Person> personList = Arrays.asList(
                new Person(1111, "Ion", 20, 180),
                new Person(3333, "Ana", 22, 170),
                new Person(2222, "Vasile", 22, 170),
                new Person(4444, "Maria", 19, 160));

        Set<Person> persons = new TreeSet<>(personList);
        System.out.println("\nPersons (default sorting):");
        for (Person p : persons) System.out.println(p);

        Set<Person> sortedByCnp = new TreeSet<>(new PersonComparatorByCnp());
        sortedByCnp.addAll(personList);
        System.out.println("\nPersons (sorted by cnp):");
        for (Person p : sortedByCnp) System.out.println(p);

        //TreeSet+Comparator => contains only 3 persons, not 4!! persons for which comparator returns 0 are considered duplicates by a TreeSet!!
        Set<Person> sortedByHeightAndAge = new TreeSet<>(new PersonComparatorByHeightAndAge());
        sortedByHeightAndAge.addAll(personList);
        System.out.println("\nPersons (sorted by height+age, with a TreeSet):");
        for (Person p : sortedByHeightAndAge) System.out.println(p);

        //but if we use a List with .sort(), we will retain all 4 persons, including the ones for which comparator returns 0 (unlike for the TreeSet case)
        List<Person> listSortedByHeightAndAge = new ArrayList<>(personList);
        listSortedByHeightAndAge.sort(new PersonComparatorByHeightAndAge());
        System.out.println("\nPersons (sorted by height+age, in a List):");
        for (Person p : listSortedByHeightAndAge) System.out.println(p);

        Set<Person> sortedByHeightAndAgeAndCnp = new TreeSet<>(new PersonComparatorByHeightAndAgeAndCnp());
        sortedByHeightAndAgeAndCnp.addAll(personList);
        System.out.println("\nPersons (sorted by height+age+cnp):");
        for (Person p : sortedByHeightAndAgeAndCnp) System.out.println(p);
    }

    static class PersonComparatorByCnp implements Comparator<Person> {
        @Override
        public int compare(Person p1, Person p2) {
            return Long.compare(p1.cnp, p2.cnp); //equiv to: Long.valueOf(o1.cnp).compareTo(o2.cnp)
        }
    }

    static class PersonComparatorByHeightAndAge implements Comparator<Person> {
        @Override
        public int compare(Person p1, Person p2) {
            int result = Long.compare(p1.height, p2.height);
            return result != 0 ?
                    result :
                    -Long.compare(p1.age, p2.age); //for equal heights case, need to compare also the ages (and reverse the result for descending order)
        }
    }

    static class PersonComparatorByHeightAndAgeAndCnp implements Comparator<Person> {
        @Override
        public int compare(Person p1, Person p2) {
            int result = Long.compare(p1.height, p2.height);
            if (result != 0) return result;
            result = -Long.compare(p1.age, p2.age);
            //needed just because if compare(p1,p2)==0, then they are considered 'duplicates' by TreeSet and only 1st one is added to set (even if !p1.equals(p2))
            return result != 0 ?
                    result :
                    Long.compare(p1.cnp, p2.cnp);
        }
    }

    static class Person implements Comparable<Person> {
        final long cnp;
        final String name;
        final int age, height;

        Person(long cnp, String name, int age, int height) {
            this.cnp = cnp;
            this.name = name;
            this.age = age;
            this.height = height;
        }

        @Override
        public int compareTo(Person o) {
            return name.compareTo(o.name);
        }

        @Override
        public String toString() {
            return "Person{" +
                    "cnp=" + cnp +
                    ", name='" + name + '\'' +
                    ", age=" + age +
                    ", height=" + height +
                    '}';
        }

        /*not really needed (as we don't work with hash sets here)
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return cnp == person.cnp &&
                    age == person.age &&
                    height == person.height &&
                    Objects.equals(name, person.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(cnp, name, age, height);
        }
        */
    }
}
