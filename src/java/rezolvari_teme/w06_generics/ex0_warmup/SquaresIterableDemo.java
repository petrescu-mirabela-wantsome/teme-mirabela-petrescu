package rezolvari_teme.w06_generics.ex0_warmup;

import java.util.Iterator;

/*
Ex0, d) Squares Iterable:

- Write a class SquaresIterable which implements Iterable<Long> and allows to iterate over the squared values of all positive numbers,
  starting from 1. (you’ll probably need to define also a SquaresIterator class, to return as the result from iterator() method..)
  Test it then in a for loop, printing all squares of numbers which are less than a max limit value (like 100_000).
  Questions:
  - How do you write the stop condition here?
  - Could we end the loop in some other way except using a break? (like making the iterator itself stop when the limit was reached;
    but how do you make that limit configurable from outside the iterator/iterable?)
*/

public class SquaresIterableDemo {
    public static void main(String[] args) {

        //iterable without limit
        System.out.println("\nUsing iterable without limit:");
        for (long l : new SquaresIterable()) {
            if (l > 100_000) {
                break;
            }
            System.out.println(l);
        }

        //iterable with limit!
        System.out.println("\nUsing iterable with limit:");
        for (long l : new SquaresIterableWithLimit(100_000)) {
            System.out.println(l);
        }
    }
}

class SquaresIterable implements Iterable<Long> {

    @Override
    public Iterator<Long> iterator() {
        return new SquaresIterator();
    }

    //Example of an INNER class here - a regular class, but defined _inside_ the body of a parent class (so it will have access to all its fields/methods)
    //more info: http://tutorials.jenkov.com/java/nested-classes.html
    class SquaresIterator implements Iterator<Long> {
        private long n = 1;

        @Override
        public boolean hasNext() {
            return true;
        }

        @Override
        public Long next() {
            long result = n * n;
            n++;
            return result;
        }
    }
}

class SquaresIterableWithLimit implements Iterable<Long> {
    private final long maxLimit;

    SquaresIterableWithLimit(long maxLimit) {
        this.maxLimit = maxLimit;
    }

    @Override
    public Iterator<Long> iterator() {
        return new SquaresIteratorWithLimit();
    }

    //again an INNER class
    class SquaresIteratorWithLimit implements Iterator<Long> {
        private long n = 1;

        @Override
        public boolean hasNext() {
            return n * n < maxLimit; //using maxLimit field from the parent class SquaresIterableWithLimit!
        }

        @Override
        public Long next() {
            long result = n * n;
            n++;
            return result;
        }
    }
}