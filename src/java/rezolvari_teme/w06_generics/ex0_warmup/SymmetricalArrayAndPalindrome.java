package rezolvari_teme.w06_generics.ex0_warmup;

/*
Ex0

e) Symmetrical array:

- Write a static method ‘boolean isSymmetrical(int[] array)’, which receives an array of int values and returns true
  if the array is symmetrical - meaning the order of elements from start to end is the same as from end to start.

- Make the method more generic, to be able to work with an array of elements of any type (instead of just int).
  Test it on some arrays of String, Double, Person..
  Question: how much of the code did you need to change for this? (anything besides the method signature? why?)


f) Palindrome:

- Write a method ‘boolean isPalindrome(String)’ which takes a phrase as a String, eliminates the spaces between words (if any),
  lowercases it, then returns true if the resulting string is a palindrome - meaning it has the same value if read backwards or forwards.

  Try to reuse the isSymmetrical() methods from previous exercise!
  (hint: you will need to convert your String to an array of letters; but note that .toCharArray() will not work,
   as it returns a char[] - an array of primitives, not usable with your generic isSymetrical method;
   try instead to call String.split(“”) to get a String[] array...)

  Your palindrome method should respond with true for strings like these:
  “aerisirea”, “rotitor”, “calabalac”, “Ene purta patru pene”, “Ion a luat luni vinul tau la noi”,
  “Step on no pets”, “Never odd or even”, “Was it a car or a cat I saw”
  (more info: https://en.wikipedia.org/wiki/Palindrome , https://ro.wikipedia.org/wiki/Palindrom )
*/

public class SymmetricalArrayAndPalindrome {

    static <T> boolean isSymmetrical(T[] array) {
        for (int i = 0, j = array.length - 1; i < array.length / 2; i++, j--) {
            if (!array[i].equals(array[j])) {
                return false;
            }
        }
        return true;
    }

    static boolean isPalindrome(String text) {
        String[] lettersArray = text.replaceAll(" ", "").toLowerCase().split("");
        return isSymmetrical(lettersArray);
    }

    public static void main(String[] args) {
        //System.out.println(isSymmetrical(new int[]{1, 2, 3, 2, 1})); //does not work with generic method - T cannot be primitive
        System.out.println(isSymmetrical(new Integer[]{1, 2, 3, 2, 1}));
        System.out.println(isSymmetrical(new String[]{"a", "b", "b", "a"}));

        System.out.println(isPalindrome("not a palindrome"));
        System.out.println(isPalindrome("Step on no pets"));
        System.out.println(isPalindrome("Ene purta patru pene"));
        System.out.println(isPalindrome("Ion a luat luni vinul tau la noi"));
    }
}
