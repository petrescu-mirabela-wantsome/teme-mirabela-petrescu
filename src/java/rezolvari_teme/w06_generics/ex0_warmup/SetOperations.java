package rezolvari_teme.w06_generics.ex0_warmup;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/*
Ex0)
a) Set operations:
   Define these static methods; each of them receives 2 parameters of type Set<Integer> and returns a new set of the same type:
   - union() - returns a set with elements of both sets combined
   - intersection() - returns a set with only the elements which exists in both initial sets
   - difference() - returns a set with only the elements found in first given set, but not in 2nd one
 	Note: your code should NOT modify in any way the input sets.
	Then write some code to test them.

b) Generic set operations:
   Make the previous methods more generic, so they will work on sets of any type of objects. Test them on sets of String, Person, etc...
   - question: how much did you need to change your method’s code? what about your test code
   - question: why is this way (using generics) better than just working with Set<Object>?
*/

public class SetOperations {

    private static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>(set1);
        result.addAll(set2);
        return result;
    }

    private static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>();
        for (T s : set1) {
            if (set2.contains(s)) {
                result.add(s);
            }
        }
        return result;
    }

    private static <T> Set<T> difference(Set<T> set1, Set<T> set2) {
        Set<T> result = new HashSet<>();
        for (T s : set1) {
            if (!set2.contains(s)) {
                result.add(s);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        Set<String> set1 = new HashSet<>(Arrays.asList("aa", "bb", "cc"));
        Set<String> set2 = new HashSet<>(Arrays.asList("bb", "cc", "dd"));
        System.out.println("set1: " + set1);
        System.out.println("set2: " + set2);

        System.out.println("union: " + union(set1, set2));
        System.out.println("intersection: " + intersection(set1, set2));
        System.out.println("set1-set2: " + difference(set1, set2));
        System.out.println("set2-set1: " + difference(set2, set1));
    }
}
