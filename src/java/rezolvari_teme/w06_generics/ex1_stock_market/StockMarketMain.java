package rezolvari_teme.w06_generics.ex1_stock_market;

import java.time.LocalDate;

/*
1. Stock Market

Create a class called StockMarket that can record and query stock updates.

A stock update is defined by these properties:
- Stock code (e.g “AMZN”, “MSFT”)
- Date of the update (use the class java.time.LocalDate)
- Price
Note: to be able to use this in maps and sets, you should probably override equals() and hashcode(), as well as implement Comparable<StockUpdate>.

The following methods should be supported by the StockMarket class:

- add(StockUpdate update)

- List<StockUpdate> getUpdates(LocalDate from, LocalDate to) - getUpdates all stock updates between two dates (for all stock codes)

- List<StockUpdate> getUpdates(LocalDate from, LocalDate to, String stockCode) - getUpdates all stock updates between two dates, for a specific stock code

- double getPrice(LocalDate date, String stockCode) - gets the price that a stock had at a given date. Note that
  there may not be an entry for that exact date, so you need to search for the last stock update that happened before the specified date.
  (hint: the TreeSet class has some methods “floor”,“ceiling” that could be useful here)

- Map<String, Double> getPrices(LocalDate date) - gets the prices for all stocks at a given date. The note from previous point applies here as well.

Note: to store the updates, you may use multiple collections if needed. There is no restriction to only use a single collection here.
For example, you may choose to have a collection with all updates for all codes, as well as one collection for each individual stock.

Create also a test class and add tests to it (of type JUnit) to check that StockMarket class works as expected.
*/

/**
 * Manual tests for StockMarket
 */
class StockMarketMain {
    public static void main(String[] args) {
        StockMarket m = new StockMarket();
        m.add(new StockUpdate("A", LocalDate.of(2018, 1, 1), 11));
        m.add(new StockUpdate("A", LocalDate.of(2018, 1, 5), 15));
        m.add(new StockUpdate("B", LocalDate.of(2018, 1, 1), 21));
        m.add(new StockUpdate("B", LocalDate.of(2018, 1, 2), 22));
        m.add(new StockUpdate("B", LocalDate.of(2018, 1, 3), 23));
        m.add(new StockUpdate("B", LocalDate.of(2018, 1, 4), 24));
        m.add(new StockUpdate("B", LocalDate.of(2018, 1, 6), 26));

        System.out.println("\nUpdates: " + m.getUpdates(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 4)));
        System.out.println("Updates2: " + m.getUpdates(LocalDate.of(2018, 1, 2), LocalDate.of(2018, 1, 3)));

        System.out.println("\nUpdates(A): " + m.getUpdates(LocalDate.of(2018, 1, 1), LocalDate.of(2018, 1, 4), "A"));

        System.out.println("\nPrice(A): " + m.getPrice(LocalDate.of(2017, 1, 1), "A"));
        System.out.println("Price(B): " + m.getPrice(LocalDate.of(2018, 1, 5), "B"));

        System.out.println("\nPrices: " + m.getPrices(LocalDate.of(2017, 1, 1)));
        System.out.println("Prices: " + m.getPrices(LocalDate.of(2018, 1, 5)));
    }
}
