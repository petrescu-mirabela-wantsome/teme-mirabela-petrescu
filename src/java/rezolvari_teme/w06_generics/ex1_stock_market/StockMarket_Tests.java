package rezolvari_teme.w06_generics.ex1_stock_market;

import org.junit.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for StockMarket
 */
public class StockMarket_Tests {

    private static final double PRECISION = 0.01;

    @Test
    public void testGetUpdates() {
        LocalDate d1 = LocalDate.of(2019, 5, 1);
        LocalDate d2 = LocalDate.of(2019, 5, 2);
        LocalDate d3 = LocalDate.of(2019, 5, 3);
        LocalDate d4 = LocalDate.of(2019, 5, 4);

        StockMarket market = new StockMarket();
        assertTrue(market.getUpdates(d2, d3).isEmpty());
        assertTrue(market.getUpdates(d2, d3, "AMZ").isEmpty());

        market.add(new StockUpdate("GOOG", d3, 2.2));
        market.add(new StockUpdate("AMZ", d4, 1.1));

        assertEquals(
                new ArrayList(),
                market.getUpdates(d1, d2));
        assertEquals(
                new ArrayList(),
                market.getUpdates(d1, d2, "AMZ"));

        assertEquals(
                Collections.singletonList(new StockUpdate("GOOG", d3, 2.2)),
                market.getUpdates(d2, d3));
        assertEquals(
                Collections.singletonList(new StockUpdate("GOOG", d3, 2.2)),
                market.getUpdates(d2, d3, "GOOG"));
        assertEquals(
                new ArrayList(),
                market.getUpdates(d2, d3, "AMZ"));

        assertEquals(
                Arrays.asList(new StockUpdate("GOOG", d3, 2.2), new StockUpdate("AMZ", d4, 1.1)),
                market.getUpdates(d3, d4));
        assertEquals(
                Collections.singletonList(new StockUpdate("AMZ", d4, 1.1)),
                market.getUpdates(d3, d4, "AMZ"));
    }

    @Test
    public void testPrices() {
        LocalDate d1 = LocalDate.of(2019, 5, 1);
        LocalDate d2 = LocalDate.of(2019, 5, 2);
        LocalDate d3 = LocalDate.of(2019, 5, 3);
        LocalDate d4 = LocalDate.of(2019, 5, 4);

        StockMarket market = new StockMarket();

        market.add(new StockUpdate("AMZ", d4, 1.4));
        market.add(new StockUpdate("AMZ", d3, 1.3));
        market.add(new StockUpdate("GOOG", d3, 2.3));
        market.add(new StockUpdate("GOOG", d1, 2.1));

        //at d1:
        assertEquals(market.getPrice(d1, "AMZ"), 0.0, PRECISION);
        assertEquals(market.getPrice(d1, "GOOG"), 2.1, PRECISION);

        assertEquals(1, market.getPrices(d1).size());
        assertEquals(2.1, market.getPrices(d1).get("GOOG"), PRECISION);

        //at d2:
        assertEquals(market.getPrice(d2, "AMZ"), 0.0, PRECISION);
        assertEquals(market.getPrice(d2, "GOOG"), 2.1, PRECISION);

        assertEquals(1, market.getPrices(d2).size());
        assertEquals(2.1, market.getPrices(d2).get("GOOG"), PRECISION);

        //at d3:
        assertEquals(market.getPrice(d3, "AMZ"), 1.3, PRECISION);
        assertEquals(market.getPrice(d3, "GOOG"), 2.3, PRECISION);

        assertEquals(2, market.getPrices(d3).size());
        assertEquals(1.3, market.getPrices(d3).get("AMZ"), PRECISION);
        assertEquals(2.3, market.getPrices(d3).get("GOOG"), PRECISION);

        //at d4:
        assertEquals(market.getPrice(d4, "AMZ"), 1.4, PRECISION);
        assertEquals(market.getPrice(d4, "GOOG"), 2.3, PRECISION);

        assertEquals(2, market.getPrices(d4).size());
        assertEquals(1.4, market.getPrices(d4).get("AMZ"), PRECISION);
        assertEquals(2.3, market.getPrices(d4).get("GOOG"), PRECISION);
    }
}