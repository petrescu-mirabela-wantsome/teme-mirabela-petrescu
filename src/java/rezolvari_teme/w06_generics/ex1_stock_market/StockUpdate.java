package rezolvari_teme.w06_generics.ex1_stock_market;

import java.time.LocalDate;
import java.util.Objects;

class StockUpdate implements Comparable<StockUpdate> {

    private final String code;
    private final LocalDate lastUpdate;
    private final double price;

    StockUpdate(String code, LocalDate lastUpdate, double price) {
        this.code = code;
        this.lastUpdate = lastUpdate;
        this.price = price;
    }

    String getCode() {
        return code;
    }

    LocalDate getLastUpdate() {
        return lastUpdate;
    }

    double getPrice() {
        return price;
    }

    @Override
    public int compareTo(StockUpdate other) {
        int result = lastUpdate.compareTo(other.lastUpdate);
        return result != 0 ? result : code.compareTo(other.code);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StockUpdate stockUpdate = (StockUpdate) o;
        return Double.compare(stockUpdate.price, price) == 0 &&
                Objects.equals(code, stockUpdate.code) &&
                Objects.equals(lastUpdate, stockUpdate.lastUpdate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, lastUpdate, price);
    }

    @Override
    public String toString() {
        return "StockUpdate{" +
                "code='" + code + '\'' +
                ", lastUpdate=" + lastUpdate +
                ", price=" + price +
                '}';
    }
}
