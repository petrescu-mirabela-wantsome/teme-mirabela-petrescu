package rezolvari_teme.w06_generics.ex1_stock_market;

import java.time.LocalDate;
import java.util.*;

class StockMarket {

    private final List<StockUpdate> updates = new ArrayList<>();

    void add(StockUpdate update) {
        updates.add(update);
    }

    List<StockUpdate> getUpdates(LocalDate from, LocalDate to) {
        List<StockUpdate> results = new ArrayList<>();
        for (StockUpdate update : updates) {
            if (isInInterval(update, from, to)) {
                results.add(update);
            }
        }
        return results;
        //OR with streams:
        //return updates.stream().filter(u -> u.lastUpdate.compareTo(from) >= 0 && u.lastUpdate.compareTo(to) <= 0).collect(Collectors.toList());
    }

    List<StockUpdate> getUpdates(LocalDate from, LocalDate to, String code) {
        List<StockUpdate> results = new ArrayList<>();
        for (StockUpdate update : updates) {
            if (update.getCode().equals(code) && isInInterval(update, from, to)) {
                results.add(update);
            }
        }
        return results;
    }

    private boolean isInInterval(StockUpdate update, LocalDate from, LocalDate to) {
        return update.getLastUpdate().compareTo(from) >= 0 && update.getLastUpdate().compareTo(to) <= 0;
    }

    Map<String, Double> getPrices(LocalDate date) {
        List<StockUpdate> sortedUpdates = new ArrayList<>(updates);
        Collections.sort(sortedUpdates);

        Map<String, Double> pricesMap = new HashMap<>();
        for (StockUpdate u : sortedUpdates) {
            if (u.getLastUpdate().compareTo(date) <= 0) {
                pricesMap.put(u.getCode(), u.getPrice());
            }
        }
        return pricesMap;
    }

    double getPrice(LocalDate date, String code) {

        //1) Faster version (as it computes a last price only for ONE code, not all
        /*
        List<StockUpdate> sortedUpdates = new ArrayList<>(updates);
        Collections.sort(sortedUpdates);
        StockUpdate lastUpdate = null;
        for (StockUpdate u : sortedUpdates) {
            if (u.getCode().equals(code) && u.getLastUpdate().compareTo(date) <= 0) {
                lastUpdate = u;
            }
        }
        return lastUpdate != null ? lastUpdate.getPrice() : 0;
        */

        //2) Simpler version - just reuse getPrices() (will be slower, as it computes last prices for all codes)
        return getPrices(date).getOrDefault(code, 0.0); //need to also specify a default value here, in case code is not present in map at all
    }
}
