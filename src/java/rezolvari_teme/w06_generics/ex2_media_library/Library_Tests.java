package rezolvari_teme.w06_generics.ex2_media_library;

import org.junit.Test;

import java.util.Arrays;

import static java.util.stream.Collectors.toList;
import static org.junit.Assert.*;

public class Library_Tests {

    static Library getPopulatedLibrary() {
        Library lib = new Library();
        lib.addMedia(
                new Book("book1", "auth1", "pub1", 1),
                new Book("book2", "auth1", "pub1", 2),
                new Book("book3", "auth1", "pub1", 3),
                new Book("book4", "auth1", "pub1", 4),
                new Book("book5", "auth1", "pub1", 5),
                new Book("book6", "auth1", "pub1", 6),
                new Book("book7", "auth1", "pub1", 7),
                new Book("book8", "auth1", "pub1", 8),
                new Book("book9", "auth1", "pub1", 9),
                new Book("book10", "auth1", "pub1", 10),
                new Book("book11", "auth1", "pub1", 11),
                new Book("book12", "auth1", "pub1", 12),
                new Book("book13", "auth1", "pub1", 13),
                new Book("book14", "auth1", "pub1", 14),
                new Book("book15", "auth1", "pub1", 15));

        lib.addMedia(
                new Mp3("mp31", "singer1", "album1", 1),
                new Mp3("mp32", "singer1", "album1", 2),
                new Mp3("mp33", "singer1", "album1", 3),
                new Mp3("mp34", "singer1", "album1", 4),
                new Mp3("mp35", "singer1", "album1", 5),
                new Mp3("mp36", "singer1", "album1", 6),
                new Mp3("mp37", "singer1", "album1", 7));

        lib.addMedia(
                new Video("video1", 10, true, 1),
                new Video("video2", 10, true, 2),
                new Video("video3", 10, true, 3),
                new Video("video4", 10, true, 4),
                new Video("video5", 10, true, 5));
        return lib;
    }

    @Test
    public void testEmptyLib() {
        Library lib = new Library();
        assertTrue(lib.getTop20().isEmpty());
        assertTrue(lib.getArchive().isEmpty());
        assertTrue(lib.findByType(MediaType.MP3).isEmpty());
        assertTrue(lib.findByNoOfDownloads(0, 1000).isEmpty());
        assertNull(lib.findByTitle("video1"));
        assertNull(lib.findByTitle(null));

        lib.updateDownloads("video1", 10);
        assertTrue(lib.findByNoOfDownloads(0, 1000).isEmpty());

        lib.addMedia(new Book("book1", "auth1", "pub1", 10));

        assertEquals(1, lib.getTop20().size());
        assertTrue(lib.getArchive().isEmpty());

        assertTrue(lib.findByType(MediaType.MP3).isEmpty());
        assertFalse(lib.findByType(MediaType.BOOK).isEmpty());

        assertFalse(lib.findByNoOfDownloads(0, 1000).isEmpty());
        assertTrue(lib.findByNoOfDownloads(0, 5).isEmpty());

        assertNull(lib.findByTitle("video1"));
        assertNotNull(lib.findByTitle("book1"));
    }

    @Test
    public void testLibOperations() {
        Library lib = getPopulatedLibrary();

        assertEquals(
                Arrays.asList("book15", "book14", "book13", "book12", "book11", "book10", "book9", "book8", "book7", "mp37", "book6", "mp36", "book5", "mp35", "video5", "book4", "mp34", "video4", "book3", "mp33"),
                lib.getTop20().stream().map(e -> e.title).collect(toList()));

        assertEquals(
                Arrays.asList("video3", "book2", "mp32", "video2", "book1", "mp31", "video1"),
                lib.getArchive().stream().map(e -> e.title).collect(toList()));

        assertEquals(
                Arrays.asList("video5", "video4", "video3", "video2", "video1"),
                lib.findByType(MediaType.VIDEO).stream().map(e -> e.title).collect(toList()));

        assertTrue(lib.findByNoOfDownloads(20, 100).isEmpty());
        assertEquals(
                Arrays.asList("book5", "mp35", "video5", "book4", "mp34", "video4"),
                lib.findByNoOfDownloads(4, 5).stream().map(e -> e.title).collect(toList()));

        assertNull(lib.findByTitle("book16"));
        assertEquals(
                new Book("book15", "auth1", "pub1", 15),
                lib.findByTitle("book15"));
        assertEquals(
                new Video("video1", 10, true, 1),
                lib.findByTitle("video1"));

        lib.updateDownloads("video1", 100);
        assertEquals(20, lib.getTop20().size());
        assertEquals(
                new Video("video1", 10, true, 100),
                lib.getTop20().get(0));
    }
}
