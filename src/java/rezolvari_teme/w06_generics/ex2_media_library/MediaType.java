package rezolvari_teme.w06_generics.ex2_media_library;

enum MediaType {
    BOOK,
    VIDEO,
    MP3
}
