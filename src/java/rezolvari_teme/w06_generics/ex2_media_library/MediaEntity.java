package rezolvari_teme.w06_generics.ex2_media_library;

import java.util.Objects;

class MediaEntity implements Comparable<MediaEntity> {

    protected final MediaType type;
    protected final String title;
    protected int noOfDownloads;

    //Make the constructor protected (not public) to prevent construction of MediaEntity instances directly (only children are ok to be built)
    protected MediaEntity(MediaType type, String title, int noOfDownloads) {
        this.type = type;
        this.title = title;
        this.noOfDownloads = noOfDownloads;
    }

    public MediaType getType() {
        return type;
    }

    public String getTitle() {
        return title;
    }

    public int getNoOfDownloads() {
        return noOfDownloads;
    }

    public void setNoOfDownloads(int noOfDownloads) {
        this.noOfDownloads = noOfDownloads;
    }

    @Override
    public int compareTo(MediaEntity other) {
        int result = -Integer.compare(noOfDownloads, other.noOfDownloads);
        return result != 0 ?
                result :
                title.compareToIgnoreCase(other.title);
    }

    //This is also needed, even if we cannot build directly instances of just MediaEntity (due to protected constructor)
    //because the equals() of derived classes expect this to exist and work well - having code like this 'if (!super.equals(o)) return false;'
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MediaEntity that = (MediaEntity) o;
        return noOfDownloads == that.noOfDownloads &&
                type == that.type &&
                Objects.equals(title, that.title);
    }

    //and if we overrode equals(), we may as well override hashCode too...
    @Override
    public int hashCode() {
        return Objects.hash(type, title, noOfDownloads);
    }
}
