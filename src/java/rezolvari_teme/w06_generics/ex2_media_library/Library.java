package rezolvari_teme.w06_generics.ex2_media_library;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/*
Ex2. Media Library

Write a class hierarchy for a virtual library that holds books, videos and music.
Define and implement the following entities, fields and methods:

- MediaEntity (base class for Book, Video and Mp3 classes)
  - type
  - title
  - noOfDownloads

- Book
  - author
  - publisher

- Video
  - duration
  - fullHD

- Mp3
  - singer
  - album

- Library:
  - List<MediatEntity> top20 -  keeps only the last 20 most accessed media entities according to noOfDownloads field
  - List<MediatEntity> archive - keeps the rest of media entities (outside of top 20)

Hint: use inheritance by having all supported media types inherit a common base class (e.g MediaEntity).

Offering functionality for:
- searching by MediaEntity properties
- adding a new MediaEntity
- archiving all the media entities that are not included in the last 20 most accessed entities
- update for the noOfDownloads field of a MediaEntity

Hint: start with the basic functionality first: first make the library store the complete list of MediaEntities, then add the “top20” functionality to it.
*/

class Library {

    private final List<MediaEntity> top20 = new ArrayList<>();
    private final List<MediaEntity> archive = new ArrayList<>();

    /**
     * Some manual tests
     */
    public static void main(String[] args) {
        Library lib = Library_Tests.getPopulatedLibrary();

        System.out.println("--- Top 20 ---");
        lib.getTop20().forEach(System.out::println);

        System.out.println("--- Archive ---");
        lib.getArchive().forEach(System.out::println);

        System.out.println("video1: " + lib.findByTitle("video1"));
        System.out.println("video?: " + lib.findByTitle("video?"));

        System.out.println("With 10-12 downloads: " + lib.findByNoOfDownloads(10, 12));

        System.out.println("All mp3: " + lib.findByType(MediaType.MP3));

        lib.updateDownloads("video1", 100);
        System.out.println("After updating video1 to 100 downloads:");
        System.out.println("--- Top 20 ---");
        lib.getTop20().forEach(System.out::println);
    }

    public List<MediaEntity> getTop20() {
        return top20;
    }

    public List<MediaEntity> getArchive() {
        return archive;
    }

    private List<MediaEntity> allMedia() {
        List<MediaEntity> all = new ArrayList<>(top20);
        all.addAll(archive);
        return all;
    }

    void addMedia(MediaEntity... medias) {
        archive.addAll(Arrays.asList(medias)); //add new one to archive first
        recomputeTop20();
    }

    private void recomputeTop20() {
        //get all current, combined
        List<MediaEntity> all = allMedia();

        //sort all again (by default comparable rule, meaning desc by noOfDownloads, then asc by title)
        Collections.sort(all);

        //separate top20 again
        top20.clear();
        archive.clear();
        for (MediaEntity e : all) {
            if (top20.size() < 20) {
                top20.add(e);
            } else {
                archive.add(e);
            }
        }
    }

    List<MediaEntity> findByType(MediaType type) {
        List<MediaEntity> result = new ArrayList<>();
        for (MediaEntity e : allMedia()) {
            if (e.getType() == type) {
                result.add(e);
            }
        }
        return result;
    }

    MediaEntity findByTitle(String title) {
        for (MediaEntity e : allMedia()) {
            if (title.equalsIgnoreCase(e.title)) {
                return e;
            }
        }
        return null;
    }

    List<MediaEntity> findByNoOfDownloads(int min, int max) {
        List<MediaEntity> result = new ArrayList<>();
        for (MediaEntity e : allMedia()) {
            if (min <= e.getNoOfDownloads() && e.getNoOfDownloads() <= max) {
                result.add(e);
            }
        }
        return result;
    }

    void updateDownloads(String title, int downloads) {
        MediaEntity e = findByTitle(title);
        if (e != null) {
            e.setNoOfDownloads(downloads);
            recomputeTop20();
        }
    }
}
