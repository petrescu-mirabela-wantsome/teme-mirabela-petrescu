package rezolvari_teme.w01_intro.ex0;

/**
 * Ex.0 Warm-up:
 * <p>
 * Write one method for each of these cases, which should:
 * <p>
 * a) Receive 2 numbers (integer) and computes/returns their sum
 * <p>
 * b) Receive a number (double) and returns the square of its value (number to power 2)
 * <p>
 * c) Receive 2 string values, joins them and returns as a single string
 * <p>
 * d) Receives a String, computes and returns it's length (number of characters)
 * Hint: check what methods are available on the String object (type 's.' and wait for autocomplete by Idea...)
 * <p>
 * e) Receives a double value (with some fractional part), should return only the integer part of it (drop the decimals)
 * Hint: you need to use a forced casting to transform between these types (double -> int)
 * <p>
 * f) Receives 3 values (x, min, max) and returns true if first one is in interval between the other 2 values
 * (min <= x <= max), or false otherwise
 */
class Ex0 {

    static int computeSum(int a, int b) {
        return a + b;
    }

    static double squareValue(double x) {
        return x * x;
    }

    static String joinStrings(String s1, String s2) {
        return s1 + s2;
    }

    static int lengthOf(String s) {
        return s.length();
    }

    static int truncate(double x) {
        return (int) x;
    }

    static boolean isInInterval(int x, int min, int max) {
        return min <= x && x <= max;
    }

    /**
     * Main method, we use here just for easy manual testing of our methods above.
     * NOTE: after manual test run ok, don't forget to also run the automatic tests, see Ex0_Tests class!
     */
    public static void main(String[] args) {
        System.out.println("sum(2,3) = " + computeSum(2, 3));
        System.out.println("square(3) = " + squareValue(3));
        System.out.println("joinStrings('ab', 'cd') = '" + joinStrings("ab", "cd") + "'");
        System.out.println("lengthOf('abc') = " + lengthOf("abc"));
        System.out.println("truncate(3.21) = " + truncate(3.21));
        System.out.println("isInInterval(3, 0, 5) = " + isInInterval(3, 0, 5));
        System.out.println("isInInterval(3, 5, 10) = " + isInInterval(3, 5, 10));
    }
}
