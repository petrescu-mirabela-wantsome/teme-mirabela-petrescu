package rezolvari_teme.w01_intro.ex10;

/**
 * Ex10: Measuring shapes:
 * a. Given the value of a circle’s radius, print its area and circumference. Which types
 * should you use? (hint: area: A = PI * R^2 , circumference: L = 2*PI*R)
 * b. Given the length of a side of a square, print its area and perimeter.
 * c. Comparing 2 shapes: given the radius of a circle and the size of a square’s side,
 * print out which one has the greater area; the same for the perimeter.
 * i.
 * Hint: for easier testing/clearer code, you may define separate methods for
 * computing area/perimeter for the 2 shapes, with signatures like:
 * static double circleArea(double radius){...}
 * static double squareArea(double side){...} etc..
 */
public class Ex10 {

    static final double PI = 3.141592; //approximate value of PI constant from math, can be used in your computation

    //--- CIRCLE ---//
    static double computeCircleArea(double circleRadius) {
        return PI * circleRadius * circleRadius;
    }

    static double computeCircleLength(double circleRadius) {
        return 2 * PI * circleRadius;
    }

    //--- RECTANGLE ---//
    static double computeRectangleArea(double sideLength) {
        return sideLength * sideLength;
    }

    static double computeRectanglePerimeter(double sideLength) {
        return sideLength * 4;
    }

    //--- COMPARE SHAPES ---//

    /**
     * Computes and compares the AREAS of 2 different shapes
     *
     * @param circleRadius        radius of the circle shape
     * @param rectangleSideLength side of the rectangle shape
     * @return "rectangle" if the rectangle shape has the bigger area, or "circle" otherwise
     */
    static String whichHasGreaterArea(double circleRadius, double rectangleSideLength) {
        return computeCircleArea(circleRadius) > computeRectangleArea(rectangleSideLength) ? "circle" : "rectangle";
    }

    /**
     * Computes and compares the PERIMETERS of 2 different shapes
     *
     * @param circleRadius        radius of the circle shape
     * @param rectangleSideLength side of the rectangle shape
     * @return "rectangle" if the rectangle shape has the bigger perimeter, or "circle" otherwise
     */
    static String whichHasGreaterPerimeter(double circleRadius, double rectangleSideLength) {
        return computeCircleLength(circleRadius) > computeRectanglePerimeter(rectangleSideLength) ? "circle" : "rectangle";
    }

    //--- MAIN, just for manual tests ---//
    public static void main(String[] args) {
        System.out.println("Area of circle with r=5: " + computeCircleArea(5));
        System.out.println("Length of circle with r=5: " + computeCircleLength(5));

        System.out.println("Area of rectangle with side=8: " + computeRectangleArea(8));
        System.out.println("Length of rectangle with side=8: " + computeRectanglePerimeter(8));

        System.out.println("circle(r=5) vs rectangle(l=8) - has greater AREA: '" + whichHasGreaterArea(5, 8) + "'");
        System.out.println("circle(r=5) vs rectangle(l=8) - has greater PERIMETER: '" + whichHasGreaterPerimeter(5, 8) + "'");
    }
}
