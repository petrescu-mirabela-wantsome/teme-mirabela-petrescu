package rezolvari_teme.w01_intro.ex1;

/**
 * Ex.1 a)
 * Given three values representing 3 ANGLES (in degrees) of a possible triangle, write 2 methods which should:
 * - compute if the angles can form a VALID triangle (hint: their sum must be exactly 180 degrees)
 * - compute if the angles can form a RIGHT-ANGLED valid triangle (valid + one of the angles has 90 degrees)
 * <p>
 * Some example tests:
 * 10,30,50 => not a valid triangle (so not right-angled either)
 * 90,30,30 => not a valid triangle (so not right-angled either)
 * 60,60,60 => valid triangle, but not right angled
 * 30,60,90 => valid triangle, also right angled
 * 20,90,70 => valid triangle, also right angled
 */
public class Ex1a {


    //========== THE 2 METHODS YOU NEED TO COMPLETE/FIX! ==========//
    //You should:
    // - complete missing code/fix these methods...
    // - ...then run tests from Ex1a_Tests class
    // - repeat until all tests pass successfully :)
    //-------------------------------------------------------------//

    /**
     * Method which computes if 3 given ANGLE values (in degrees) can be the angles of a valid triangle
     *
     * @return true if they can form a triangle, false otherwise
     */
    static boolean canFormValidTriangle(int angle1, int angle2, int angle3) {

        return (angle1 > 0 && angle2 > 0 && angle3 > 0) //all are positive
                && (angle1 + angle2 + angle3 == 180);   //and sum is 180
    }

    /**
     * Method which computes if 3 given ANGLE values (in degrees):
     * - can form a valid triangle
     * - AND that triangle is also right-angled
     *
     * @return true if they can form a right-angled triangle, false otherwise
     */
    static boolean canFormValidRightAngledTriangle(int angle1, int angle2, int angle3) {

        return canFormValidTriangle(angle1, angle2, angle3)         //all basic checks
                && (angle1 == 90 || angle2 == 90 || angle3 == 90);  //and one of the angles is 90
    }

    //========== METHOD FOR MANUAL TESTING ==========//

    /**
     * MAIN method, you can use it to easily run some manual tests (before running all automatic tests from separate test class)
     */
    public static void main(String[] args) {
        System.out.println("Can angles (10,30,50) form a valid triangle? : " + canFormValidTriangle(10, 30, 50));
        System.out.println("Can angles (10,30,50) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(10, 30, 50));
    }
}
