package rezolvari_teme.w01_intro.ex1;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static rezolvari_teme.w01_intro.ex1.Ex1b.canFormValidRightAngledTriangle;
import static rezolvari_teme.w01_intro.ex1.Ex1b.canFormValidTriangle;

/**
 * Automatic (unit) tests for methods of Ex1b class.
 * <p>
 * - You can run all these tests by: right-click on class / 'Run..' option
 * - You should complete/fix your code in Ex1a class until all these tests pass with success (are green on run)
 */
public class Ex1b_Tests {

    @Test
    public void test_sides_1_2_4() {
        assertFalse("canFormValidTriangle(1,2,4) should return false", canFormValidTriangle(1, 2, 4));
        assertFalse("canFormValidRightAngledTriangle(1,2,4) should return false", canFormValidRightAngledTriangle(1, 2, 4));

        assertFalse("canFormValidTriangle(1,4,2) should return false", canFormValidTriangle(1, 4, 2));
        assertFalse("canFormValidRightAngledTriangle(1,4,2) should return false", canFormValidRightAngledTriangle(1, 4, 2));

        assertFalse("canFormValidTriangle(4,2,1) should return false", canFormValidTriangle(4, 2, 1));
        assertFalse("canFormValidRightAngledTriangle(4,2,1) should return false", canFormValidRightAngledTriangle(4, 2, 1));
    }

    @Test
    public void test_sides_1_2_2() {
        assertTrue("canFormValidTriangle(1,2,2) should return true", canFormValidTriangle(1, 2, 2));
        assertFalse("canFormValidRightAngledTriangle(1,2,2) should return false", canFormValidRightAngledTriangle(1, 2, 2));

        assertTrue("canFormValidTriangle(2,1,2) should return true", canFormValidTriangle(2, 1, 2));
        assertFalse("canFormValidRightAngledTriangle(2,1,2) should return false", canFormValidRightAngledTriangle(2, 1, 2));

        assertTrue("canFormValidTriangle(2,2,1) should return true", canFormValidTriangle(2, 2, 1));
        assertFalse("canFormValidRightAngledTriangle(2,2,1) should return false", canFormValidRightAngledTriangle(2, 2, 1));
    }

    @Test
    public void test_sides_3_4_5() {
        assertTrue("canFormValidTriangle(3,4,5) should return true", canFormValidTriangle(3, 4, 5));
        assertTrue("canFormValidRightAngledTriangle(3,4,5) should return true", canFormValidRightAngledTriangle(3, 4, 5));

        assertTrue("canFormValidTriangle(5,3,4) should return true", canFormValidTriangle(5, 3, 4));
        assertTrue("canFormValidRightAngledTriangle(5,3,4) should return true", canFormValidRightAngledTriangle(5, 3, 4));

        assertTrue("canFormValidTriangle(4,5,3) should return true", canFormValidTriangle(4, 5, 3));
        assertTrue("canFormValidRightAngledTriangle(4,5,3) should return true", canFormValidRightAngledTriangle(4, 5, 3));
    }
}
