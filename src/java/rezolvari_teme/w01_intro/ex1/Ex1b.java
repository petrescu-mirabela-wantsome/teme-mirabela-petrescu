package rezolvari_teme.w01_intro.ex1;

/**
 * Ex1 b)
 * Given three values representing the LENGTH of the edges of a possible triangle,
 * write 2 methods which should:
 * - compute if they can form a valid triangle (if the sum of ANY 2 edges is greater than the 3rd one)
 * - compute if they can form a RIGHT-ANGLED valid triangle (hint: use Pythagoras theorem)
 * Some tests:
 * 1,2,4 => not a valid triangle
 * 4,1,3 => not a valid triangle
 * 1,2,2 => valid, not right-angled
 * 3,4,5 => valid, also right-angled
 */
public class Ex1b {

    //========== THE 2 METHODS YOU NEED TO COMPLETE/FIX! ==========//
    //You should:
    // - complete missing code/fix these methods...
    // - ...then run tests from Ex1b_Tests class
    // - repeat until all tests pass successfully :)
    //=============================================================//

    /**
     * Method which computes if 3 given LENGTH values can be the sides of a valid triangle
     *
     * @return true if they can form a triangle, false otherwise
     */
    static boolean canFormValidTriangle(int side1, int side2, int side3) {

        //check sum of any 2 sides is greater than third (need to be true for all combinations, so using && between them)
        return (side1 + side2 > side3)
                && (side1 + side3 > side2)
                && (side2 + side3 > side1);
    }

    /**
     * Method which computes if 3 given LENGTH values:
     * - can be the sides of a valid triangle
     * - AND that triangle is also right-angled
     *
     * @return true if they can form a right-angled triangle, false otherwise
     */
    static boolean canFormValidRightAngledTriangle(int side1, int side2, int side3) {

        return canFormValidTriangle(side1, side2, side3) //basic checks
                && (pitagora(side1, side2, side3) || pitagora(side1, side3, side2) || pitagora(side2, side3, side1)); //and Pitagora's theoreme is valid for the 3 sides (in any of possible ordering)
    }

    //checks if Pitagora's theoreme is true, for short/long sides given in this particular order
    private static boolean pitagora(int shortSide1, int shortSide2, int longSide) {
        return shortSide1 * shortSide1 + shortSide2 * shortSide2 == longSide * longSide;
    }

    //========== METHOD FOR MANUAL TESTING ==========//

    /**
     * MAIN method, you can use it to easily run some manual tests (before running all automatic tests from separate test class)
     */
    public static void main(String[] args) {
        System.out.println("Can segments of length (1,2,4) form a valid triangle? : " + canFormValidTriangle(1, 2, 4));
        System.out.println("Can segments of length (1,2,4) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(1, 2, 4));

        System.out.println("Can segments of length (1,2,2) form a valid triangle? : " + canFormValidTriangle(1, 2, 2));
        System.out.println("Can segments of length (1,2,2) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(1, 2, 2));

        System.out.println("Can segments of length (3,4,5) form a valid triangle? : " + canFormValidTriangle(3, 4, 5));
        System.out.println("Can segments of length (3,4,5) form a valid right-angle triangle? : " + canFormValidRightAngledTriangle(3, 4, 5));
    }
}
