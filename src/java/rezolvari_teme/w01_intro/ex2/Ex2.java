package rezolvari_teme.w01_intro.ex2;

/**
 * 2. Time Converter:
 * Print the total number of milliseconds contained in H hours, M minutes and S seconds,
 * where H, M, S are inputs introduced by the user.
 * (should check first that each value is in valid range, for example 0<= S < 60...)
 */
public class Ex2 {

    /**
     * Given a time duration expressed in a number of hours + some minutes + some seconds,
     * it converts it to a total number of milliseconds
     *
     * @param h number of hours (valid range: 0..23)
     * @param m number of minutes (valid range: 0..59)
     * @param s number of seconds (valid range: 0..59)
     * @return the total number of milliseconds, or -1 if any of the input parameters has invalid value
     */
    static int convertToMilliseconds(int h, int m, int s) {

        boolean allValid = inRange(h, 0, 23) && inRange(m, 0, 59) && inRange(s, 0, 59);

        int totalMs = ((h * 60 + m) * 60 + s) * 1000; //shorter version, equivalent to: (h*60*60*1000 + m*60*1000 + s*1000)

        return allValid ? totalMs : -1;
    }

    //helper method for validation, returns true if value x is in [min,max] interval
    private static boolean inRange(int x, int min, int max) {
        return min <= x && x <= max;
    }

    /**
     * Main method added just to easily run some manual tests (for automatic tests see separate class)
     */
    public static void main(String[] args) {
        //1h + 2min + 3sec => should result in: 3 723 000 ms
        System.out.println("convertToMilliseconds(1,2,3)=" + convertToMilliseconds(1, 2, 3));

        //1h + 61min + 0 sec => invalid range (for min), should return: -1
        System.out.println("convertToMilliseconds(1,62,0)=" + convertToMilliseconds(1, 62, 0));
    }
}
