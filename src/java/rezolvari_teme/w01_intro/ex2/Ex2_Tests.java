package rezolvari_teme.w01_intro.ex2;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static rezolvari_teme.w01_intro.ex2.Ex2.convertToMilliseconds;

/**
 * Automatic (unit) tests for Ex2 class
 */
public class Ex2_Tests {

    @Test
    public void testRangeValidation_invalidValuesReturnMinusOne() {
        assertEquals(-1, convertToMilliseconds(-1, 0, 0));
        assertEquals(-1, convertToMilliseconds(24, 0, 0));
        assertEquals(-1, convertToMilliseconds(0, -1, 0));
        assertEquals(-1, convertToMilliseconds(0, 60, 0));
        assertEquals(-1, convertToMilliseconds(0, 0, -1));
        assertEquals(-1, convertToMilliseconds(0, 0, 60));
    }

    @Test
    public void testRangeValidation_validValuesDontReturnMinusOne() {
        assertNotEquals(-1, convertToMilliseconds(0, 0, 0));
        assertNotEquals(-1, convertToMilliseconds(23, 59, 59));
    }

    @Test
    public void testConversionOfValidValues() {
        assertEquals(0, convertToMilliseconds(0, 0, 0));
        assertEquals(7_200_000, convertToMilliseconds(2, 0, 0));
        assertEquals(180_000, convertToMilliseconds(0, 3, 0));
        assertEquals(4_000, convertToMilliseconds(0, 0, 4));
        assertEquals(18_367_000, convertToMilliseconds(5, 6, 7));
        assertEquals(86_399_000, convertToMilliseconds(23, 59, 59));
    }
}
