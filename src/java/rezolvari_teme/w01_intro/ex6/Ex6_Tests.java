package rezolvari_teme.w01_intro.ex6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static rezolvari_teme.w01_intro.ex6.Ex6.removeMiddleDigit;

public class Ex6_Tests {

    @Test
    public void testRemoveMiddleDigit_forInvalidValues() {
        assertEquals(-1, removeMiddleDigit(0));
        assertEquals(-1, removeMiddleDigit(9_999));
        assertEquals(-1, removeMiddleDigit(100_000));
        assertEquals(-1, removeMiddleDigit(-12_345));
    }

    @Test
    public void testRemoveMiddleDigit_forValidValues() {
        assertEquals(1245, removeMiddleDigit(12345));
        assertEquals(1000, removeMiddleDigit(10000));
    }
}
