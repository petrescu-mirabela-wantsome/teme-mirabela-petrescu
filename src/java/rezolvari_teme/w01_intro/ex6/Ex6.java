package rezolvari_teme.w01_intro.ex6;

/**
 * Ex 6:
 * Digit remover:
 * Write a program that for a given an integer positive number, of exactly 5 digits,
 * it computes and prints a new number based on first one from which the middle digit was
 * removed.
 * For invalid number (not of exactly 5 digits), it should print -1 instead.
 * Examples:
 * 12345 => should print: 1245
 * 1234  => should print: -1
 */
public class Ex6 {

    static int removeMiddleDigit(int n) {

        int first2 = n / 1000; //removing last 3 digits => we get first 2 ones (assuming we started with 5 digits number)
        int last2 = n % 100;
        int all4 = first2 * 100 + last2; //combine them back in a single number

        return 10000 <= n && n <= 99999 ? //check first if in valid range
                all4 :
                -1;
    }
}
