package rezolvari_teme.w01_intro.ex3;

/**
 * Ex 3: Height Converter
 * <p>
 * Write a program to:
 * <p>
 * a. Convert the height of a person from feet and inches (e.g 5 feet 10 inches) to
 * centimeters (178cm). The printed value should be an integer (no decimal part)
 * Hint: to convert (truncate) a double value to an integer, you can use the cast
 * operator: double d = 2.45; int i = ​ (int) ​ d; //i will be 2
 * <p>
 * b. Convert the height of a person from centimeters to feet and inches.
 * The printed values should be two integers.
 */
public class Ex3 {

    //helper constants showing the relation between foot/inch and cm
    static final double CM_IN_ONE_FOOT = 30.48;
    static final double CM_IN_ONE_INCH = 2.54;

    /**
     * Convert a length value of X feet + Y inches to equivalent Z centimeters (rounded down to an integer value)
     *
     * @param feet   number of feet
     * @param inches number of inches
     * @return equivalent length in centimeters
     */
    static int convertToCentimeters(int feet, int inches) {
        return (int) (feet * CM_IN_ONE_FOOT + inches * CM_IN_ONE_INCH); //translate both to cm, then truncate to int by casting
    }

    static String convertToFeetAndInches(int centimeters) {

        //transform cm to a full number of feet; must also round it (casting from double to int)
        int feet = (int) (centimeters / CM_IN_ONE_FOOT);

        //besides the number of full feet, we have a few extra cm left (a rest, find it with modulo operator '%')
        double restCm = centimeters % CM_IN_ONE_FOOT;
        int inches = (int) (restCm / CM_IN_ONE_INCH); //transform this small rest of cm to inches

        return feet + " feet, " + inches + " inches";
    }

    /**
     * Main method, just for running manual tests
     */
    public static void main(String[] args) {
        //0feet + 3inches = 7.62 cm => should return 7 cm
        System.out.println("convertToCentimeters( 0 feet + 3 inches) = " + convertToCentimeters(0, 3));

        //2feet + 3inches => should return 68 cm
        System.out.println("convertToCentimeters( 2 feet + 3 inches) = " + convertToCentimeters(2, 3));

        //convert 69cm to feet+inches => should return: "2 feet, 3 inches"
        System.out.println("convertToFeetAndInches(68 cm) = '" + convertToFeetAndInches(69) + "'");
    }
}
