package rezolvari_teme.w01_intro.ex3;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static rezolvari_teme.w01_intro.ex3.Ex3.convertToCentimeters;
import static rezolvari_teme.w01_intro.ex3.Ex3.convertToFeetAndInches;

/**
 * Unit tests for Ex3 class
 */
public class Ex3_Tests {

    @Test
    public void testConvertInchesToCentimeters() {
        assertEquals(0, convertToCentimeters(0, 0));
        assertEquals(254, convertToCentimeters(0, 100));
        assertEquals(7, convertToCentimeters(0, 3));
    }

    @Test
    public void testConvertFeetAndInchesToCentimeters() {
        assertEquals(254, convertToCentimeters(8, 4));
        assertEquals(60, convertToCentimeters(2, 0));
        assertEquals(104, convertToCentimeters(3, 5));
    }

    @Test
    public void testConvertCentimetersToInches() {
        assertEquals("0 feet, 0 inches", convertToFeetAndInches(0));
        assertEquals("0 feet, 10 inches", convertToFeetAndInches(26));
    }

    @Test
    public void testConvertCentimetersToFeetAndInches() {
        assertEquals("2 feet, 0 inches", convertToFeetAndInches(61));
        assertEquals("3 feet, 8 inches", convertToFeetAndInches(112));
    }
}
