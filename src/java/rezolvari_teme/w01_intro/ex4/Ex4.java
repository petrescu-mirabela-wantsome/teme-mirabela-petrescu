package rezolvari_teme.w01_intro.ex4;

/**
 * Ex4: Even-Odd Checker:
 * Write a program that is given a positive number, not bigger than 1000, and checks if the
 * number is even or not.
 * a. Prints 0 if the number is even or 1 if the number is odd.
 * b. Change your program so it prints “even”/”odd” instead of 0/1 (hint: you may use the
 * ternary operator)
 */
public class Ex4 {

    /**
     * Checks a given integer number if is even or odd
     *
     * @param n number to check, valid interval 1..1000
     * @return 0 if number is even, 1 if it's odd, or -1 if outside valid interval
     */
    static int convertEvenOddToZeroOne(int n) {
        return isValid(n) ? //if in valid range..
                n % 2 :     //then return rest of division by 2 (is 0 for even, 1 for odd)
                -1;         //else return value for invalid
    }

    /**
     * Checks a given integer number if is even or odd
     *
     * @param n a number to check, valid interval 1..1000
     * @return "even" if number is even, "odd" if it's odd, or "invalid" if outside valid interval
     */
    static String convertEvenOddToString(int n) {
        //using a nested ternary operator here - not very nice, but bearable in this case, as logic is simple enough
        //otherwise we could have defined a separate variable first: String validResult = n%2==0 ? "even" : "odd";
        return isValid(n) ?
                (n % 2 == 0 ? "even" : "odd") :
                "invalid";
    }

    private static boolean isValid(int n) {
        return 1 <= n && n <= 1000;
    }
}
