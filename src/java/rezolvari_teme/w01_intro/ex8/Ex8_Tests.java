package rezolvari_teme.w01_intro.ex8;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Ex8_Tests {

    @Test
    public void testMax() {
        assertEquals(3, Ex8.max(2, 3));
        assertEquals(8, Ex8.max(8, 7));
        assertEquals(-2, Ex8.max(-2, -3));
        assertEquals(7, Ex8.max(7, 7));
    }
}
