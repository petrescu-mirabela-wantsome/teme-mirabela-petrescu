package rezolvari_teme.w01_intro.ex8;

/**
 * Ex8: Max value method:
 * Write a method to compute the maximum value of 2 integer numbers given as parameters.
 * Method’s signature should be: static int max (int x; int y) {...}
 * <p>
 * Examples:
 * max( 2, 3) -> should return:  3
 * max(-2,-3) -> should return: -2
 * max( 7, 7) -> should return:  7
 * <p>
 * Hint: you may use the ternary operator to decide between values)
 */
public class Ex8 {

    static int max(int x, int y) {
        return x > y ? x : y;
    }
}
