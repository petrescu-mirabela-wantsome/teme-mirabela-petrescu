package rezolvari_teme.w01_intro.ex11;

/**
 * Ex 11. Knight Moves
 * <p>
 * Given the current position of a knight on a chess board (column+row), compute and print
 * the list of all possible destination squares to which it can be moved.
 * <p>
 * Example: for current position of (col= 2, row= 4), the list of possible moves is:
 * (1,6)(3,6)(4,5)(4,3)(3,2)(1,2)
 * <p>
 * Notes:
 * a. You should check that the original position is valid (within the limits of the 8x8 chess table).
 * b. You should also check that any possible destination is a valid position too.
 * <p>
 * c. Hints:
 * <p>
 * i. You could define a method to validate any position from the table, and
 * then use it to check both original position and any destination position.
 * What signature (parameters, return type) should this method have?
 * <p>
 * ii. You could define another method for computing and printing each
 * position; it would receive as parameters the current position and 2 values
 * describing the relative movement, and would just print out the destination
 * position (if valid), or an empty line (if not valid); it may call the validation
 * method as needed. You could the just call this new method multiple
 * times, to print out all possible movements from current cell..
 */
public class Ex11 {

    /**
     * Check if a given position is valid (within the bounds of a 8x8 chess table)
     *
     * @param row row number (valid range: 1..8)
     * @param col column number (valid range: 1..8)
     * @return true if both row and col values are in valid range, false otherwise
     */
    static boolean isValidPos(int row, int col) {
        return 1 <= row && row <= 8 &&
                1 <= col && col <= 8;
    }

    /**
     * Starting from a current position (row+column) and a given piece movement on table
     * (specified as a number of rows to move, and a number of columns, relative to current pos)
     * it will compute and return the destination position, encoded as a single String value.
     * <p>
     * If either the current or the destination position are invalid (outside the table)
     * it will return instead just an empty string.
     *
     * @param crtRow     current row (valid range: 1..8)
     * @param crtCol     current column (valid range: 1..8)
     * @param rowsToMove number of rows to move (a positive or negative number)
     * @param colsToMove number of rows to move (a positive or negative number)
     * @return the destination position as a string with format "(row,col)",
     * or empty string if source or destination positions are invalid
     */
    static String getDestinationIfValid(int crtRow, int crtCol,
                                        int rowsToMove, int colsToMove) {
        //compute row/col for destination
        int destRow = crtRow + rowsToMove;
        int destCol = crtCol + colsToMove;

        return isValidPos(crtRow, crtCol) && isValidPos(destRow, destCol) ? //check if both source and destination pos are valid
                "(" + destRow + "," + destCol + ")" :
                "";

    }

    /**
     * Given the current position of a knight (row+column), it will compute and return
     * the list of all VALID possible destinations to reach in 1 valid knight move.
     *
     * @param crtRow current row
     * @param crtCol current column
     * @return list of all valid destinations, in format "(row1,col1)(row2,col2)...(rowN,colN)"
     * and listed in clockwise order starting from top-right position (1 o'clock)
     */
    static String getAllValidDestinations(int crtRow, int crtCol) {
        //concatenate all valid destination, attempting the moves in the particular order expected by the tests (invalid moves will result in "" which doesn't count)
        return getDestinationIfValid(crtRow, crtCol, -2, +1) +
                getDestinationIfValid(crtRow, crtCol, -1, +2) +
                getDestinationIfValid(crtRow, crtCol, +1, +2) +
                getDestinationIfValid(crtRow, crtCol, +2, +1) +
                getDestinationIfValid(crtRow, crtCol, +2, -1) +
                getDestinationIfValid(crtRow, crtCol, +1, -2) +
                getDestinationIfValid(crtRow, crtCol, -1, -2) +
                getDestinationIfValid(crtRow, crtCol, -2, -1);
    }

    /**
     * Main, just for running manual tests
     */
    public static void main(String[] args) {
        System.out.println(isValidPos(1, 9));
        System.out.println(isValidPos(2, 4));

        System.out.println(getDestinationIfValid(2, 4, -2, +1));
        System.out.println(getDestinationIfValid(2, 4, +2, +1));

        System.out.println(getAllValidDestinations(2, 4));
    }
}
