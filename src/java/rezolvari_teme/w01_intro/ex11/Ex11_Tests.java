package rezolvari_teme.w01_intro.ex11;

import org.junit.Test;

import static org.junit.Assert.*;
import static rezolvari_teme.w01_intro.ex11.Ex11.*;

public class Ex11_Tests {

    @Test
    public void testIsValidPosition() {
        assertTrue(isValidPos(8, 1));
        assertTrue(isValidPos(1, 8));
        assertFalse(isValidPos(1, 9));
        assertFalse(isValidPos(0, 4));
    }

    @Test
    public void testComputeDestinationIfValid() {
        assertEquals("", getDestinationIfValid(0, 1, +1, +1)); //invalid source
        assertEquals("", getDestinationIfValid(2, 4, -2, +1)); //invalid dest
        assertEquals("(4,5)", getDestinationIfValid(2, 4, +2, +1));
    }

    @Test
    public void testComputeAllKnightMoveDestinations() {
        assertEquals("", getAllValidDestinations(0, 1));
        assertEquals("(1,6)(3,6)(4,5)(4,3)(3,2)(1,2)", getAllValidDestinations(2, 4));
    }
}
