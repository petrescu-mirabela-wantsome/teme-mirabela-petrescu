package rezolvari_teme.w03_oop1.ex2;

import org.junit.Test;
import rezolvari_teme.w03_oop1.ex1.Person;

import static org.junit.Assert.*;

/**
 * Unit tests for Room class (should compile and pass all tests after you complete that one first)
 */
public class RoomTests {

    @Test
    public void testRoomCapacity() {
        assertEquals(0, new Room(0).getCapacity());
        assertEquals(3, new Room(3).getCapacity());
    }

    @Test
    public void testGetCount() {
        assertEquals(0, new Room(0).getCount());
    }

    @Test
    public void testEnterExitCount() {
        Room r = new Room(3);
        assertEquals(0, r.getCount());

        //add 1 person
        r.enter(new Person("Ion", 1997, "blonde"));
        assertEquals(1, r.getCount());

        //try to add it again (should be rejected)
        r.enter(new Person("Ion", 1997, "blonde"));
        assertEquals(1, r.getCount());

        //add 2 more (should be accepted)
        r.enter(new Person("Maria", 2001, "blonde"));
        r.enter(new Person("Ana", 1995, "black"));
        assertEquals(3, r.getCount());

        //try to add 4th - rejected, no room for him
        r.enter(new Person("Marius", 1995, "black"));
        assertEquals(3, r.getCount());

        //test exit - for unknown name, should have no effect
        r.exit("???");
        assertEquals(3, r.getCount());

        //exit an existing person
        r.exit("Ion");
        assertEquals(2, r.getCount());

        //try exit same person again (should be ignored)
        r.exit("Ion");
        assertEquals(2, r.getCount());

        //exit the rest of persons
        r.exit("maria");
        r.exit("ANA");
        assertEquals(0, r.getCount());
    }

    @Test
    public void testContains() {
        Room r = new Room(2);
        assertFalse(r.isPresent("ion"));

        r.enter(new Person("Ion", 2000));
        assertTrue(r.isPresent("ion"));
        assertTrue(r.isPresent("ION"));

        r.exit("ION");
        assertFalse(r.isPresent("Ion"));
    }

    @Test
    public void testGetOldest() {
        Room r = new Room(2);
        assertEquals("", r.getOldest());

        r.enter(new Person("Maria", 2001));
        r.enter(new Person("Ana", 1995));
        assertEquals("Ana(1995)", r.getOldest());
    }

    @Test
    public void testGetNames() {
        Room r = new Room(5);
        assertArrayEquals(new String[]{}, r.getNames("red"));

        r.enter(new Person("Maria", 2000, "red"));
        r.enter(new Person("Ion", 2000, "blonde"));
        r.enter(new Person("Ana", 2000, "red"));
        r.enter(new Person("Dan", 2000, "black"));

        Person p5 = new Person("Ioana", 2000, "blonde");
        r.enter(p5);

        assertArrayEquals(new String[]{"Maria", "Ana"}, r.getNames("red"));
        assertArrayEquals(new String[]{"Dan"}, r.getNames("BLACK"));
        assertArrayEquals(new String[]{}, r.getNames("blue"));

        //change hair color for a person currently in room! (using old reference we keept for it in p5 varialbe)
        p5.setHairColor("blue");
        assertArrayEquals(new String[]{"Ioana"}, r.getNames("blue"));
    }

    @Test
    public void testEmptyRoom() {
        Room r = new Room(10);

        //all this should work (throw no exceptions) and give some meaningful answers..
        r.printAll();
        assertEquals(0, r.getCount());
        assertEquals("", r.getOldest());
        assertArrayEquals(new String[]{}, r.getNames("blue"));
        r.exit("Yeti");
        assertFalse(r.isPresent("DarthVader"));
    }
}
