package rezolvari_teme.w03_oop1.ex2;

import rezolvari_teme.w03_oop1.ex1.Person;

import java.util.Arrays;

/*
---------------------------------
Ex2. Modelling a room of persons
---------------------------------

Create a class Room, which will model a room in which multiple persons can be present at the same time:

a) the room has a field named capacity (showing the maximum number of person which can be in the room at the same time)
   - this property is mandatory and should be always required when a new Room instance is built (hint: need to handle it in a constructor)
   - once a room is built, the capacity cannot be changed, but it should be readable from outside (hint: need a getter, but no setter)

b) the room should be able to store multiple Person objects (up to its max capacity)
   hint: you’ll need a field holding an array of Person objects (initialized also in constructor, based on the given room capacity)

c) the room should have these methods defined:
  - int getCount() - return the number of persons currently in the room

  - void printAll() - should print the room’s capacity, the number of persons currently in the room, and the details
    of each person (using the toString() method of Person, printed one per row)

  - void enter(Person person) -> should add the given person to the room, if there is still space left
    (current number of persons < max capacity), or else print an error message and ignore this person
    hint: you may need to have a separate field to count the actual number of persons in the room,
          personsCount, which is different/lower than max room capacity
    optional: add a validation so we don’t accept in the room 2 persons with the exact same name
              (in that case may just print an error message and ignore the person with duplicate name)

  - boolean isPresent(String personName) -> should return true only if a person with the specified name is currently in the room

  - String getOldest() -> returns the name and age of the oldest person in the room, as a single String value
    (format: “<name>(<birthYear>)”), or empty string if no person found (question: can this ever happen? when?..)

  - String[] getNames(String hairColor) -> returns an array with the names of all persons in room
    which have the hairColor property equal to the given color

  - void exit(String personName) -> search and remove from room the person(s) which have the specified name
    hint1: once a person is found and should be removed, you need to update the array so all persons coming after it
           should be moved one position lower (towards the beginning of the array); also, the current persons counter
           needs to be updated after this
    hint2: you may want to define a helper method int indexOf(String personName) which searches for and returns
           the index of the person in room or -1 if not found; this would have some code very similar to one in isPresent(),
           so you should avoid duplication and make isPresent() use this helper method;
           you can then easily use same helper method for this exit() method
*/
public class Room {

    private final int capacity;
    private final Person[] persons;
    private int count; //can change (non-final)

    public Room(int capacity) {
        this.capacity = capacity;
        persons = new Person[capacity];
        count = 0;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getCount() {
        return count;
    }

    public void printAll() {
        System.out.println("Room: capacity: " + capacity + ", persons count: " + count);
        for (int i = 0; i < count; i++) {
            System.out.println("person " + i + ": " + persons[i]);
        }
    }

    void enter(Person person) {
        if (count >= capacity) {
            System.err.println("Sorry, room is full, new person rejected: " + person);
        } else if (isPresent(person.getName())) {
            System.err.println("Sorry, person with same name is already in the room, rejecting new person: " + person);
        } else {
            persons[count++] = person;
            System.out.println("New person added to room: " + person + " => count is: " + count);
        }
    }

    void exit(String personName) {
        int idx = indexOf(personName);
        if (idx < 0) {
            System.err.println("Person " + personName + " not present in room now, cannot exit it!");
        } else {
            //we want to eliminate the person at index 'idx':
            //we can do that by moving all persons after it ([idx+1..count-1])
            //one position lower, and then also decrementing count as needed
            for (int i = idx + 1; i < count; i++) {
                persons[i - 1] = persons[i];
            }
            count--;
        }
    }

    boolean isPresent(String personName) {
        return indexOf(personName) >= 0;
    }

    //helper method, to find the position in room of a person with given name
    private int indexOf(String personName) {
        for (int i = 0; i < count; i++) {
            if (persons[i].getName().equalsIgnoreCase(personName)) {
                return i;
            }
        }
        return -1;
    }

    String getOldest() {
        int oldestYear = Integer.MAX_VALUE;
        String oldestName = "";
        for (int i = 0; i < count; i++) {
            Person p = persons[i];
            if (p.getBirthYear() < oldestYear) {
                oldestName = p.getName();
                oldestYear = p.getBirthYear();
            }
        }
        return oldestYear != Integer.MAX_VALUE ? oldestName + "(" + oldestYear + ")" : "";
    }

    String[] getNames(String hairColor) {
        String[] names = new String[count];
        int j = 0;
        for (int i = 0; i < count; i++) {
            if (persons[i].getHairColor().equalsIgnoreCase(hairColor)) {
                names[j] = persons[i].getName();
                j++;
            }
        }
        return Arrays.copyOf(names, j);
    }
}
