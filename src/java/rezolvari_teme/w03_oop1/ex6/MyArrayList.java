package rezolvari_teme.w03_oop1.ex6;

import java.util.Arrays;

/*
Ex6. MyArrayList

Create a class MyArrayList, which can hold a collection of integer values:
The object should hold the integers in a field of type array (int[] values)
It should start with an empty collection (meaning an empty array - of size 0)

It should have methods for:
- Add new values to its end:  void add(int newValue)
- Delete last value from the end:  void remove()
- Get elements count:   int size()
- Get element at specific index:   int get(int index)

BONUS:
- A method to insert a new element at a specific position (by index, shifting all elements after it to the next position)
- A method to remove an element from a specified position (by index)

Hints: for operations which modify the number of elements (add, remove), you will need to “change the size”
       of the array field holding the values -> basically you will need to create a new array of proper size,
       and then copy some of the values there…

Testing: write some tests (in the main() method of a separate class) testing all methods, for all regular/tricky cases you can think of..
*/
public class MyArrayList {

    private int[] arr = new int[0];

    /**
     * Some manual tests...
     */
    public static void main(String[] args) {
        MyArrayList list = new MyArrayList();
        System.out.println(list); //[]

        list.add(3);
        list.addAll(4, 5, 7);
        System.out.println(list); //[3, 4, 5, 7]

        list.remove();
        System.out.println(list); //[3, 4, 5]

        System.out.println("indexOf(3): " + list.indexOf(3)); //0
        System.out.println("indexOf(5): " + list.indexOf(5)); //2

        list.addAt(0, 8); //[8, 3, 4, 5]
        System.out.println(list);

        list.addAt(3, 9); //[8, 3, 4, 9, 5]
        System.out.println(list);

        list.removeFrom(0); //[3, 4, 9, 5]
        System.out.println(list);

        list.removeFrom(3); //[3, 4, 9]
        System.out.println(list);
    }

    int size() {
        return arr.length;
    }

    int get(int index) {
        return arr[index]; //throws exception if index invalid, is ok like that
    }

    void set(int index, int value) {
        arr[index] = value; //throws exception if index invalid, is ok like that
    }

    void add(int newValue) {
        arr = Arrays.copyOf(arr, arr.length + 1);
        arr[arr.length - 1] = newValue;
    }

    void addAll(int... newValues) {
        //kind of inefficient, but simple code (copies the array multiple times, once per each elem form newValues)
        for (int value : newValues) {
            add(value);
        }
    }

    void addAt(int index, int value) {
        if (index >= 0 && index < arr.length) {
            arr = Arrays.copyOf(arr, arr.length + 1);
            for (int i = arr.length - 1; i > index; i--) {
                arr[i] = arr[i - 1];
            }
            arr[index] = value;
        }
    }

    int remove() {
        int result = arr[arr.length - 1]; //throws exception if array is empty, is ok like that
        arr = Arrays.copyOf(arr, arr.length - 1);
        return result;
    }

    void removeFrom(int index) {
        if (index >= 0 && index < arr.length) {
            for (int i = index; i < arr.length - 1; i++) {
                arr[i] = arr[i + 1];
            }
            arr = Arrays.copyOf(arr, arr.length - 1);
        }
    }

    //some other nice methods:
    int indexOf(int value) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == value) {
                return i;
            }
        }
        return -1;
    }

    boolean contains(int value) {
        return indexOf(value) != -1;
    }

    @Override
    public String toString() {
        return "MyArrayList{" + Arrays.toString(arr) + "}";
    }
}
