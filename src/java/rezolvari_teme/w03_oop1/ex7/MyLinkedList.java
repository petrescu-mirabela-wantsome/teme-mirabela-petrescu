package rezolvari_teme.w03_oop1.ex7;

/*
A linked list is a linear collection of data elements, whose order is not given by their physical placement in memory. Instead, each element points to the next.
It is a data structure consisting of a collection of nodes which together represent a sequence.
In its most basic form, each node contains: data, and a reference (in other words, a link) to the next node in the sequence.

Think of a object that can model this kind of data structure:
- what kind of attributes should you consider?
- which of these attributes are public which should be hidden?
- what are the messages that this object should respond to?

Consider that a linked list should offer the following operations:
- iterate over the elements
- add a new element
- remove an existing element
- update the data stored in an element
*/

/**
 * Represents a 'cell' of this linked list - containing the stored value,
 * and a link to the next cell in chain (will be null for last cell)
 */
class Cell {
    int value;
    Cell next;

    Cell(int value, Cell next) {
        this.value = value;
        this.next = next;
    }
}

public class MyLinkedList {

    //to access a whole chain of Cell instances, all we need to know/store here is the address of the first one (will be null when list is empty)
    private Cell head = null;
    private int size = 0; //not absolutely necessary, but makes checks faster!

    public static void main(String[] args) {
        MyLinkedList l = new MyLinkedList();
        l.add(10);
        l.add(20);
        l.add(30);
        System.out.println(l);
        l.remove();
        System.out.println("After remove: " + l);
        l.add(31);
        System.out.println("After add: " + l);

        System.out.println("On idx 0: " + l.get(0));

        l.update(0, 42);
        System.out.println("After update: " + l);
    }

    boolean isEmpty() {
        return size == 0;
    }

    //add in front of head
    void add(int elem) {
        head = new Cell(elem, head);
        size++;
    }

    //removes head value, returning its value
    int remove() {
        if (isEmpty()) {
            throw new RuntimeException("Empty list!");
        }
        int res = head.value;
        head = head.next;
        size--;
        return res;
    }

    //get element at index
    private Cell getCellAt(int idx) {
        if (idx < 0 || idx > size - 1) {
            throw new RuntimeException("Index out of bounds: " + idx);
        }
        Cell cell = head;
        for (int i = 0; i < idx; i++) {
            cell = cell.next;
        }
        return cell;
    }

    int get(int idx) {
        return getCellAt(idx).value;
    }

    void update(int idx, int value) {
        getCellAt(idx).value = value;
    }

    @Override
    public String toString() {
        String res = "[";
        for (Cell p = head; p != null; p = p.next) {
            res += p.value + " ";
        }
        res += "]";
        return res;
    }
}
