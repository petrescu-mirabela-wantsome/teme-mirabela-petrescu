package rezolvari_teme.w03_oop1.ex3;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for Point class (should compile and pass all tests after you complete that one first)
 */
public class PointTests {

    private static final double PRECISION = 0.001; //precision to use when checking equality on double values (as they may loose some precision/cannot be represented exactly in binary)

    @Test
    public void testCreateAndToString() {
        Point p1 = new Point(1.1, 1.2);
        Point p2 = new Point(2.1, 2.2);
        Point p3 = new Point(-3.1, -3.2);
        assertTrue(p1.toString().contains("1.1"));
        assertTrue(p1.toString().contains("1.2"));
        assertTrue(p2.toString().contains("2.1"));
        assertTrue(p2.toString().contains("2.2"));
        assertTrue(p3.toString().contains("-3.1"));
        assertTrue(p3.toString().contains("-3.2"));

        assertNotSame(new Point(1, 1), new Point(1, 1));
    }

    @Test
    public void testDistanceTo() {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 4);
        Point p3 = new Point(3, 0);

        //distance to self is 0
        assertEquals(0, p1.distanceTo(p1), PRECISION);
        assertEquals(0, p2.distanceTo(p2), PRECISION);
        assertEquals(0, p3.distanceTo(p3), PRECISION);

        //distance between a-b is same as b-a
        assertEquals(p1.distanceTo(p2), p2.distanceTo(p1), PRECISION);
        assertEquals(p1.distanceTo(p3), p3.distanceTo(p1), PRECISION);
        assertEquals(p2.distanceTo(p3), p3.distanceTo(p2), PRECISION);

        //computed values are right
        assertEquals(4, p2.distanceTo(p1), PRECISION);
        assertEquals(3, p3.distanceTo(p1), PRECISION);
        assertEquals(5, p3.distanceTo(p2), PRECISION);
    }

    @Test
    public void testCanFormTriangle() {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 4);
        Point p3 = new Point(3, 0);

        Point p4 = new Point(0, 2);

        assertTrue(Point.canFormTriangle(p1, p2, p3));
        assertTrue(Point.canFormTriangle(p1, p3, p2));
        assertTrue(Point.canFormTriangle(p3, p2, p1));

        assertFalse(Point.canFormTriangle(p1, p2, p4));
        assertFalse(Point.canFormTriangle(p1, p4, p2));
        assertFalse(Point.canFormTriangle(p4, p2, p1));
    }

    @Test
    public void testCanFormRightAngledTriangle() {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 4);
        Point p3 = new Point(3, 0);

        Point p4 = new Point(0, 2);

        Point p5 = new Point(4, 2);

        assertTrue(Point.canFormRightAngledTriangle(p1, p2, p3));
        assertTrue(Point.canFormRightAngledTriangle(p1, p3, p2));
        assertTrue(Point.canFormRightAngledTriangle(p3, p2, p1));

        assertFalse(Point.canFormRightAngledTriangle(p1, p2, p4));
        assertFalse(Point.canFormRightAngledTriangle(p1, p4, p2));
        assertFalse(Point.canFormRightAngledTriangle(p4, p2, p1));

        assertFalse(Point.canFormRightAngledTriangle(p1, p2, p5));
        assertFalse(Point.canFormRightAngledTriangle(p1, p5, p2));
        assertFalse(Point.canFormRightAngledTriangle(p5, p2, p1));
    }

    @Test
    public void testCanFormTriangle_trickyCases() {
        Point p1 = new Point(2.5, 2.5);
        Point p2 = new Point(3.2, 3.2);

        //triangle of just 1 point
        assertFalse(Point.canFormTriangle(p1, p1, p1));
        assertFalse(Point.canFormRightAngledTriangle(p1, p1, p1));

        //triangle of just 2 points
        assertFalse(Point.canFormTriangle(p1, p2, p2));
        assertFalse(Point.canFormRightAngledTriangle(p1, p2, p2));
    }

    @Test
    public void testMove() {
        Point p1 = new Point(0, 0);
        Point p2 = new Point(0, 4);
        Point p3 = new Point(3, 0);

        //test some properties of initial points: (distance, can form a right angled triangle)
        assertEquals(5, p2.distanceTo(p3), PRECISION);
        assertTrue(Point.canFormRightAngledTriangle(p1, p2, p3));

        //move them (with exact same amount)
        p1.move(-0.5, -1.5);
        p2.move(-0.5, -1.5);
        p3.move(-0.5, -1.5);

        //test new positions are the the expected ones
        //note: we have NO way to test a point is at some expected coordinates (as we didn't require to have getters),
        //      except by using its distanceTo() method!
        assertEquals(0, p1.distanceTo(new Point(-0.5, -1.5)), 0.0);
        assertEquals(0, p2.distanceTo(new Point(-0.5, 2.5)), 0.0);
        assertEquals(0, p3.distanceTo(new Point(2.5, -1.5)), 0.0);

        //also the initial properties based on distance should remain the same after they were moved in same direction/amount:
        assertEquals(5, p2.distanceTo(p3), PRECISION);
        assertTrue(Point.canFormRightAngledTriangle(p1, p2, p3));

        //further small movement of just 1 point should break the right angled triangle shape:
        p3.move(0, 0.01);
        assertFalse(Point.canFormRightAngledTriangle(p1, p2, p3));
    }
}
