package rezolvari_teme.w03_oop1.ex3;

import static java.lang.Math.pow;
import static java.lang.Math.sqrt;

/*
--------------------------
Ex3. Points and triangles
--------------------------

Create a class Point for modeling a 2-dimensional point:

a) Add some fields to store the values for 2 coordinates (x,y of type double).
   Question1: should you also make them ‘private’? What about ‘final’? (yes/no? why?)
   Question2: should you add getters/setter from them from the start? (are they needed for this case,
              considering all the requirements below?)

b) Add one constructor for easily building new points with given x,y values;
   Question: before you add this constructor, can you build instances like this: “new Point()” (with no params) ?
             what values will the x,y fields have in this case?  will that exact syntax work (no params)
             after you add your constructor? why yes/no?..

c) Add a few methods:
   - double distanceTo(Point other) -> should return the computed distance between this and another point
     hint1: using Pitagora theorem, the distance between the 2 points in a plane is:  dist = squareRoot(deltaX^2 +deltaY^2)
            where deltaX/Y are the difference between the x/y coordinates of the 2 points, and x^2 = x to power of 2
     hint2: for computations, you may use use these static methods from java.lang.Math class: sqrt(), pow().
            for a shorter code, try importing them directly, using a static import (instead of regular import of Math class)

   - void move(double deltaX, double deltaY) -> should move the current point in plane, by updating its current coordinates
     (add to each x,y the given deltaX/Y)

   - public String toString() - should return a string description of the point (including the values of x/y);
     make sure you declare this one as ‘public’ and exactly with this signature (has a special meaning for Java)

d) Add some static methods, which we can then use to check if a list of any three Point instances can be the corners
   of a regular or a right-angled triangle:
   - static boolean canFormTriangle(Point p1, Point p2, Point p3)
   - static boolean canFormRightAngledTriangle(Point p1, Point p2, Point p3)

   hint: to implement these, you could first compute the 3 distances between each of the pairs of 2 points
         (this being the sides of our possible triangle), then remember what you did for exercises from week 1.
 */
public class Point {

    //not final, as they need to be changeable (by move() method)
    private double x;
    private double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    static boolean canFormTriangle(Point p1, Point p2, Point p3) {

        //first compute the distances between them
        double a = p1.distanceTo(p2);
        double b = p2.distanceTo(p3);
        double c = p3.distanceTo(p1);

        //check if those distances can be the sides of a triangle
        return (a + b > c) &&
                (b + c > a) &&
                (c + a > b);
    }

    static boolean canFormRightAngledTriangle(Point p1, Point p2, Point p3) {
        if (!canFormTriangle(p1, p2, p3)) {
            return false;
        }

        double a = p1.distanceTo(p2);
        double b = p2.distanceTo(p3);
        double c = p3.distanceTo(p1);

        return pitagora(a, b, c) ||
                pitagora(b, c, a) ||
                pitagora(c, a, b);
    }

    //helper method
    private static boolean pitagora(double longSide, double shortSide1, double shortSide2) {
        return pow(longSide, 2) == pow(shortSide1, 2) + pow(shortSide2, 2);
    }

    double distanceTo(Point other) {
        return sqrt(pow(x - other.x, 2) + pow(y - other.y, 2));
    }

    void move(double deltaX, double deltaY) {
        x += deltaX;
        y += deltaY;
    }

    @Override
    public String toString() {
        return "Point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
