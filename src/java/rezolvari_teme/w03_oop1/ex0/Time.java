package rezolvari_teme.w03_oop1.ex0;

/*
--------------------------
Ex0. WARM-UP
--------------------------

a) Define an empty class named Time. Then create 2 separate instance of it. Check if they are equal (with “==” operator).
   Question: why does == return that result for this case?..

b) Add 3 fields to the Time class: hours, minutes, seconds, all of type int. Create an instance of class Time
   and set all it’s fields to some values ( like: 1:10:30).

c) Create a static method (outside of Time class) with signature “static String convertToString(Time)”
   which receives as input parameter an instance of Time and returns the values of its fields as a single String value
   with format “<hour>:<minutes>:<seconds>”, and test it with previously created Time instance.

d) Move the method convertToString inside the Time class, and change it: rename it as “toString”,
   make it non-static, public and remove its unnecessary input parameter (as it should use the fields of current instance).
   It should now have the signature “public String toString()”, but return similar value like the old method.
   Question1: could this method have remained of type static? (why yes/no?)
   Question2: if you have a variable “Time t1= new Time()”, what happens when you call “System.out.println(t.toString());” ?
              What about “System.out.println(t);”? How do you explain that?...

e) Add one constructor to Time class, which should receive 3 parameters and use them to initialize the 3 fields.
   Test that it works ok (create some instances, call .print() on them)

f) Add a validation to your constructor so it will ignore any negative values (will not set the fields
   for those parameters, leaving them set to default 0 value). Test your validation.

g) Add a 2nd constructor to Time class, which has no parameters and will set the field values to 23:59:59. Test it.

h) Modify one of your constructors (question: which one?) so that to avoid repeating code between them -
   you should use “this(...)” in one of them to call the other (with the right values for that case).

i) Change your code so that the fields of Time instances cannot be changed after they were build (making Time instances “immutable”) -
   make the fields “private” (and possibly also “final”), and add just getters (but no setters) for them.
   Check that you can still read the fields (like hours) from outside the Time class, by using the getters.

j) Add a method on Time class with signature “int secondsSinceMidnight()” which will compute and return
   the number of seconds since start of current day (0:0:0) up to the current value of Time instance.
   Test this method for a few Time instances.

k) Add to your Time class a static field “int createInstancesCounter”, and change your code (constructors)
   so each time a new instance is created, this counter is incremented. Create then 3 new instances of Time class,
   and print the value of this counter after each instance creation.

l) Add a new method to Time clase, “int secondsSince(Time)” which receives another Time instance as parameter,
   and returns the number of seconds since that time to current instance’s time (value may be negative or positive,
   depending on their order). Hint: try to use secondsSinceMidnight() to avoid repeating code.

m) Optional: draw an UML diagram for your Time class (on paper)
   UML info: https://medium.com/@smagid_allThings/uml-class-diagrams-tutorial-step-by-step-520fd83b300b)
*/

class Time {

    private static int createInstancesCounter;

    private final int hours;
    private final int minutes;
    private final int seconds;

    //1st constructor (sets all fields)
    Time(int hours, int minutes, int seconds) {
        this.hours = hours >= 0 ? hours : 0;
        this.minutes = minutes >= 0 ? minutes : 0;
        this.seconds = seconds >= 0 ? seconds : 0;
        createInstancesCounter++;
        System.out.println("Created Time instances: " + createInstancesCounter);
    }

    //2nd constructor (sets fields to some default values, by calling 1st one)
    Time() {
        this(23, 59, 59);
    }

    //old static method
    public static String convertToString(Time t) {
        return t.hours + ":" + t.minutes + ":" + t.seconds;
    }

    //--- this is here just for manual testing ---//
    public static void main(String[] args) {
        Time t1 = new Time(1, 10, 30);
        System.out.println(Time.convertToString(t1)); //old method
        System.out.println(t1.toString());
        System.out.println(t1.secondsSinceMidnight());

        Time t2 = new Time(3, 3, 3);
        System.out.println(t2);
        System.out.println(t2.secondsSince(t1));

        System.out.println("Time instance created: " + Time.createInstancesCounter);
    }

    int getHours() {
        return hours;
    }

    int getMinutes() {
        return minutes;
    }

    int getSeconds() {
        return seconds;
    }

    int secondsSinceMidnight() {
        return (hours * 60 + minutes) * 60 + seconds;
    }

    int secondsSince(Time other) {
        return this.secondsSinceMidnight() - other.secondsSinceMidnight();
    }

    public String toString() {
        return hours + ":" + minutes + ":" + seconds;
    }
}