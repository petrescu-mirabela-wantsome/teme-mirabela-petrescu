package rezolvari_teme.w03_oop1.ex4;

/*
Ex4. Intercom

Think of the objects needed for an intercom system in an apartment building.

Design the objects (with their properties and methods) needed to allow someone to enter the building:
- either by scanning their key card
- or by ringing an apartment
 */

public class IntercomDemo {
    public static void main(String[] args) {

        //intercom setup:
        Intercom ic = new Intercom(10, new String[]{"100", "123"});
        ic.setExpectedGuestForApartment(1, "ionel");
        ic.setExpectedGuestForApartment(2, "gigel");

        //some visitors come:
        Visitor v1 = new Visitor("Ionel", new Card("123"));
        Visitor v2 = new Visitor("Danut", new Card("999"));

        //and try to enter:
        System.out.println("Can visitor " + v1 + " enter using his card?  : " + ic.isAllowedToEnterUsingCard(v1.card));
        System.out.println("Can visitor " + v1 + " enter by calling ap.1? : " + ic.isAllowedToEnterByCallingApartment(1, v1.name));
        System.out.println("Can visitor " + v1 + " enter by calling ap.2? : " + ic.isAllowedToEnterByCallingApartment(2, v1.name));

        System.out.println("\nCan visitor " + v2 + " enter using his card?   : " + ic.isAllowedToEnterUsingCard(v2.card));
        System.out.println("Can visitor " + v2 + " enter by calling ap.2?  : " + ic.isAllowedToEnterByCallingApartment(2, v2.name));
        System.out.println("Can visitor " + v2 + " enter by calling ap.3?  : " + ic.isAllowedToEnterByCallingApartment(3, v2.name));
        System.out.println("Can visitor " + v2 + " enter by calling ap.20? : " + ic.isAllowedToEnterByCallingApartment(20, v2.name));

        //expected guests change
        ic.setExpectedGuestForApartment(1, null);
        ic.setExpectedGuestForApartment(3, "danut");
        System.out.println("\nGuest 'ionel' is no longer expected by ap 1...");
        System.out.println("Guest 'danut' is now expected by ap 3...");

        System.out.println("Can visitor " + v1 + " enter by calling ap.1? : " + ic.isAllowedToEnterByCallingApartment(1, v1.name));
        System.out.println("Can visitor " + v2 + " enter by calling ap.3? : " + ic.isAllowedToEnterByCallingApartment(3, v2.name));
    }
}

/**
 * Class representing the intercom system.
 * It can authorize visitors to enter in 2 ways:
 * - either directly checking a card (verify that its number if its in list of recognized numbers)
 * - or may delegate (call) a specified apartment (by number); the apartment is then responsible for accepting the visitor entry
 */
class Intercom {

    private final int numberOfApartments;
    private final Apartment[] apartments;
    private final String[] allowedCardNumbers;

    Intercom(int numberOfApartments, String[] allowedCardNumbers) {
        this.numberOfApartments = numberOfApartments;
        this.allowedCardNumbers = allowedCardNumbers;
        apartments = new Apartment[numberOfApartments];
        for (int i = 0; i < numberOfApartments; i++) {
            apartments[i] = new Apartment();
        }
    }

    //method called later to change the state of apartments (which guests are they currently expecting)
    void setExpectedGuestForApartment(int apNumber, String guestName) {
        apartments[apNumber].setExpectedGuest(guestName);
    }

    boolean isAllowedToEnterUsingCard(Card card) {
        for (String allowed : allowedCardNumbers) {
            if (allowed.equalsIgnoreCase(card.number)) {
                return true;
            }
        }
        return false;
    }

    boolean isAllowedToEnterByCallingApartment(int apNumber, String guestName) {
        if (apNumber >= 1 && apNumber <= numberOfApartments) { //check valid range
            return apartments[apNumber - 1].isGuestAllowed(guestName); //apNumber is 1-based, our array is 0-based, must adapt to it!
        }
        return false;
    }
}

/**
 * Class representing a visitor.
 * It has a Card, and a name - the basic properties needed to authenticate his entry into the building.
 */
class Visitor {
    final String name;
    final Card card;

    Visitor(String name, Card card) {
        this.name = name;
        this.card = card;
    }

    @Override
    public String toString() {
        return "Visitor{" + "name='" + name + '\'' + ", card=" + card + '}';
    }
}

/**
 * Class representing an access card.
 * All we care about now is the unique serial number (but it may have other properties too)
 */
class Card {
    final String number;

    Card(String number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Card{" + "number='" + number + '\'' + '}';
    }
}

/**
 * Represents an apartment. Each apartment can have at most 1 guest which is
 * expected (and will be allowed to enter by intercom). By default no guest is expected.
 */
class Apartment {
    private String expectedGuest = null;

    void setExpectedGuest(String expectedGuest) {
        this.expectedGuest = expectedGuest;
    }

    boolean isGuestAllowed(String guestName) {
        return expectedGuest != null && expectedGuest.equalsIgnoreCase(guestName);
    }
}
