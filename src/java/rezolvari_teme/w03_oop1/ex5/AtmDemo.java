package rezolvari_teme.w03_oop1.ex5;

/*
Ex5. ATM Machine

Think of the objects needed for an ATM machine.

Design the objects (with their properties and methods) needed for allowing some user
to withdraw an amount of money from this ATM.
*/

public class AtmDemo {
    public static void main(String[] args) {

        //setup the Bank (with some valid accounts)
        Bank bank = new Bank(
                new Account(new Card("1111-2222", "1234"), 100.0),
                new Account(new Card("3333-4444", "2019")),
                new Account(new Card("5555-6666", "9876"))
        );
        //also create multiple ATM instances, all linked to same bank!
        Atm atm1 = new Atm("ATM-1", bank);
        Atm atm2 = new Atm("ATM-2", bank);


        //Now users come:
        System.out.println("\n========== Some INVALID attempts ==========");
        atm1.showMyBalance("", "");
        atm1.showMyBalance("0000-0000", "0000");
        atm2.showMyBalance("1111-2222", "0000");
        atm1.getMeTheMoney("1111-2222", "1234", 200);
        atm2.depositSomeMoney("1111-2222", "1234", -50);

        System.out.println("\n========== Some valid attempts ==========");
        atm1.depositSomeMoney("1111-2222", "1234", 50);
        atm1.showMyBalance("1111-2222", "1234");
        System.out.println("\n--- Now from a different ATM, for same card ---");
        atm2.showMyBalance("1111-2222", "1234");
        atm2.getMeTheMoney("1111-2222", "1234", 150);
        atm1.showMyBalance("1111-2222", "1234");

        System.out.println("\n========== For some other account ==========");
        atm2.showMyBalance("3333-4444", "2019");
        atm1.getMeTheMoney("3333-4444", "2019", 0);
        atm2.getMeTheMoney("3333-4444", "2019", 10);
        atm2.depositSomeMoney("3333-4444", "2019", 20);
        atm1.showMyBalance("3333-4444", "2019");
    }
}

/**
 * Represents the summary info the bank knows about a card.
 * The only properties we care about now are the unique number, and the authorization pin.
 * <p>
 * This is to be used in the Bank code. So for example we expose the number, but not the pin (no getter for it),
 * so other parts of the code may use isPinValid() to validate if a pin value is the correct one,
 * but cannot read the actual (secret) pin of this card.
 */
class Card {
    private final String number, pin;

    Card(String number, String pin) {
        this.number = number;
        this.pin = pin;
    }

    String getNumber() {
        return number;
    }

    boolean isPinValid(String pinToCheck) {
        return pin.equals(pinToCheck);
    }
}

/**
 * Represents the data a bank has about an account.
 * We assume we have a simple balance value, and also a single Card associated with this account.
 * <p>
 * It also encapsulates here the methods to update this balance (deposit/withdraw), with all required validations.
 */
class Account {
    private final Card card;
    private double balance;

    //constructor accepting also an initial balance
    Account(Card card, double balance) {
        this.card = card;
        this.balance = balance;
    }

    Account(Card card) {
        this(card, 0);
    }

    Card getCard() {
        return card;
    }

    double getBalance() {
        return balance;
    }

    /**
     * Deposits some money in the account, returns the deposited sum (or 0 if invalid and was not accepted)
     */
    double deposit(double amount) {
        if (amount <= 0) {
            System.err.println("  [Account " + card.getNumber() + "] Error: amount to deposit must be positive! (but was: " + amount + ")");
            return 0;
        } else {
            balance += amount;
            System.out.println("  [Account " + card.getNumber() + "] Deposited amount: " + amount + ", new balance is: " + balance);
            return amount;
        }
    }


    double withdraw(double amount) {
        if (amount > balance) {
            System.err.println("  [Account " + card.getNumber() + "] Error: amount to withdraw (" + amount + ") exceeds current balance (" + balance + ")!");
            return 0;
        } else {
            balance -= amount;
            System.out.println("  [Account " + card.getNumber() + "] Withdraw amount: " + amount + ", new balance is: " + balance);
            return amount;
        }
    }
}

/**
 * Represents the bank which owns the ATMs and the cards.
 * Its the single part of code which manages the list of valid accounts.
 * <p>
 * Is called by the ATMs to get info about a specific accounts,
 * validate card number and pin, perform allowed transactions etc..
 * <p>
 * Because it's a single central one, we can have MULTIPLE ATMs linked to
 * this class, and performing operations on same account, and the results
 * will still be correct!
 */
class Bank {
    private final Account[] accounts;

    Bank(Account... accounts) {
        this.accounts = accounts;
    }

    Account getAccountOfCard(String cardNumber) {
        for (Account a : accounts) {
            if (a.getCard().getNumber().equals(cardNumber)) {
                return a;
            }
        }
        System.err.println("  [BANK] Error: no account found for card number '" + cardNumber + "'!");
        return null;
    }
}

/**
 * Represents one of the ATM machines owned by a bank.
 * It has a (unique) name representing its location,
 * and also has to know about the bank it works for (so it has the bank field).
 * Both fields need to be set at construction time (and cannot change later).
 * <p>
 * Its role is to receive user commands (through the methods showMyBalance(),...),
 * send them to the bank, and display error or info messages.
 */
class Atm {
    private final String name;
    private final Bank bank;

    Atm(String name, Bank bank) {
        this.name = name;
        this.bank = bank;
    }

    void showMyBalance(String cardNumber, String pin) {
        Account a = getAccountAfterCheckingPin(cardNumber, pin);
        if (a != null) {
            showMessage("Your current balance is: " + a.getBalance());
        }
    }

    void getMeTheMoney(String cardNumber, String pin, double amount) {
        Account a = getAccountAfterCheckingPin(cardNumber, pin);
        if (a != null) {
            showMessage("Trying to withdraw " + amount + "...");
            double withdrawn = a.withdraw(amount);
            if (withdrawn <= 0) {
                showMessage("Tough luck: found your account, but no money for you this time!");
            } else {
                showMessage("You withdraw " + withdrawn + " from your account! Feels nice, but you are a little poorer now." +
                        " Your new balance is: " + a.getBalance());
            }
        }
    }

    void depositSomeMoney(String cardNumber, String pin, double amount) {
        Account a = getAccountAfterCheckingPin(cardNumber, pin);
        if (a != null) {
            showMessage("Trying to deposit " + amount + "...");
            double deposited = a.deposit(amount);
            if (deposited <= 0) {
                showMessage("Tough luck: found your account, but cannot deposit your sum now (" + amount + ")!");
            } else {
                showMessage("You deposited " + deposited + " to your account! Good for you!" +
                        " Your new balance is: " + a.getBalance());
            }
        }
    }

    private Account getAccountAfterCheckingPin(String cardNumber, String pin) {
        System.out.println("\n[" + name + "] Getting info for account '" + cardNumber + "' from bank...");
        Account account = bank.getAccountOfCard(cardNumber);
        if (account == null) {
            showMessage("Error: bank account not found! Retry with a valid/accepted card.");
            return null;
        }
        if (!account.getCard().isPinValid(pin)) { //account is not null here..
            showMessage("Error: bank account found, but given pin ('" + pin + "') is invalid! Retry with the correct pin.");
            return null;
        }
        return account; //must be ok by now
    }

    /**
     * Prints a message from this ATM (prefixed by its name, to help differentiate
     * between output of multiple ATMs used in parallel)
     */
    private void showMessage(String msg) {
        System.out.println("[" + name + "] " + msg);
    }
}
