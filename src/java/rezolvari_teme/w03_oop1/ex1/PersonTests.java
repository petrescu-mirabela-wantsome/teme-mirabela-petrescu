package rezolvari_teme.w03_oop1.ex1;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit tests for Person class (should compile and pass all tests after you complete that one first)
 */
public class PersonTests {

    @Test
    public void testCreatePersons() {
        Person p1 = new Person("Ion", 1989, "black");
        Person p2 = new Person("Geo", 1979);

        assertEquals("Ion", p1.getName());
        assertEquals(1989, p1.getBirthYear());
        assertEquals("black", p1.getHairColor());

        assertEquals("Geo", p2.getName());
        assertEquals(1979, p2.getBirthYear());
        assertEquals("brown", p2.getHairColor());
    }

    @Test
    public void testSetters() {
        Person p1 = new Person("Ion", 1989, "black");
        assertEquals("black", p1.getHairColor());
        p1.setHairColor("blonde");
        assertEquals("blonde", p1.getHairColor());
    }

    @Test
    public void testIsOlderThan() {
        Person p1 = new Person("Ion", 1989);
        Person p2 = new Person("Geo", 1979);
        assertFalse(p1.isOlderThan(p2));
        assertTrue(p2.isOlderThan(p1));
    }

    @Test
    public void testGetAgeInYear() {
        Person p1 = new Person("Ion", 1989);
        assertEquals(1, p1.getAgeInYear(1990));
        assertEquals(41, p1.getAgeInYear(2030));
        assertEquals(0, p1.getAgeInYear(1900));
    }

    @Test
    public void testToString() {
        Person p1 = new Person("Ion", 1989, "black");
        assertTrue(p1.toString().contains("Ion"));
        assertTrue(p1.toString().contains("1989"));
        assertTrue(p1.toString().contains("black"));
    }
}
