package rezolvari_teme.w03_oop1.ex1;

/*
--------------------------
 Ex1. Modelling a person
--------------------------

Create a class Person, which will model some properties of a real person (hint: use the similar examples from presentation)

a) should have these fields: name, birthYear, hairColor (all private)

b) constructors: name and birthYear are mandatory, need to be specified when building each Person;
   but hairColor is optional, and when not specified, it should be 'brown' (hint: create 2 constructors,
   one with hairColor param and one without, and try to use a “this(..)” call to avoid repeating code between them)

c) getters/setters - add only the ones needed so that we can later read all properties of the person,
   but can modify only the hairColor field (hint: you’ll need 3 getters and 1 setter)

d) add some other methods:

   - boolean isOlderThan(Person other)  -> it should return true if current person instance is older than the other person

   - public String toString() -> it should return a description of the current person (with all his properties),
                                 as a String (which we can then print, etc..)

   - int getAgeInYear(int year) -> return the age the person would have in the specified year
                                   (may be a future or past year), or 0 if year is before his birth.
*/
public class Person {

    private final String name;
    private final int birthYear;
    private String hairColor; //can be changed

    public Person(String name, int birthYear, String hairColor) {
        this.name = name;
        this.birthYear = birthYear;
        this.hairColor = hairColor;
    }

    public Person(String name, int birthYear) {
        this(name, birthYear, "brown");
    }

    //--- just for manual testing ---//
    public static void main(String[] args) {

        //You may add your manual tests here, like:
        Person p1 = new Person("Ion", 1989, "black");
        Person p2 = new Person("Ana", 2001, "blonde");
        Person p3 = new Person("Geo", 1979);

        System.out.println("p1: " + p1.toString());
        System.out.println("p2: " + p2);
        System.out.println("p1 older than p2: " + p1.isOlderThan(p2));
        System.out.println(p3.getAgeInYear(2019));
    }

    public String getName() {
        return name;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    public boolean isOlderThan(Person other) {
        return this.birthYear < other.birthYear;
    }

    public int getAgeInYear(int year) {
        return year > birthYear ? year - birthYear : 0;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", birthYear=" + birthYear +
                ", hairColor='" + hairColor + '\'' +
                '}';
    }
}
