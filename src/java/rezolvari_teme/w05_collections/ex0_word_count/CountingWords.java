package rezolvari_teme.w05_collections.ex0_word_count;

import java.util.*;

/*
Ex0. Collections Warm-up

You have a String representing a text composed of multiple words, separated by space(s).
Fulfill these requirements, also trying to avoid using arrays in your code
(as much as possible; instead of arrays, think about which of the newly learned
collections types are the best fit for each point...)

a) Print the number of all words (including any duplicates). Hint: may use String.split(), regexp..
b) Print all words in the initial order they appear in text.
c) Print all words, but sorted alphabetically. Hint: see Collections.sort()

d) Print the number of unique words.
e) Print all unique words, in the initial order they appear in text.
f) Print all unique words, sorted alphabetically.

g) For each word, count how many times it appears, and then print all unique words
   (in their initial order in text) together with the number of times each appears.
h) The same, but now sort the word-count pairs alphabetically by word.

i) Optional: print the word-count pairs but now sorted like this: first by count (descending),
   then for pairs with same count: alphabetically by word (hint: you will need to learn more
   about comparators, and also probably to transform your Map to some other kind of collection,
   but still keep the words and their counts as pairs..)
*/

class CountingWords {
    public static void main(String[] args) {
        String s = "Once upon a time in a land far far away there lived a great king whose name was a great mystery";

        List<String> words = Arrays.asList(s.toLowerCase().split("\\s+"));
        System.out.println("word count: " + words.size());
        System.out.println("all (initial order): " + words);

        List<String> wordsSorted = new ArrayList<>(words);
        Collections.sort(wordsSorted);
        System.out.println("all (sorted): " + wordsSorted);

        Set<String> unique = new LinkedHashSet<>(words);
        System.out.println("\nunique count: " + unique.size());
        System.out.println("unique (initial order): " + unique);

        Set<String> uniqueSorted = new TreeSet<>(unique);
        System.out.println("unique (sorted): " + uniqueSorted);

        Map<String, Integer> wordCount = new LinkedHashMap<>();
        for (String w : words) {
            wordCount.put(w, 1 + wordCount.getOrDefault(w, 0));
        }
        System.out.println("\nword counts (initial order): " + wordCount);
        Map<String, Integer> sortedWC = new TreeMap<>(wordCount);
        System.out.println("word counts (sorted by word): " + sortedWC);

        //Transform the map to a List of pairs (Map.Entry objects), so we can sort it as we like,
        //based on both the key and value parts
        List<Map.Entry<String, Integer>> pairsList = new ArrayList<>(wordCount.entrySet());

        //pass a custom implementation of Comparator for our custom sorting
        pairsList.sort(new PairComparatorByValueThenKey());
        System.out.println("word counts (sorted by count, then word): " + pairsList);
    }
}

class PairComparatorByValueThenKey implements Comparator<Map.Entry<String, Integer>> {

    @Override
    public int compare(Map.Entry<String, Integer> e1, Map.Entry<String, Integer> e2) {
        if (e1.getValue() < e2.getValue()) {
            return 1;
        } else if (e1.getValue() > e2.getValue()) {
            return -1;
        } else { //values are equal
            return e1.getKey().compareTo(e2.getKey()); //so compare also the keys in this case
        }
    }
}