package rezolvari_teme.w05_collections.ex2_card_deck;

import org.junit.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

/**
 * Unit tests for CardDeck class (should compile and pass all tests after you complete that one first)
 */
public class CardDeck_Tests {

    @Test
    public void testCard() {
        Card c1 = new Card(5, Suit.DIAMONDS);
        Card c2 = new Card(5, Suit.DIAMONDS);
        Card c3 = new Card(1, Suit.DIAMONDS);
        Card c4 = new Card(5, Suit.CLUBS);

        assertEquals(c1, c2);
        assertNotSame(c1, c2);
        assertNotEquals(c1, c3);
        assertNotEquals(c1, c4);

        assertTrue(c1.toString().toLowerCase().contains("diamonds"));
        assertTrue(c1.toString().toLowerCase().contains("5"));
    }

    @Test
    public void testDeckContents() {
        CardDeck d = new CardDeck();
        //deal all cards, to check them
        List<Card> hand = d.dealHand(100);
        assertEquals(52, hand.size());
        for (int i = 2; i <= 14; i++) {
            assertContains(hand, new Card(i, Suit.DIAMONDS));
            assertContains(hand, new Card(i, Suit.CLUBS));
            assertContains(hand, new Card(i, Suit.HEARTS));
            assertContains(hand, new Card(i, Suit.SPADES));
        }
    }

    private void assertContains(List<Card> hand, Card card) {
        assertTrue("hand should contain card: " + card, hand.contains(card));
    }

    @Test
    public void testDeckDealHand() {
        CardDeck d = new CardDeck();

        assertEquals(52, d.getAvailableCardCount());
        assertEquals(0, d.getUsedCardCount());

        //deals right number of cards
        List<Card> h1 = d.dealHand(3);
        assertEquals(3, h1.size());
        assertEquals(52 - 3, d.getAvailableCardCount());
        assertEquals(3, d.getUsedCardCount());

        //deals all remaining cards if more requested
        List<Card> h2 = d.dealHand(55);
        assertEquals(49, h2.size());
        assertEquals(0, d.getAvailableCardCount());
        assertEquals(52, d.getUsedCardCount());

        //cards should be unique in each hand, and toghether they should form the full package
        Set<Card> s = new HashSet<>(h1);
        s.addAll(h2);
        assertEquals(52, s.size());

        //reset/reshuffle it
        d.shuffle();
        assertEquals(52, d.getAvailableCardCount());
        assertEquals(0, d.getUsedCardCount());

        //now first hand (of same size) is different (should be random order)
        assertNotEquals(h1, d.dealHand(3)); //should maybe compare them as sets? (ignoring order)
    }
}
