package rezolvari_teme.w05_collections.ex2_card_deck;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/*
2. Card CardDeck

a) Create a Card class to remember the individual cards. It should have 2 properties:
- number (2 - 14)
- suit (one of these types: diamonds (♦), clubs (♣), hearts (♥) and spades (♠) ).

b) Create a CardDeck class used for dealing hands.
Should contain a list of all 52 cards and should remember which ones are dealt and which ones are available.

Should contain the following methods:
- List<Card> dealHand(int cards): this method returns the specified number of cards by randomly picking from the list of available cards.
  Once picked, a card is marked as “used” (removed from “available” cards list)
  If there are not enough available cards then return as many as possible, or an empty list when there are no longer any available cards.

- void shuffle(): this should mark all cards in the deck as available (essentially emptying the “used” collection) and re-shuffle all the cards.
- int getAvailableCardCount()
- int getUsedCardCount()

Hints:
- for storing the full list of cards, as well as the lists of available/used cards, you could use some instances of List
- for shuffling a collection, the Collections.shuffle() method can be used.
 */

class CardDeck {

    private final List<Card> cards = new ArrayList<>();

    CardDeck() {
        shuffle();
    }

    /**
     * Just for some manual tests
     */
    public static void main(String[] args) {
        CardDeck deck = new CardDeck();
        System.out.println(deck.dealHand(5)); // <- print 5 cards 3 times
        System.out.println(deck.dealHand(5));
        System.out.println(deck.dealHand(5));
        System.out.println(deck.dealHand(50)); // <- only 39 cards should be printed here
        System.out.println(deck.dealHand(50)); // <- and empty list should be printed
        deck.shuffle();
        System.out.println(deck.dealHand(5)); // <- another 5 cards
    }

    void shuffle() {
        cards.clear();
        for (int i = 2; i <= 14; i++) {
            cards.add(new Card(i, Suit.DIAMONDS));
            cards.add(new Card(i, Suit.CLUBS));
            cards.add(new Card(i, Suit.HEARTS));
            cards.add(new Card(i, Suit.SPADES));
        }
        Collections.shuffle(cards);
    }

    List<Card> dealHand(int handSize) {
        List<Card> hand = new ArrayList<>();
        for (int i = 0; i < handSize && cards.size() > 0; i++) {
            Card card = cards.remove(0); //always take 1st, as they are in random order anyway
            hand.add(card);
        }
        return hand;
    }

    int getAvailableCardCount() {
        return cards.size();
    }

    int getUsedCardCount() {
        return 52 - cards.size();
    }
}
