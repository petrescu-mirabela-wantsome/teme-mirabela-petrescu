package rezolvari_teme.w05_collections.ex3_person_registry;

import java.util.Set;

/*
3. Persons Registry

We want to create a class which can hold the details of some registered persons, and then allow us to search for them, and get some statistical info about them.

a) Create a class Person having these properties:
   - cnp - an integer number (optional: validate that it’s >0)
   - name - a string (optioan: validate it’s not empty)
   - age - an integer number (optional: validate that it’s >0)
   - hasVoted - a boolean, set to true if person has voted in last elections

b) You are given this PersonsRegistry interface.
   Create a class PersonsRegistryImpl which implements this interface:

   - the methods implementations should respect not only the contract imposed by the interface (method signatures),
     but also consider the details from method comments (what to do in special cases, etc)

   - the class will need some way to store a list of persons - like a (private) field of some type of collection
     (hint: it could be preferable to index the persons by their CNP, for easy find by cnp
      and easy check/avoid duplicate cnps, so you could try using a Map for that..)
*/

interface PersonsRegistry {

    //Adds a new person to the registry. If a person with same CNP already exists,
    //it will NOT register this new person (just ignore it, and show an error)
    void register(Person p);

    //Finds a person by cnp and removes it from registry
    //If person not found, will still work (does nothing, but raises no error either)
    void unregister(int cnp);

    //Get the number of currently registered persons
    int count();

    //Get the list of cnp values of all persons
    Set<Integer> cnps();

    //Get the list of unique names of all persons
    Set<String> uniqueNames();

    //Find a person by cnp. Returns null if no person found.
    Person findByCnp(int cnp);

    //Find the persons with a specified name (may be zero, one or more)
    //(comparing person name with specified name should be case insensitive)
    Set<Person> findByName(String name);

    //Get the average age for all persons (or 0 as default if it cannot be computed)
    double averageAge();

    //Get the percent (a value between 0-100) of adults (persons with age >=18)
    //from the number of all persons (or 0 as default if it cannot be computed)
    double adultsPercentage();

    //Get the percent (a value between 0-100) of adults who voted
    //from the number of all adult persons (age>=18)
    double adultsWhoVotedPercentage();
}
