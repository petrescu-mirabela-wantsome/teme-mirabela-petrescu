package rezolvari_teme.w05_collections.ex1_building_registry;

class Building {

    private final String name;
    private final Category category;
    private final double price;
    private final String neighborhood;

    Building(String name, Category category, int price, String neighborhood) {
        this.name = name;
        this.category = category;
        this.price = price;
        this.neighborhood = neighborhood;
    }

    String getName() {
        return name;
    }

    Category getCategory() {
        return category;
    }

    double getPrice() {
        return price;
    }

    String getNeighborhood() {
        return neighborhood;
    }

    @Override
    public String toString() {
        return "Building{" +
                "name='" + name + '\'' +
                ", category=" + category +
                ", price=" + price +
                ", neighborhood='" + neighborhood + '\'' +
                '}';
    }
}
