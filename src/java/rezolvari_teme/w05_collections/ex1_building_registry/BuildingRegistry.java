package rezolvari_teme.w05_collections.ex1_building_registry;

import java.util.*;

/*
1. Buildings Registry

a) Create a Building class representing a building, with the following properties:
- name
- category (possible values: residential, office, hospital, religious)
- price
- neighborhood

Tip: you should use an enum for the values of category field.


b) Create a BuildingRegistry class, containing these static methods (each receiving a List<Building> as input parameter, and you need to decide on the return type):

- categoriesCount() - returns the number of categories (of the actual buildings, not all possible categories)
- neighborhoodsList() - return the list of names of unique neighborhoods, sorted alphabetically (hint: may use a Set; what implementation can you use to also help you with sorting?..)

- averagePriceForOneCategory() - return the average price for building from only one category (given as a 2nd input parameter to the method)
  - question: what should be the return type for this method?..

- averagePricePerCategory() - return the average price for each building category (for ALL defined categories, even the ones without building)
  - question: what kind of return type should this method have? (as it should return an average price for each used category, so kind of returning a list of pairs of 2 values - category and price)

- averagePricePerNeighborhood() - return the average price for each neighborhood.
*/

public class BuildingRegistry {

    static int categoriesCount(List<Building> bs) {
        Set<Category> categs = new HashSet<>();
        for (Building b : bs) {
            categs.add(b.getCategory());
        }
        return categs.size();
    }

    static List<String> neighborhoodsList(List<Building> bs) {
        Set<String> names = new TreeSet<>();
        for (Building b : bs) {
            names.add(b.getNeighborhood());
        }
        return new ArrayList<>(names);
    }

    static double averagePriceForOneCategory(List<Building> bs, Category cat) {
        int count = 0;
        double sum = 0.0;
        for (Building b : bs) {
            if (b.getCategory() == cat) {
                count++;
                sum += b.getPrice();
            }
        }
        return count == 0 ? 0 : sum / count;
    }

    static Map<Category, Double> averagePricePerCategory(List<Building> bs) {
        Map<Category, Double> map = new HashMap<>();
        for (Category cat : Category.values()) {
            double avgPrice = averagePriceForOneCategory(bs, cat);
            map.put(cat, avgPrice);
        }
        return map;
    }

    static Map<String, Double> averagePricePerNeighborhood(List<Building> bs) {
        //some temporary maps to store the sum and count for each neighborhood! (needed to compute the averages later)
        Map<String, Double> neighVsSum = new HashMap<>();
        Map<String, Integer> neighVsCount = new HashMap<>();
        for (Building b : bs) {

            double oldSum = neighVsSum.getOrDefault(b.getNeighborhood(), 0.0);
            neighVsSum.put(b.getNeighborhood(), oldSum + b.getPrice());

            int oldCount = neighVsCount.getOrDefault(b.getNeighborhood(), 0);
            neighVsCount.put(b.getNeighborhood(), oldCount + 1);
        }

        Map<String, Double> neighVsAveragePrice = new HashMap<>();
        for (String neigh : neighVsSum.keySet()) {
            //get the sum/count values for this neigh from the 2 separate maps
            double sum = neighVsSum.getOrDefault(neigh, 0.0);
            int count = neighVsCount.getOrDefault(neigh, 0);
            double avg = count == 0 ? 0 : sum / count;
            neighVsAveragePrice.put(neigh, avg);
        }
        return neighVsAveragePrice;
    }


    /**
     * Some manual tests
     */
    public static void main(String[] args) {

        List<Building> buildings = Arrays.asList(
                new Building("a", Category.OFFICE, 10, "tudor"),
                new Building("b", Category.OFFICE, 40, "centru"),
                new Building("c", Category.OFFICE, 20, "pacurari"),
                new Building("d", Category.RESIDENTIAL, 15, "pacurari"),
                new Building("e", Category.HOSPITAL, 35, "pacurari"),
                new Building("f", Category.HOSPITAL, 30, "copou"));

        System.out.println("Actual categories: " + categoriesCount(buildings));
        System.out.println("Actual neighborhoods: " + neighborhoodsList(buildings));

        System.out.println("Average price per categ: " + averagePricePerCategory(buildings));
        System.out.println("Average price per neighborhood: " + averagePricePerNeighborhood(buildings));
    }
}
