package rezolvari_teme.w09_files;

import java.io.File;
import java.util.Date;
import java.util.Scanner;

/*
 2) File Info

 Given the path of a file, print this info about it:
  - if file exists
  - its absolute path, type (dir or file), size and last update date
  - if it's writable

  Test it then with some absolute and some relative paths (even something like “.”)
*/

public class Ex2_FileInfo {
    public static void main(String[] args) {

        System.out.print("Enter file name (full path): ");
        Scanner scn = new Scanner(System.in);
        String path = scn.next();

        File file = new File(path);
        System.out.println("Absolute path for file is: " + file.getAbsolutePath());
        System.out.println("File is on the disc: " + file.exists());
        System.out.println("File is a dir: " + file.isDirectory());
        System.out.println("File size: " + file.length());
        System.out.println("Last update time: " + file.lastModified());
        System.out.println("File is writable: " + file.canWrite());

        long lastTime = file.lastModified();
        Date date = new Date(lastTime);
        System.out.println(date);
    }
}
