package rezolvari_teme.w09_files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

/*
6. Reading file lines

  Given a file path, read its content (hint: may use Scanner with .nextLine()) and then:
  a. Display first N lines (N read from console), each line being prefixed by its line count
  b. Find and display the longest line (including its length)
     - Optional: also include the line number for this longest line

  c. Display these statistics:
     - number of lines, number of words (hint: for splitting a line to words may use String.split() method + some regular expression, see RegExExample.java..)
     - show the list of all words (sorted alphabetically) together with the usage count for each (hint: store them in a kind of Map<String, Integer>)
     - OPTIONAL: top 5 most used words (hint: use a TreeMap<String,Integer> to store each word+count, and create a custom comparator which sorts by count - and use an instance of this comparator when creating the TreeMap, so its entries will be automatically sorted by it..)
     - the longest word
  d. Write these statistics to a new text file (named like initial file + '_statistics.txt' suffix)
*/

public class Ex6_ReadFileLines {
    public static void main(String[] args) throws IOException {

        String path = "./curs/doc/w10_recap_tema.txt";

        File file = new File(path);
        if (!file.exists()) {
            System.err.println("Error: file not found: " + file.getAbsolutePath());
            return;
        }
        System.out.println("Input file exists: " + file.getAbsolutePath());


        //--- First N lines ---//
        System.out.print("Give max number of lines to show: ");
        Scanner keyScanner = new Scanner(System.in);
        int maxLines = keyScanner.nextInt();

        System.out.println("\nShowing first " + maxLines + " lines of the file " + file.getAbsolutePath() + ":");
        int lineCount = 0;
        Scanner fileScanner = new Scanner(file);
        while (lineCount < maxLines && fileScanner.hasNext()) {
            String line = fileScanner.nextLine();
            lineCount++;
            System.out.println("[" + lineCount + "] " + line);
        }

        //--- Longest line (without line number, Stream.max() doesn't provide that..) ---//
        Optional<String> longestLineOpt = Files
                .lines(file.toPath()) //Stream<String>
                .max(Comparator.comparingInt(String::length));
        String longestLine = longestLineOpt.orElse("");
        System.out.println("\nLongest line: " + longestLine.length() + " chars: '" + longestLine + "'");

        //--- Longest line (with line number) ---//
        List<String> lines = Files.lines(file.toPath()).collect(toList());
        List<LineInfo> linesInfo = new ArrayList<>();
        for (int i = 0; i < lines.size(); i++) {
            linesInfo.add(new LineInfo(i, lines.get(i)));
        }

        Optional<LineInfo> longestLineInfoOpt = linesInfo.stream()
                .max(Comparator.comparingInt(lineInfo -> lineInfo.content.length()));
        System.out.println("Longest line (with info): "
                + longestLineInfoOpt.map(info -> info.content.length()).orElse(0) + " chars: "
                + longestLineInfoOpt);

        //--- Lines/words statistics ---//
        List<String> words = lines.stream()
                .flatMap(line -> Arrays.stream(line.split("\\W")))
                .filter(w -> !w.isEmpty())
                .map(String::toLowerCase)
                .collect(toList());
        System.out.println("\nTotal lines: " + lines.size() + ", words: " + words.size());

        Map<String, Long> wordsWithCount =
                words.stream()
                        .collect(Collectors.groupingBy(w -> w, TreeMap::new, Collectors.counting()));
        System.out.println("\nWords vs usage count:");
        wordsWithCount.forEach((word, count) ->
                System.out.println(word + " : " + count));

        List<Map.Entry<String, Long>> top5words = wordsWithCount.entrySet().stream()
                .sorted(Comparator.comparingLong((Map.Entry<String, Long> e) -> e.getValue()).reversed())
                .limit(5)
                .collect(toList());
        System.out.println("\nTop 5 most used words: " + top5words);

        System.out.println("\nLongest word: " + words.stream().max(Comparator.comparing(String::length)));

        //todo: write statistics also to a file...
    }


    /**
     * Simple class to store some info about each lines
     */
    static class LineInfo {
        final int number;
        final String content;

        LineInfo(int number, String content) {
            this.number = number;
            this.content = content;
        }

        @Override
        public String toString() {
            return "LineInfo{" + "number=" + number + ", content='" + content + '\'' + '}';
        }
    }
}
