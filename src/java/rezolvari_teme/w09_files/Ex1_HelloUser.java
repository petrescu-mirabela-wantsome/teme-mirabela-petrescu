package rezolvari_teme.w09_files;

import java.io.*;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

/*
 1) Greeting with Properties file:

 Write a greeting app which remembers the user:
  - it should display a message like "Hello, [user name], nice to see you again! (last visit time: [h:m:s])"
  - it will use a properties files (named 'user.properties') to store the username and the time of last visit
  - it will first try to find the file and load the 2 expected keys from it:
    - if keys are loaded, just use them
    - if file/keys are missing, ask the user for his name, and then use that one (also saving it to the properties file)

  Note: the program should ask the user for the name only once (when missing, and save it to prop file);
  but it needs to update the time of the last visit on each run
*/

public class Ex1_HelloUser {

    //define some constants (related to config file)
    private final static String CONFIG_FILENAME = "config.properties";
    private final static String CONFIG_KEY_USERNAME = "username";
    private final static String CONFIG_KEY_LAST_VISIT_TIME = "last_visit_time";


    public static void main(String[] args) {

        //try to load last visit data from file
        Properties config = loadConfigFromFile();

        //get user name (from file or console)
        String userName = readUserNameFromFileOrConsole(config);

        //get time of last visit (if found in file), or compute it
        Date lastVisitTime = readTimeOfLastVisitFromFile(config);

        printUserGreeting(userName, lastVisitTime);

        saveTimeOfCurrentVisitToConfig(config);

        //save config to file (with updated username and visit time)
        saveConfigToFile(config);
    }

    private static Properties loadConfigFromFile() {
        Properties config = new Properties();
        try (InputStream in = new FileInputStream(CONFIG_FILENAME)) {
            config.load(in);
            System.out.println("Success loading data from config file: " + config);
        } catch (IOException io) {
            System.err.println("Config file missing.");
        }
        return config;
    }

    private static String readUserNameFromFileOrConsole(Properties config) {
        String userName;//if data was loaded from config file and we found our expected key for username
        if (config.containsKey(CONFIG_KEY_USERNAME)) {
            userName = config.getProperty(CONFIG_KEY_USERNAME); //then get the value for that key from the config, and use it as username
        } else { //data not loaded or user key is missing
            //ask the user for its name instead
            System.out.print("Please enter your name: ");
            Scanner scn = new Scanner(System.in);
            userName = scn.next();
            //need to also save this new name to our Properties object (to be saved later to file)
            config.setProperty(CONFIG_KEY_USERNAME, userName);
        }
        return userName;
    }

    private static Date readTimeOfLastVisitFromFile(Properties config) {
        String lastVisitTimeAsString = config.getProperty(CONFIG_KEY_LAST_VISIT_TIME);
        if (lastVisitTimeAsString != null) { //means we have a String value loaded from config file for this key
            long lastVisitTimeAsLong = Long.valueOf(lastVisitTimeAsString); //transform the String value to a number of type long (should be possible)
            return new Date(lastVisitTimeAsLong); //then convert the long value to a Date value, for nicer display
        }
        return null;
    }

    private static void printUserGreeting(String userName, Date lastVisitTime) {
        if (lastVisitTime != null) { //means we have a String value loaded from config file for this key
            System.out.println("Hello " + userName + ", nice to see you again! (last visit time: " + lastVisitTime + ")");
        } else { //null value, meaning file or key was missing, so we show a simplified message without it
            System.out.println("Hello " + userName + ", nice to meet you!");
        }
    }

    private static void saveTimeOfCurrentVisitToConfig(Properties config) {
        //also update the last visit time to the current time (in same format, as a long number of milliseconds since 1970)
        long currentTime = new Date().getTime(); //get now in millis
        config.setProperty(CONFIG_KEY_LAST_VISIT_TIME, String.valueOf(currentTime)); //put value under the time key in our Properties instance
    }

    private static void saveConfigToFile(Properties config) {
        try (OutputStream out = new FileOutputStream(CONFIG_FILENAME)) {
            config.store(out, "Holds config for Ex1_HelloUser.java");
        } catch (IOException e) {
            System.err.println("Error saving properties to file '" + CONFIG_FILENAME + "', cause: " + e);
        }
    }
}
