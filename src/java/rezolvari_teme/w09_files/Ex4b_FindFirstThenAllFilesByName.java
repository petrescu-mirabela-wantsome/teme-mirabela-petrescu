package rezolvari_teme.w09_files;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/*
4. Find file (by name)

 - Read from user:
   - the path of a base directory
   - a file name pattern to search for (a String value)

 - Search under that base dir (and all its subfolders!) for a file/dir which contains in its name the given pattern (case insensitive)

 - You should stop and return only the first file found (we don't care if there are multiple matching files),
   and display its full path, or display another message if no file was found

 Example:
 - Given path: ".\doc":
   - for pattern: "week8_TE" it should find and display the path of this file (if started from this project, like: "C:\....\doc\s4_week8_tema.txt")
   - for pattern: "week9_" it should display a message like: "No files found matching pattern: 'week9_'

 BONUS:
 - if a file was found, show more info about it: name, size, if it's a dir or not (how can you do that in a more optimal way, what could you change in the signature of your recursive function?)
 - show all the matching files, not just the first one (show full info for each, not just name)
 */

public class Ex4b_FindFirstThenAllFilesByName {

    public static void main(String[] args) {

        String path = "./doc";
        String pattern = "WEek4";

        File baseDir = new File(path);


        //--- Test findFirst() method ---//
        System.out.println("\n--- With findFirst() method ---\n");

        File foundFile = findFirst(baseDir, pattern, "");

        //need to handle differently the case of getting back a null value (for file not found case)
        if (foundFile != null) {
            System.out.println("\nFirst file found matching pattern '" + pattern + "': " + foundFile.getAbsolutePath());
        } else {
            System.out.println("\nNo file found matching pattern '" + pattern + "'");
        }


        //--- Test findAll() method ---//
        System.out.println("\n\n--- With findAll() method ---\n");

        List<File> foundFiles = findAll(baseDir, pattern, "");
        System.out.println("\nFound " + foundFiles.size() + " files under '" + path + "' with names matching pattern '" + pattern + "':");
        for (File f : foundFiles) {
            System.out.println(f.getAbsolutePath());
        }
    }

    private static File findFirst(File parent, String pattern, String prefix) {

        System.out.println(prefix + "Checking name of: " + parent.getName() + " ...");
        if (fileNameContainsPattern(parent.getName(), pattern)) {

            System.out.println(prefix + "Current name is a good one, returning this: " + parent.getName());
            return parent;

        } else { //current file name was not ok

            //if is dir, we should check each of its children (with recursive calls!)
            if (parent.isDirectory()) {
                System.out.println(prefix + "File is a dir, checking its children: " + parent.getAbsolutePath());

                File[] children = parent.listFiles();
                for (File child : children) {
                    System.out.println(prefix + "- checking child: " + child.getName());

                    File foundNephew = findFirst(child, pattern, prefix + "  ");
                    if (foundNephew != null) {
                        System.out.println(prefix + "!Found good nephew: " + foundNephew.getName());
                        return foundNephew;
                    }
                }
            }

            //if we reached this point, means we found no good child in for{} loop
            System.out.println(prefix + "Found no matching file under: " + parent.getName());
            return null;
        }
    }

    private static List<File> findAll(File parent, String pattern, String prefix) {
        List<File> result = new ArrayList<>();

        System.out.println(prefix + "Checking name of: " + parent.getName() + " ...");

        if (fileNameContainsPattern(parent.getName(), pattern)) {
            System.out.println(prefix + "Found good name :" + parent.getName());
            result.add(parent);
        }

        if (parent.isDirectory()) {
            System.out.println(prefix + "File is a dir, checking its children: " + parent.getAbsolutePath());

            File[] children = parent.listFiles();
            for (File child : children) {
                System.out.println(prefix + "- checking child: " + child.getName());

                List<File> foundNephews = findAll(child, pattern, prefix + "  ");
                result.addAll(foundNephews);
            }
        }
        return result;
    }

    private static boolean fileNameContainsPattern(String name, String pattern) {
        return name.toLowerCase().contains(pattern.toLowerCase());
    }
}

