package rezolvari_teme.w09_files;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

/*
 3) Folder contents

 Given the path of a directory:
 - list its direct descendants (files+dirs):
   - should display one line for each, with the name and size
   - list should be sorted descending by size
   - OPTIONAL: place the directories first and sorted ascending by name (and after them the files descending by size)
 - display the total size of all files
*/

public class Ex3_FolderContentsDirectChildren {

    public static void main(String[] args) {
        String path = "./curs/doc";
        File file = new File(path);

        //list all files under this dir (we just assume here is a dir, we don't check it..)
        File[] files = file.listFiles();

        //sort the list with custom comparator (by size and/or name)
        Arrays.sort(files, new FileComparator());

        //show the list of files (one per line), and also compute total size
        long totalSize = 0;
        for (File f : files) {
            System.out.println(f.length() + "," + f.getName());
            totalSize += f.length();
        }
        System.out.println("Total size: " + totalSize);
    }


    static class FileComparator implements Comparator<File> {
        @Override
        public int compare(File f1, File f2) {

            //if both are directories, compare/sort them by name (alphabetically):
            if (f1.isDirectory() && f2.isDirectory()) {
                return f1.getName().compareTo(f2.getName());
            }

            //else if both are simple files, compare them by size (sorting them descending, biggest first):
            if (f1.isFile() && f2.isFile()) {
                return -Long.compare(f1.length(), f2.length());
            }

            //else if one of them is a file and the other a directory, the directory is considered always smaller (to be placed first when sorted)
            if (f1.isFile() && f2.isDirectory()) {
                return 1;
            } else { //the only remaining case is: f1=dir, f2=file
                return -1;
            }
        }
    }
}
