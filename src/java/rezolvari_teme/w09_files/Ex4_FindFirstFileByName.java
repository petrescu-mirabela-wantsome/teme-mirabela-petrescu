package rezolvari_teme.w09_files;

import java.io.File;
import java.util.Scanner;

/*
 4) Find file by name

 - Read from user:
   - the path of a base directory
   - a file name pattern to search for (a String value)

 - Search under that base dir (and all its subfolders!) for a file/dir which contains in its name the given pattern (case insensitive)

 - You should stop and return only the first file found (we don't care if there are multiple matching files),
   and display its full path, or display another message if no file was found

 Example:
 - Given path: ".\doc":
   - for pattern: "week8_TE" it should find and display the path of this file (if started from this project, like: "C:\....\doc\s4_week8_tema.txt")
   - for pattern: "week9_" it should display a message like: "No files found matching pattern: 'week9_'
*/

public class Ex4_FindFirstFileByName {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.print("Base dir (like '.') : ");
        String path = in.next();
        System.out.print("File name pattern (like 'Ex4_Find') : ");
        String pattern = in.next().toLowerCase();

        File file = findFile(new File(path), pattern);

        if (file != null) {
            System.out.println("Found a matching file: " + file.getAbsolutePath() +
                    " (size: " + file.length() + " bytes, isDir: " + file.isDirectory() + ")");
        } else {
            System.out.println("Sorry, found no files with name matching '" + pattern + "' under " + path);
        }
    }

    private static File findFile(File parent, String pattern) {
        if (parent.getName().toLowerCase().contains(pattern)) {
            return parent;
        }
        if (parent.isDirectory()) {
            for (File child : parent.listFiles()) {
                File foundChild = findFile(child, pattern);
                if (foundChild != null) {
                    return foundChild;
                }
            }
        }
        return null;
    }
}
