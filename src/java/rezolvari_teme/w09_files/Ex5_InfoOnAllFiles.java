package rezolvari_teme.w09_files;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/*
5. Info on all files

  Given the path of a base directory, scan all the file tree under it (including all levels) and:

  a. Display the total number of files and directories under it
  b. Find and show the top 5 biggest files (display for each file the path and size; list should be sorted desc by size)
  c. Write the previous displayed info to a new text file (placed under base dir, named 'dir_info.txt')

  Hint: this is a good place to use RECURSIVE methods:
   - In general, your recursive method should receive as input a directory path and return some value (total files count, or biggest file, or a list of files..)
     computed for that whole tree; it will also call itself as needed on the smaller trees under the current dir
     (with a stop condition when the received tree has no more levels, etc)
   - See ListDirectoryTreeExample.java for something similar. Also, to better understand recursion, read from: /doc/books/thinkjava.pdf, chapters 5.8, 5.9!
*/

public class Ex5_InfoOnAllFiles {

    private static final String BASE_PATH = "./curs/doc";

    public static void main(String[] args) {

        File startDir = new File(BASE_PATH);

        System.out.println("\nFiles count: " + countFiles(startDir) + "\n");
        System.out.println("\nFiles & Directories count: " + countFilesAndDirs(startDir));


        List<File> filesInfo = getAllFilesInfo(startDir);
        List<File> top5BySize =
                filesInfo.stream()
                        .sorted(Comparator.comparing(File::length).reversed())
                        .limit(5)
                        .collect(Collectors.toList());
        String top5 = top5BySize.stream()
                .map(file -> file.getAbsolutePath() + " : " + file.length() + " bytes")
                .collect(Collectors.joining("\n"));
        System.out.println("\nTop 5 biggest files:\n" + top5);
    }

    /**
     * Method which counts all files or directories (mixed, no separate counters)
     * found under the given parent directory or any of its subdirectories.
     * It uses recursion, and returns just a single total counter value (of primitive type int)
     */
    private static int countFiles(File parent) {
        System.out.println(parent.getAbsolutePath() + " - start counting files...");

        //easy case (stop condition) first: if it's a simple file, no need for recursion, just return here
        if (parent.isFile()) {

            System.out.println(parent.getAbsolutePath() + " - ...done counting, it's a file");
            return 1;

        } else { //parent is dir

            //so we should also get (and sum) the counts for each of the children;
            //we do this by recursion: method calls itself again, but passing the child dir as the new base dir

            int count = 1; //start by adding 1 for itself (1 dir)
            for (File child : parent.listFiles()) {
                count += countFiles(child);
            }

            System.out.println(parent.getAbsolutePath() + " - ...done counting, it has: " + count + " files");
            return count;
        }
    }

    /**
     * Very similar method, but this one counts files and directories separately.
     * For this, we need to use a custom class (Counts) to store and finally return
     * the pair of 2 int values (as methods cannot return 2 separate values at once,
     * like 2 primitive values, but they can return an instance of a combined class to store 2 values, like Counts here)
     */
    private static Counts countFilesAndDirs(File parent) {

        System.out.println(parent.getAbsolutePath() + " - start counting files & dirs...");

        if (parent.isFile()) {

            System.out.println(parent.getAbsolutePath() + " - ...done counting, it's a file");
            return new Counts(1, 0);

        } else { //parent is dir

            //similar to above, but needs a little more work to sum
            //the 2 values from child counts to parent counts separately

            Counts parentCounts = new Counts(0, 1); //add 1 from start for parent dir itself
            for (File child : parent.listFiles()) {
                Counts childCounts = countFilesAndDirs(child);
                parentCounts.dirCount += childCounts.dirCount;
                parentCounts.fileCount += childCounts.fileCount;
            }

            System.out.println(parent.getAbsolutePath() + " - ...done counting, it has: " + parentCounts);
            return parentCounts;
        }
    }

    /**
     * Similar method, but it just goes over all files in subtree and builds a list of File objects for all of them
     */
    private static List<File> getAllFilesInfo(File parent) {

        System.out.println(parent.getAbsolutePath() + " - start getting files info...");

        if (parent.isFile()) {

            System.out.println(parent.getAbsolutePath() + " - ...done counting, it's a file");
            return Collections.singletonList(parent);

        } else { //parent is dir

            List<File> filesInfo = new ArrayList<>();
            for (File child : parent.listFiles()) {
                filesInfo.addAll(getAllFilesInfo(child));
            }
            System.out.println(parent.getAbsolutePath() + " - ...done counting, got info for " + filesInfo.size() + " files");
            return filesInfo;
        }
    }

    /**
     * Simple custom class which packs 2 int values in a single object.
     * We defined it in order to be able to return 2 values (not just one) from a method.
     */
    static class Counts {
        int fileCount;
        int dirCount;

        //having a constructor is helpful
        Counts(int fileCount, int dirCount) {
            this.fileCount = fileCount;
            this.dirCount = dirCount;
        }

        //also good to have a nice toString, to print this pair of values directly
        @Override
        public String toString() {
            return "Counts{" + "fileCount=" + fileCount + ", dirCount=" + dirCount + '}';
        }
    }
}