package rezolvari_teme.w08_streams.ex0_warmup;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/*
0. Warm-up (streams)

a) Given a list of Strings, write a method that returns a list of all strings that start with the letter 'a' (lower case) and have exactly 3 letters.

b) Write a method that returns a comma separated string based on a given list of integers. Each element should be preceded by the letter 'e' if the number is even, and preceded by the letter 'o' if the number is odd. For example, if the input list is (3,44), the output should be 'o3,e44'.

c) Given a string, count the occurrences of each letter (hint: see Collectors.groupingBy, Collectors.counting..)

d) Print the first 10 powers of 3 which are greater than 1000 (hint: use IntStream.iterate(), limit(), filter(), boxed()..)
   Output should be like: 2187, 6561, ...

e) Print all the square numbers (of form n^2) between 1000 and 10000 (hint: use IntStream.range(), map(), boxed()..)
   Output should be like: 1024, 1089, 1156, ....

f) Solve again the problems from Week 5 (Collections), but using stream operations whenever possible - the problems:
   “Warm-up” (counting words)
   “Buildings Registry”
   “Persons Registry”
*/

public class StreamsExercises {

    public static void main(String[] args) {

        System.out.println(startWithLetterAndHaveLength('a', 3, Arrays.asList("aaabbccccd", "aaa", "bcd")));
        System.out.println(addLetterForEvenOdd(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)));
        System.out.println(countChars("eeaaabbccccdaaxxyyy"));

        pow3greater();
        countSquares();
    }

    /**
     * Given a list of Strings, write a method that returns a list of all strings that
     * start with the letter 'a' (lower case) and have exactly 3 letters.
     */
    private static List<String> startWithLetterAndHaveLength(char letter, int wordLength, List<String> words) {
        return words.stream()
                .filter(s -> s.length() == wordLength && s.startsWith(String.valueOf(letter)))
                .collect(Collectors.toList());
    }

    /**
     * Write a method that returns a comma separated string based on a given list of integers. Each element should be
     * preceded by the letter 'e' if the number is even, and preceded by the letter 'o' if the number is odd.
     * For example, if the input list is (3,44), the output should be 'o3,e44'.
     */
    private static String addLetterForEvenOdd(List<Integer> numbers) {
        return numbers.stream()
                .map(i -> (i % 2 == 0 ? "e" : "o") + i)
                .collect(Collectors.joining(", "));
    }

    /**
     * Given a string, count the occurrences of each letter.
     */
    private static Map<String, Long> countChars(String text) {

        String[] letters = text.split("");

        //Can be done in a SINGLE stream pipeline, but with a more complex collect, using .groupingBy() with extra params!
        //see more info:  https://dzone.com/articles/the-ultimate-guide-to-the-java-stream-api-grouping
        return Arrays
                .stream(letters)
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
    }

    /**
     * Print the first 10 powers of 3 which are greater than 1000 (hint: use IntStream.iterate(), limit(), filter(), boxed()..)
     */
    private static void pow3greater() {
        IntStream
                .iterate(1, i -> i * 3)
                .filter(i -> i >= 1000)
                .limit(10)
                .forEach(i -> System.out.println(i + " "));
        System.out.println();
    }

    /**
     * Print all the square numbers (of form n^2) between 1000 and 10000 (hint: use IntStream.range(), map(), boxed()..)
     */
    private static void countSquares() {
        System.out.println("squares: " +
                IntStream
                        .range(1, 100)
                        .map(i -> i * i)
                        .filter(i -> i >= 1000)
                        .boxed()
                        .collect(Collectors.toList()));
    }
}
