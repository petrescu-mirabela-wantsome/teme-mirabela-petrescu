package rezolvari_teme.w08_streams.ex2_fizzbuzz;

import java.util.stream.IntStream;

/*
1. FizzBuzz with Streams

Write 2 solutions to the the FizzBuzz problem:
- First a regular one (imperative style, with loops, ifs, etc)
- Then one only with streams and lambdas (hint: see IntStream.rangeClosed to simulate a stream of numbers)

The FizzBuzz problem: Given a range of numbers, for each number, display: fizz, if the number divides by 3,
buzz if it divides by 5, fizzbuzz if it divides by both or the number itself if none of the above.
*/

public class FizzBuzzStream {

    private static void fizzBuzzImperative(int n) {
        System.out.println("\nImperative:");

        for (int i = 1; i < n; i++) {
            System.out.println(toFizzBuzz(i));
        }
    }

    private static void fizzBuzzWithStreams(int n) {
        System.out.println("\nWith streams:");

        IntStream.rangeClosed(1, n)
                .mapToObj(FizzBuzzStream::toFizzBuzz)
                .forEach(System.out::println);
    }

    //helper method translating one number to fizzbuzz value
    private static String toFizzBuzz(int i) {
        if (i % 15 == 0) return "fizzbuzz";
        if (i % 3 == 0) return "fizz";
        if (i % 5 == 0) return "buzz";
        return String.valueOf(i);
    }

    public static void main(String[] args) {
        fizzBuzzImperative(30);
        fizzBuzzWithStreams(30);
    }
}
