package rezolvari_teme.w08_streams.ex1_set_ops_streams;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toSet;

/*
1. Set operations with Streams

Given 2 Set<T> instances (holding elements of a generic type T), find the intersection, union, and difference between them,
but only using stream operations.
*/

public class SetOperationsWithStreams {

    private static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        //version1: with Stream.concat()
        return Stream.concat(set1.stream(), set2.stream())
                .collect(toSet());

        //version2: with flatMap over Stream of Streams
        //return Stream.of(set1.stream(), set2.stream())
        //        .flatMap(s -> s)
        //        .collect(toSet());
    }

    private static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        return set1.stream()
                .filter(set2::contains) //method ref, equivalent to: .filter(s -> c2.contains(s))
                .collect(toSet());
    }

    private static <T> Set<T> difference(Set<T> set1, Set<T> set2) {
        return set1.stream()
                .filter(elem1 -> !set2.contains(elem1))
                .collect(toSet());
    }

    public static void main(String[] args) {

        Set<String> set1 = new HashSet<>(Arrays.asList("aa", "bb", "cc"));
        Set<String> set2 = new HashSet<>(Arrays.asList("bb", "cc", "dd"));

        System.out.println("set1: " + set1);
        System.out.println("set2: " + set2);

        System.out.println("union: " + union(set1, set2));
        System.out.println("intersection: " + intersection(set1, set2));
        System.out.println("diff(set1-set2): " + difference(set1, set2));
        System.out.println("diff(set2-set1): " + difference(set2, set1));
    }
}
