package rezolvari_teme.w08_streams.ex3_trader;

/*
3. Trader Transactions

Create 2 classes:
  - Trader - with properties: name, city
  - Transaction - with properties: year, value, trader

Compute the following based on a list of such transactions:
  - Find all transactions from 2011 and sort them by value (small to high)
  - What are all the unique cities where traders work?
  - Find all traders from Cambridge and sort them by name (descending)
  - Return a string of all traders’ names sorted alphabetically and separated by comma
  - Determine if there are any traders from Milan
  - Update all transactions so that the traders from Milan are moved to Cambridge
  - What’s the highest value in all transactions?
  - What’s the transaction with the lowest value?
*/

import java.util.*;
import java.util.stream.Collectors;

public class TraderMain {
    public static void main(String[] args) {

        //build some traders
        Trader tony = new Trader("Tony", "Milan");
        Trader john = new Trader("John", "Cambridge");
        Trader oliver = new Trader("Oliver", "Cambridge");
        Trader ion = new Trader("Ion", "Iasi");

        //and some transactions
        List<Transaction> trans = Arrays.asList(
                new Transaction(2011, 100, tony),
                new Transaction(2012, 80, tony),
                new Transaction(2013, 120, tony),
                new Transaction(2011, 50, oliver),
                new Transaction(2010, 130, john),
                new Transaction(2011, 70, john),
                new Transaction(2012, 90, john),
                new Transaction(2011, 60, ion),
                new Transaction(2012, 140, ion));


        // - Find all transactions from 2011 and sort them by value (small to high)
        List<Transaction> from2011SortedByValue =
                trans.stream()
                        .filter(t -> t.getYear() == 2011)
                        .sorted(Comparator.comparingInt(Transaction::getValue)) //equivalent to: (t1,t2)-> Integer.compare(t1.getValue(), t2.getValue())
                        .collect(Collectors.toList());
        System.out.println("from2011SortedByValue: " + from2011SortedByValue);

        // - What are all the unique cities where traders work?
        Set<String> cities =
                trans.stream()
                        .map(t -> t.getTrader().getCity())
                        .collect(Collectors.toSet());
        System.out.println("traders cities: " + cities);

        // - Find all traders from Cambridge and sort them by name (descending)
        List<String> tradersFromCambridge =
                trans.stream()
                        .map(Transaction::getTrader)
                        .filter(t -> t.getCity().equalsIgnoreCase("Cambridge"))
                        .distinct()
                        .map(Trader::getName)
                        .sorted()
                        .collect(Collectors.toList());
        System.out.println("tradersFromCambridge: " + tradersFromCambridge);

        // - Return a string of all traders’ names sorted alphabetically and separated by comma
        String allTraders =
                trans.stream()
                        .map(t -> t.getTrader().getName())
                        .distinct()
                        .sorted()
                        .collect(Collectors.joining(","));
        System.out.println("allTraders: " + allTraders);


        // - Determine if there are any traders from Milan
        boolean anyFromMilan =
                trans.stream()
                        .anyMatch(t -> t.getTrader().getCity().equalsIgnoreCase("Milan"));
        System.out.println("anyFromMilan?: " + anyFromMilan);


        //   Update all transactions so that the traders from Milan are moved to Cambridge
        //Version1: making a copy with relocated traders
        List<Transaction> transAfterRelocation =
                trans.stream()
                        .map(t -> {
                            //if transaction's trader is from Milan
                            if (t.getTrader().getCity().equalsIgnoreCase("Milan")) {
                                //make a copy of the transactions, with all data like in original, except trader's city
                                return new Transaction(
                                        t.getYear(),
                                        t.getValue(),
                                        new Trader(t.getTrader().getName(), "Cambridge"));
                            } else { //else keep original data (transaction)
                                return t;
                            }
                        })
                        .collect(Collectors.toList());
        System.out.println("\nnew transAfterRelocation: " + transAfterRelocation);

        //Version2: actually updating the trader cities in original list
        //(this requires a mutable Trader class, where city field can be changed, and is not very nice/functional..)
        Set<Trader> tradersFromMilan =
                trans.stream()
                        .map(Transaction::getTrader)
                        .filter(t -> t.getCity().equalsIgnoreCase("Milan"))
                        .collect(Collectors.toSet());
        System.out.println("relocating tradersFromMilan: " + tradersFromMilan);
        tradersFromMilan.forEach(t -> t.setCity("Cambridge"));
        System.out.println("initial trans updated (after relocation): " + trans);


        // - What’s the highest value in all transactions? What’s the transaction with the lowest value?
        Optional<Transaction> highestValueTrans = trans.stream().max(Comparator.comparingInt(Transaction::getValue));
        System.out.println("\nhighestValueTrans: " + highestValueTrans);

        Optional<Transaction> lowestValueTrans = trans.stream().min(Comparator.comparingInt(Transaction::getValue));
        System.out.println("lowestValueTrans: " + lowestValueTrans);
    }
}
