package rezolvari_teme.w04_oop2.ex1_cylinder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Unit tests for CircleCylinder class (should compile and pass all tests after you complete that one first)
 */
public class CircleCylinder_Tests {
    private static final double PRECISION = 0.001;

    @Test
    public void testCircle() {
        Circle c = new Circle(0.1, 0.2, 2.5);
        assertEquals(19.634, c.area(), PRECISION);
        assertEquals(15.707, c.length(), PRECISION);
        assertEquals(0.1, c.getCenterX(), PRECISION);
        assertEquals(0.2, c.getCenterY(), PRECISION);
        assertEquals(2.5, c.getRadius(), PRECISION);
        assertTrue(c.toString().contains("Circle"));
        assertTrue(c.toString().contains("0.1"));
        assertTrue(c.toString().contains("0.2"));
        assertTrue(c.toString().contains("2.5"));
    }

    @Test
    public void testCylinderH() {
        CylinderH c = new CylinderH(0.1, 0.2, 2.5, 3.5);

        assertEquals(3.5, c.getHeight(), PRECISION);
        Circle base = c.getBase();
        assertEquals(0.1, base.getCenterX(), PRECISION);
        assertEquals(0.2, base.getCenterY(), PRECISION);
        assertEquals(2.5, base.getRadius(), PRECISION);

        assertEquals(68.722, c.volume(), PRECISION);
        assertEquals(94.247, c.area(), PRECISION);

        assertTrue(c.toString().contains("Cylinder"));
        assertTrue(c.toString().contains("0.1"));
        assertTrue(c.toString().contains("0.2"));
        assertTrue(c.toString().contains("2.5"));
        assertTrue(c.toString().contains("3.5"));
    }

    @Test
    public void testCylinderC() {
        CylinderC c = new CylinderC(0.1, 0.2, 2.5, 3.5);

        //rest is the same like for CylinderH
        assertEquals(3.5, c.getHeight(), PRECISION);
        Circle base = c.getBase();
        assertEquals(0.1, base.getCenterX(), PRECISION);
        assertEquals(0.2, base.getCenterY(), PRECISION);
        assertEquals(2.5, base.getRadius(), PRECISION);

        assertEquals(68.722, c.volume(), PRECISION);
        assertEquals(94.247, c.area(), PRECISION);

        assertTrue(c.toString().contains("Cylinder"));
        assertTrue(c.toString().contains("0.1"));
        assertTrue(c.toString().contains("0.2"));
        assertTrue(c.toString().contains("2.5"));
        assertTrue(c.toString().contains("3.5"));
    }

    @Test
    public void testCylinderH_getBaseReturnsSimpleCircle() {
        CylinderH c = new CylinderH(0.1, 0.2, 2.5, 3.5);
        assertEquals(2.5, c.getBase().getRadius(), PRECISION);
        assertEquals(19.634, c.getBase().area(), PRECISION);
        assertEquals(15.707, c.getBase().length(), PRECISION);
    }

    @Test
    public void testCylinderC_getBaseReturnsSimpleCircle() {
        CylinderC c = new CylinderC(0.1, 0.2, 2.5, 3.5);
        assertEquals(2.5, c.getBase().getRadius(), PRECISION);
        assertEquals(19.634, c.getBase().area(), PRECISION);
        assertEquals(15.707, c.getBase().length(), PRECISION);
    }

    @Test
    public void testCylinderInterface() {
        runTestOnGenericCylinder(new CylinderH(0.1, 0.2, 2.5, 3.5));
        runTestOnGenericCylinder(new CylinderC(0.1, 0.2, 2.5, 3.5));
    }

    private void runTestOnGenericCylinder(Cylinder c) {
        Circle base = c.getBase();
        assertEquals(0.1, base.getCenterX(), PRECISION);
        assertEquals(0.2, base.getCenterY(), PRECISION);
        assertEquals(2.5, base.getRadius(), PRECISION);

        assertEquals(19.634, c.getBase().area(), PRECISION);
        assertEquals(15.707, c.getBase().length(), PRECISION);

        assertEquals(3.5, c.getHeight(), PRECISION);

        assertEquals(68.722, c.volume(), PRECISION);
        assertEquals(94.247, c.area(), PRECISION);

        assertTrue(c.toString().contains("Cylinder"));
        assertTrue(c.toString().contains("0.1"));
        assertTrue(c.toString().contains("0.2"));
        assertTrue(c.toString().contains("2.5"));
        assertTrue(c.toString().contains("3.5"));
    }
}
