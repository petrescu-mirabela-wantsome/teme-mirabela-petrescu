package rezolvari_teme.w04_oop2.ex1_cylinder;

import static java.lang.Math.PI;

/*
Ex1. Circle & Cylinder (composition vs inheritance)

a) Define a Circle class (fields: centerX/Y,radius; methods: area(), length(), toString(), getters)
   - hint: circle area = PI*R^2, length = 2*PI*R ; may use Math.PI, Math.pow.. if needed
   - question: can you make the fields final? (can you think of any benefits it might bring?) What about the methods and/or the class itself?

b) Based on it, define a CylinderH class, first using inheritance (no composition)
   - with 2 constructors:
        Cylinder(double centerX, double centerY, double radius, double height)
        Cylinder(Circle circle, double height)
     Can you avoid repeating code between constructors? (hint: try to use this())

   - members: similar to Circle (inherited), but also with some extra fields (height) and extra methods:  volume(), getBase() (returns a Circle representing the base), getHeight()
     - hint: volume = base area * height
     - question: for volume(), can you use the existing area() method?
     - question: do all inherited methods make sense as they are for Cylinder? (like: area(), length(), toString())

   - now override methods to better fit Cylinder logic: area(), toString()
     - hint: cylinder surface area = 2*base area + h*base perimeter
     - question: can we use area() from Circle for computing area() for Cylinder, instead of repeating that code? how?..
     - question: after overriding area(), is the volume() still working correctly? if not: why, and how can we fix it?
     - question: can we reuse Circle.toString() when overriding toString() for cylinder?

   - question: what can we do with length(), as its really not relevant/needed anymore for Cylinder? (hide it somehow? return some special value?..)

c) Now solve it again, but using composition (instead of inheritance) and creating a different class named CylinderC (which should NOT extend Circle, but should instead contain a Circle field…) (which should NOT extend Circle, but should instead contain a Circle field…)
   - question: does it look better/simpler, or worse that 1st? any pluses/minuses?

d) Define a Cylinder generic interface, and define in it the common properties of CylinderH and CylinderC
   - modify the 2 classes so that both of them implement this interface
   - question: how many lines did you need to change in the 2 classes?
   - question: does client code which wants to work with one of our 2 cylinder implementations need to still mention
               the name of one of the 2 classes (CylinderH/CylinderC), or can it use only Cylinder name everywhere?
               (if class names are still needs, where exactly?)

e) Is it possible to further reduce duplication between the 2 classes? Is there any code which you could move from
   the 2 cylinder implementation classes to the interface itself (reducing duplication) AND which would remain correct
   in the future, even if we add other type of cylinder classes implementing this interface? (hint: see ‘default’ methods)
   - is there any difference between the 2 classes, regarding how easy is to move some of their code to the interface?..
   - is any client code affected by such a move, what do you think?
   - do all the tests still pass? (check after you did the moving)

f) Optional: draw an UML class diagram for all these classes/interfaces

f) Optional: draw an UML class diagram for all these classes/interfaces
 */

interface Cylinder {

    Circle getBase();

    double getHeight();

    default double volume() {
        return getBase().area() * getHeight();
    }

    default double area() {
        return 2 * getBase().area() + getBase().length() * getHeight();
    }
}

public class CircleCylinder {
    public static void main(String[] args) {

        Circle base = new Circle(0.1, 0.2, 2.5);
        System.out.println("base: " + base);
        System.out.println("base area: " + base.area() + ", length: " + base.length());

        //with inheritance:
        CylinderH ch = new CylinderH(0.1, 0.2, 2.5, 3.5);
        System.out.println("\nch: " + ch);
        System.out.println("ch: base: " + ch.getBase());
        System.out.println("ch: base area: " + ch.getBase().area());
        System.out.println("ch: total area: " + ch.area() + ", volume: " + ch.volume());
        System.out.println("ch length?: " + ch.length());

        //with composition:
        CylinderC cc = new CylinderC(0.1, 0.2, 2.5, 3.5);
        System.out.println("\ncc: " + cc);
        System.out.println("cc: base: " + cc.getBase());
        System.out.println("cc: base area: " + cc.getBase().area());
        System.out.println("cc: total area: " + cc.area() + ", volume: " + cc.volume());
        //System.out.println("cc length?: " + cc.length()); //compile error
    }
}

class Circle {
    private final double centerX, centerY, radius;

    Circle(double centerX, double centerY, double radius) {
        this.centerX = centerX;
        this.centerY = centerY;
        this.radius = radius;
    }

    double getCenterX() {
        return centerX;
    }

    double getCenterY() {
        return centerY;
    }

    double getRadius() {
        return radius;
    }

    double area() {
        return PI * radius * radius;
    }

    double length() {
        return 2 * PI * radius;
    }

    @Override
    public String toString() {
        return "Circle{" + "centerX=" + centerX + ", centerY=" + centerY + ", radius=" + radius + '}';
    }
}

/**
 * Cylinder implementation using inheritance
 */
class CylinderH extends Circle implements Cylinder {

    private final double height;

    CylinderH(double centerX, double centerY, double radius, double height) {
        super(centerX, centerY, radius);
        this.height = height;
    }

    CylinderH(Circle circle, double height) {
        this(circle.getCenterX(), circle.getCenterY(), circle.getRadius(), height);
    }

    //A simpler, but WRONG version: the result('this') has the properties of Circle base, but .area() is computed wrong! (computed for full cylinder, not for base)
    //  public Circle getBase() {return this;}
    //A better version, returns actually just a simple Circle (with right .area() impl)
    public Circle getBase() {
        return new Circle(getCenterX(), getCenterY(), getRadius());
    }

    public double getHeight() {
        return height;
    }

    //These 2 just CANNOT be replaced by the default implementations of methods from interface!
    //True even after we also changed getBase() to return an actual simple Circle, NOT 'this'! (so then getBase().getArea() works correctly in interface)
    //The cause seems to be that: CylinderH extends Circle, so it inherits its .area() method, and then even if it implements Cylinder interface
    // the default impl of area() from interface will NOT override the implementation inherited from Circle!
    public double volume() {
        return super.area() * height;
    }

    @Override
    public double area() {
        return super.area() * 2 + super.length() * height;
    }

    @Override
    public String toString() {
        return "CylinderH{" + "height=" + height + ", base=" + super.toString() + '}';
    }
}

/**
 * Cylinder implementation using composition
 */
class CylinderC implements Cylinder {

    private final Circle base;
    private final double height;

    CylinderC(Circle base, double height) {
        this.base = base;
        this.height = height;
    }

    CylinderC(double centerX, double centerY, double radius, double height) {
        this(new Circle(centerX, centerY, radius), height);
    }

    public Circle getBase() {
        return base;
    }

    public double getHeight() {
        return height;
    }

    /*These 2 can easily/safely be removed after adding default implementations in interface:
    public double volume() { return base.area() * height; }
    public double area() { return base.area() * 2 + base.length() * height; }
    */

    @Override
    public String toString() {
        return "CylinderH{" + "height=" + height + ", base=" + base + '}';
    }
}