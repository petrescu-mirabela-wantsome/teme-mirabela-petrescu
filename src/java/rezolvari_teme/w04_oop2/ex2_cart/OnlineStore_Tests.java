package rezolvari_teme.w04_oop2.ex2_cart;

import org.junit.Test;
import rezolvari_teme.w04_oop2.ex2_cart.customer.Address;
import rezolvari_teme.w04_oop2.ex2_cart.customer.Customer;
import rezolvari_teme.w04_oop2.ex2_cart.discount.Discount;
import rezolvari_teme.w04_oop2.ex2_cart.discount.FixedDiscount;
import rezolvari_teme.w04_oop2.ex2_cart.discount.PercentageDiscount;
import rezolvari_teme.w04_oop2.ex2_cart.product.Product;

import static org.junit.Assert.assertEquals;

/**
 * Unit tests for classes of Online Store app (should compile and pass all tests after you complete those first)
 */

public class OnlineStore_Tests {

    private static final double PRECISION = 0.001; //precision used in assertEquals() for doubles

    @Test
    public void testCart() {
        Customer cust = new Customer("Ionel", "Popescu", "1234", new Address("Popauti", 1, "Iasi"));

        Cart cart = new Cart(cust);

        //1) Add products to cart (no discount yet):
        cart.addProduct(new Product(1, "p1", "phone", 1900, "blue"));
        cart.addProduct(new Product(2, "p2", "watch", 490, "black"));
        Product p3 = new Product(3, "p3", "hat", 990, "white");
        cart.addProduct(p3);

        //!should print: ... TOTAL: 3380.0
        System.out.println("Before discounts: \n " + cart.generateInvoice());
        assertEquals(3380.0, cart.computeTotalPrice(), PRECISION);

        //2) Also add some discounts:
        cart.addDiscount(new FixedDiscount(710));
        Discount d2 = new PercentageDiscount(10);
        cart.addDiscount(d2);

        //!should print: ... TOTAL: 2403.0
        System.out.println(" \n After discounts: \n " + cart.generateInvoice());
        assertEquals(2403.0, cart.computeTotalPrice(), PRECISION);


        //3) Replace a product and a discount:
        cart.removeProduct(p3);
        cart.addProduct(new Product(4, "p4", "gloves", 290, "brown"));
        cart.removeDiscount(d2);
        cart.addDiscount(new PercentageDiscount(10));

        //!should print: ... TOTAL: 1773.0
        System.out.println(" \n After replacing a product and a discount: \n " + cart.generateInvoice());
        assertEquals(1773.0, cart.computeTotalPrice(), PRECISION);
    }
}
