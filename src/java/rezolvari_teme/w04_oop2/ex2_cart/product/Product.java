package rezolvari_teme.w04_oop2.ex2_cart.product;

public class Product {

    private final int id;
    private final String name;
    private final String type;
    private final double price;
    private final String color;

    public Product(int id, String name, String type, double price, String color) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.price = price;
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", price=" + price +
                ", color='" + color + '\'' +
                '}';
    }
}
