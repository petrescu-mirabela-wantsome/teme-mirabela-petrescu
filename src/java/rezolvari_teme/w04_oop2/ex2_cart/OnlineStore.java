package rezolvari_teme.w04_oop2.ex2_cart;

import rezolvari_teme.w04_oop2.ex2_cart.customer.Address;
import rezolvari_teme.w04_oop2.ex2_cart.customer.Customer;
import rezolvari_teme.w04_oop2.ex2_cart.discount.Discount;
import rezolvari_teme.w04_oop2.ex2_cart.discount.FixedDiscount;
import rezolvari_teme.w04_oop2.ex2_cart.discount.PercentageDiscount;
import rezolvari_teme.w04_oop2.ex2_cart.product.Product;

/*
Ex2: Online Store

Write a class hierarchy for an online store. Please define and implement the following packages, entities, fields and methods:

- customer package:
    - Customer class:
        - fields: firstName, lastName, cnp, address (this one should be of type Address, see below)
    - Address class:
        - fields: street, streetNumber, town
- product package:
    - Product class:
        - fields: id, name, type (ex: clothing, electronics etc), price, color

- discount package:
    - Discount - class, abstract class or interface? it should allow the code to somehow be able to apply a discount in a general way, and then support 2 types of discounts (two implementations with compute the discount in different ways):
    - PercentageDiscount class
        - field: percent: what percentage of the price should be discounted (e.g 4%)
        - methods ?
    - FixedDiscount class
        - field: amount: a fixed amount (e.g 35 RON)
        - methods ?
      Use inheritance to somehow define/support these two different discount types.

- (directly in top package of assignment)
    - Cart class:
        - fields: customer, list of products and discounts
        - methods:
            Cart(Customer customer) - constructor
            void addProduct(Product p)
            void removeProduct(Product p)
            void addDiscount(Discount d)
            void removeDiscount(Discount d)
            double computeProductsPrice() - compute total price of products (without any discounts applied)
            double computeTotalPrice() - compute final cart price (total price of products + all discounts applied)
            String generateInvoice()- should return a String (multi-line, use “\n” to separate lines) representing the invoice for current cart; it should contain this info:
                - Customer info
                - List of products
                - Total products price (without discounts)
                - List of discounts
                - Total amount (total products price + discounts)
*/

/**
 * This should be the main class used to start the Online Store app.
 * <p>
 * But here it just contains some manual tests to check the classes
 */
public class OnlineStore {

    public static void main(String[] args) {
        Customer cust = new Customer("Ionel", "Popescu", "1234", new Address("Popauti", 1, "Iasi"));

        Cart cart = new Cart(cust);
        cart.addProduct(new Product(1, "p1", "phone", 190.99, "blue"));
        cart.addProduct(new Product(2, "p2", "watch", 49.99, "black"));
        Product p3 = new Product(3, "p3", "hat", 99.99, "white");
        cart.addProduct(p3);

        System.out.println("Before discounts:\n" + cart.generateInvoice());

        cart.addDiscount(new FixedDiscount(100));
        Discount d2 = new PercentageDiscount(5);
        cart.addDiscount(d2);

        System.out.println("\nAfter discounts:\n" + cart.generateInvoice());

        //replacing a product
        cart.removeProduct(p3);
        cart.addProduct(new Product(4, "p4", "gloves", 29.99, "brown"));

        //replacing a discount
        cart.removeDiscount(d2);
        cart.addDiscount(new PercentageDiscount(15));

        System.out.println("\nAfter replacing a product and a discount:\n" + cart.generateInvoice());
    }
}
