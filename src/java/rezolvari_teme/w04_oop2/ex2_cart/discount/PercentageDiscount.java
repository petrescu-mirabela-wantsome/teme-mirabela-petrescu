package rezolvari_teme.w04_oop2.ex2_cart.discount;

public class PercentageDiscount implements Discount {

    private final int discountPercentValue;

    public PercentageDiscount(int discountPercentValue) {
        this.discountPercentValue = discountPercentValue;
    }

    @Override
    public double apply(double initialAmount) {
        return initialAmount * (100 - discountPercentValue) / 100;
    }

    @Override
    public String toString() {
        return "PercentageDiscount{" +
                "discountPercentValue=" + discountPercentValue +
                '}';
    }
}
