package rezolvari_teme.w04_oop2.ex2_cart.discount;

public class FixedDiscount implements Discount {

    private final int discountValue; //the value of the fixed discount to apply

    public FixedDiscount(int discountValue) {
        this.discountValue = discountValue;
    }

    @Override
    public String toString() {
        return "FixedDiscount{" +
                "discountValue=" + discountValue +
                '}';
    }

    @Override
    public double apply(double initialAmount) {
        double afterDiscount = initialAmount - discountValue;
        return afterDiscount > 0 ? afterDiscount : 0;
    }
}
