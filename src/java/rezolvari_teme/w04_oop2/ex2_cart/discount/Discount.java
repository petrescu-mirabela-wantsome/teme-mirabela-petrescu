package rezolvari_teme.w04_oop2.ex2_cart.discount;

public interface Discount {

    /**
     * Receives an initial amount, applies a discount
     * (in a specific way) and returns the sum remaining after discount
     */
    double apply(double initialAmount);

}