package rezolvari_teme.w04_oop2.ex2_cart;

import rezolvari_teme.w04_oop2.ex2_cart.customer.Customer;
import rezolvari_teme.w04_oop2.ex2_cart.discount.Discount;
import rezolvari_teme.w04_oop2.ex2_cart.product.Product;

import java.util.ArrayList;
import java.util.List;

class Cart {

    private final Customer customer;
    //note: could have used for these 2 fields some arrays, but would be more painful, so I preferred to use directly collections (List)
    private final List<Discount> discounts = new ArrayList<>(); //may have 1 or more discounts
    private final List<Product> products = new ArrayList<>();

    Cart(Customer customer) {
        this.customer = customer;
    }


    void addProduct(Product p) {
        products.add(p);
    }

    void removeProduct(Product p) {
        products.remove(p);
    }

    void addDiscount(Discount d) {
        discounts.add(d);
    }

    void removeDiscount(Discount d) {
        discounts.remove(d);
    }


    double computeTotalPrice() {
        double finalPrice = computeProductsPrice();
        for (Discount d : discounts) {
            finalPrice = d.apply(finalPrice);
        }
        return finalPrice;
    }

    private double computeProductsPrice() {
        double productsPrice = 0;
        for (Product p : products) {
            productsPrice += p.getPrice();
        }
        return productsPrice;
    }

    String generateInvoice() {
        String productsList = "";
        for (Product p : products) {
            productsList += " - " + p + "\n";
        }
        return "===============================================" +
                "\nINVOICE:\n" + customer +
                "\nProducts:\n" + productsList +
                "-----------------------------------------------" +
                "\nProducts price: " + computeProductsPrice() +
                "\nDiscounts: " + discounts +
                "\n-----------------------------------------------" +
                "\nTOTAL: " + computeTotalPrice() +
                "\n===============================================";
    }

    @Override
    public String toString() {
        return "Cart{" +
                "customer=" + customer +
                ", discounts=" + discounts +
                ", products=" + products +
                '}';
    }
}
