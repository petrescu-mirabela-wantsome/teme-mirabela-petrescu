package rezolvari_teme.w04_oop2.ex3_complex;

import java.util.Objects;

/*
Ex3. Complex numbers

Create a Complex class that represents a complex number (more info: https://en.wikipedia.org/wiki/Complex_number)
It should have the following methods:
    - Complex add(Complex other): adds this and other number and returns a new Complex instance; hint: (a+bi) + (c+di) = (a+c) + (b+d)i
    - Complex negate(): returns a new Complex number representing the negative value of this; hint: if z = (a+bi) then -z = (-a-bi)
    - Complex multiply(Complex other): returns a new complex number that is equal to this * other number; hint: (a+bi)(c+di) = (ac−bd) + (ad+bc)i
    - boolean equals(Object other): should return true only if ‘other’ is also an instance of Complex class and the 2 numbers are equal (both their parts..)
    - String toString(): should return strings such as “3 + 4i”

    - static Complex complex(double realPart, double imaginaryPart): should create a new Complex instance with given values;
      this will be basically just a shortcut for users of this class to create instances in a more concise manner
      (calling ‘complex(..)’ vs ‘new Complex(...)’)
 */

class Complex {

    private final double r, i;

    private Complex(double r, double i) {
        this.r = r;
        this.i = i;
    }

    //builder method
    static Complex complex(double r, double i) {
        return new Complex(r, i);
    }

    //just for running some manual tests
    public static void main(String[] args) {
        Complex c1 = complex(1, 2);
        Complex c2 = complex(3, 4);

        //manual testing
        System.out.println("c1: " + c1 + ", c2: " + c2);
        System.out.println("!c1 = " + c1.negate());
        System.out.println("c1+c2 = " + c1.add(c2));
        System.out.println("c1*c2 = " + c1.multiply(c2));
    }

    Complex add(Complex other) {
        return new Complex(r + other.r, i + other.i);
    }

    Complex negate() {
        return new Complex(-r, -i);
    }

    Complex multiply(Complex other) {
        return new Complex(r * other.r - i * other.i, r * other.i + i * other.r);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Complex complex = (Complex) o;
        return r == complex.r && i == complex.i;
    }

    @Override
    public int hashCode() {
        return Objects.hash(r, i);
    }

    @Override
    public String toString() {
        return "(" + r + (i > 0 ? "+" : "") + i + "i)";
    }
}
