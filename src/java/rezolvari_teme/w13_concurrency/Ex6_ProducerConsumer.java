package rezolvari_teme.w13_concurrency;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Consider having a class called StringsGenerator, that generates random strings using the following algorithm:
 * Generate a random integer between 3 and 13; this will be the length of the word
 * Generate the string, by randomly generate each letter (in ‘a’-’z’ interval)
 * <p>
 * Write a StringsProcessor interface with the following implementations:
 * StringsPrinter, prints a string to the console
 * VowelsCounter, prints to the console the number of vowels in a word
 * StringsLength, prints to the console the length of a string
 * VowelsRemover, prints to the console the string that results from removing all vowels
 * <p>
 * Then write a StringsProducer thread that randomly generates 100 string values, and for each of them it also
 * chooses randomly one of the strings processing algorithms described above, to be applied on that string later in consumer.
 * <p>
 * Finally write a StringsConsumer thread that receives the above objects (string value + processing algorithm)
 * and applies the algorithm to the string, and prints the result.
 * <p>
 * The communication between the two threads should be done via an ArrayBlockingQueue instance.
 * Use the put() and take() methods (and possibly peek())
 * <p>
 * Optional:
 * 1) For a cleaner end of the program:
 * - after generating all values, put an extra special value on the queue (a “poison” item) which will signal to the consumer threads, when they detect it, that they should end too
 * - in main thread, after starting the new threads (1producer+2consumers), wait until all new threads completed, before ending main thread too
 * 2) Make your solution work with 2 or more consumer threads; and for clearer output, print from the consumers also the thread id, when processing a value from queue
 * - questions here: how many changes did you have to make to support this? What about having 2 producers + 3 consumers? Did you need any changes also to the consumer code handling the “poison” value? (for all consumer threads to finish)
 */
public class Ex6_ProducerConsumer {

    public static void main(String[] args) throws InterruptedException {

        //create shared queue (blocking)
        ArrayBlockingQueue<StringAndProcessor> queue = new ArrayBlockingQueue<>(10);

        //create some threads
        List<Thread> threads = Arrays.asList(
                new StringProducerThread(queue),
                new StringConsumerThread(queue),
                new StringConsumerThread(queue)
        );

        //start threads
        System.out.println("[MAIN] Starting " + threads.size() + " new threads...");
        threads.forEach(Thread::start);

        //wait for all threads to finish, before ending main thread
        for (Thread thread : threads) {
            thread.join();
        }
        System.out.println("[MAIN] ...finished");
    }

    /**
     * Processor interface and all its implementations
     */
    interface StringsProcessor {
        String process(String s);
    }

    /**
     * Generates random string values
     */
    static class StringsGenerator {
        private static Random random = new Random();

        static String generate() {
            int len = 3 + random.nextInt(11);
            String s = "";
            for (int i = 0; i < len; i++) {
                s += (char) ('a' + random.nextInt('z' - 'a'));
            }
            return s;
        }

        //for manually testing the generator
        public static void main(String[] args) {
            for (int i = 0; i < 10; i++) System.out.println(StringsGenerator.generate());
        }
    }

    /**
     * Chooses a random processor from available ones
     */
    static class ProcessorChooser {
        private static final Random random = new Random();
        private static final StringsProcessor[] processors = {
                new ToUpper(),
                new StringLength(),
                new VowelsCounter(),
                new VowelRemover(),
        };

        static StringsProcessor choose() {
            return processors[random.nextInt(processors.length)];
        }
    }

    /**
     * Class packing in a single object a pair of: a String value to process,
     * and its processor type
     */
    static class StringAndProcessor {
        static final StringAndProcessor POISON_VALUE = new StringAndProcessor("[END_PROCESSING]", null);

        final String string;
        final StringsProcessor processor;

        StringAndProcessor(String string, StringsProcessor processor) {
            this.string = string;
            this.processor = processor;
        }

        @Override
        public String toString() {
            return "StringAndProcessor{" +
                    "string='" + string + '\'' +
                    ", processor=" + processor +
                    '}';
        }
    }

    static class StringProducerThread extends Thread {
        private final ArrayBlockingQueue<StringAndProcessor> queue;

        StringProducerThread(ArrayBlockingQueue<StringAndProcessor> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            for (int i = 0; i < 20; i++) {
                StringAndProcessor sp = new StringAndProcessor(
                        StringsGenerator.generate(),
                        ProcessorChooser.choose()
                );
                putInQueue(sp);
            }
            putInQueue(StringAndProcessor.POISON_VALUE); //final value, to finish processing
        }

        private void putInQueue(StringAndProcessor sp) {
            try {
                print("Produced value: " + sp);
                queue.put(sp); //blocking (if queue is full)
            } catch (InterruptedException e) {
                System.err.println("Interrupted while waiting to put element in queue");
            }
        }

        private void print(String s) {
            System.out.println("[PRODUCER:" + Thread.currentThread().getId() + "] " + s);
        }
    }

    static class StringConsumerThread extends Thread {
        private final ArrayBlockingQueue<StringAndProcessor> queue;

        StringConsumerThread(ArrayBlockingQueue<StringAndProcessor> queue) {
            this.queue = queue;
        }

        @Override
        public void run() {
            try {
                while (true) {
                    if (queue.peek() == StringAndProcessor.POISON_VALUE) {
                        print("GOT POISON VALUE, ending...");
                        break;
                    } else {
                        StringAndProcessor sp = queue.take(); //blocking, waits for at least 1 element to be present in queue
                        print("Processed value: " + sp.processor.process(sp.string));
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        private void print(String s) {
            System.out.println("[CONSUMER:" + Thread.currentThread().getId() + "] " + s);
        }
    }

    static class ToUpper implements StringsProcessor {
        @Override
        public String process(String s) {
            return "uppercase(" + s + ")=" + s.toUpperCase();
        }
    }

    static class VowelsCounter implements StringsProcessor {
        @Override
        public String process(String s) {
            int vowels = 0;
            for (char c : s.toCharArray()) if (isVowel(c)) vowels++;
            return "vowelsCount(" + s + ")=" + vowels;
        }

        private boolean isVowel(char c) {
            return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
        }
    }

    static class StringLength implements StringsProcessor {
        @Override
        public String process(String s) {
            return "length(" + s + ")=" + s.length();
        }
    }

    static class VowelRemover implements StringsProcessor {
        @Override
        public String process(String s) {
            //for replacement, use a regex
            String noVowels = s.replaceAll("[aeiou]", "");
            return "vowelRemover(" + s + ")=" + noVowels;
        }
    }
}
