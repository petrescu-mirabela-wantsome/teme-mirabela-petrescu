package rezolvari_teme.w13_concurrency;

import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Create 10 new threads. They should have each a number (1..10) and run in the background
 * waiting.
 * - The main thread should start reading from user (from console) some integer numbers, in a loop.
 * - if the current number read matches the number of one of the threads, then ​ only that thread
 * should print "Hello from thread [number] (thread id: [id])" (and only once)
 * - if current number is -1 then ​ all​ threads should write a goodbye message and then end (each
 * one writing "Goodbye from thread [number]").
 */
public class Ex4_InteractiveGreeterThreads {
    public static void main(String[] args) throws InterruptedException {

        //the shared value
        AtomicInteger readValue = new AtomicInteger(0);

        //create the threads
        System.out.println("[MAIN]: Creating new threads...");
        for (int i = 1; i <= 10; i++) {
            new NumThread(i, readValue).start();
        }

        Scanner sc = new Scanner(System.in);
        do {
            Thread.sleep(100); //wait a little after creation/reaction from other threads, before writing to console from main one
            System.out.print("\nEnter a number (1..10, or -1 to exit): ");
            readValue.set(sc.nextInt());
        } while (readValue.get() != -1);

        System.out.println("[MAIN] Main thread finished.");
    }
}

class NumThread extends Thread {
    private final int number;
    private final AtomicInteger readValue;

    NumThread(int number, AtomicInteger readValue) {
        this.number = number;
        this.readValue = readValue;
    }

    @Override
    public void run() {
        print("Started thread " + number + "...");
        while (readValue.get() != -1) {
            if (readValue.compareAndSet(number, 0)) {
                print("Hello from thread " + number);
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                //ignore it
            }
        }
        print("Goodbye from thread " + number);
    }

    private void print(String text) {
        System.out.println(text + " (th.id: " + Thread.currentThread().getId() + ")");
    }
}