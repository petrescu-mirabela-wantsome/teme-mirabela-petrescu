package rezolvari_teme.w13_concurrency;

/**
 * Start 2 new threads, and find a way to share a variable between them.
 * The 2 thread will have different roles:
 * - one should sleep a little, then change the value of the shared variable, then end;
 * - the 2nd one should wait for the change (in a loop with sleep), detect when the variable
 * has changed and print a message about the change (old and new value), and then end;
 * Optional​ : once you have a solution, think about a 2nd different way in which you can
 * share the variable between the 2 threads..
 */
public class Ex2_SharedResource {

    private static int sharedStaticField = 0;

    public static void main(String[] args) {
        solution1_usingAStaticFieldAsSharedResource();
        //solution2_passingSharedMutableVariableToThreadConstructor();
    }

    private static void solution1_usingAStaticFieldAsSharedResource() {
        System.out.println("\n=== SOLUTION 1 - with static field ===");

        //define 2 thread using lambdas
        Runnable changer = () -> {
            System.out.println("  [CHANGER] Started thread...");
            sleepFor(500);
            System.out.println("  [CHANGER] Updating shared value...");
            sharedStaticField = 7;
            System.out.println("  [CHANGER] ...ending thread.");
        };

        Runnable observer = () -> {
            int oldValue = sharedStaticField;
            System.out.println("Started observer thread, shared variable value is: " + oldValue);

            while (sharedStaticField == oldValue) {
                System.out.println("...observing...");
                sleepFor(100);
            }
            System.out.println("!Observer: detected a change of shared variable: newValue: " + sharedStaticField + ", oldValue: " + oldValue);
            System.out.println("...ending observer thread");
        };

        new Thread(observer).start();
        new Thread(changer).start();
    }


    private static void solution2_passingSharedMutableVariableToThreadConstructor() {
        System.out.println("\n=== SOLUTION 2 - with variable passed to Runnable constructor ===");

        //create the variable to be used as shared resource
        MutableInt sharedVar = new MutableInt(0);

        //define 2 threads using anon classes (cannot use lambdas due to RunnableWithField being an interface, not a class)
        Runnable changer = new RunnableWithField(sharedVar) {
            @Override
            public void run() {
                System.out.println("  [CHANGER] Started thread...");
                sleepFor(500);
                System.out.println("  [CHANGER] Updating shared value...");
                field.value = 7;
                System.out.println("  [CHANGER] ...ending thread.");
            }
        };

        Runnable observer = new RunnableWithField(sharedVar) {
            @Override
            public void run() {
                int oldValue = field.value;
                System.out.println("Started observer thread, shared variable value is: " + oldValue);

                while (field.value == oldValue) {
                    System.out.println("...observing...");
                    sleepFor(100);
                }
                System.out.println("!Observer: detected a change of shared variable: newValue: " + sharedStaticField + ", oldValue: " + oldValue);
                System.out.println("...ending observer thread");
            }
        };

        new Thread(observer).start();
        new Thread(changer).start();
    }

    private static void sleepFor(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

//For both threads we need a Runnable which receives an outside object in their constructor (to be used as the shared variable)
//So instead of defining 2 similar concrete classes extending runnable (with this common field but different run() methods)
//we just define an abstract class for the common field/constructor logic, and will implement this by 2 anon classes/lambdas later,
//for the different code in run()
abstract class RunnableWithField implements Runnable {
    protected final MutableInt field;

    RunnableWithField(MutableInt field) {
        this.field = field;
    }
}

class MutableInt {
    int value; //mutable public field (for easy read/write)

    MutableInt(int value) {
        this.value = value;
    }
}
