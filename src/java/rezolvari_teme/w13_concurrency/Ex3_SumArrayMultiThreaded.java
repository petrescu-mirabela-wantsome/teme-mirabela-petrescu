package rezolvari_teme.w13_concurrency;

import java.util.Arrays;

/**
 * Write a program that sums the values from an array of ints using 4 threads.
 * <p>
 * - Each thread gets to sum a quarter of the array and stores the result somehow
 * (in some kind of shared location?).
 * <p>
 * - The main thread needs to start and wait for the above threads to finish, after which it
 * collects the answers and displays the final result.
 * <p>
 * For this exercise, try to create the threads by implementing the Runnable interface.
 */
public class Ex3_SumArrayMultiThreaded {
    public static void main(String[] args) throws InterruptedException {

        //create and populate an input array
        int[] inputArray = new int[100];
        for (int i = 0; i < 100; i++) {
            inputArray[i] = i * i;
        }
        //also compute the expected sum (in main thread directly, not in parallel), to compare at the end
        int expectedSum = Arrays.stream(inputArray).sum();
        System.out.println("[MAIN] Array populated; expected sum at the end: " + expectedSum);

        //create a shared place where the threads should put their results
        int[] partialResultsArray = new int[4];

        //create the 4 threads, allocating for each: a slice of original array, and an index in results array
        Thread t1 = new Thread(new SumRunnable(inputArray, 0, 24, partialResultsArray, 0));
        Thread t2 = new Thread(new SumRunnable(inputArray, 25, 49, partialResultsArray, 1));
        Thread t3 = new Thread(new SumRunnable(inputArray, 50, 74, partialResultsArray, 2));
        Thread t4 = new Thread(new SumRunnable(inputArray, 75, 99, partialResultsArray, 3));

        System.out.println("\n[MAIN] Starting the 4 sum threads...");
        t1.start();
        t2.start();
        t3.start();
        t4.start();

        System.out.println("[MAIN] Waiting for all sum threads to finish...");
        //wait for all threads to end - by using .join()
        t1.join();
        t2.join();
        t3.join();
        t4.join();
        System.out.println("[MAIN] All sum threads finished, computing total sum now...\n");

        //now may safely compute total sum, based on array with partial results
        int totalSum = Arrays.stream(partialResultsArray).sum();
        System.out.println("[MAIN] Partial results: " + Arrays.toString(partialResultsArray) + " => computed total sum: " + totalSum);
        System.out.println("[MAIN] Parallel sum is same as expected one? : " + (expectedSum == totalSum));
    }
}

class SumRunnable implements Runnable {

    private final int[] inputArray;
    private final int beginIdx;
    private final int endIdx;

    private final int[] resultsArray;
    private final int resultIdx;

    SumRunnable(int[] inputArray, int beginIdx, int endIdx,
                int[] resultsArray, int resultIdx) {
        this.inputArray = inputArray;
        this.beginIdx = beginIdx;
        this.endIdx = endIdx;
        this.resultsArray = resultsArray;
        this.resultIdx = resultIdx;
    }

    @Override
    public void run() {
        print("Start computing sum for interval [" + beginIdx + ".." + endIdx + "]...");

        //compute the sum, iterating over the shared array of values, for the indexes allocated to this thread
        int sum = 0;
        for (int i = beginIdx; i <= endIdx; i++) {
            sum += inputArray[i];
        }
        print("Computed sum for interval [" + beginIdx + ".." + endIdx + "] = " + sum);

        //save result of thread computation to the shared results array
        resultsArray[resultIdx] = sum;
    }

    private void print(String text) {
        System.out.println("   [thread:" + Thread.currentThread().getName() + "] " + text);
    }
}