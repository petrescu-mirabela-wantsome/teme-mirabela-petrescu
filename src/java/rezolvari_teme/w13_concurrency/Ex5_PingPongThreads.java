package rezolvari_teme.w13_concurrency;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Read a number N from keyboard (between 1 and 10).
 * Then start 2 threads, T1 and T2, which will wait in a waiting state:
 * - main thread should 'start the match' by sending message "Start" to thread T1
 * - T1 sends "Ping 1" to T2, which responds with "Pong 1"
 * - on receiving 'Pong X', T1 sends 'Ping X+1' and so on...
 * - the match 'ends' when message "Pong N" was sent -> in this case both threads should end, and also the main one.
 * Other rules:
 * - threads should sleep for a random amount of time (1000-2000ms) before sending a message back
 * - threads should write messages to console when receiving and when sending any messages (including thread role + id for clarity)
 * - threads must not get out of sync (each thread must wait for the right message before responding)
 * <p>
 * Questions:
 * - In what format should you send the message? (think about how you'll need consider both the type Ping/Pong and the number of message)
 * (hint: your message doesn't need to be a single/primitive value..)
 * - Can/should your message be a mutable value?
 * - How should you send it? Multiple ways are possible, what is the easiest/better fit here? (shared variable, atomic object, a blocking pingerQueue?..)
 * Do you need more than 1 channel / shared resource to send the messages? (how do you send to different destinations?)
 */
public class Ex5_PingPongThreads {

    private static ArrayBlockingQueue<Message> pingerQueue = new ArrayBlockingQueue<>(1);
    private static ArrayBlockingQueue<Message> pongerQueue = new ArrayBlockingQueue<>(1);

    private static int MAX_MSG = 0;
    private static Random random = new Random();

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter number: ");
        MAX_MSG = scanner.nextInt();

        System.out.println("Starting match with " + MAX_MSG + " steps...");

        Thread pinger = new Thread(() -> {
            while (true) {
                Message msg = receive(pingerQueue);//wait and get message (blocking)
                print("  ...received: " + msg);

                randomSleep();

                if (msg.type.equalsIgnoreCase("Start")) {
                    send(pongerQueue, new Message("Ping", 1));
                } else if (msg.type.equalsIgnoreCase("Pong")) {
                    if (msg.count < MAX_MSG) {
                        send(pongerQueue, new Message("Ping", msg.count + 1));
                    } else {
                        send(pongerQueue, new Message("GAME-OVER!", 0));
                        break; //game over
                    }
                }
            }
            print("... I'm done!");
        }, "pinger");

        Thread ponger = new Thread(() -> {
            while (true) {
                Message msg = receive(pongerQueue); //wait and get message (blocking)
                print("  ...received: " + msg);

                if (msg.type.equalsIgnoreCase("Ping") && msg.count <= MAX_MSG) {
                    send(pingerQueue, new Message("Pong", msg.count));
                } else {
                    break; //game over
                }
            }
            print("... I'm done!");
        }, "    ponger");

        print("Starting threads...");
        pinger.start();
        ponger.start();

        print("Giving 'Start' signal...");
        send(pingerQueue, new Message("Start", 0));
    }

    private static Message receive(ArrayBlockingQueue<Message> queue) {
        try {
            return queue.take(); //waiting for next message
        } catch (InterruptedException e) {
            e.printStackTrace();
            return null;
        }
    }

    private static void send(ArrayBlockingQueue<Message> queue, Message msg) {
        print("Sending: " + msg + "...");
        try {
            Thread.sleep(10); //wait a little for print to reach console!
            queue.put(msg);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void print(String text) {
        System.out.println("[" + Thread.currentThread().getName() + "-" + Thread.currentThread().getId() + "] " + text);
    }

    private static void randomSleep() {
        try {
            Thread.sleep(1000 + random.nextInt(1000));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    static class Message {
        final String type;
        final int count;

        public Message(String type, int count) {
            this.type = type;
            this.count = count;
        }

        @Override
        public String toString() {
            return '{' + type + " " + count + '}';
        }
    }
}