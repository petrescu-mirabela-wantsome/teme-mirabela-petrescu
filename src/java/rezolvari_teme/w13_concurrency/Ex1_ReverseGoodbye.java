package rezolvari_teme.w13_concurrency;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Write a program that creates a thread (let's call it Thread 1). Thread 1 creates another thread (Thread 2);
 * Thread 2 creates Thread 3, and so on, up to Thread 10.
 * <p>
 * Each thread should print "Hello from thread <num>" and then “Goodbye from thread <num>”,
 * but you should structure your program such that the threads print their greetings in this nested order:
 * Hello from thread 1
 * Hello from thread 2
 * ...
 * Hello from thread 10
 * Goodbye from thread 10
 * ...
 * Goodbye from thread 1
 * <p>
 * Note that the requirement is to actually create/run this code on multiple parallel threads! (not a single main one)
 * <p>
 * Also, you should not use Thread.sleep() in your threads (would be too easy a solution :) and
 * also the program would run slower than necessary...)
 * Hint​ : read about .join() method of Thread class...
 * <p>
 * For this exercise, try to create the threads by extending the Thread class.
 */
public class Ex1_ReverseGoodbye {

    public static void main(String[] args) throws InterruptedException {
        System.out.println("\n=== Solution with child.join() ===");
        Thread t1 = new HelloThread1(1);
        t1.start();
        t1.join(); //just to wait for this to end before running another test

        System.out.println("\n=== Solution with atomic counter ===");
        new HelloThread2(1).start();
    }
}

/**
 * Solution 1: parent thread uses .join() on child thread to wait for it to finish
 */
class HelloThread1 extends Thread {
    private final int number;

    HelloThread1(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        System.out.println("Hello from thread " + number + " (th.id: " + Thread.currentThread().getId() + ")");

        //start next thread if needed
        if (number < 10) {
            Thread child = new HelloThread1(number + 1);
            child.start();

            //and also wait for it to complete
            try {
                child.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Goodbye from thread " + number + " (th.id: " + Thread.currentThread().getId() + ")");
    }
}

/**
 * Solution 2: use a static AtomicInteger value which determines which thread will run next
 * (and each thread waits to be its turn before running goodbye part, and then decrement the counter)
 */
class HelloThread2 extends Thread {
    private static AtomicInteger nextToRun = new AtomicInteger(0);
    private final int number;

    HelloThread2(int number) {
        this.number = number;
    }

    @Override
    public void run() {
        System.out.println("Hello from thread " + number + " (th.id: " + Thread.currentThread().getId() + ")");

        //start next thread if needed
        if (number < 10) {
            nextToRun.set(number + 1);
            new HelloThread2(number + 1).start();
        }

        //wait my turn to react (for nextUp to decrease to this thread's number value)
        while (!nextToRun.compareAndSet(number, number - 1)) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Goodbye from thread " + number + " (th.id: " + Thread.currentThread().getId() + ")");
    }
}