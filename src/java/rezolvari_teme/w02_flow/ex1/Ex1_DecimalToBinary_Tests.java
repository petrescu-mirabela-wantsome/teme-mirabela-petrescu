package rezolvari_teme.w02_flow.ex1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static rezolvari_teme.w02_flow.ex1.Ex1_DecimalToBinary.decimalToBinary;

public class Ex1_DecimalToBinary_Tests {

    @Test
    public void testBinaryConversion() {
        assertEquals("0", decimalToBinary(0));
        assertEquals("1", decimalToBinary(1));
        assertEquals("10", decimalToBinary(2));
        assertEquals("11", decimalToBinary(3));
        assertEquals("100", decimalToBinary(4));
        assertEquals("1000", decimalToBinary(8));
        assertEquals("1010", decimalToBinary(10));
        assertEquals("1111111", decimalToBinary(127));
        assertEquals("101100101", decimalToBinary(357));
        assertEquals("1111111111111111111111111111111", decimalToBinary(Integer.MAX_VALUE));
    }
}
