package rezolvari_teme.w02_flow.ex1;

/**
 * Write a function that receives a positive integer value and returns its base 2 representation as a string.
 * E.g. decimalToBinary(3) -> "11", decimalToBinary(10) -> “1010”, decimalToBinary(127) = “1111111
 * <p>
 * More info: https://en.wikipedia.org/wiki/Binary_number#Conversion_to_and_from_other_numeral_systems
 */
public class Ex1_DecimalToBinary {

    static String decimalToBinary(int n) {

        String binNumber = "";

        do {
            int reminder = n % 2; //compute a new binary digit, as the reminder of dividing number by 2 (-> an int value, either 0 or 1)
            n = n / 2; //reduce size of original number (divide it by 2)

            binNumber = reminder + binNumber; //append reminder to the front (left side) of binary number (String)

        } while (n > 0); //until decimal number gets to 0

        return binNumber;
    }

    /**
     * Just for manual testing
     */
    public static void main(String[] args) {
        System.out.println(decimalToBinary(0));
        System.out.println(decimalToBinary(1));
        System.out.println(decimalToBinary(2));
        System.out.println(decimalToBinary(3));
        System.out.println(decimalToBinary(4));
        System.out.println(decimalToBinary(10));
    }
}
