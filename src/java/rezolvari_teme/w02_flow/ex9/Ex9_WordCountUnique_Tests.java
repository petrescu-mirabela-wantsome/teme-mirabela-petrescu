package rezolvari_teme.w02_flow.ex9;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static rezolvari_teme.w02_flow.ex9.Ex9_WordCountUnique.countUniqueWords;

public class Ex9_WordCountUnique_Tests {

    @Test
    public void testEmptyCase() {
        assertEquals(0, countUniqueWords(""));
    }

    @Test
    public void testSimpleCases() {
        assertEquals(1, countUniqueWords("abc"));
        assertEquals(2, countUniqueWords("aa bb"));
        assertEquals(2, countUniqueWords("aa bb aa"));
    }

    @Test
    public void testTrickyCases() {
        assertEquals(0, countUniqueWords("   "));
        assertEquals(1, countUniqueWords("  abc "));
        assertEquals(3, countUniqueWords("  aa   bb cc  AA  aA "));
    }
}
