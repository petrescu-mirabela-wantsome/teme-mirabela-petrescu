package rezolvari_teme.w02_flow.ex9;

/**
 * Given a phrase of words separated by space, count how many UNIQUE words it contains.
 * Example: "aa bb cc aa bb" => 3 unique words
 * Hint: you will need a way to compare each word with the list of unique words detected up until that moment,
 * in order to decide if to increase unique words counter for this word or not..
 */
public class Ex9_WordCountUnique {

    static int countUniqueWords(String phrase) {

        if (phrase.trim().isEmpty()) {
            return 0;
        }

        String[] words = phrase.trim().split("\\s+");

        String[] uniqueWords = new String[words.length];

        int count = 0;
        for (String w : words) {
            if (!alreadyFound(w, uniqueWords, count)) {
                uniqueWords[count] = w;
                count++;
            }
        }
        return count;
    }

    private static boolean alreadyFound(String wordToCheck, String[] words, int maxIdx) {
        for (int i = 0; i < maxIdx; i++) {
            if (words[i].equalsIgnoreCase(wordToCheck)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        System.out.println(countUniqueWords(" aa  bb AA "));

        System.out.println(countUniqueWords(""));
        System.out.println(countUniqueWords("abc"));
        System.out.println(countUniqueWords("aa bb"));
        System.out.println(countUniqueWords(" aa  bb aa "));
        System.out.println(countUniqueWords(" aa  bb AA "));
    }
}
