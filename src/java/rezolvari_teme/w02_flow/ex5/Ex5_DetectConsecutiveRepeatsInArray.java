package rezolvari_teme.w02_flow.ex5;

/**
 * Write a function that takes an array of numbers and returns just the numbers
 * that are repeating on consecutive positions in the array,
 * as a single String value (containing the numbers separated by space).
 * E.g:
 * [1, 4, 3, 4] -> “”
 * [1, 4, 4, 4, 3, 5, 5, 4, 4] -> “4 5 4”
 * [1, 4, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9] -> “4 2 9”
 * [1, 1, 2, 1, 1] -> "1 1" (ok to repeat value 1, as it comes from 2 different sequences)
 */
public class Ex5_DetectConsecutiveRepeatsInArray {

    static String onlyConsecutiveRepeating2(int... numbers) {
        String out = "";
        int i = 0;
        while (i < numbers.length) {
            int j = i; //use j index to store the start position of a new repeating sequence
            i++;       //increment i
            if (i < numbers.length && numbers[i] == numbers[j]) { //if i didn't reach array's end AND has same number as on j position
                out += numbers[i] + " ";                            //then we have a repeating sequence, so add the value at i to list of repeating values
            }
            while (i < numbers.length && numbers[i] == numbers[j]) { //keep increasing i to also skip over the rest of repetitions of same value (as on j pos)
                i++;
            }
        }
        return out;
    }


    /**
     * Alternative solution by Tudor Strachinescu, maybe clearer/easier to understand
     */
    static String onlyConsecutiveRepeating(int... numbers) {
        String out = "";
        boolean checked = false;
        for (int i = 1; i < numbers.length; i++) {
            if ((numbers[i] == numbers[i - 1]) && !checked) {
                out += numbers[i] + " ";
                checked = true;
            }
            if (numbers[i] != numbers[i - 1]) {
                checked = false;
            }
        }
        return out;
    }

    /*
    //A naive solution - doesn't work ok on all cases
    static String onlyConsecutiveRepeating1(int... numbers) {
        String s = "";
        for (int i = 1; i < numbers.length; i++) {
            if (numbers[i] == numbers[i - 1]) s += numbers[i] + " ";
        }
        return s;
    }
    */

    public static void main(String[] args) {
        System.out.println(onlyConsecutiveRepeating());
        System.out.println(onlyConsecutiveRepeating(1, 1));
        System.out.println(onlyConsecutiveRepeating(1, 1, 1));
        System.out.println(onlyConsecutiveRepeating(1, 4, 3, 4));
        System.out.println(onlyConsecutiveRepeating(1, 4, 4, 4, 3, 5, 5, 4, 4));
        System.out.println(onlyConsecutiveRepeating(1, 4, 4, 4, 4, 4, 5, 7, 8, 7, 2, 2, 9, 9, 9));
    }
}
