package rezolvari_teme.w02_flow.ex2;

/**
 * Write a function that returns a string representation of an array of doubles.
 * Format the numbers with two decimals. (hint: look up info on using String.format() for float numbers)
 * E.g  [1.1, 2.345, 3.0] -> “[1.10, 2.34, 3.00]”
 */
public class Ex2_ArrayToString {

    static String arrayToString(double[] array) {

        String s = "";
        for (double d : array) {
            //first add a separator (,), but only if needed (not the first time, when s is still empty)
            if (!s.isEmpty()) {
                s += ", ";
            }

            //then add formatted number to s
            s += String.format("%.2f", d);
        }

        return "[" + s + "]"; //add end delimiters ([]) and return the final value
    }

    public static void main(String[] args) {
        System.out.println(arrayToString(new double[]{}));
        System.out.println(arrayToString(new double[]{1.1, 2.345, 3.0}));
    }
}
