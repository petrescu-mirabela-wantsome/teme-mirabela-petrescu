package rezolvari_teme.w02_flow.ex13;

/**
 * Write a function that receives two sorted arrays and returns a new sorted array with elements from both arrays.
 * (note: this is called the merge algorithm)
 */
public class Ex13_MergeArrays {

    static int[] merge(int[] arr1, int[] arr2) {

        int[] out = new int[arr1.length + arr2.length];

        int i = 0; //next elem from arr1
        int j = 0; //next elem from arr2
        int k = 0; //next free pos in out array

        while (i < arr1.length || j < arr2.length) { //while we have elements left in one of the arrays
            //take from 1st array if:
            boolean takeFromArr1 =
                    (j >= arr2.length) ||                            //(array2 ended)
                            (i < arr1.length && arr1[i] < arr2[j]);  //OR  (array1,2 both not ended AND next elem form 1st is less than from 2nd)

            out[k++] = takeFromArr1 ? arr1[i++] : arr2[j++];
        }

        return out;
    }
}
