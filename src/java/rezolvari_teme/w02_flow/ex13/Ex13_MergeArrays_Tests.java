package rezolvari_teme.w02_flow.ex13;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static rezolvari_teme.w02_flow.ex13.Ex13_MergeArrays.merge;

public class Ex13_MergeArrays_Tests {

    @Test
    public void testMerge() {
        assertArrayEquals(
                new int[]{},
                merge(new int[]{}, new int[]{}));

        assertArrayEquals(
                new int[]{1},
                merge(new int[]{1}, new int[]{}));

        assertArrayEquals(
                new int[]{1},
                merge(new int[]{}, new int[]{1}));

        assertArrayEquals(
                new int[]{1, 2, 3},
                merge(new int[]{1, 2, 3}, new int[]{}));

        assertArrayEquals(
                new int[]{1, 2},
                merge(new int[]{}, new int[]{1, 2}));

        assertArrayEquals(
                new int[]{1, 2, 3, 4, 5},
                merge(new int[]{5}, new int[]{1, 2, 3, 4}));

        assertArrayEquals(
                new int[]{1, 2, 3, 4, 5},
                merge(new int[]{1}, new int[]{2, 3, 4, 5}));

        assertArrayEquals(
                new int[]{1, 1, 2, 2, 3, 3},
                merge(new int[]{1, 2, 3}, new int[]{1, 2, 3}));
    }
}
