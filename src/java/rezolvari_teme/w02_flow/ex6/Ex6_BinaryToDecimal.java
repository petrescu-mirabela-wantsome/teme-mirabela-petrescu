package rezolvari_teme.w02_flow.ex6;

public class Ex6_BinaryToDecimal {

    static int binaryToDecimal(String binaryNumber) {

        int n = 0;
        for (char b : binaryNumber.toCharArray()) {
            int valueOfDigit = (b - '0'); //converts '0'->0 .. '9'->9
            n = n * 2 + valueOfDigit;
        }
        return n;
    }

    //for manual testing
    public static void main(String[] args) {
        System.out.println(binaryToDecimal("0"));
        System.out.println(binaryToDecimal("11"));
        System.out.println(binaryToDecimal("101"));
        System.out.println(binaryToDecimal("1010"));
    }
}
