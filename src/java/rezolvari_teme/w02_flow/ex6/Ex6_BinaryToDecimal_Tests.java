package rezolvari_teme.w02_flow.ex6;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static rezolvari_teme.w02_flow.ex6.Ex6_BinaryToDecimal.binaryToDecimal;

public class Ex6_BinaryToDecimal_Tests {

    @Test
    public void testBinaryToDecimal() {
        assertEquals(0, binaryToDecimal("0"));
        assertEquals(1, binaryToDecimal("1"));
        assertEquals(2, binaryToDecimal("10"));
        assertEquals(3, binaryToDecimal("11"));
        assertEquals(4, binaryToDecimal("100"));
        assertEquals(8, binaryToDecimal("1000"));
        assertEquals(10, binaryToDecimal("1010"));
        assertEquals(127, binaryToDecimal("1111111"));
        assertEquals(357, binaryToDecimal("101100101"));
        assertEquals(Integer.MAX_VALUE, binaryToDecimal("1111111111111111111111111111111"));
    }
}
