package rezolvari_teme.w02_flow.ex7;

/**
 * Starting with the solution for Exercise #1, write a more general function that receives
 * a number and a base (between 2..36) and returns a string with the number’s representation in that base.
 * E.g:
 * decimalToBase(10, 2)  -> “1010”
 * decimalToBase(10, 10) -> “10”
 * decimalToBase(10, 8)  -> “12”
 * <p>
 * Note: To support bases higher than 10 you can use letters instead of digits, e.g. for base 16: A = 10, B = 11, ..., F = 15...
 * For example:
 * decimalToBase(47, 16) -> “2F”
 * decimalToBase(1295, 36) -> “ZZ”
 */
public class Ex7_DecimalToBase {

    static String decimalToBase(int number, int base) {
        String res = "";
        do {
            res = digitAsString(number % base) + res;
            number /= base;
        } while (number > 0);
        return res;
    }

    /**
     * Helper function, converts a number which should representing a single digit in some base
     * to a single char String representation, like:
     * 1 -> '1'
     * 9 -> '9'
     * 10 -> 'A'
     * 15 -> 'F'
     * ...
     */
    static String digitAsString(int digit) {
        return digit < 10 ?
                String.valueOf(digit) :
                String.valueOf((char) ('A' + (digit - 10)));
    }

    //for manual testing
    public static void main(String[] args) {

        System.out.println(decimalToBase(9, 10));
        System.out.println(decimalToBase(10, 10));

        System.out.println(decimalToBase(0, 2));
        System.out.println(decimalToBase(1, 2));
        System.out.println(decimalToBase(2, 2));
        System.out.println(decimalToBase(10, 2));
        System.out.println(decimalToBase(127, 2));

        System.out.println(decimalToBase(0, 16));
        System.out.println(decimalToBase(15, 16));
        System.out.println(decimalToBase(16, 16));
        System.out.println(decimalToBase(127, 16));
        System.out.println(decimalToBase(255, 16));

        System.out.println(decimalToBase(35, 36));
        System.out.println(decimalToBase(1295, 36));
    }
}
