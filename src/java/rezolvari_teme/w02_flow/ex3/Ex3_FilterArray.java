package rezolvari_teme.w02_flow.ex3;

import java.util.Arrays;

/**
 * Write a function that takes an array of numbers and returns a new array with only the odd numbers.
 * (hint: test it for a few cases, using Arrays.toString() to display the content of the returned array)
 */
public class Ex3_FilterArray {

    static int[] onlyOdd(int[] array) {
        //1) copy only odd values ones to a temp array
        int[] tmp = new int[array.length]; //must declare size of temp array, make it for now as big as original array

        int i = 0; //the position in new array where to add new elements
        for (int d : array) { //go over each value of original array
            if (d % 2 == 1) { //if value is odd
                tmp[i] = d;   //copy it to tmp array (on current free pos i)
                i++;          //increment i to point to next free pos
            }
        }

        //2) temp array has the right values, but it may be too large (has extra 0 values at the end)
        //   so copy only the useful part of temp array to new smaller array to return

        /*
        //a) Manual copy (you should know how to do this too):
        int[] res = new int[i];       //create a new smaller array, as we know the right size for it now (value of i after finishing previous for loop)
        for (int j = 0; j < i; j++) { //copy elements form pos 0..(i-1) from tmp to new smaller res array, using a new index j
            res[j] = tmp[j];
        }
        return res;
        */

        //b) OR: can do the partial copy of an array contents using Arrays.copyOf:
        return Arrays.copyOf(tmp, i);
    }

    public static void main(String[] args) {
        System.out.println(
                Arrays.toString(
                        onlyOdd(new int[]{1, 2, 3, 4, 5})));
    }
}
