package rezolvari_teme.w02_flow.ex12;

import java.util.Arrays;

/**
 * Write a function that receives an array and returns a new array sorted by insertion sort.
 * More info: https://en.wikipedia.org/wiki/Insertion_sort
 */
public class Ex12_InsertionSort {

    /**
     * Receives an array and sorts it in place, using Insertion sort algorithm
     */
    static void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            insert(array[i], array, i - 1);
        }
    }

    /**
     * Inserts given element elem to its proper position in given array:
     * - looks over elements only in the range [0..lastPos]
     * - finding the right position (i) between them, for the new element
     * - shifts all from [i..lastPos] one position up
     * - on remaining i position saves the new element
     */
    private static void insert(int elem, int[] array, int lastPos) {
        int i = lastPos;                     //start from end of allowed range
        while (i >= 0 && array[i] > elem) {  //and go back as long as needed, or until reached start of array
            array[i + 1] = array[i];         //move existing item to the right, to make room on left for smaller 'elem' item
            i--;
        }
        array[i + 1] = elem; //current value i (at end of previous loop) is 1 position lower than the right position to insert elem
    }

    //for manual tests
    public static void main(String[] args) {
        sortAndPrint(new int[]{});
        sortAndPrint(new int[]{1});
        sortAndPrint(new int[]{1, 2, 3});
        sortAndPrint(new int[]{1, 3, 2});
        sortAndPrint(new int[]{5, 2, 3, 1, 4});
        sortAndPrint(new int[]{5, 4, 3, 2, 1, 0});
    }

    //helper function for main
    private static void sortAndPrint(int[] arr) {
        sort(arr);
        System.out.println(Arrays.toString(arr));
    }
}
