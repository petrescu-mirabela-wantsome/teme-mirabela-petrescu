package rezolvari_teme.w02_flow.ex10;

/**
 * Given a phrase of words separated by space, write a method which returns the list of unique words
 * (case-insensitive) and for each one number of times it appears.
 * The returned value should be a string, with this format: “<word1>=<count1> <word2>=<count2> ...”
 * Example: "Aa bb cc aa dd BB dd" => "aa=3 bb=2 cc=1 dd=2”
 * <p>
 * Hint: you'll need to keep somehow a list of pairs of 2 separate values {word + counter};
 * for this, you may consider using 2 arrays updated/iterated in parallel - 1st one for the words,
 * and 2nd one for the counters (so a word is stored at some index i in 1st array,
 * and its counter is stored at same index i but in 2nd array)
 */
public class Ex10_WordsWithCount {

    static String countWords(String phrase) {

        if (phrase.trim().isEmpty()) {
            return "";
        }

        String[] words = phrase.trim().split("\\s+");

        String[] uniqueWords = new String[words.length];
        int[] counts = new int[words.length];

        int uniqueCount = 0;
        for (String w : words) {
            int i = indexOf(w, uniqueWords, uniqueCount);
            if (i == -1) { //first time encountered
                uniqueWords[uniqueCount] = w;
                counts[uniqueCount] = 1;
                uniqueCount++;
            } else {
                counts[i] += 1;
            }
        }

        String res = "";
        for (int i = 0; i < uniqueCount; i++) {
            res += uniqueWords[i] + "=" + counts[i] + " ";
        }
        return res.trim();
    }

    private static int indexOf(String wordToCheck, String[] words, int maxIdx) {
        for (int i = 0; i < maxIdx; i++) {
            if (words[i].equalsIgnoreCase(wordToCheck)) {
                return i;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        System.out.println(countWords(""));
        System.out.println(countWords("   "));
        System.out.println(countWords("aa bb aa"));
        System.out.println(countWords("  Aa  bb  aa cc BB  "));
    }
}
