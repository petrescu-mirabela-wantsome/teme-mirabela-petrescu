package rezolvari_teme.w02_flow.ex10;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static rezolvari_teme.w02_flow.ex10.Ex10_WordsWithCount.countWords;

public class Ex10_WordsWithCount_Tests {

    @Test
    public void testEmptyCase() {
        assertEquals("", countWords(""));
    }

    @Test
    public void testSimpleCases() {
        assertEquals("abc=1", countWords("abc"));
        assertEquals("aa=1 bb=1", countWords("aa bb"));
        assertEquals("aa=2 bb=1", countWords("aa bb aa"));
    }

    @Test
    public void testTrickyCases() {
        assertEquals("", countWords("   "));
        assertEquals("abc=1", countWords("  abc "));
        assertEquals("aa=3 bb=1 cc=1", countWords("  aa   bb cc  AA  aA "));
    }
}
