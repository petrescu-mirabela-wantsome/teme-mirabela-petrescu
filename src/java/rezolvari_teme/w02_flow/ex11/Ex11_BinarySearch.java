package rezolvari_teme.w02_flow.ex11;

/**
 * Write a function that receives a sorted array and a number and performs a binary search on the array
 * to check whether the element is contained therein or not.
 * More info: https://en.wikipedia.org/wiki/Binary_search_algorithm
 * <p>
 * Bonus: write two versions of the function: one iterative and one utilizing recursion
 */
public class Ex11_BinarySearch {

    /**
     * Iterative version.
     * Searches for value x in whole given array, returns true if found
     */
    static boolean contains(int x, int[] array) {
        int b = 0;
        int e = array.length - 1;
        while (b <= e) {
            int mid = (b + e) / 2;
            if (x == array[mid]) {
                return true;
            }
            if (x < array[mid]) {
                e = mid - 1;
            } else {
                b = mid + 1;
            }
        }
        return false;
    }

    /**
     * Recursive version.
     * Searches for value x in given array, but only in specified range (between beginIdx and endIdx, inclusive)
     * May call itself as needed (but with other params - different begin/end idx values)
     */
    static boolean containsRec(int x, int[] array, int beginIdx, int endIdx) {
        if (beginIdx > endIdx) { //handle stop condition first (interval invalid/too small, so not found)
            return false;
        }

        //choose a middle point for current index range
        int mid = (beginIdx + endIdx) / 2;

        if (array[mid] == x) { //handle stop condition - happy case (found elem x on middle position)
            return true;
        }

        //recursive handling of rest of cases (search in one of the 2 remaining halves)
        return x < array[mid] ?
                containsRec(x, array, beginIdx, mid - 1) :
                containsRec(x, array, mid + 1, endIdx);
    }

    /**
     * Just a helper function for the recursive version, for easier calling by clients/tests
     * (without needing to specify begin/end idx values)
     */
    static boolean containsRecStart(int x, int[] array) {
        return containsRec(x, array, 0, array.length - 1);
    }


    public static void main(String[] args) {
        System.out.println("\nITERATIVE version:");
        System.out.println(contains(2, new int[]{2, 3, 4}));
        System.out.println(contains(3, new int[]{2, 3, 4}));
        System.out.println(contains(4, new int[]{2, 3, 4}));
        System.out.println(contains(3, new int[]{1, 2, 3, 4, 5}));

        System.out.println(contains(1, new int[]{}));
        System.out.println(contains(1, new int[]{2}));
        System.out.println(contains(1, new int[]{2, 3}));
        System.out.println(contains(7, new int[]{1, 2, 3, 4, 5}));
        System.out.println(contains(2, new int[]{1, 3, 5}));

        System.out.println("\nRECURSIVE version:");
        System.out.println(containsRecStart(2, new int[]{2, 3, 4}));
        System.out.println(containsRecStart(3, new int[]{2, 3, 4}));
        System.out.println(containsRecStart(4, new int[]{2, 3, 4}));
        System.out.println(containsRecStart(3, new int[]{1, 2, 3, 4, 5}));

        System.out.println(containsRecStart(1, new int[]{}));
        System.out.println(containsRecStart(1, new int[]{2}));
        System.out.println(containsRecStart(1, new int[]{2, 3}));
        System.out.println(containsRecStart(7, new int[]{1, 2, 3, 4, 5}));
        System.out.println(containsRecStart(2, new int[]{1, 3, 5}));
    }
}
