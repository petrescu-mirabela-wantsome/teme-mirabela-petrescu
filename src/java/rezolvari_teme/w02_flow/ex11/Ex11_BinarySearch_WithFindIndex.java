package rezolvari_teme.w02_flow.ex11;

/**
 * Binary search for a value, but also return the position of found element, not just true/false!
 */
public class Ex11_BinarySearch_WithFindIndex {

    public static void main(String[] args) {
        int[] arr = {1, 3, 4, 7, 8, 9, 12, 15};

        System.out.println(findPos(13, arr));
    }

    static int findPos(int x, int[] arr) {
        return findPos(x, arr, 0, arr.length - 1);
    }

    static int findPos(int x, int[] arr, int start, int end) {
        if (end < start)
            return -1;

        int mid = (start + end) / 2;
        int midElem = arr[mid];
        if (x < midElem)
            return findPos(x, arr, start, mid - 1);
        else if (x > midElem)
            return findPos(x, arr, mid + 1, end);
        else
            return mid;
    }
}
