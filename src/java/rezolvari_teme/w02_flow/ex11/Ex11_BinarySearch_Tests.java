package rezolvari_teme.w02_flow.ex11;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static rezolvari_teme.w02_flow.ex11.Ex11_BinarySearch.contains;
import static rezolvari_teme.w02_flow.ex11.Ex11_BinarySearch.containsRecStart;

public class Ex11_BinarySearch_Tests {

    @Test
    public void testFound() {
        assertTrue(contains(2, new int[]{2}));
        assertTrue(contains(2, new int[]{2, 3}));
        assertTrue(contains(3, new int[]{2, 3, 4}));
        assertTrue(contains(4, new int[]{2, 3, 4}));
        assertTrue(contains(4, new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void testNotFound() {
        assertFalse(contains(1, new int[]{}));
        assertFalse(contains(1, new int[]{2}));
        assertFalse(contains(1, new int[]{2, 3}));
        assertFalse(contains(2, new int[]{1, 3}));
        assertFalse(contains(0, new int[]{1, 3, 5}));
        assertFalse(contains(7, new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void testRecFound() {
        assertTrue(containsRecStart(2, new int[]{2}));
        assertTrue(containsRecStart(2, new int[]{2, 3}));
        assertTrue(containsRecStart(3, new int[]{2, 3, 4}));
        assertTrue(containsRecStart(4, new int[]{2, 3, 4}));
        assertTrue(containsRecStart(4, new int[]{1, 2, 3, 4, 5}));
    }

    @Test
    public void testRecNotFound() {
        assertFalse(containsRecStart(1, new int[]{}));
        assertFalse(containsRecStart(1, new int[]{2}));
        assertFalse(containsRecStart(1, new int[]{2, 3}));
        assertFalse(containsRecStart(2, new int[]{1, 3}));
        assertFalse(containsRecStart(0, new int[]{1, 3, 5}));
        assertFalse(containsRecStart(7, new int[]{1, 2, 3, 4, 5}));
    }
}
