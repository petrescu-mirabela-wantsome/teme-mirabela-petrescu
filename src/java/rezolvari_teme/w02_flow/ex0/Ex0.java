package rezolvari_teme.w02_flow.ex0;

import java.util.Arrays;

/**
 * a) Write a method which, given a string and a non-negative int n, returns a larger string that is n copies of the original string.
 * Example: xTimes(“Abc”, 2) => “AbcAbc”
 * <p>
 * b) Given a number non-negative int n, return the sum of the squares of all numbers between 1 and n.
 * Example: sumSquares(4) = 1*1+2*2+3*3+4*4 = 30
 * <p>
 * c) Given a positive number n (type: int), compute and return factorial of n (defined as: n! = 1*..*n). It should work well at least up to n=16
 * Try first to write an iterative version for it, then an recursive one (considering that: n! = n*(n-1)! and 1!=1)
 * <p>
 * d) Given the (romanian) name of a day of the week, return a number showing its position in the week, or -1 if name is invalid.
 * Try to use the switch block. Example: 'luni'->1 .. 'Duminica'->7, 'invalid'-> -1
 * (hint: check out also these useful methods of String class: toLowerCase, toUpperCase)
 * <p>
 * e) Given a string, count how many vowels (a,e,i,o,u) it contains. Try to solve it first without using arrays, then with arrays.
 * Hint: check String class for useful methods you may need here (like: toLowerCase(), length(), charAt(), toCharArray()..)
 * <p>
 * f) Given an array of numbers (of type double), compute their sum and then the average value (hint: when computing average, you may reuse the sum() method)
 * <p>
 * g) Given an array of numbers (of type double), find and return the max value (or special value Double.NEGATIVE_INFINITY if cannot find a max value - when can this happen?)
 * <p>
 * h) Given an array of numbers, compute the sum of all numbers until first negative one is found (excluding it)
 * <p>
 * i) Given an array of numbers, create and return another array which has same numbers but in reversed order (note:original array should remain unchanged)
 * <p>
 * j) Given an array of numbers, and some other separate number x, update the array so that each initial number is
 * replaced by its value multiplied by the fixed x value
 * (note: the method should return void, but affect the initial array directly - doing an ‘in-place’ update)
 */
public class Ex0 {

    static String xTimes(String s, int n) {
        String res = "";
        for (int i = 1; i <= n; i++) {
            res += s;
        }
        return res;
    }

    static int sumSquares(int n) {
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += i * i;
        }
        return sum;
    }

    static long factorial(int n) {
        long res = 1;
        for (int i = 1; i <= n; i++) {
            res *= i;
        }
        return res;
    }

    static long factorialRec(int n) {
        return n == 1 ? 1 : n * factorialRec(n - 1);
    }

    static byte dayOfWeek(String s) {
        switch (s.toLowerCase()) {
            case "luni":
                return 1;
            case "marti":
                return 2;
            case "miercuri":
                return 3;
            case "joi":
                return 4;
            case "vineri":
                return 5;
            case "sambata":
                return 6;
            case "duminica":
                return 7;
            default:
                return -1;
        }
    }

    static int countVowels(String s) {
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            char c = s.toLowerCase().charAt(i);
            if (isVowel(c)) {
                count++;
            }
        }
        return count;
    }

    private static boolean isVowel(char c) {
        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }

    static int countVowelsWithArray(String s) {
        int count = 0;
        for (char c : s.toLowerCase().toCharArray()) {
            if (isVowel(c)) {
                count++;
            }
        }
        return count;
    }

    static double sum(double[] arr) {
        double sum = 0;
        for (double d : arr) {
            sum += d;
        }
        return sum;
    }

    static double avg(double[] arr) {
        return sum(arr) / arr.length;
    }

    static double max(double[] arr) {
        double max = Double.NEGATIVE_INFINITY;
        for (double d : arr) {
            if (d > max) {
                max = d;
            }
        }
        return max;
    }

    static double sumPositives(double[] arr) {
        /*
        //1) with a while loop:
        double sum = 0;
        int i = 0;
        while (i < arr.length && arr[i] > 0) {
            sum += arr[i];
            i++;
        }
        return sum;
        */

        /*
        //2) with a simplified for loop + separate 'break' to stop when needed:
        double sum = 0;
        for (double d : arr) {
            if (d < 0) {
                break;
            }
            sum += d;
        }
        return sum;
        */

        //3) with regular for loop (shortest):
        double sum = 0;
        for (int i = 0; i < arr.length && arr[i] > 0; i++) {
            sum += arr[i];
        }
        return sum;
    }

    static double[] reversed(double[] arr) {
        double[] rev = new double[arr.length];
        for (int i = 0; i < arr.length; i++) {
            rev[arr.length - 1 - i] = arr[i];
        }
        return rev;
    }

    static void multiply(double[] arr, double x) {
        for (int i = 0; i < arr.length; i++) {
            arr[i] *= x;
        }
    }

    public static void main(String[] args) {
        System.out.println("xTimes('abc',2)= '" + xTimes("abc", 2) + "'");
        System.out.println("sumSquares(4)= " + sumSquares(4));

        System.out.println("\ndayOfWeek('luni') = " + dayOfWeek("luni"));
        System.out.println("dayOfWeek('DUMINICA') = " + dayOfWeek("DUMINICA"));
        System.out.println("dayOfWeek('invalid') = " + dayOfWeek("invalid"));

        System.out.println("\nfactorial(1) = " + factorial(1));
        System.out.println("factorial(5) = " + factorial(5));
        System.out.println("factorial(13) = " + factorial(13));
        System.out.println("factorialRec(1) = " + factorialRec(1));
        System.out.println("factorialRec(5) = " + factorialRec(5));
        System.out.println("factorialRec(13) = " + factorialRec(13));

        System.out.println("\ncountVowels('Run, Forest, run!') = " + countVowels("Run, Forest, run!"));
        System.out.println("countVowels('I will..') = " + countVowels("I will.."));
        System.out.println("countVowelsWithArray('Run, Forest, run!') = " + countVowelsWithArray("Run, Forest, run!"));
        System.out.println("countVowelsWithArray('I will..') = " + countVowelsWithArray("I will.."));

        double[] values1 = {1.1, 2.2, 3.3, 4.4};
        System.out.println("\nsum(" + Arrays.toString(values1) + ") = " + sum(values1));
        System.out.println("avg(" + Arrays.toString(values1) + ") = " + avg(values1));
        System.out.println("max(" + Arrays.toString(values1) + ") = " + max(values1));

        double[] values2 = {}; //an empty array
        System.out.println("\nsum(" + Arrays.toString(values2) + ") = " + sum(values2));
        System.out.println("avg(" + Arrays.toString(values2) + ") = " + avg(values2));
        System.out.println("max(" + Arrays.toString(values2) + ") = " + max(values2));

        double[] values3 = {1, 2, 3, -4, 5, 6};
        System.out.println("\nsumPositives(" + Arrays.toString(values3) + ") = " + sumPositives(values3));

        System.out.println("\nreverse(" + Arrays.toString(values2) + ") = " + Arrays.toString(reversed(values2)));
        System.out.println("reverse(" + Arrays.toString(values3) + ") = " + Arrays.toString(reversed(values3)));
    }
}
