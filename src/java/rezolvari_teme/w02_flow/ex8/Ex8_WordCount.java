package rezolvari_teme.w02_flow.ex8;

/**
 * Given a phrase (as a String value), count how many words it contains. Words are considered to be separated only by space (one or more spaces)
 * Example: "Aa bb cc dd" => 4 words
 * Hint: you may use the split() method present on String to separate the words (search online info about using it)
 * (other useful String methods to check: trim, isEmpty)
 */
public class Ex8_WordCount {

    static int countWords(String phrase) {

        /*
        //basic solution:
        int count = 0;
        for (String s : phrase.split(" ")) {
            if (!s.trim().isEmpty())
                count++;
        }
        return count;
        */

        //shorter one, with regex :)
        return phrase.trim().isEmpty() ?
                0 :
                phrase.trim().split("\\s+").length;

        //later on, when we learn of streams
        //return (int) Arrays.stream(phrase.split("\\s+")).filter(s -> !s.isEmpty()).count();
    }

    public static void main(String[] args) {
        System.out.println(countWords(""));
        System.out.println(countWords("abc"));
        System.out.println(countWords("aa bb"));
        System.out.println(countWords(" aa  bb  "));
    }
}
