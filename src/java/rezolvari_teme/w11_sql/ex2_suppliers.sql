create TABLE suppliers ( supplier_id int NOT NULL, supplier_name char(50) NOT NULL, city char(50), state char(25), CONSTRAINT suppliers_pk PRIMARY KEY (supplier_id) );

insert into suppliers (supplier_id, supplier_name, city, state) values (100, 'Microsoft', 'Redmond', 'Washington');
insert into suppliers (supplier_id, supplier_name, city, state) values (200, 'Google', 'Mountain View', 'California');
insert into suppliers (supplier_id, supplier_name, city, state) values (300, 'Oracle', 'Redwood City', 'California');
insert into suppliers (supplier_id, supplier_name, city, state) values (400, 'Kimberly-Clark', 'Irving', 'Texas');
insert into suppliers (supplier_id, supplier_name, city, state) values (500, 'Tyson Foods', 'Springdale', 'Arkansas');
insert into suppliers (supplier_id, supplier_name, city, state) values (600, 'SC Johnson', 'Racine', 'Wisconsin');
insert into suppliers (supplier_id, supplier_name, city, state) values (700, 'Dole Food Company', 'Westlake Village', 'California');
insert into suppliers (supplier_id, supplier_name, city, state) values (800, 'Flowers Foods', 'Thomasville', 'Georgia');
insert into suppliers (supplier_id, supplier_name, city, state) values (900, 'Electronic Arts', 'Redwood City', 'California');


select distinct city
from suppliers
where state = 'California'
order by city;

select * from suppliers
order by state, city;
