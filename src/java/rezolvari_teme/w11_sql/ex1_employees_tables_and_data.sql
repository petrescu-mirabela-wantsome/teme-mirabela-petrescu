-- Tables structure:
create TABLE employees (
  ID INT NULL,
  FIRST_NAME VARCHAR(145) NULL,
  LAST_NAME VARCHAR(145) NULL,
  EMAIL VARCHAR(145) NULL,
  PHONE_NUMBER VARCHAR(145) NULL,
  HIREDATE DATE NULL,
  JOB_ID VARCHAR(145) NULL,
  SALARY INT NULL,
  COMISSION_PCT DECIMAL(2,2) NULL,
  MANAGER_ID INT NULL,
  DEPARTMENT_ID INT NULL);

create TABLE departments (
  ID INT NULL,
  NAME VARCHAR(145) NULL,
  MANAGER_ID INT NULL,
  LOCATION_ID VARCHAR(145) NULL);

create TABLE jobs (
  ID VARCHAR(145) NULL,
  NAME VARCHAR(145) NULL,
  MIN_SALARY INT NULL,
  MAX_SALARY INT NULL);

create TABLE job_history (
  EMPLOYEE_ID INT NULL,
  START_DATE DATE NULL,
  END_DATE DATE NULL,
  JOB_ID VARCHAR(145) NULL,
  DEPARTMENT_ID INT NULL);

create TABLE locations (
  ID VARCHAR(145) NULL,
  STREET_ADDRESS VARCHAR(145) NULL,
  POSTAL_CODE VARCHAR(145) NULL,
  CITY VARCHAR(145) NULL,
  STATE VARCHAR(145) NULL,
  COUNTRY_ID VARCHAR(145) NULL);

create TABLE countries (
  ID VARCHAR(145) NULL,
  NAME VARCHAR(145) NULL,
  REGION_ID VARCHAR(145) NULL);

create TABLE regions (
  ID VARCHAR(145) NULL,
  NAME VARCHAR(145) NULL);

-- Sample data:
insert into regions values('2','Americas');
insert into regions values('3','Asia');
insert into regions values('1','Europe');
insert into regions values('4','Middle East and Africa');
insert into regions values('7','Moldova');

insert into countries values('AR','Argentina','2');
insert into countries values('AU','Australia','3');
insert into countries values('BE','Belgium','1');
insert into countries values('BR','Brazil','2');
insert into countries values('CA','Canada','2');
insert into countries values('CH','Switzerland','1');
insert into countries values('CN','China','3');
insert into countries values('DE','Germany','1');
insert into countries values('DK','Denmark','1');
insert into countries values('EG','Egypt','4');
insert into countries values('FR','France','1');
insert into countries values('HK','HongKong','3');
insert into countries values('IL','Israel','4');
insert into countries values('IN','India','3');
insert into countries values('IT','Italy','1');
insert into countries values('JP','Japan','3');
insert into countries values('KW','Kuwait','4');
insert into countries values('MX','Mexico','2');
insert into countries values('NG','Nigeria','4');
insert into countries values('NL','Netherlands','1');
insert into countries values('SG','Singapore','3');
insert into countries values('UK','United Kingdom','1');
insert into countries values('US','United States of America','2');
insert into countries values('ZM','Zambia','4');
insert into countries values('ZW','Zimbabwe','4');

insert into locations values(1000,'1297 Via Cola di Rie','989','Roma','','IT');
insert into locations values(1100,'93091 Calle della Testa','10934','Venice','','IT');
insert into locations values(1200,'2017 Shinjuku-ku','1689','Tokyo','Tokyo Prefecture','JP');
insert into locations values(1300,'9450 Kamiya-cho','6823','Hiroshima','','JP');
insert into locations values(1400,'2014 Jabberwocky Rd','26192','Southlake','Texas','US');
insert into locations values(1500,'2011 Interiors Blvd','99236','South San Francisco','California','US');
insert into locations values(1600,'2007 Zagora St','50090','South Brunswick','New Jersey','US');
insert into locations values(1700,'2004 Charade Rd','98199','Seattle','Washington','US');
insert into locations values(1800,'147 Spadina Ave','M5V 2L7','Toronto','Ontario','CA');
insert into locations values(1900,'6092 Boxwood St','YSW 9T2','Whitehorse','Yukon','CA');
insert into locations values(2000,'40-5-12 Laogianggen','190518','Beijing','','CN');
insert into locations values(2100,'1298 Vileparle (E)','490231','Bombay','Maharashtra','IN');
insert into locations values(2200,'12-98 Victoria Street','2901','Sydney','New South Wales','AU');
insert into locations values(2300,'198 Clementi North','540198','Singapore','','SG');
insert into locations values(2400,'8204 Arthur St','','London','','UK');
insert into locations values(2500,'''Magdalen Centre','OX9 9ZB',' The Oxford ','Oxford','Ox');
insert into locations values(2600,'9702 Chester Road','9629850293','Stretford','Manchester','UK');
insert into locations values(2700,'Schwanthalerstr. 7031','80925','Munich','Bavaria','DE');
insert into locations values(2800,'Rua Frei Caneca 1360','01307-002','Sao Paulo','Sao Paulo','BR');
insert into locations values(2900,'20 Rue des Corps-Saints','1730','Geneva','Geneve','CH');
insert into locations values(3000,'Murtenstrasse 921','3095','Bern','BE','CH');
insert into locations values(3100,'Pieter Breughelstraat 837','3029SK','Utrecht','Utrecht','NL');
insert into locations values(3200,'Mariano Escobedo 9991','11932','Mexico City','''Distrito Federal','''');

insert into jobs values("AC_ACCOUNT","Public Accountant",4200.0,9000.0);
insert into jobs values("AC_MGR","Accounting Manager",8200.0,16000.0);
insert into jobs values("AD_ASST","Administration Assistant",3000.0,6000.0);
insert into jobs values("AD_PRES","President",20000.0,40000.0);
insert into jobs values("AD_VP","Administration Vice President",15000.0,30000.0);
insert into jobs values("AL CLERK","ALL Clerk",3500.0,5000.0);
insert into jobs values("FI_ACCOUNT","Accountant",4200.0,9000.0);
insert into jobs values("FI_MGR","Finance Manager",8200.0,16000.0);
insert into jobs values("HR_REP","Human Resources Representative",4000.0,9000.0);
insert into jobs values("IT_PROG","Programmer",4000.0,10000.0);
insert into jobs values("MK_MAN","Marketing Manager",9000.0,15000.0);
insert into jobs values("MK_REP","Marketing Representative",4000.0,9000.0);
insert into jobs values("PR_REP","Public Relations Representative",4500.0,10500.0);
insert into jobs values("PU_CLERK","Purchasing Clerk",2500.0,5500.0);
insert into jobs values("PU_MAN","Purchasing Manager",8000.0,15000.0);
insert into jobs values("SA_MAN","Sales Manager",10000.0,20000.0);
insert into jobs values("SA_REP","Sales Representative",6000.0,12000.0);
insert into jobs values("SH_CLERK","Shipping Clerk",2500.0,5500.0);
insert into jobs values("ST_CLERK","Stock Clerk",2000.0,5000.0);
insert into jobs values("ST_MAN","Stock Manager",5500.0,8500.0);

insert into employees values(100,'Steven','King','daking','0747291186',str_to_date('1987-06-17', '%Y-%m-%d'),'AD_PRES',24000,0.00,0,90);
insert into employees values(101,'Neena','Kochhar','NKOCHHAR','515.123.4568',str_to_date('1987-06-18', '%Y-%m-%d'),'AD_VP',17000,0.00,100,90);
insert into employees values(102,'Lex','De Haan','LDEHAAN','515.123.4569',str_to_date('1987-06-19', '%Y-%m-%d'),'AD_VP',17000,0.00,100,90);
insert into employees values(103,'Alexander','Hunold','AHUNOLD','590.423.4567',str_to_date('1987-06-20', '%Y-%m-%d'),'IT_PROG',9000,0.00,102,60);
insert into employees values(104,'Bruce','Ernst','BERNST','590.423.4568',str_to_date('1987-06-21', '%Y-%m-%d'),'IT_PROG',6000,0.00,103,60);
insert into employees values(105,'David','Austin','DAUSTIN','590.423.4569',str_to_date('1987-06-22', '%Y-%m-%d'),'IT_PROG',3000,0.00,103,60);
insert into employees values(106,'Valli','Pataballa','VPATABAL','590.423.4560',str_to_date('1987-06-23', '%Y-%m-%d'),'IT_PROG',4800,0.00,103,60);
insert into employees values(107,'Diana','Lorentz','DLORENTZ','590.423.5567',str_to_date('1987-06-24', '%Y-%m-%d'),'IT_PROG',4200,0.00,103,60);
insert into employees values(108,'Nancy','Greenberg','NGREENBE','515.124.4569',str_to_date('1987-06-25', '%Y-%m-%d'),'FI_MGR',12000,0.00,101,100);
insert into employees values(109,'Daniel','Faviet','DFAVIET','515.999.4169',str_to_date('1987-06-26', '%Y-%m-%d'),'FI_ACCOUNT',9000,0.00,108,100);
insert into employees values(110,'John','Chen','JCHEN','515.999.4269',str_to_date('1987-06-27', '%Y-%m-%d'),'FI_ACCOUNT',8200,0.00,108,100);
insert into employees values(111,'Ismael','Sciarra','ISCIARRA','515.124.4369',str_to_date('1987-06-28', '%Y-%m-%d'),'FI_ACCOUNT',7700,0.00,108,100);
insert into employees values(112,'Jose Manuel','Urman','JMURMAN','515.124.4469',str_to_date('1987-06-29', '%Y-%m-%d'),'FI_ACCOUNT',7800,0.00,108,100);
insert into employees values(113,'Luis','Popp','LPOPP','515.124.4567',str_to_date('1987-06-30', '%Y-%m-%d'),'FI_ACCOUNT',6900,0.00,108,100);
insert into employees values(114,'Den','Raphaely','DRAPHEAL','515.127.4561',str_to_date('1987-07-01', '%Y-%m-%d'),'PU_MAN',11000,0.00,100,30);
insert into employees values(115,'Alexander','Khoo','AKHOO','515.127.4562',str_to_date('1987-07-02', '%Y-%m-%d'),'PU_CLERK',3100,0.00,114,30);
insert into employees values(116,'Shelli','Baida','SBAIDA','515.127.4563',str_to_date('1987-07-03', '%Y-%m-%d'),'PU_CLERK',2900,0.00,114,30);
insert into employees values(117,'Sigal','Tobias','STOBIAS','515.127.4564',str_to_date('1987-07-04', '%Y-%m-%d'),'PU_CLERK',2800,0.00,114,30);
insert into employees values(118,'Guy','Himuro','GHIMURO','515.127.4565',str_to_date('1987-07-05', '%Y-%m-%d'),'PU_CLERK',2600,0.00,114,30);
insert into employees values(119,'Karen','Colmenares','KCOLMENA','515.127.4566',str_to_date('1987-07-06', '%Y-%m-%d'),'PU_CLERK',2500,0.00,114,30);
insert into employees values(120,'Matthew','Weiss','MWEISS','650.123.1234',str_to_date('1987-07-07', '%Y-%m-%d'),'ST_MAN',8000,0.00,100,50);
insert into employees values(121,'Adam','Fripp','AFRIPP','650.123.2234',str_to_date('1987-07-08', '%Y-%m-%d'),'ST_MAN',8200,0.00,100,50);
insert into employees values(122,'Payam','Kaufling','PKAUFLIN','650.123.3234',str_to_date('1987-07-09', '%Y-%m-%d'),'ST_MAN',7900,0.00,100,50);
insert into employees values(123,'Shanta','Vollman','SVOLLMAN','650.123.4234',str_to_date('1987-07-10', '%Y-%m-%d'),'ST_MAN',6500,0.00,100,50);
insert into employees values(124,'Kevin','Mourgos','KMOURGOS','650.123.5234',str_to_date('1987-07-11', '%Y-%m-%d'),'ST_MAN',5800,0.00,100,20);
insert into employees values(125,'Julia','Nayer','JNAYER','650.124.1214',str_to_date('1987-07-12', '%Y-%m-%d'),'ST_CLERK',3200,0.00,120,50);
insert into employees values(126,'Irene','Mikkilineni','IMIKKILI','650.124.1224',str_to_date('1987-07-13', '%Y-%m-%d'),'ST_CLERK',2700,0.00,120,50);
insert into employees values(127,'James','Landry','JLANDRY','650.124.1334',str_to_date('1987-07-14', '%Y-%m-%d'),'ST_CLERK',2400,0.00,120,50);
insert into employees values(128,'Steven','Markle','SMARKLE','650.124.1434',str_to_date('1987-07-15', '%Y-%m-%d'),'ST_CLERK',2200,0.00,120,50);
insert into employees values(129,'Laura','Bissot','LBISSOT','650.124.5234',str_to_date('1987-07-16', '%Y-%m-%d'),'ST_CLERK',3300,0.00,121,50);
insert into employees values(130,'Mozhe','Atkinson','MATKINSO','650.124.6234',str_to_date('1987-07-17', '%Y-%m-%d'),'ST_CLERK',2800,0.00,121,50);
insert into employees values(131,'James','Marlow','JAMRLOW','650.124.7234',str_to_date('1987-07-18', '%Y-%m-%d'),'ST_CLERK',2500,0.00,121,50);
insert into employees values(132,'TJ','Olson','TJOLSON','650.124.8234',str_to_date('1987-07-19', '%Y-%m-%d'),'ST_CLERK',2100,0.00,121,50);
insert into employees values(133,'Jason','Mallin','JMALLIN','650.127.1934',str_to_date('1987-07-20', '%Y-%m-%d'),'ST_CLERK',3300,0.00,122,50);
insert into employees values(134,'Michael','Rogers','MROGERS','650.127.1834',str_to_date('1987-07-21', '%Y-%m-%d'),'ST_CLERK',2900,0.00,122,50);
insert into employees values(135,'Ki','Gee','KGEE','650.127.1734',str_to_date('1987-07-22', '%Y-%m-%d'),'ST_CLERK',2400,0.00,122,20);
insert into employees values(136,'Hazel','Philtanker','HPHILTAN','650.127.1634',str_to_date('1987-07-23', '%Y-%m-%d'),'ST_CLERK',2200,0.00,122,50);
insert into employees values(137,'Renske','Ladwig','RLADWIG','650.121.1234',str_to_date('1987-07-24', '%Y-%m-%d'),'ST_CLERK',3600,0.00,123,50);
insert into employees values(138,'Stephen','Stiles','SSTILES','650.121.2034',str_to_date('1987-07-25', '%Y-%m-%d'),'ST_CLERK',3200,0.00,123,50);
insert into employees values(139,'John','Seo','JSEO','650.121.2019',str_to_date('1987-07-26', '%Y-%m-%d'),'ST_CLERK',2700,0.00,123,50);
insert into employees values(140,'Joshua','Patel','JPATEL','650.121.1834',str_to_date('1987-07-27', '%Y-%m-%d'),'ST_CLERK',2500.00,0.00,123,50);
insert into employees values(141,'Trenna','Rajs','TRAJS','650.121.8009',str_to_date('1987-07-28', '%Y-%m-%d'),'ST_CLERK',3500,0.00,124,50);
insert into employees values(142,'Curtis','Davies','CDAVIES','650.121.2994',str_to_date('1987-07-29', '%Y-%m-%d'),'ST_CLERK',3100,0.00,124,50);
insert into employees values(143,'Randall','Matos','RMATOS','650.121.2874',str_to_date('1987-07-30', '%Y-%m-%d'),'ST_CLERK',2600,0.00,124,50);
insert into employees values(144,'Peter','Vargas','PVARGAS','650.121.2004',str_to_date('1987-07-31', '%Y-%m-%d'),'ST_CLERK',2500,0.00,124,50);
insert into employees values(145,'John','Russell','JRUSSEL','011.44.1344.429268',str_to_date('1987-08-01', '%Y-%m-%d'),'SA_MAN',14000,0.40,100,80);
insert into employees values(146,'Karen','Partners','KPARTNER','011.44.1344.467268',str_to_date('1987-08-02', '%Y-%m-%d'),'SA_MAN',13500,0.30,100,80);
insert into employees values(147,'Alberto','Errazuriz','AERRAZUR','011.44.1344.429278',str_to_date('1987-08-03', '%Y-%m-%d'),'SA_MAN',12000,0.30,100,80);
insert into employees values(148,'Gerald','Cambrault','GCAMBRAU','011.44.1344.619268',str_to_date('1987-08-04', '%Y-%m-%d'),'SA_MAN',11000,0.30,100,80);
insert into employees values(149,'Eleni','Zlotkey','EZLOTKEY','011.44.1344.429018',str_to_date('1987-08-05', '%Y-%m-%d'),'SA_MAN',10500,0.20,100,80);
insert into employees values(150,'Peter','Tucker','PTUCKER','011.44.1344.129268',str_to_date('1987-08-06', '%Y-%m-%d'),'SA_REP',10000,0.30,145,80);
insert into employees values(151,'David','Bernstein','DBERNSTE','011.44.1344.345268',str_to_date('1987-08-07', '%Y-%m-%d'),'SA_REP',9500,0.25,145,80);
insert into employees values(152,'Peter','Hall','PHALL','011.44.1344.478968',str_to_date('1987-08-08', '%Y-%m-%d'),'SA_REP',9000,0.25,145,80);
insert into employees values(153,'Christopher','Olsen','COLSEN@wantsome.ro','011.44.1344.498718',str_to_date('1987-08-09', '%Y-%m-%d'),'SA_REP',8000,0.20,145,80);
insert into employees values(154,'Nanette','Cambrault','NCAMBRAU','011.44.1344.987668',str_to_date('1987-08-10', '%Y-%m-%d'),'SA_REP',7500,0.20,145,80);
insert into employees values(155,'Oliver','Tuvault','OTUVAULT','011.44.1344.486508',str_to_date('1987-08-11', '%Y-%m-%d'),'SA_REP',7000,0.15,145,80);
insert into employees values(156,'Janette','King','JKING','011.44.1345.429268',str_to_date('1987-08-12', '%Y-%m-%d'),'SA_REP',10000,0.35,146,80);
insert into employees values(157,'Patrick','Sully','PSULLY','011.44.1345.929268',str_to_date('1987-08-13', '%Y-%m-%d'),'SA_REP',9500,0.35,146,80);
insert into employees values(158,'Allan','McEwen','AMCEWEN','011.44.1345.829268',str_to_date('1987-08-14', '%Y-%m-%d'),'SA_REP',9000,0.35,146,80);
insert into employees values(159,'Lindsey','Smith','LSMITH','011.44.1345.729268',str_to_date('1987-08-15', '%Y-%m-%d'),'SA_REP',8000,0.30,146,80);
insert into employees values(160,'Louise','Doran','LDORAN','011.44.1345.629268',str_to_date('1987-08-16', '%Y-%m-%d'),'SA_REP',7500,0.30,146,80);
insert into employees values(161,'Sarath','Sewall','SSEWALL','011.44.1345.529268',str_to_date('1987-08-17', '%Y-%m-%d'),'SA_REP',7000,0.25,146,80);
insert into employees values(162,'Clara','Vishney','CVISHNEY','011.44.1346.129268',str_to_date('1987-08-18', '%Y-%m-%d'),'SA_REP',10500,0.25,147,80);
insert into employees values(163,'Danielle','Greene','DGREENE','011.44.1346.229268',str_to_date('1987-08-19', '%Y-%m-%d'),'SA_REP',9500,0.15,147,80);
insert into employees values(164,'Mattea','Marvins','MMARVINS','011.44.1346.329268',str_to_date('1987-08-20', '%Y-%m-%d'),'SA_REP',7200,0.10,147,80);
insert into employees values(165,'David','Lee','DLEE','011.44.1346.529268',str_to_date('1987-08-21', '%Y-%m-%d'),'SA_REP',6800,0.10,147,80);
insert into employees values(166,'Sundar','Ande','SANDE','011.44.1346.629268',str_to_date('1987-08-22', '%Y-%m-%d'),'SA_REP',6400,0.10,147,80);
insert into employees values(167,'Amit','Banda','ABANDA','011.44.1346.729268',str_to_date('1987-08-23', '%Y-%m-%d'),'SA_REP',6200,0.10,147,80);
insert into employees values(168,'Lisa','Ozer','LOZER','011.44.1343.929268',str_to_date('1987-08-24', '%Y-%m-%d'),'SA_REP',11500,0.25,148,80);
insert into employees values(169,'Harrison','Bloom','HBLOOM','011.44.1343.829268',str_to_date('1987-08-25', '%Y-%m-%d'),'SA_REP',10000,0.20,148,80);
insert into employees values(170,'Tayler','Fox','TFOX','011.44.1343.729268',str_to_date('1987-08-26', '%Y-%m-%d'),'SA_REP',9600,0.20,148,80);
insert into employees values(171,'William','Smith','WSMITH','011.44.1343.629268',str_to_date('1987-08-27', '%Y-%m-%d'),'SA_REP',7400,0.15,148,80);
insert into employees values(172,'Elizabeth','Bates','EBATES','011.44.1343.529268',str_to_date('1987-08-28', '%Y-%m-%d'),'SA_REP',7300,0.15,148,80);
insert into employees values(173,'Sundita','Kumar','SKUMAR','011.44.1343.329268',str_to_date('1987-08-29', '%Y-%m-%d'),'SA_REP',6100,0.10,148,80);
insert into employees values(174,'Ellen','Abel','EABEL','011.44.1644.429267',str_to_date('1987-08-30', '%Y-%m-%d'),'SA_REP',11000,0.30,149,80);
insert into employees values(175,'Alyssa','Hutton','AHUTTON','011.44.1644.429266',str_to_date('1987-08-31', '%Y-%m-%d'),'SA_REP',8800,0.25,149,80);
insert into employees values(176,'Jonathon','Taylor','JTAYLOR','011.44.1644.429265',str_to_date('1987-09-01', '%Y-%m-%d'),'SA_REP',8600,0.20,149,80);
insert into employees values(177,'Jack','Livingston','JLIVINGS','011.44.1644.429264',str_to_date('1987-09-02', '%Y-%m-%d'),'SA_REP',8400,0.20,149,80);
insert into employees values(178,'Kimberely','Grant','KGRANT','011.44.1644.429263',str_to_date('1987-09-03', '%Y-%m-%d'),'SA_REP',7000,0.15,149,20);
insert into employees values(179,'Charles','Johnson','CJOHNSON','011.44.1644.429262',str_to_date('1987-09-04', '%Y-%m-%d'),'SA_REP',6200,0.10,149,80);
insert into employees values(180,'Winston','Taylor','WTAYLOR','650.507.9876',str_to_date('1987-09-05', '%Y-%m-%d'),'SH_CLERK',3200,0.00,120,50);
insert into employees values(181,'Jean','Fleaur','JFLEAUR','650.507.9877',str_to_date('1987-09-06', '%Y-%m-%d'),'SH_CLERK',3100,0.00,120,50);
insert into employees values(182,'Martha','Sullivan','MSULLIVA','650.507.9878',str_to_date('1987-09-07', '%Y-%m-%d'),'SH_CLERK',2500,0.00,120,50);
insert into employees values(183,'Girard','Geoni','GGEONI','650.507.9879',str_to_date('1987-09-08', '%Y-%m-%d'),'SH_CLERK',2800,0.00,120,50);
insert into employees values(184,'Nandita','Sarchand','NSARCHAN','650.509.1876',str_to_date('1987-09-09', '%Y-%m-%d'),'SH_CLERK',4200,0.00,121,50);
insert into employees values(185,'Alexis','Bull','ABULL','650.509.2876',str_to_date('1987-09-10', '%Y-%m-%d'),'SH_CLERK',4100,0.00,121,50);
insert into employees values(186,'Julia','Dellinger','JDELLING','650.509.3876',str_to_date('1987-09-11', '%Y-%m-%d'),'SH_CLERK',3400,0.00,121,50);
insert into employees values(187,'Anthony','Cabrio','ACABRIO','650.509.4876',str_to_date('1987-09-12', '%Y-%m-%d'),'SH_CLERK',3000,0.00,121,50);
insert into employees values(188,'Kelly','Chung','KCHUNG','650.505.1876',str_to_date('1987-09-13', '%Y-%m-%d'),'SH_CLERK',3800,0.00,122,20);
insert into employees values(189,'Jennifer','Dilly','JDILLY','650.505.2876',str_to_date('1987-09-14', '%Y-%m-%d'),'SH_CLERK',3600,0.00,122,50);
insert into employees values(190,'Timothy','Gates','TGATES','650.505.3876',str_to_date('1987-09-15', '%Y-%m-%d'),'SH_CLERK',2900,0.00,122,50);
insert into employees values(191,'Randall','Perkins','RPERKINS','650.505.4876',str_to_date('1987-09-16', '%Y-%m-%d'),'SH_CLERK',2500,0.00,122,50);
insert into employees values(192,'Sarah','Bell','SBELL','650.501.1876',str_to_date('1987-09-17', '%Y-%m-%d'),'SH_CLERK',4000,0.00,123,50);
insert into employees values(193,'Britney','Everett','BEVERETT','650.501.2876',str_to_date('1987-09-18', '%Y-%m-%d'),'SH_CLERK',3900,0.00,123,50);
insert into employees values(194,'Samuel','McCain','SMCCAIN','650.501.3876',str_to_date('1987-09-19', '%Y-%m-%d'),'SH_CLERK',3200,0.00,123,50);
insert into employees values(195,'Vance','Jones','VJONES','650.501.4876',str_to_date('1987-09-20', '%Y-%m-%d'),'SH_CLERK',2800,0.00,123,50);
insert into employees values(196,'Alana','Walsh','AWALSH','650.507.9811',str_to_date('1987-09-21', '%Y-%m-%d'),'SH_CLERK',3100,0.00,124,50);
insert into employees values(197,'Kevin','Feeney','KFEENEY','650.507.9822',str_to_date('1987-09-22', '%Y-%m-%d'),'SH_CLERK',3000,0.00,124,20);
insert into employees values(198,'Donald','OConnell','DOCONNEL','650.507.9833',str_to_date('1987-09-23', '%Y-%m-%d'),'SH_CLERK',2600,0.00,124,50);
insert into employees values(199,'Douglas','Grant','DGRANT','650.507.9844',str_to_date('1987-09-24', '%Y-%m-%d'),'SH_CLERK',2600,0.00,124,50);
insert into employees values(200,'Jennifer','Whalen','JWHALEN','515.123.4444',str_to_date('1987-09-25', '%Y-%m-%d'),'AD_ASST',4400,0.00,101,10);
insert into employees values(201,'Michael','Hartstein','MHARTSTE','515.123.5555',str_to_date('1987-09-26', '%Y-%m-%d'),'MK_MAN',13000,0.00,100,20);
insert into employees values(202,'Pat','Fay','PFAY','603.123.6666',str_to_date('1987-09-27', '%Y-%m-%d'),'MK_REP',6000,0.00,201,20);
insert into employees values(203,'Susan','Mavris','SMAVRIS','515.123.7777',str_to_date('1987-09-28', '%Y-%m-%d'),'HR_REP',6500,0.00,101,40);
insert into employees values(204,'Hermann','Baer','HBAER','515.123.8888',str_to_date('1987-09-29', '%Y-%m-%d'),'PR_REP',10000,0.00,101,70);
insert into employees values(205,'Shelley','Higgins','SHIGGINS','515.123.8080',str_to_date('1987-09-30', '%Y-%m-%d'),'AC_MGR',12000,0.00,101,110);
insert into employees values(206,'William','Gietz','WGIETZ','515.123.8181',str_to_date('1987-10-01', '%Y-%m-%d'),'AC_ACCOUNT',8300,0.00,205,110);

insert into departments values(10.0,'Administration',200.0,1700.0);
insert into departments values(20.0,'Marketing',201.0,1800.0);
insert into departments values(30.0,'Purchasing',114.0,1700.0);
insert into departments values(40.0,'Human Resources',203.0,2400.0);
insert into departments values(50.0,'Shipping',121.0,1500.0);
insert into departments values(60.0,'IT',103.0,1400.0);
insert into departments values(70.0,'Public Relations',204.0,2700.0);
insert into departments values(80.0,'Sales',145.0,2500.0);
insert into departments values(90.0,'Executive',100.0,1700.0);
insert into departments values(100.0,'Finance',108.0,1700.0);
insert into departments values(110.0,'Accounting',205.0,1700.0);
insert into departments values(120.0,'Treasury',100.0,1700.0);
insert into departments values(130.0,'Corporate Tax',100.0,1700.0);
insert into departments values(140.0,'Control And Credit',100.0,1700.0);
insert into departments values(150.0,'Shareholder Services',100.0,1700.0);
insert into departments values(160.0,'Benefits',100.0,1700.0);
insert into departments values(170.0,'Manufacturing',100.0,1700.0);
insert into departments values(180.0,'Construction',100.0,1700.0);
insert into departments values(190.0,'Contracting',100.0,1700.0);
insert into departments values(200.0,'Operations',100.0,1700.0);
insert into departments values(210.0,'IT Support',100.0,1700.0);
insert into departments values(220.0,'NOC',100.0,1700.0);
insert into departments values(230.0,'IT Helpdesk',100.0,1700.0);
insert into departments values(240.0,'Government Sales',100.0,1700.0);
insert into departments values(250.0,'Retail Sales',100.0,1700.0);
insert into departments values(260.0,'Recruiting',100.0,1700.0);
insert into departments values(270.0,'Payroll',100.0,1700.0);

insert into job_history values (0,str_to_date('1999-03-23', '%Y-%m-%d'),str_to_date('1999-03-23', '%Y-%m-%d'),"",0);
insert into job_history values (101,str_to_date('1989-09-21', '%Y-%m-%d'),str_to_date('1993-10-27', '%Y-%m-%d'),"AC_ACCOUNT",110);
insert into job_history values (101,str_to_date('1993-10-28', '%Y-%m-%d'),str_to_date('1997-03-15', '%Y-%m-%d'),"AC_MGR",110);
insert into job_history values (102,str_to_date('1993-01-13', '%Y-%m-%d'),str_to_date('1998-07-24', '%Y-%m-%d'),"IT_PROG",60);
insert into job_history values (114,str_to_date('1998-03-24', '%Y-%m-%d'),str_to_date('1999-12-31', '%Y-%m-%d'),"ST_CLERK",50);
insert into job_history values (122,str_to_date('1999-01-01', '%Y-%m-%d'),str_to_date('1999-12-31', '%Y-%m-%d'),"ST_CLERK",50);
insert into job_history values (176,str_to_date('1998-03-24', '%Y-%m-%d'),str_to_date('1998-12-31', '%Y-%m-%d'),"SA_REP",80);
insert into job_history values (176,str_to_date('1999-01-01', '%Y-%m-%d'),str_to_date('1999-12-31', '%Y-%m-%d'),"SA_MAN",80);
insert into job_history values (201,str_to_date('1996-02-17', '%Y-%m-%d'),str_to_date('1999-12-19', '%Y-%m-%d'),"MK_REP",20);
insert into job_history values (70000,str_to_date('1987-09-17', '%Y-%m-%d'),str_to_date('1993-06-17', '%Y-%m-%d'),"AD_ASST",90);
insert into job_history values (70000,str_to_date('1994-07-01', '%Y-%m-%d'),str_to_date('1998-12-31', '%Y-%m-%d'),"AC_ACCOUNT",90);
