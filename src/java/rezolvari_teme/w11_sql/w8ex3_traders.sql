-- Warmup: solving w08.ex3 with sql
SET SQL_MODE = '';-- to allow not null fields without a default value

CREATE TABLE trader (
    id INT PRIMARY KEY,
    name VARCHAR(20),
    city VARCHAR(20)
);
insert into trader values (1, 'Cristi', 'Iasi');
insert into trader values (2, 'Florin', 'Vaslui');
insert into trader values (3, 'Ana', 'Iasi');
insert into trader values (4, 'Maria', 'Bacau');
SELECT 
    *
FROM
    trader;

CREATE TABLE transactions (
    id INT PRIMARY KEY,
    year INT,
    value INT,
    trader_id INT NOT NULL,
    CONSTRAINT trader_fk FOREIGN KEY (trader_id)
        REFERENCES trader (id)
);
insert into transactions values(1, 2018, 2000, 1); 
insert into transactions values(2, 2019, 3000, 1); 
insert into transactions values(3, 2018, 1500, 2); 
insert into transactions values(4, 2017, 3500, 3);
insert into transactions values(5, 2018, 500, 3);
insert into transactions values(6, 2018, 700, 4);
SELECT * FROM transactions;

-- Solved:
SELECT * FROM transactions WHERE year=2018 ORDER BY value;
SELECT DISTINCT city FROM trader ORDER BY city; 
SELECT * from trader where city='Iasi' ORDER BY name;
SELECT name from trader ORDER BY name;

select count(*) from trader;
select count(*) from trader where city='Iasi';

select min(value), max(value), avg(value) from transactions;
select sum(value) from transactions where trader_id=1;

select trader_id, sum(value) from transactions GROUP BY (trader_id);
select trader_id, min(value) Min, max(value) Max, avg(value) Average, sum(value) Total from transactions GROUP BY (trader_id);
select trader_id, min(value) Min, max(value) Max, avg(value) Average, sum(value) Total from transactions GROUP BY (trader_id) ORDER BY Total Desc;

select tr.name, tx.year, tx.value from transactions tx join trader tr on tx.trader_id=tr.id ORDER BY tr.name, tx.year;
select tr.name, sum(tx.value) total from transactions tx join trader tr on tx.trader_id=tr.id GROUP BY tr.name ORDER BY total DESC;

SELECT * FROM transactions ORDER BY value desc LIMIT 3;
SELECT * FROM trader WHERE name LIKE 'C%';

