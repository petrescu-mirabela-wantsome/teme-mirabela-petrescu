------------
-- UPDATE --
------------

UPDATE employees 
SET 
    phone_number = REPLACE(phone_number, '590', '111')
WHERE
    phone_number LIKE '%590%';
SELECT 
    phone_number
FROM
    employees
WHERE
    phone_number LIKE '%111%';
    
    
UPDATE employees 
SET 
    email = CONCAT(email, '@wantsome.ro');
SELECT 
    email
FROM
    employees;
    
    
UPDATE employees 
SET 
    SALARY = 8000
WHERE
    id = 105 AND salary < 5000;
SELECT 
    *
FROM
    employees
WHERE
    id = 105;
    
    
UPDATE employees 
SET 
    JOB_ID = 'SH_CLERK'
WHERE
    id = 118 AND department_id = 30
        AND JOB_ID NOT LIKE 'SH%';
SELECT 
    job_id
FROM
    employees
WHERE
    id = 118;
    

UPDATE employees 
SET 
    salary = CASE department_id
        WHEN 40 THEN salary + (salary * 2)
        WHEN 90 THEN salary + (salary * 3)
        WHEN 110 THEN salary + (salary * 10)
        ELSE salary
    END;
SELECT 
    salary
FROM
    employees
WHERE
    department_id IN (40 , 90, 110);


------------
-- SELECT --
------------

SELECT 
    *
FROM
    employees
WHERE
    LENGTH(first_name) >= 8;
    
    
SELECT 
    first_name 'Name', LENGTH(first_name) 'Length'
FROM
    employees
WHERE
    first_name LIKE 'J%'
        OR first_name LIKE 'M%'
        OR first_name LIKE 'A%'
ORDER BY first_name;


SELECT 
    *
FROM
    departments
WHERE
    id NOT IN (SELECT 
            department_id
        FROM
            employees);


SELECT 
    id, first_name
FROM
    employees AS e
WHERE
    salary > (SELECT 
            AVG(salary)
        FROM
            employees
        WHERE
            department_id = e.department_id);
            
            
SELECT 
    first_name, last_name, salary
FROM
    employees
WHERE
    salary > (SELECT 
            salary
        FROM
            employees
        WHERE
            last_name = 'Bell')
ORDER BY first_name;


SELECT 
    first_name, last_name, salary, salary * .15 TAX
FROM
    employees;


------------
-- JOIN   --
------------

SELECT 
    jh.*
FROM
    job_history jh
        JOIN
    employees e ON (jh.employee_id = e.id)
WHERE
    salary > 10000;
    
    
SELECT 
    e.id, name, first_name, salary - min_salary 'Salary - Min_Salary'
FROM
    employees e
        INNER JOIN
    jobs j ON e.job_id = j.id;
    
    
SELECT 
    name, AVG(SALARY) av
FROM
    jobs j
        INNER JOIN
    employees e ON j.id = e.JOB_ID
GROUP BY e.JOB_ID;


SELECT 
    d.name, e.first_name, l.city
FROM
    departments d
        INNER JOIN
    employees e ON (d.manager_id = e.id)
        INNER JOIN
    locations l ON (d.location_id = l.id);
    
    
SELECT 
    d.id, d.name, d.manager_id, e.first_name
FROM
    departments d
        INNER JOIN
    employees e ON (d.manager_id = e.id);
    
    
SELECT 
    employee_id, j.id, name, DATEDIFF(end_date, start_date) Days
FROM
    job_history jh
        INNER JOIN
    jobs j ON jh.job_id = j.id
WHERE
    department_id = 90;
