package my_work.TestOnline;

class Super {
    public int i = 0;

    public Super(String text) /* Line 4 */ {
        i = 1;
    }
}

class Sub extends Super {
    public Sub(String text) {
        /*
        A default no-args constructor is not created because there is a constructor supplied that has an argument, line 4.
        Therefore the sub-class constructor must explicitly make a call to the super class constructor:
         */
        super(text);
        i = 2;
    }

    public static void main(String args[]) {
        Sub sub = new Sub("Hello");
        System.out.println(sub.i);
    }
}
