package my_work.test_s5_s7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class Test2 {
    public static void main(String[] args) {

        Integer a = new Integer(8);
        Integer b = new Integer(4);
        Integer c = new Integer(4);
        List<Integer> list1 = new ArrayList<>();
        list1.add(a);
        list1.add(b);
        list1.add(c);

        Iterator<Integer> it = list1.iterator();

        while (it.hasNext()) {
            System.out.print(" " + it.next());
        }

        List list = new ArrayList();
        list.add("one");
        list.add("two");
        list.add(7);
        list.add('a');
        // incompatible types
//        for (String s : list) {
//            System.out.print(s);
//        }

        List<Integer> list3 = Arrays.asList(10, 4, -1, 5);
        java.util.Collections.sort(list3);

        Integer[] array = list3.toArray(new Integer[4]);

        System.out.println();
        System.out.println(array[0]);

        int[][] m = {{1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};
        int sum = 0;

        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < m[i].length; j++) {
                if (i * j % 2 == 0) {
                    sum += m[i][j];
                }
            }
        }

        System.out.println(sum);

        int y = 1;
        do {
            y++;
            if (y <= 14) {
                System.out.print(y + " ");
            }
        } while (y < 20);
    }
}
