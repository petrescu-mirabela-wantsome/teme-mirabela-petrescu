package my_work.test_s5_s7;

class Beverage { //1
    boolean carbonated = false; //3     // package level
    private int ounces = 12;    //2     // class level

    public static void main(String[] args) { //4
        System.out.println(new SodaPop());   //5
    }
}

class SodaPop extends Beverage { //6
    public String toString() {   //7
        //return ounces + " " + carbonated; //8
        return " " + carbonated; //8
    }
}
