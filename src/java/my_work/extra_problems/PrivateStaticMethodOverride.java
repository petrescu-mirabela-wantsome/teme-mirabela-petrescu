package my_work.extra_problems;

/*
Link: https://www.edureka.co/blog/interview-questions/java-interview-questions/

Q9. Can you override a private or static method in Java?
You cannot override a private or static method in Java.
If you create a similar method with the same return type and same method arguments in child class then it will hide the superclass method; this is known as method hiding.
Similarly, you cannot override a private method in subclass because it’s not accessible there.
What you can do is create another private method with the same name in the child class.
Let’s take a look at the example below to understand it better.
 */

class Base {
    public static void display() {  // or protected
        System.out.println("Static or class method from Base");
    }

    public void print() {
        System.out.println("Non-static or instance method from Base");
    }
}

class Derived extends Base {
    public static void display() {
        System.out.println("Static or class method from Derived");
    }

    public void print() {
        System.out.println("Non-static or instance method from Derived");
    }
}

public class PrivateStaticMethodOverride {
    public static void main(String args[]) {
        Base obj = new Derived();
        obj.display();      // Static or class method from Base
        obj.print();        // Non-static or instance method from Derived
    }
}
