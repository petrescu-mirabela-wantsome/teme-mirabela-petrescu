package my_work.interview_problems.run_length_encoding;

// Link: https://tests4geeks.com/java-interview-questions/#q1

/*
Write a method that accepts a string and performs a run length encoding on the string.
Run length encoding works by looking for long runs of a character in the string and
replacing the run with a pair consisting of the number of occurrences in the run, followed by the character itself.

For example:
"AAABBAAAAABBBCCCCCCCCCAAAAA" -> "3A2B5A3B9C5A
 */

import java.util.Arrays;
import java.util.stream.Stream;

public class RunLength {

    public static void main(String[] args) {

        String sir = "AAABBAAAAABBBCCCCCCCCCAAAAA";
        StringBuilder newSir = new StringBuilder();

        for (int i = 0; i < sir.length(); i++) {
            int counter = 1;

            while (i + 1 < sir.length() &&
                    sir.charAt(i) == sir.charAt(i + 1)) {
                counter++;
                i++;
            }
            newSir.append(counter);
            newSir.append(sir.charAt(i));
        }

        System.out.println(newSir);

    }
}
