package my_work.interview_problems.bank_multithreading;

import java.util.HashMap;
import java.util.Map;

// Link: https://tests4geeks.com/java-interview-questions/#q1

/*
How will this code behave in case of multithreaded environment?
What are the worse outcome from client/bank point-of-view?
What changes could be made to make this code thread safe?
 */

public class Bank {

    // Singleton implementation omitted for brevity's sake

    // map from account number to balance
    private Map<Account, Integer> accounts = new HashMap<>();

    public static void main(String[] args) {

        Bank bank = new Bank();
        Account accountPeMi = new Account("PeMi");
        Account accountPeRa = new Account("PeRa");
        bank.accounts.put(accountPeMi, 100);
        bank.accounts.put(accountPeRa, 200);

        System.out.println(bank.deposit(accountPeMi, 300));
        System.out.println(bank.withdraw(accountPeMi, 1000));
        System.out.println(bank.deposit(accountPeRa, -10));

    }

    public int deposit(Account account, int sum) throws IllegalArgumentException {
        if (sum < 0) {
            throw new IllegalArgumentException("sum cannot be negative");
        }
        synchronized (account) {
            return accounts.put(account, accounts.get(account) + sum);
        }
    }

    public int withdraw(Account account, int sum) {
        synchronized (account) {
            if (sum > accounts.get(account)) {
                return -1;
            }
            accounts.put(account, accounts.get(account) - sum);
            return sum;
        }
    }
}
