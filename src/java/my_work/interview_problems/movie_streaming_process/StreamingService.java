package my_work.interview_problems.movie_streaming_process;

/*
Write a program that will read this log file and produce the following information:

1. An ordered list of movies watched by length
2. The average percentage of the movie that this user watches.
That is, if a movie is 60 minutes long and the user watched 30 minutes of the movie, the percentage watched is 50%.
What is this average over all movies watched.
3. The user’s favourite genre of movies. This is determined by first removing all movies
where the user watched less than 50% of the movie then counting the movies in each genre that remains.
 */

import java.util.*;
import java.util.stream.Collectors;

public class StreamingService {

    // return an ordered list of movies watched by length
    public List<LogEntry> orderByMovieLength(List<LogEntry> logEntries) {
        return logEntries.stream().sorted(new Comparator<LogEntry>() {
            @Override
            public int compare(LogEntry o1, LogEntry o2) {
                return Integer.valueOf(o1.getMovieLength().split("min")[0])
                        .compareTo(Integer.valueOf(o2.getMovieLength().split("min")[0]));
            }
        }).collect(Collectors.toList());
    }

    // The average percentage of the movie that this user watches.
    // That is, if a movie is 60 minutes long and the user watched 30 minutes of the movie, the percentage watched is 50%.
    // What is this average over all movies watched.
    public OptionalDouble averageMovieWatches(List<LogEntry> logEntries) {
        return logEntries.stream()
                .mapToDouble(s -> Double.valueOf(s.getMinutesWatched().split("min")[0]) / Double.valueOf(s.getMovieLength().split("min")[0]))
                .average();
    }

    // The user’s favourite genre of movies.
    // This is determined by first removing all movies where the user watched less than 50% of the movie
    // then counting the movies in each genre that remains.
    public String favouriteGenreMovies(List<LogEntry> logEntries) {
        List<LogEntry> logEntriesMoreThan50Percent = logEntries.stream()
                .filter(s -> Double.valueOf(s.getMinutesWatched().split("min")[0]) / Double.valueOf(s.getMovieLength().split("min")[0]) >= 0.5)
                .collect(Collectors.toList());
        return logEntriesMoreThan50Percent.stream()
                .collect(Collectors.groupingBy(LogEntry::getMovieGenre, Collectors.counting()))
                .entrySet()
                .stream()
                .collect(Collectors.maxBy((a, b) -> (int) (a.getValue() - b.getValue())))
                .get()
                .getKey();
    }
}
