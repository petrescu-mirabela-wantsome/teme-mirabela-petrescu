package my_work.interview_problems.movie_streaming_process;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StreamingServiceApplication {

    public static void main(String[] args) {


        StreamingService streamingService = new StreamingService();

        String logString = "9/24/2016 TheMagnificentSeven 133min 126min Action\n" +
                "9/30/2016 MissPeregrine'sHomeforPeculiarChildren 127min 100min Fantasy\n" +
                "11/5/2015 Trolls 92min 40min Fantasy\n" +
                "11/5/2015 DoctorStrange 115min 110min Fantasy\n" +
                "11/19/2016 FantasticBeastsandWheretoFindThem 133min 120min Fantasy\n" +
                "11/12/2016 Arrival 116min 20min SciFi\n";

        List<LogEntry> logEntries = new ArrayList<>();
        String[] logStringArrayLine = logString.split("\n");
        String[] logStringOneLine;

        for (String line : logStringArrayLine) {
            logStringOneLine = line.split(" ");
            logEntries.add(new LogEntry(new Date(logStringOneLine[0]), logStringOneLine[1], logStringOneLine[2], logStringOneLine[3], logStringOneLine[4]));
        }

        System.out.println(streamingService.orderByMovieLength(logEntries));
        System.out.println(streamingService.averageMovieWatches(logEntries));
        System.out.println(streamingService.favouriteGenreMovies(logEntries));
    }

}
