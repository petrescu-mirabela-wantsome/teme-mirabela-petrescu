package my_work.interview_problems.palindrome;

// Link: https://tests4geeks.com/java-interview-questions/#q7

/*
Write Java methods that checks if a String is a palindrome (i.e. is equal to itself when reversed).

Check if a string has any permutation that is a palindrome.
In other words, is there any way to rearrange the characters in the string so that a palindrome is produced?
 */

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Palindrome {

    public static void main(String[] args) {

        System.out.println(isPalindrome(""));
        System.out.println(isPalindrome("c"));
        System.out.println(isPalindrome("cc"));
        System.out.println(isPalindrome("madam"));
        System.out.println(isPalindrome("tata"));

        System.out.println(">>>>>>>> hasPalindromePermutation >>>>>>>>");
        System.out.println(hasPalindromePermutation("tata"));

        System.out.println(">>>>>>>> hasPalindromePermutation - Streams >>>>>>>>");
        System.out.println(hasPalindromePermutationStream(""));
        System.out.println(hasPalindromePermutationStream("c"));
        System.out.println(hasPalindromePermutationStream("cc"));
        System.out.println(hasPalindromePermutationStream("ab"));
        System.out.println(hasPalindromePermutationStream("abc"));
        System.out.println(hasPalindromePermutationStream("tata"));
        System.out.println(hasPalindromePermutationStream("abcd"));

    }

    public static boolean isPalindrome(String string) {

        String[] stringArray = string.split("");

        for (int i = 0; i < stringArray.length / 2; i++) {
            if (!stringArray[i].equals(stringArray[stringArray.length - 1 - i])) {
                return false;
            }
        }
        return true;
    }

    public static boolean hasPalindromePermutation(String string) {
        Map<Character, Integer> counts = new HashMap<>();
        for (Character c : string.toCharArray()) {
            if (!counts.containsKey(c)) {
                counts.put(c, 0);
            }
            counts.put(c, counts.get(c) + 1);
        }
        int oddCount = 0;
        for (Integer count : counts.values()) {
            if (count % 2 == 1) {
                oddCount++;
                if (oddCount > 1) {
                    return false;
                }
            }
        }
        return true;
    }

    public static boolean hasPalindromePermutationStream(String string) {
        Map<String, Long> mapString = Stream.of(string.split(""))
                .collect(Collectors.groupingBy(s -> s, Collectors.counting()));
        System.out.print(mapString);

        return mapString.values()
                .stream()
                .filter(s -> s % 2 == 1)
                .count() <= 1;

    }
}
