package my_work.interview_problems.player_weapon;

public class Player {

    private Weapon weapon;
    private String name;

    public Player(Weapon weapon, String name) {
        this.weapon = weapon;
        this.name = name;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public void action() {
        this.weapon.attack();
    }
}
