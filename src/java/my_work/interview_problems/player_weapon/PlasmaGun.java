package my_work.interview_problems.player_weapon;

public class PlasmaGun extends Weapon {
    @Override
    protected void attack() {
        System.out.println("Perform plasma gun attack");
    }
}
