package my_work.interview_problems.polyline;

import java.util.ArrayList;

/*
An immutable data structure in Java must meet the following requirements:

- The class itself must be declared final, so subclasses cannot add mutable parts to it (even though the immutable parts would remain immutable).
Junior candidates can easily overlook this point.
- All fields must be private so they cannot be written to from outside the class. (Making fields protected in a final class is pointless.)
- All fields must be final so they cannot be reassigned.
This is not strictly necessary, but it’s a useful safeguard which is enforced by the compiler.
- If the object is “composite” (contains other objects inside it), all contained objects must also be immutable.
In this particular case, this means the Point class must meet all of the above requirements as well.
Alternatively, defensive copies should be made if points are retrieved from the polyline.
 */

public final class Polyline {

    private final ArrayList<Point> points;

    private Polyline(ArrayList<Point> points) {
        this.points = points;
    }

    public static Polyline create() {
        return new Polyline(new ArrayList<>());
    }

    public Polyline add(Point point) {
        ArrayList<Point> newPoint = new ArrayList<>(this.points);
        newPoint.add(point);
        return new Polyline(newPoint);
    }

    public int size() {
        return points.size();
    }

    public Point getPoint(int index) {
        return points.get(index);
    }

    @Override
    public String toString() {
        return "Polyline{" +
                "points=" + points +
                '}';
    }
}
