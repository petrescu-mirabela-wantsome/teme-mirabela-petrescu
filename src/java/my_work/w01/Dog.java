package my_work.w01;

// exercitii_extra.txt
/*
===============================================

(Tricky?) questions:

1)
  a) Is this code valid (compiles without error) ?

  public class Dog{
    public int age;

    public void Dog(int age){
      this.age = age;
    }
  }

  b) If yes, does this code compile, and if so what does it display?

  Dog d1 = new Dog(1);
  System.out.println(d1.age);

  c) How about this one?
  Dog d2 = new Dog();
  d2.Dog(2);
  System.out.println(d2.age);


2) Having this class:

      public class Dog{
        public int age;

        public Dog(int age){
          this.age = age;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Dog dog = (Dog) o;
            return age == dog.age;
        }
      }

  What will this code display?

        Dog d1 = new Dog();
        Dog d2 = new Dog();
        System.out.println("d1 equals d2 ?: " + d1.equals(d2));

3) Can static be also applied to classes?
   A: yes, to inner classes (only)

   ---------------------------------------------------------------------------------------------
*/

public class Dog {
    public int age;

    public Dog() {
    }

    public Dog(int age) {
        this.age = age;
    }

    public static void main(String[] args) {

        System.out.println("Hello!");

        // nu merge ca apeleaza constructorul care nu exista initial (constructorul nu returneaza void)
        Dog d1 = new Dog(1);
        System.out.println(d1.age);

        // merge - apeleaza constructorul fara param care e generat by default
        // daca se creeaza constructorul cu parametri urm cod nu va mai merge deoarace construcotrul fara param nu se mai genereaza by default
        Dog d2 = new Dog();
        d2.Dog(2);
        System.out.println(d2.age);

        Dog d3 = new Dog();
        Dog d4 = new Dog();
        System.out.println("d3 equals d4 ?: " + d3.equals(d4));
        System.out.println(d3);
        System.out.println(d3.hashCode());
        System.out.println(d4.hashCode());

//        Set<Dog> s1 = new HashSet<>();
//        Set<Dog> s2 = new HashSet<>();
//        s1.add(d1);
//        s2.add(d2);
//        System.out.println("s1 equals s2 ?: " + s1.equals(s2));

    }

    public void Dog(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;     // two different instances
        if (o == null || getClass() != o.getClass()) return false;  // no instance class/different class
        Dog dog = (Dog) o;
        return age == dog.age;
    }
}
